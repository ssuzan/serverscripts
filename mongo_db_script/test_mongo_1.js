//- Export each query to CSV table
//- filter each query by dates
//- incidents by severity: (Low/Minor, High/Major, Unknown)  and per road type Highways/Arterials
//- Incidents by reported source:
//# incidents identified by FAST/WayCare
//# incidents identified by Spillman 
//- prediction hits success:
//per each freeway:   I-15        US-95  
//# crashes identified by system
//% predicted from total crashes



//or query for total number of incidents
//running from command line
//get 1,2,3 for road types
//ask shai about differense between street and standard: standard is out of city roads that are not highways
//csv
//switch to incidents database

//1 finish script
//2 convert for production
//3 command line

//db.getCollection('api_add_incident_enriched').distinct('payload.entity_type')
//mongo waycare_db_backup < test_mongo_1.js

//x = db.getCollection('api_add_incident_enriched').find({creation_time: { $gte:ISODate("2017-10-01T14:00:00Z"), $lt: ISODate("2017-11-01T20:00:00Z") }} )
//db.getCollection('api_add_incident_enriched').find({creation_time: { $gte:ISODate("2017-10-01T14:00:00Z"), $lt: ISODate("2017-11-01T20:00:00Z") }} ).count()
//sudo mongoexport --db waycare_production_db --collection incidents --query '{ $or: [ {"section.road_type":"highway"}, {"section.road_type":"interstate"} ],"type":"incident" }' --out high_in.json


//sudo mongoexport --db waycare_production_db --collection incidents --query '{ "creation_time": {$gte:ISODate(1506805200000), $lt: ISODate(1509487200000) },$or: [ {"section.road_type":"highway"}, {"section.road_type":"interstate"} ],"type":"incident" }' --out high_in.json
//sudo mongoexport --db waycare_production_db --collection incidents --query '{ "creation_time": {$gte:ISODate(1506805200000), $lt: ISODate(1509487200000) },$or: [ {"section.road_type":"highway"}, {"section.road_type":"interstate"} ],"type":"crash" }' --out high_crash.json
//sudo mongoexport --db waycare_production_db --collection incidents --query '{ "creation_time": {$gte:ISODate(1506805200000), $lt: ISODate(1509487200000) },$or: [ {"section.road_type":"highway"}, {"section.road_type":"interstate"} ],$or: [ {"type":"construction"}, {"type":"hazard"},{"type":"infrastructure"},{"type":"special_event"} ] }' --out high_other.json

//sudo mongoexport --db waycare_production_db --collection incidents --query '{ "creation_time": {$gte:ISODate(1506805200000), $lt: ISODate(1509487200000) },$or: [ {"section.road_type":"street"}, {"section.road_type":"standard"} ],"type":"incident" }' --out street_in.json
//sudo mongoexport --db waycare_production_db --collection incidents --query '{ "creation_time": {$gte:ISODate(1506805200000), $lt: ISODate(1509487200000) },$or: [ {"section.road_type":"street"}, {"section.road_type":"standard"} ],"type":"crash" }' --out street_crash.json
//sudo mongoexport --db waycare_production_db --collection incidents --query '{ "creation_time": {$gte:ISODate(1506805200000), $lt: ISODate(1509487200000) },$or: [ {"section.road_type":"street"}, {"section.road_type":"standard"} ],$or: [ {"type":"construction"}, {"type":"hazard"},{"type":"infrastructure"},{"type":"special_event"} ] }' --out street_other.json

//sudo mongoexport --db waycare_production_db --collection incidents --query '{"creation_time": {$gte:ISODate(1506819600000), $lt: ISODate(1509494400000) },"source":"waze_traffic_alert","incident_state":"confirmed"}' --out street_in.json


var start_Date = "2017-10-01T01:00:00Z"
var end_Date = "2017-11-01T00:00:00Z"
//1: incidents
//db.getCollection('incidents').find({creation_time: { $gte:ISODate(start_Date), $lt: ISODate(end_Date) },'type':"incident"} )
//db.getCollection('incidents').find({creation_time: { $gte:ISODate(start_Date), $lt: ISODate(end_Date) },'type':"incident"} ).count()
//2: crashes
//db.getCollection('incidents').find({creation_time:{$gte:ISODate(start_Date), $lt: ISODate(end_Date) },'type':"crash"})
//db.getCollection('incidents').find({creation_time:{$gte:ISODate(start_Date), $lt: ISODate(end_Date) },'type':"crash"}).count()
//3: other payload types
//db.getCollection('incidents').find({creation_time:{$gte:ISODate(start_Date), $lt: ISODate(end_Date) }, $or: [ {'type':"construction"}, {'type':"hazard"},{'type':"infrastructure"},{'type':"special_event"} ] })
//db.getCollection('api_add_incident_enriched').distinct('payload.type')
//construction_count = db.getCollection('api_add_incident_enriched').find({'type':"construction"}).count() 
//hazard_count = db.getCollection('api_add_incident_enriched').find({'payload.type':"hazard"}).count()
//infrastructure_count = db.getCollection('api_add_incident_enriched').find({'payload.type':"infrastructure"}).count()
//special_event_count = db.getCollection('api_add_incident_enriched').find({'payload.type':"special_event"}).count()
//result=construction_count+hazard_count+infrastructure_count+special_event_count
//4&5: querying for road type
//db.getCollection('api_add_incident_enriched').distinct('payload.section.road_type')
//db.getCollection('incidents').find({ $or: [ {"section.road_type":"highway"}, {"section.road_type":"interstate"} ] })
//db.getCollection('incidents').find({ $or: [ {"section.road_type":"highway"}, {"section.road_type":"interstate"} ] }).count()
//db.getCollection('incidents').find({ $or: [ {"section.road_type":"street"}, {"section.road_type":"standard"} ] })
//db.getCollection('incidents').find({ $or: [ {"section.road_type":"street"}, {"section.road_type":"standard"} ] }).count()

//db.getCollection('incidents').find({ creation_time: {$gte:ISODate(start_Date), $lt: ISODate(end_Date) },$or: [ {"section.road_type":"highway"}, {"section.road_type":"interstate"} ] })

//db.getCollection('incidents').find({ creation_time: {$gte:ISODate(start_Date), $lt: ISODate(end_Date) },$or: [ {"section.road_type":"highway"}, {"section.road_type":"interstate"} ],'type':"incident" })
//db.getCollection('incidents').find({ creation_time: {$gte:ISODate(start_Date), $lt: ISODate(end_Date) },$or: [ {"section.road_type":"highway"}, {"section.road_type":"interstate"} ],'type':"crash" })
//db.getCollection('incidents').find({ creation_time: {$gte:ISODate(start_Date), $lt: ISODate(end_Date) },$or: [ {"section.road_type":"highway"}, {"section.road_type":"interstate"} ],$or: [ {'type':"construction"}, {'type':"hazard"},{'type':"infrastructure"},{'type':"special_event"} ] })


//db.getCollection('incidents').find({ creation_time: {$gte:ISODate(start_Date), $lt: ISODate(end_Date) },$or: [ {"section.road_type":"street"}, {"section.road_type":"standard"} ],'type':"incident" })
//db.getCollection('incidents').find({ creation_time: {$gte:ISODate(start_Date), $lt: ISODate(end_Date) },$or: [ {"section.road_type":"street"}, {"section.road_type":"standard"} ],'type':"crash" })
//db.getCollection('incidents').find({ creation_time: {$gte:ISODate(start_Date), $lt: ISODate(end_Date) },$or: [ {"section.road_type":"street"}, {"section.road_type":"standard"} ], $or: [ {'type':"construction"}, {'type':"hazard"},{'type':"infrastructure"},{'type':"special_event"} ]})


//db.getCollection('incidents').find({ creation_time: {$gte:ISODate(start_Date), $lt: ISODate(end_Date) },camera_feed:{$ne:null}}).count()
//db.getCollection('incidents').distinct('source')
//db.getCollection('incidents').find({creation_time: {$gte:ISODate(start_Date), $lt: ISODate(end_Date) },source:'waze_traffic_alert',incident_state:'confirmed'}).count()
db.getCollection('incidents').find({creation_time: {$gte:ISODate(start_Date), $lt: ISODate(end_Date) },source:'spillman_incident',incident_state:'confirmed'}).count()
