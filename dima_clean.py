import json
import os
import datetime
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

HOST = 'smtp.gmail.com'
PASSWORD = 'SomePassword'
FROM = 'dmitryfleisher@gmail.com'
TO = 'example@gmail.com'
LOGIN = 'dmitryfleisher'
PORT = 465

pattern = json.loads('{"SP6": 0, "D59": 0, "P9": 0, "RMIN2": "7", "SAVG7": 0, "SA9": 0, "REALTIMECLK": '
                     '"2017-11-17T23:53:35", "D49": 1, "PC2": 0, "D7": 1, "D64": 0, "D46": 1, "PC9": 0, "PC1": 0, '
                     '"D61": 0, "PP10": 0, "O10": 0, "D45": 0, "PC6": 0, "RMIN4": "", "P3": 1, "D60": 0, "PC15": 0, '
                     '"SP10": 0, "PC10": 0, "ALTTIME": 0, "CH14": 1, "D20": 0, "CRDSRC": "TBC", "CH22": 0, '
                     '"TOTAL": 495695, "O9": 0, "C15": 0, "D56": 0, "O12": 0, "P12": 0, "PP15": 0, "SAVG1": 13, '
                     '"P14": 0, "D36": 0, "SAVG16": 0, "CURENTTIME": "2017-12-07T20:08:45", "D27": 0, "D63": 1, '
                     '"P8": 1, "SP12": 0, "SA10": 0, "D30": 1, "SAVG9": 0, "P15": 0, "CH2": 3, "D39": 0, "O8": 0, '
                     '"CH18": 0, "SAVG11": 0, "ALTCIR": 0, "D21": 1, "O16": 0, "D12": 0, "PP5": 0, "D35": 0, '
                     '"O14": 0, "SEQSTR": "1", "SAVG10": 0, "CH20": 0, "D41": 0, "SA4": 26, "PC3": 0, "CH15": 1, '
                     '"PP6": 1, "DTSTAMP": "2017-11-17T23:53:35", "CH3": 1, "PP9": 0, "D22": 0, "PP4": 1, "SA13": 0, '
                     '"PP8": 1, "RMIN1": "", "RPED4": "", "D38": 0, "CYCLE_NO": 0, "CH8": 1, "PP2": 1, "D11": 0, '
                     '"PC16": 0, "D15": 0, "SAVG12": 0, "D34": 0, "D29": 1, "TBC": 6, "D8": 1, "D47": 0, "D13": 0, '
                     '"SAVG5": 13, "D54": 0, "D42": 0, "RMAX1": "13", "Name": null, "C3": 1, "CH10": 1, "O7": 0, '
                     '"P4": 1, "ALTDET": 0, "D44": 0, "SA15": 0, "C1": 0, "PC4": 0, "SP9": 0, "SA2": 29, "D18": 0, '
                     '"SP_CNTDN_R4": "0-0", "P5": 1, "CH5": 1, "SAVG2": 29, "D26": 0, "SAVG13": 0, "SP_CNTDN_R3": '
                     '"0-0", "PP14": 0, "D33": 0, "PC5": 0, "C2": 0, "O4": 0, "D58": 0, "O13": 0, "SA7": 0, '
                     '"SA1": 13, "D48": 0, "C8": 1, "D17": 0, "SA3": 10, "O1": 0, "D6": 1, "RPED3": "", "ACTIONNO": '
                     '"1", "SCHEDULENO": "5", "D24": 0, "CH23": 0, "CH6": 3, "CH17": 0, "C16": 0, "SA12": 0, "C6": 0, '
                     '"D3": 0, "EVENTNO": "6", "D37": 0, "P11": 0, "D25": 1, "SP_CNTDN_R2": "0-0", "PC8": 0, "P2": 3, '
                     '"D43": 0, "FAIL": 1495, "SAVG14": 0, "PP16": 0, "PREEMPTSTR": "", "PP7": 0, "SPLITINDEXSTR": '
                     '"", "TIMEDIFF": 0, "SAVG4": 26, "D23": 0, "COORD": "FREE", "D5": 0, "O6": 0, "D1": 0, "P7": 1, '
                     '"SP7": 0, "CH9": 1, "D40": 0, "PP12": 0, "RPED1": "", "SA5": 13, "D9": 0, "SP15": 0, "D55": 0, '
                     '"PC13": 0, "CH4": 1, "D16": 0, "SP14": 0, "RPED2": "", "SAVG3": 10, "O5": 0, "D53": 0, '
                     '"D51": 0, "C13": 0, "SA8": 36, "P13": 0, "C7": 1, "C10": 0, "C14": 0, "SP_CNTDN_R1": "0-0", '
                     '"SAVG6": 29, "PATTERN_NO": 1, "CH24": 0, "RMAX2": "27", "D32": 0, "SP5": 0, "LOCALCOUNTER": 1, '
                     '"CH16": 1, "C9": 0, "SAVG15": 0, "CH19": 0, "PC12": 0, "CommStatus": 1, "CH11": 1, "SA16": 0, '
                     '"FREE": "FREE", "OFFSETSTR": "", "O3": 0, "C4": 1, "D14": 0, "D31": 1, "SA11": 0, "SP4": 0, '
                     '"DAYPLANNO": "5", "SA14": 0, "SP2": 0, "O15": 0, "C12": 0, "SA6": 29, "P16": 0, "P1": 1, '
                     '"D57": 0, "PP11": 0, "SP11": 0, "SAVG8": 36, "D2": 0, "Cid": 2002, "D19": 0, "RMAX3": "", '
                     '"PP3": 0, "PC7": 0, "RMAX4": "", "D62": 0, "D10": 0, "C11": 0, "D28": 0, "SP8": 0, "CH12": 1, '
                     '"CH21": 0, "C5": 0, "SP16": 0, "SP13": 0, "PP1": 0, "PC14": 0, "P10": 0, "CH1": 1, "P6": 3, '
                     '"D4": 0, "O11": 0, "SP1": 0, "ALTOPT": 0, "CH13": 1, "D50": 0, "O2": 0, "SP3": 0, "CH7": 1, '
                     '"PP13": 0, "RMIN3": "", "D52": 0, "PC11": 0, "OK": 494200, "Offline": false}')


def store_original(path, filename):
    global path_dst
    global current_time
    path_dst = path
    current_time = datetime.datetime.utcnow()
    with open(path + '/' + filename) as fp:
        book = json.loads(fp.read())
    os.rename(path + '/' + filename, path + '/' + current_time.strftime("%Y_%m_%dT%H:%M:%S") + '.json')
    fp.close()
    global file
    file = filename
    return book


def store():
    if not os.path.exists(path_dst + '/output'):
        os.makedirs(path_dst + '/output')
    for element in valid_elements:
        real_time = datetime.datetime.strptime(element['REALTIMECLK'], "%Y-%m-%dT%H:%M:%S") \
            .strftime("%d_%m_%Y_%H_%M_%S")
        s = json.dumps(element)
        with open(path_dst + '/output' + '/cid_' + str(element['Cid']) + '_' +
                  real_time +
                  '.json', "w") as f:
            f.write(s)
        f.close()


def send_report():
    for error_element in error_elements:
        msg = MIMEMultipart()
        text = MIMEText(error_element[1])
        msg['Subject'] = error_element[0]
        msg['To'] = TO
        msg['From'] = FROM
        msg.attach(text)

        server = smtplib.SMTP_SSL(HOST, PORT)
        server.ehlo()
        server.login(LOGIN, PASSWORD)
        server.ehlo()
        server.sendmail(FROM, TO, msg.as_string())
        server.quit()


def clean():
    global valid_elements
    global error_elements
    global current_time
    pattern_keys = pattern.keys()
    for element in original:
        # First of all - check Cid and what type Cid have
        if 'Cid' not in element or type(element['Cid']) != int:
            error_elements.append(['Error', 'Error, '
                                            ' Cid is missing or value have  wrong type, file : ' + file])
            continue
        # Check -What keys must be removed .Keys exists in the element , but doesnt exists in the pattern
        remove_keys = element.keys() - pattern_keys
        for remove_key in remove_keys:
            del element[remove_key]
            error_elements.append(['Warning', 'Cid : ' + str(element['Cid']) +
                                   ' have a new key : ' + remove_key + '.Removed'])
        # Check -Keys must be add to element
        add_keys = pattern_keys - element.keys()
        for add_key in add_keys:
            element[add_key] = ''
            error_elements.append(['Warning', 'Cid : ' + str(element['Cid']) +
                                   ' have no key : ' + add_key + '.Added'])
        # Check - If Type of values is correct
        for key, value in pattern.items():
            # Avoiding double adding missing key
            if key in add_keys:
                continue
            # Updating curent_time key
            if key == 'CURENTTIME':
                element[key] = current_time.strftime("%Y-%m-%dT%H:%M:%S")
            # Checking type of values
            if not isinstance(value, type(element[key])):
                element[key] = ''
                error_elements.append(['Warning', 'Cid : ' + str(element['Cid']) +
                                       ' have wrong type : ' + key + '.Added'])

        valid_elements.append(element)


file = ''
path_dst = ''
current_time = ''
original = store_original('/home/dmitry/PycharmProjects/WayCareTask/resources',
                          '2017_12_25T07:29:20.json')
valid_elements = []
error_elements = []

clean()
print(error_elements)
print('ALl ' + str(len(original)))
print('Valid ' + str(len(valid_elements)))
print('ERRORS ' + str(len(error_elements)))

store()
send_report()
