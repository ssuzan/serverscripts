# Yaniv Costica
# yaniv.costica@waycaretech.com                                                       
# fetch file

import sys,os
import json
import requests

ENV = 'dev' # 'dev' for Development, 'prod' for production
SCHEMA = "http://" # URL schema
SERVER_IP = '10.0.0.21'
PORT = '27000'
GET_DATA = '/Data/Get/'
GET_SENSOR = '/Sensor/Get/'

# Load configurations
dire = os.path.dirname(os.path.realpath(__file__))
c_file = dire + '/config.j'
with open(c_file) as json_data_file:
    data = json.load(json_data_file)
sys.path.append(os.environ['WORKSPACE'] + '/' + data[ENV]['scripts_path'] + '/globals')

import universal_imports
import traffic_lights_lv_store

access_file = os.environ['WORKSPACE'] + data[ENV]['scripts_path'] + '/globals/traffic_lights_lv_access.txt'

headers = {
    'Content-Type': "application/x-www-form-urlencoded",
    'credentials': 'include',
}

# Connect to traffic lights and authenticate
def traffic_lights_lv_fetch(save_path, filename, store_original, is_prod):
    try:
        r = requests.get(SCHEMA + SERVER_IP + ':' + PORT + GET_DATA)  
    except Exception as e:
        return [False, 'traffic_lights_lv_fetch failed to retrieve data (bad get request).\nfilname is:' + filename + '\nexception is: ' + str(e)]
    if (r.status_code == 200):
        # Store original
        if store_original:
            traffic_lights_lv_store.traffic_lights_lv_store_original(r.json(), save_path, filename, ENV)
        return [True, r.json()]
    else:
        return [False, 'traffic_lights_lv failed to authorize.\nfilname is:' + filename]

#save_path = os.environ['WORKSPACE']+'/DB_backups/data_from_traffic_lights_lv'
#test = traffic_lights_lv_fetch(save_path, 'test_filename', True, False)
