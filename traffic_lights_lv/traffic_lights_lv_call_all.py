# Yaniv Costica
# yaniv.costica@waycaretech.com
# main file

import sys,os
import os.path
import json
from email.mime.text import MIMEText

ENV = 'dev' # 'dev' for Development, 'prod' for production

# Load configurations
dire = os.path.dirname(os.path.realpath(__file__))
c_file = dire + '/config.j'
with open(c_file) as json_data_file:
    data = json.load(json_data_file)

sys.path.append(os.environ['WORKSPACE'] + '/' + data[ENV]['scripts_path'] + '/globals')

import universal_imports
import sendemail
import traffic_lights_lv_fetch
import traffic_lights_lv_store
import traffic_lights_lv_clean

email_file = os.environ['WORKSPACE']+'/'+data[ENV]['scripts_path']+'/globals/emails'
store_original = True
dump_to_output = "FILE"
save_path = os.environ['WORKSPACE']+'/DB_backups/data_from_traffic_lights_lv'
prod_fifo_ip = ''
type_name = 'traffic_lights_live_lv'
is_prod_flag = False
is_dev_flag = False

def send_report(subject_i, message_i):
    text = open(email_file, 'r')
    email_string = text.read()
    email_json = universal_imports.json.loads(email_string)
    sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = subject_i, message = message_i, login = email_json['email'], password = email_json['password'])

def traffic_lights_lv_call_all():
    try:
        now = str(universal_imports.datetime.utcnow())
        now = now.split(" ")
        now[0] = now[0].split("-")
        now[1] = now[1].split(":")
        filename = type_name + "_" + now[0][2] + "_" + now[0][1] + "_" + now[0][0] + "_" + now[1][0] + "_" + now[1][1] + "_" + now[1][2].replace(".","_")
        curr_t = universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")
        print('traffic_lights_lv_call_all.py started! time is: ' + curr_t + ' UTC')

        # Call fetch
        doc = traffic_lights_lv_fetch.traffic_lights_lv_fetch(save_path, filename, store_original, is_prod_flag)
        if(doc[0]):
            try:
                doc_xml = doc[1]
                clean_results = traffic_lights_lv_clean.traffic_lights_lv_clean(doc_xml, filename) # Sending the json and the output file name
                if len(clean_results[0]) == 0:
                    saving_result = traffic_lights_lv_store.traffic_lights_lv_store_final(curr_t, clean_results[1], dump_to_output, filename, prod_fifo_ip, type_name, is_prod_flag, is_dev_flag)
                    if saving_result:
                        print('traffic_lights_lv_call_all.py completed successfuly! time is: ' + universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
                else:
                    send_report('error in traffic_lights_lv_clean', clean_results[0])
            except Exception as e:
                send_report('error in traffic_lights_lv_sensors', 'error caught in the clean or store script.\nexception is: ' + str(e))
        else:
            send_report('error in traffic_lights_lv_fetch', doc[1])
    except Exception as e:
        send_report('error in traffic_lights_lv_sensors,', 'error caught in traffic_lights_lv_call_all.py.\nexception is: ' + str(e))


if __name__ == "__main__":
    traffic_lights_lv_call_all()
