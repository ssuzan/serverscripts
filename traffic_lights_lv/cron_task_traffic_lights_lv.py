# Yaniv Costica
# yaniv.costica@waycaretech.com
# cron file
import sys, os
import json

ENV = 'dev' # 'dev' for Development, 'prod' for production
# Load configurations
with open('config.json') as json_data_file:
    data = json.load(json_data_file)

sys.path.append(os.environ['WORKSPACE'] + '/' + data[ENV]['scripts_path'] + '/globals')

import universal_imports
import apscheduler
from apscheduler.schedulers.blocking import BlockingScheduler
import traffic_lights_lv_call_all

def scheduled_call():
    print('traffic_lights_lv_call_all started successfuly! time is: '+ universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
    #initialize new scheduler
    sched = BlockingScheduler(timezone='UTC',coalesce=True)
    sched.add_job(traffic_lights_lv_call_all.traffic_lights_lv_call_all, 'interval', id='job_id1', seconds=10,coalesce=True)
    sched.start()

scheduled_call()
