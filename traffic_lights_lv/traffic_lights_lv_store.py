# Yaniv Costica
# yaniv.costica@waycaretech.com                                                       
# store file

import sys,os
import os.path
import json

ENV = 'dev'

# Load configurations
dire = os.path.dirname(os.path.realpath(__file__))
c_file = dire + '/config.j'
with open(c_file) as json_data_file:
    data = json.load(json_data_file)
sys.path.append(os.environ['WORKSPACE'] + '/' + data[ENV]['scripts_path'] + '/globals')

import universal_imports
import sendemail
import push_to_fifo
import push_to_fifo_dev
from collections import OrderedDict
from email.mime.text import MIMEText

dire = os.path.dirname(__file__)
save_path = os.environ['WORKSPACE']+'/DB_backups/data_from_traffic_lights_lv'
email_file = os.environ['WORKSPACE'] + '/' + data[ENV]['scripts_path'] + '/globals/emails'

def send_report(subject_i, message_i):
    text = open(email_file, 'r')
    email_string = text.read()
    email_json = universal_imports.json.loads(email_string)
    sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = subject_i, message = message_i, login = email_json['email'], password = email_json['password'])

def traffic_lights_lv_store_original(raw_data,save_path, filename, ENV):
    with open(save_path + "/database_original/" + filename + '.json', 'w') as fd:
        fd.write(universal_imports.json.dumps(raw_data, sort_keys=True, indent=4, separators=(',', ': ')))


# Store cleaned traffic_lights_lv_json file
def traffic_lights_lv_store_final(curr_t, traffic_lights_lv_json, dump_to_output, filename, fifo_ip, type_name, is_prod, is_dev):
    err_report = []
    warning_report = []
    new_store_json = {'type':str(type_name),'date_time':str(curr_t).replace("-","_")+' UTC','suspicious':False,'error':False,'data':traffic_lights_lv_json}
    data_out = universal_imports.json.dumps(new_store_json, sort_keys=True, indent=4, separators=(',', ': '))
    # Send to FILE:
    if ((dump_to_output == "FILE") or (dump_to_output == "ALL")):
        try:
            print (save_path + '/database/' + filename + '.json')
            with open(os.path.join(save_path + '/database/' + filename + '.json'), 'w') as outfile:
                #json.dump(traffic_lights_lv_json, outfile)
                outfile.write(data_out)
        except Exception as e:
            send_report('error in traffic_lights_lv_store', 'failed to store clean file' + filename + '.json\nexception is: ' + str(e))

    #sends file to FIFO
    if ((dump_to_output == "FIFO") or (dump_to_output == "ALL")):
        try:
            if (is_prod):
                push_to_fifo.push_to_fifo(data_out)
        except Exception as e:
            send_report('error in traffic_lights_lv_store', 'fifo error in traffic_lights_lv_store\nexception is: ' + str(e))

        #push to development FIFO
        try:
            if (is_dev):
                push_to_fifo_dev.push_to_fifo_dev(new_store_json)
        except Exception as e:
            send_report('warning in traffic_lights_lv_store', 'dev-fifo failed in traffic_lights_lv_store\nexception is: ' + str(e))



    return len(err_report) == 0

