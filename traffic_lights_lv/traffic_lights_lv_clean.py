# Yaniv Costica
# yaniv.costica@waycaretech.com                                                       
# clean file

import sys, os
from email.mime.text import MIMEText
import lxml.etree as etree
from pprint import pprint
import json
import datetime

ENV = 'dev'

# Load configurations
dire = os.path.dirname(os.path.realpath(__file__))
c_file = dire + '/config.j'
with open(c_file) as json_data_file:
    data = json.load(json_data_file)

sys.path.append(os.environ['WORKSPACE'] + '/' + data[ENV]['scripts_path'] + '/globals')

import universal_imports
import sendemail

email_file = os.environ['WORKSPACE'] + '/' + data[ENV]['scripts_path'] + '/globals/emails'

# The incoming file to check categories & keys against existing set (look for new keys)
dire = os.path.dirname(__file__)
input_file = os.path.join(dire + 'test.json')


# filename - string
# Return type -> [bool, [{},{},...{}]
def traffic_lights_lv_clean(input_file, filename):
    # for input from fetch directly
    item_order = 0
    clean_jsons = []
    err_report = []
    warn_report = []
    controllers_list = input_file
    if controllers_list: # If valid and not empty
        #### Stable categories obtained from pre-fetch (Seperated to int, str, bool & other):
        category_stable_int = ['O2', 'C16', 'TOTAL', 'CH5', 'SA14', 'O16', 'P4', 'SA12', 'TBC', 'P9', 'SP15', 'D48', 'CH23', 'D44', 'D7', 'CH22', 'CH20', 'C3', 'D20', 'D47', 'Cid', 'D62', 'C13', 'SP14', 'O1', 'PATTERN_NO', 'O14', 'PC6', 'D19', 'CH13', 'D2', 'D36', 'SA10', 'SP16', 'D9', 'CH12', 'P7', 'D34', 'O8', 'D5', 'PP3', 'SAVG3', 'C12', 'C5', 'P8', 'O3', 'O12', 'C1', 'D32', 'SP12', 'SA1', 'D3', 'C11', 'D31', 'D46', 'C7', 'SP10', 'P13', 'CH19', 'C14', 'P2', 'O13', 'D21', 'SP5', 'PP5', 'PC16', 'SA3', 'P6', 'SAVG8', 'D56', 'C2', 'P5', 'D50', 'PC8', 'P12', 'PC4', 'D58', 'D51', 'SA5', 'SAVG14', 'O10', 'D16', 'P10', 'PC5', 'D23', 'CH21', 'D12', 'SAVG12', 'D25', 'D49', 'SAVG1', 'D13', 'SP7', 'SA13', 'SAVG11', 'D27', 'D26', 'D14', 'SP2', 'D43', 'CH8', 'SA6', 'PP12', 'CH18', 'D35', 'CH1', 'O5', 'PP2', 'PP14', 'D60', 'PP10', 'SAVG9', 'CYCLE_NO', 'D30', 'O15', 'PC12', 'PP16', 'O6', 'D6', 'PP15', 'D39', 'PP4', 'C9', 'SP1', 'CH10', 'O4', 'SAVG6', 'C4', 'CH17', 'SA16', 'PP9', 'LOCALCOUNTER', 'D22', 'P15', 'D54', 'D53', 'C8', 'FAIL', 'SA2', 'SAVG2', 'SAVG13', 'CH9', 'D33', 'P16', 'SP6', 'D24', 'D52', 'SA4', 'PC14', 'CH7', 'D38', 'PC9', 'SAVG7', 'PC2', 'PP13', 'OK', 'SA7', 'SP9', 'ALTDET', 'PC1', 'SA8', 'CH3', 'CH4', 'P1', 'SP11', 'D57', 'CH11', 'CommStatus', 'PC3', 'D17', 'D28', 'PC15', 'PP11', 'ALTTIME', 'P14', 'D41', 'SP3', 'SA11', 'PC11', 'C10', 'TIMEDIFF', 'D18', 'D15', 'D29', 'CH24', 'D11', 'SA9', 'D10', 'D42', 'D40', 'O7', 'SP4', 'SAVG5', 'SAVG16', 'D64', 'C15', 'PC13', 'CH14', 'D55', 'D37', 'D63', 'D4', 'O11', 'PC7', 'CH16', 'SAVG10', 'SP8', 'ALTOPT', 'PP8', 'C6', 'CH15', 'D1', 'SAVG15', 'SA15', 'D61', 'D45', 'O9', 'SP13', 'PP7', 'D8', 'CH2', 'P3', 'PP1', 'PP6', 'P11', 'ALTCIR', 'CH6', 'SAVG4', 'PC10', 'D59']
        category_stable_str = ['RMIN1', 'CRDSRC', 'DTSTAMP', 'RMAX1', 'RMIN4', 'EVENTNO', 'FREE', 'RPED1', 'OFFSETSTR', 'SP_CNTDN_R2', 'RMAX3', 'SP_CNTDN_R1', 'DAYPLANNO', 'RPED3', 'SP_CNTDN_R4', 'SCHEDULENO', 'COORD', 'REALTIMECLK', 'PREEMPTSTR', 'RMIN2', 'SEQSTR', 'SPLITINDEXSTR', 'RPED2', 'ACTIONNO', 'RMIN3', 'RMAX4', 'RMAX2', 'RPED4', 'SP_CNTDN_R3']
        category_stable_bool = ['Offline']
        category_stable_other = ['Name']
        category_stable_all = category_stable_other + category_stable_bool + category_stable_str + category_stable_int
        # default_values will be used in case of missing keys in the input
        default_values = {'int': 0, 'str': "", 'bool': False, 'other': None}
        #### Some intermediary keys that will be skipped in flat JSON object returned by the function (to make output more concise)
        skip_list = ['Name']
        ### MAIN LOOP - TRAVERSING CONTROLLERS ###
        item_order = 1 # Used for error/warning report
        for controller in controllers_list:
            clean_json = []
            keys_list = list(controller)
            # Check if CID exists:
            if 'Cid' not in keys_list:
                err_report.append([filename, str(item_order), '', 'Cid is missing', 'Cid is missings in ' + filename])
                continue
            # Check if CID type is valid:
            if type(controller['Cid']) != int:
                err_report.append([filename, str(item_order), '', 'Wrong CID value type', 'Wrong CID value type ' + filename])
                continue
            # Iterate through each field at the controller:
            for k in keys_list:
                # If field in skip list, continue to the next field without adding it to clean_json
                if k in skip_list:
                    continue
                v = controller[k]
                # Check type:
                if k in category_stable_int:
                    if not isinstance(v, int):
                        # Enforce default value and add to warning report
                        warn_report.append([filename, item_order, k, 'traffic_lights_lv wrong type', 'Wrong category "' + str(k) + '" in ' + filename + ', item num=' + str(item_order)])
                        clean_json.append({k: default_values['int']})
                    else:
                        clean_json.append({k: v})
                elif k in category_stable_str:
                    if not isinstance(v, str):
                        # Convert value to string and add to warning report
                        warn_report.append([filename, item_order, k, 'traffic_lights_lv wrong type', 'Wrong category "' + str(k) + '" in ' + filename + ', item num=' + str(item_order)])
                        clean_json.append({k: str(v)})
                    else:
                        v = v.replace("&amp;", "and")
                        clean_json.append({k: v})
                elif k in category_stable_bool:
                    if not isinstance(v, bool):
                        # Enforce default value and add to warning report
                        warn_report.append([filename, item_order, k, 'traffic_lights_lv wrong type', 'Wrong category "' + str(k) + '" in ' + filename + ', item num=' + str(item_order)])                
                        clean_json.append({k: default_values['bool']})
                    else:
                        clean_json.append({k: v})
                elif k not in category_stable_other:
                    # It's a new key: Add to warning report and remove it from output
                    warn_report.append([filename, item_order, k, 'new_category', 'New category "' + str(k) + '" in ' + filename + ', item num=' + str(item_order)])
                    continue
                else:
                    clean_json.append({k: v})
            # Check for missing keys and report. Append missing key to result object
            diff_list = list(set(category_stable_all) - set(keys_list))
            if len(diff_list) != 0:
                for missing_element in diff_list:
                    # Add to warning report and add the missing element to the clean json:
                    warn_report.append([filename, item_order, k, 'missing_category', 'Missing category "' + str(missing_element) + '" in ' + filename + ', item num=' + str(item_order)])
                    if missing_element in category_stable_int:
                        clean_json.append({missing_element: default_values['int']})
                    elif missing_element in category_stable_bool:
                        clean_json.append({missing_element: default_values['bool']})
                    elif missing_element in category_stable_str:
                        clean_json.append({missing_element: default_values['other']})
                    else:
                        clean_json.append({missing_element: default_values['str']})
            clean_jsons.append(clean_json) 
            item_order += 1
    else: # Here if list is empty or format not valid
        err_report.append([filename, '', '', 'Input json to clean is empty or not valid', filename])


    # Send error and warning report contents - each item individually (if needed - it's possible to send one combined report)
    err_message_content = []
    err_message_content_email = []
    for e in err_report:
        prefix = "traffic_lights_lv clean errors: "
        #... This is a report in form of string, but error_report has also other fields that can be used to automate the process:
        message_text = prefix + e[3] + ", " + e[4]
        err_message_content.append(message_text)
    if err_message_content:
        text = open(email_file,'r')
        email_string = text.read()
        email_json = universal_imports.json.loads(email_string)
        err_message_content_email = "\n".join(err_message_content)
        if len(err_message_content_email) > 511:
            err_message_content_email = 'First errors are:\n' + err_message_content_email[0:500]
        #sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'traffic_lights_lv error report', message = err_message_content_email, login = email_json["email"], password = email_json["password"]) 
    
    warn_message_content = []
    warn_message_content_email = []
    for w in warn_report:
        prefix = "traffic_lights_lv clean warnings: "
        #... This is a report in form of string, but error_report has also other fields that can be used to automate the process:
        message_text = prefix + w[3] + ", " + w[4]
        warn_message_content.append(message_text)
    if warn_message_content:
        text = open(email_file,'r')
        email_string = text.read()
        email_json = universal_imports.json.loads(email_string)
        warn_message_content_email = "\n".join(warn_message_content)
        if len(warn_message_content_email) > 511:
            warn_message_content_email = 'First warnings are:\n' + warn_message_content_email[0:500]
        sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'traffic_lights_lv warning report', message = warn_message_content_email, login = email_json["email"], password = email_json["password"]) 


    #return [len(err_report) == 0, clean_jsons]
    return [err_message_content_email, clean_jsons]
    
# test = traffic_lights_lv_clean(pattern, filename)
# pprint (test)