#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: json containing data from waze database
#output: properly formatted json containing data from waze database
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports
#import jsonlib_python3
import requests


#def clean_incident(incident):

def clean(json_clean):
    if('startTime' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['startTime'],str)):
            place_holder_variable = True
        else:
            print(json_clean['startTime'])
            return(False,'startTime not str')
    else:
        return(False,'startTime not present')
    if('startTimeMillis' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['startTimeMillis'],int)):
            place_holder_variable = True
        else:
            print(json_clean['startTimeMillis'])
            return(False,'startTimeMillis not int')
    else:
        return(False,'startTimeMillis not present')
    if('endTime' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['endTime'],str)):
            place_holder_variable = True
        else:
            print(json_clean['endTime'])
            return(False,'endTime not str')
    else:
        return(False,'endTime not present')
    if('endTimeMillis' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['endTimeMillis'],int)):
            place_holder_variable = True
        else:
            print(json_clean['endTimeMillis'])
            return(False,'endTimeMillis not int')
    else:
        return(False,'endTimeMillis not present')    
    if('irregularities' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['irregularities'],list)):
            place_holder_variable = True
            for i in range(0,len(json_clean['irregularities'])):
                if('irregularities' in json_clean['irregularities'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(json_clean['irregularities'][i]['irregularities'],list)):
                        place_holder_variable = True
                    else:
                        print(json_clean['irregularities'][i]['irregularities'])
                        return(False,'irregularities not list')
                else:
                    return(False,'irregularities not present')
                if('endTimeMillis' in json_clean['irregularities'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(json_clean['irregularities'][i]['endTimeMillis'],int)):
                        place_holder_variable = True
                    else:
                        print(json_clean['irregularities'][i]['endTimeMillis'])
                        return(False,'endTimeMillis not int')
                else:
                    return(False,'endTimeMillis not present')
                if('endTime' in json_clean['irregularities'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(json_clean['irregularities'][i]['endTime'],str)):
                        place_holder_variable = True
                    else:
                        print(json_clean['irregularities'][i]['endTime'])
                        return(False,'endTime not str')
                else:
                    return(False,'endTime not present')
                if('startTime' in json_clean['irregularities'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(json_clean['irregularities'][i]['startTime'],str)):
                        place_holder_variable = True
                    else:
                        print(json_clean['irregularities'][i]['startTime'])
                        return(False,'startTime not str')
                else:
                    return(False,'startTime not present')
                if('startTimeMillis' in json_clean['irregularities'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(json_clean['irregularities'][i]['startTimeMillis'],int)):
                        place_holder_variable = True
                    else:
                        print(json_clean['irregularities'][i]['startTimeMillis'])
                        return(False,'startTimeMillis not int')
                else:
                    return(False,'startTimeMillis not present')
                
        else:
            print(json_clean['irregularities'])
            return(False,'irregularities not list')
    else:
        return(True,'irregularities not present')    
    return(True, 'irregularities present')     
