#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: waze database https request (url) 
#output: json of data given by https request or json containing error message
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports
import requests
import store_wz_jam


def downloadWaze(url_waze,save_path,filename):  
    #text = open(workspace_environ+'/DB_backups/data_from_wz_jam/database_original/'+'waze_traffic_jam_lv_09_08_2017_11_08_21_361765.json','r')
    #string = text.read()
    #email_json = universal_imports.json.loads(string)     
    #r = email_json
    try:
        #gets json from url
        r = requests.get(url_waze)
        try:
            r = r.json()
            try:
                store_wz_jam.store_original(r,save_path,filename)
            except:
                r = universal_imports.json.loads('{"error" : "unable to save orignal"}') 
        except:
            r = universal_imports.json.loads('{"error" : "unable to convert to json"}') 
    except requests.exceptions.ConnectionError:
        r = universal_imports.json.loads('{"error" : "requests.exceptions.ConnectionError"}')  
    return r
     
