#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: json containing data from waze database
#output: properly formatted json containing data from waze database
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
from automation_building_scripts import automate_build_clean
import universal_imports
#import jsonlib_python3
import requests
  

def clean_incident(incident):
    errors=[]  
    if('city' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['city'],str)):
            place_holder_variable = True
        else:
            incident['city'] = ''
            errors.append('city not str')
    else:
        #errors.append('city not present')
        incident['city'] = ''              
    if('blockingAlertUuid' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['blockingAlertUuid'],str)):
            place_holder_variable = True
        else:
            incident['blockingAlertUuid'] = ''
            errors.append('blockingAlertUuid not str')
    else:
        #errors.append('blockingAlertUuid not present')
        incident['blockingAlertUuid'] = ''                              
    if('endNode' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['endNode'],str)):
            place_holder_variable = True
        else:
            incident['endNode'] = ''
            errors.append('endNode not str')
    else:
        #errors.append('endNode not present')
        incident['endNode'] = ''
    if('pubMillis' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['pubMillis'],int)):
            place_holder_variable = True
        else:
            incident['pubMillis'] = ''
            errors.append('pubMillis not int')
    else:
        #errors.append('pubMillis not present')
        incident['pubMillis'] = ''
    if('line' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['line'],list)):  
            for n in range(0,len(incident['line'])):
                for key in incident['line'][n]:
                    if key not in ['x', 'y']:
                        errors.append('new key '+key+' in json_clean[jams][i][line] '+str(n))   
                if('y' in incident['line'][n].keys()):
                    place_holder_variable = True
                    if(isinstance(incident['line'][n]['y'],float)):
                        place_holder_variable = True
                    else:
                        incident['line'][n]['y'] = ''
                        errors.append('y not float')
                else:
                    #errors.append('y not present')
                    incident['line'][n]['y'] = ''
                if('x' in incident['line'][n].keys()):
                    place_holder_variable = True
                    if(isinstance(incident['line'][n]['x'],float)):
                        place_holder_variable = True
                    else:
                        incident['line'][n]['x'] = ''
                        errors.append('x not float')
                else:
                    #errors.append('x not present')
                    incident['line'][n]['x'] = ''
        else:
            incident['line'] = ''
            errors.append('type not list')                    
    else:
        #errors.append('line not present')
        incident['line'] = ''
    if('type' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['type'],str)):
            place_holder_variable = True
        else:
            incident['type'] = ''
            errors.append('type not str')
    else:
        #errors.append('type not present')
        incident['type'] = ''
    if('speed' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['speed'],int) or isinstance(incident['speed'],float)):
            place_holder_variable = True
        else:
            incident['speed'] = ''
            errors.append('speed not int or float')
    else:
        #errors.append('speed not present')
        incident['speed'] = ''
    if('uuid' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['uuid'],int)):
            place_holder_variable = True
        else:
            return([False])
    else:
        return([False])
    if('city' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['city'],str)):
            place_holder_variable = True
        else:
            incident['city'] = ''
            errors.append('city not str')
    else:
        #errors.append('city not present')
        incident['city'] = ''
    if('length' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['length'],int)):
            place_holder_variable = True
        else:
            incident['length'] = ''
            errors.append('length not int')
    else:
        #errors.append('length not present')
        incident['length'] = ''
    if('turnType' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['turnType'],str)):
            place_holder_variable = True
        else:
            incident['turnType'] = ''
            errors.append('turnType not str')
    else:
        #errors.append('turnType not present')
        incident['turnType'] = ''
    if('street' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['street'],str)):
            place_holder_variable = True
        else:
            incident['street'] = ''
            errors.append('street not str')
    else:
        #errors.append('street not present')
        incident['street'] = ''
    if('segments' in incident.keys()):
        place_holder_variable = True
        for n in range(0,len(incident['segments'])):
            for key in incident['segments'][n]:
                if key not in []:
                    errors.append('new key '+key+' in json_clean[jams][i][segments] '+str(n))        
    else:
        #errors.append('segments not present')
        incident['segments'] = ''
    if('level' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['level'],int)):
            place_holder_variable = True
        else:
            incident['level'] = ''
            errors.append('level not int')
    else:
        #errors.append('level not present')
        incident['level'] = ''
    if('country' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['country'],str)):
            place_holder_variable = True
        else:
            incident['country'] = ''
            errors.append('country not str')
    else:
        #errors.append('country not present')
        incident['country'] = ''
    if('blockingAlertUuid' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['blockingAlertUuid'],str)):
            place_holder_variable = True
        else:
            incident['blockingAlertUuid'] = ''
            errors.append('blockingAlertUuid not str')
    else:
        #errors.append('blockingAlertUuid not present')
        incident['blockingAlertUuid'] = ''
    if('id' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['id'],int)):
            place_holder_variable = True
        else:
            incident['id'] = ''
            errors.append('id not int')
    else:
        #errors.append('id not present')
        incident['id'] = ''
    if('roadType' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['roadType'],int)):
            place_holder_variable = True
        else:
            incident['roadType'] = ''
            errors.append('roadType not int')
    else:
        #errors.append('roadType not present')
        incident['roadType'] = ''
    if('delay' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['delay'],int)):
            place_holder_variable = True
        else:
            incident['delay'] = ''
            errors.append('delay not int')
    else:
        #errors.append('delay not present')
        incident['delay'] = ''    
    return (True, incident,errors)


def clean(json_clean):  
    errors = []
    for key in json_clean:
        if key not in ['startTimeMillis', 'startTime', 'jams', 'endTime', 'endTimeMillis']:
            errors.append('new key '+key+' in json_clean')    

    if('startTimeMillis' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['startTimeMillis'],int)):
            place_holder_variable = True
        else:
            json_clean['startTimeMillis'] = ''
            errors.append('startTimeMillis not int')
    else:
        errors.append('startTimeMillis not present')
        json_clean['startTimeMillis'] = ''
    if('endTimeMillis' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['endTimeMillis'],int)):
            place_holder_variable = True
        else:
            json_clean['endTimeMillis'] = ''
            errors.append('endTimeMillis not int')
    else:
        errors.append('endTimeMillis not present')
        json_clean['endTimeMillis'] = ''
    if('endTime' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['endTime'],str)):
            place_holder_variable = True
        else:
            json_clean['endTime'] = ''
            errors.append('endTime not str')
    else:
        errors.append('endTime not present')
        json_clean['endTime'] = ''
    if('startTime' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['startTime'],str)):
            place_holder_variable = True
        else:
            json_clean['startTime'] = ''
            errors.append('startTime not str')
    else:
        errors.append('startTime not present')
        json_clean['startTime'] = ''    
    
    if('jams' in json_clean.keys()):

        place_holder_variable = True
        if(isinstance(json_clean['jams'],list)): 
            for i in range(0,len(json_clean['jams'])):
                clean_out = clean_incident(json_clean['jams'][i])
                if clean_out[0] == True:   
                    json_clean['jams'][i] = clean_out[1]
                    if len(clean_out[2])>0:
                        errors.append("for jam "+str(json_clean['jams'][i]['uuid'])+" "+str(clean_out[2]))  
                else:
                    json_clean['alerts'][i] = False
                    errors.append("for jam number "+str(i)+" uuid not correct or present")
        elif(isinstance(json_clean['jams'],dict)): 
            json_clean['jams'] = [json_clean['jams']]
            for i in range(0,len(json_clean['jams'])):
                clean_out = clean_incident(json_clean['jams'][i])
                if clean_out[0] == True:   
                    json_clean['jams'][i] = clean_out[1]
                    if len(clean_out[2])>0:
                        errors.append("for jam "+str(json_clean['jams'][i]['uuid'])+" "+str(clean_out[2]))  
                else:
                    json_clean['alerts'][i] = False
                    errors.append("for jam number "+str(i)+" uuid not correct or present")                    

        else:
            json_clean['jams'] = []
            errors.append('jams not dict or list')        

    else:
        errors.append('jams not present')
        json_clean['jams'] = []   
    #    for i in range(0,len(incident['segments']):
    #        for i in range(0,len(incident['line']):    
    return(True, 'placeholder',json_clean,errors)
