#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: json input and file directory path
#output: json stored to directory
#import hashlib
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports
#import jsonlib_python3
import requests
import hashlib
import sendemail
from email.mime.text import MIMEText
import push_to_fifo
full_file_path = 'backup_full_files'
type_name = 'waze_traffic_irregularities_lv'
email_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/emails'

def store_original(store_json, save_path,filename):
    #saves file   
    lastFiletxt = open(save_path+"/database_original/"+filename+'.json', 'w')
    lastFiletxt.write(universal_imports.json.dumps(store_json, sort_keys=True, indent=4, separators=(',', ': ')))      

def store(store_json, save_path,hash_log,dump_output_to,default_age,filename,errors):
    #gets date for filename as now
    hashes = {}
    repeat_hashes=[]
    for i in range(0,len(store_json['data'])):
        if store_json['data'][i] != False:
            hash_match_flag= False
            hash_uuid = hashlib.md5()
            encoded_uuid = str(store_json['data'][i])
            #encoded_uuid.pop(store_json['data'][i]))
            #print(encoded_uuid)
            encoded_uuid = encoded_uuid.encode("utf-8")
            hash_uuid.update(encoded_uuid)
            if store_json['data'][i]['id'] in hash_log.keys():
                if hash_log[store_json['data'][i]['id']][0]!=hash_uuid.hexdigest():
                    hashes[store_json['data'][i]['id']] = [hash_uuid.hexdigest(),default_age]
                    hash_match_flag= False
                else:
                    repeat_hashes.append(store_json['data'][i]['id'])
                    hash_match_flag= True
            else:
                hashes[store_json['data'][i]['id']] = [hash_uuid.hexdigest(),default_age]
                hash_match_flag= False
            filename_new = filename+"_incident_"+str(store_json['data'][i]['id'])
            #saves file
            if (hash_match_flag == False):
                curr_t = universal_imports.datetime.utcnow().strftime("%d-%m-%Y %H:%M:%S")
                new_store_json = {'type':type_name,'date_time':str(curr_t).replace("-","_")+' UTC','suspicious':False,'data':store_json['data'][i],'error':False}  
                if store_json['data'][i]['id'] in errors:
                    new_store_json['error'] = True
                if ((dump_output_to == "FILE") or (dump_output_to == "ALL")): 
                    lastFiletxt = open(save_path+"/database/"+filename_new+'.json', 'w')
                    lastFiletxt.write(universal_imports.json.dumps(new_store_json, sort_keys=True, indent=4, separators=(',', ': ')))  
                #sends file to FIFO
                if ((dump_output_to == "FIFO") or (dump_output_to == "ALL")):
                    try:
                        push_to_fifo.push_to_fifo(new_store_json,"fifo_name")         
                    except:
                        text = open(email_file,'r')
                        email_string = text.read()
                        email_json = universal_imports.json.loads(email_string)  
                        sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'store_wz_jam() error', message = 'fifo error in store_wz_jam() '+filename, login = email_json['email'], password = email_json['password'])         
  
                if (dump_output_to == "NONE"):
                    print("wz_irregularities- dump_output_to is set to NONE")

    return [hashes,repeat_hashes]
