#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: json containing data from waze database
#output: properly formatted json containing data from waze database
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
from automation_building_scripts import automate_build_clean
import universal_imports
#import jsonlib_python3
import requests
  

def clean_incident(incident):
    errors=[]  
    if('seconds' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['seconds'],int)):
            place_holder_variable = True
        else:
            incident['seconds'] = ''
            errors.append('seconds not int')
    else:
        #errors.append('seconds not present')
        incident['seconds'] = ''
    if('endNode' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['endNode'],str)):
            place_holder_variable = True
        else:
            incident['endNode'] = ''
            errors.append('endNode not str')
    else:
        #errors.append('endNode not present')
        incident['endNode'] = ''
    if('id' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['id'],int)):
            place_holder_variable = True
        else:
            return([False])
    else:
        return([False])
    if('updateDate' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['updateDate'],str)):
            place_holder_variable = True
        else:
            incident['updateDate'] = ''
            errors.append('updateDate not str')
    else:
        #errors.append('updateDate not present')
        incident['updateDate'] = ''
    if('line' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['line'],list)):
            for i in range(0,len(incident['line'])):
                for key in incident['line'][i].keys():
                    if key not in ['x','y']:
                        errors.append('new element ' +str(key)+' in incident '+str(i))
            for i in range(0,len(incident['line'])):
                if('x' in incident['line'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(incident['line'][i]['x'],float)):
                        place_holder_variable = True
                    else:
                        incident['line'][i]['x'] = ''
                        errors.append('x not float in incident '+str(i))
                else:
                    #errors.append('x not present in incident '+str(i))
                    incident['line'][i]['x'] = ''    
                if('y' in incident['line'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(incident['line'][i]['y'],float)):
                        place_holder_variable = True
                    else:
                        incident['line'][i]['y'] = ''
                        errors.append('y not float in incident '+str(i))
                else:
                    #errors.append('y not present in incident '+str(i))
                    incident['line'][i]['y'] = ''   
        else:
            errors.append('line not list')
            incident['line'] = []            
    else:
        #errors.append('line not present')
        incident['line'] = []
    if('regularSpeed' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['regularSpeed'],float)):
            place_holder_variable = True
        else:
            incident['regularSpeed'] = ''
            errors.append('regularSpeed not float')
    else:
        #errors.append('regularSpeed not present')
        incident['regularSpeed'] = ''
    if('detectionDateMillis' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['detectionDateMillis'],int)):
            place_holder_variable = True
        else:
            incident['detectionDateMillis'] = ''
            errors.append('detectionDateMillis not int')
    else:
        #errors.append('detectionDateMillis not present')
        incident['detectionDateMillis'] = ''
    if('delaySeconds' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['delaySeconds'],int)):
            place_holder_variable = True
        else:
            incident['delaySeconds'] = ''
            errors.append('delaySeconds not int')
    else:
        #errors.append('delaySeconds not present')
        incident['delaySeconds'] = ''
    if('nComments' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['nComments'],int)):
            place_holder_variable = True
        else:
            incident['nComments'] = ''
            errors.append('nComments not int')
    else:
        #errors.append('nComments not present')
        incident['nComments'] = ''
    if('nImages' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['nImages'],int)):
            place_holder_variable = True
        else:
            incident['nImages'] = ''
            errors.append('nImages not int')
    else:
        #errors.append('nImages not present')
        incident['nImages'] = ''
    if('severity' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['severity'],int)):
            place_holder_variable = True
        else:
            incident['severity'] = ''
            errors.append('severity not int')
    else:
        #errors.append('severity not present')
        incident['severity'] = ''
    if('alerts' in incident.keys()):
        place_holder_variable = True
        #all examples contain empty list
        if(isinstance(incident['alerts'],list)):
            place_holder_variable = True
        else:
            incident['alerts'] = ''
            errors.append('alerts not list')
    else:
        #errors.append('alerts not present')
        incident['alerts'] = ''
    if('highway' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['highway'],int)):
            place_holder_variable = True
        else:
            incident['highway'] = ''
            errors.append('highway not int')
    else:
        #errors.append('highway not present')
        incident['highway'] = ''
    if('detectionDate' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['detectionDate'],str)):
            place_holder_variable = True
        else:
            incident['detectionDate'] = ''
            errors.append('detectionDate not str')
    else:
        #errors.append('detectionDate not present')
        incident['detectionDate'] = ''
    if('alertsCount' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['alertsCount'],int)):
            place_holder_variable = True
        else:
            incident['alertsCount'] = ''
            errors.append('alertsCount not int')
    else:
        #errors.append('alertsCount not present')
        incident['alertsCount'] = ''
    if('driversCount' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['driversCount'],int)):
            place_holder_variable = True
        else:
            incident['driversCount'] = ''
            errors.append('driversCount not int')
    else:
        #errors.append('driversCount not present')
        incident['driversCount'] = ''
    if('length' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['length'],int)):
            place_holder_variable = True
        else:
            incident['length'] = ''
            errors.append('length not int')
    else:
        #errors.append('length not present')
        incident['length'] = ''
    if('speed' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['speed'],float)):
            place_holder_variable = True
        else:
            incident['speed'] = ''
            errors.append('speed not float')
    else:
        #errors.append('speed not present')
        incident['speed'] = ''
    if('jamLevel' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['jamLevel'],int)):
            place_holder_variable = True
        else:
            incident['jamLevel'] = ''
            errors.append('jamLevel not int')
    else:
        #errors.append('jamLevel not present')
        incident['jamLevel'] = ''
    if('country' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['country'],str)):
            place_holder_variable = True
        else:
            incident['country'] = ''
            errors.append('country not str')
    else:
        #errors.append('country not present')
        incident['country'] = ''
    if('nThumbsUp' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['nThumbsUp'],int)):
            place_holder_variable = True
        else:
            incident['nThumbsUp'] = ''
            errors.append('nThumbsUp not int')
    else:
        #errors.append('nThumbsUp not present')
        incident['nThumbsUp'] = ''
    if('type' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['type'],str)):
            place_holder_variable = True
        else:
            incident['type'] = ''
            errors.append('type not str')
    else:
        #errors.append('type not present')
        incident['type'] = ''
    if('street' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['street'],str)):
            place_holder_variable = True
        else:
            incident['street'] = ''
            errors.append('street not str')
    else:
        #errors.append('street not present')
        incident['street'] = ''
    if('trend' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['trend'],int)):
            place_holder_variable = True
        else:
            incident['trend'] = ''
            errors.append('trend not int')
    else:
        #errors.append('trend not present')
        incident['trend'] = ''
    if('updateDateMillis' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['updateDateMillis'],int)):
            place_holder_variable = True
        else:
            incident['updateDateMillis'] = ''
            errors.append('updateDateMillis not int')
    else:
        #errors.append('updateDateMillis not present')
        incident['updateDateMillis'] = ''
    if('city' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['city'],str)):
            place_holder_variable = True
        else:
            incident['city'] = ''
            errors.append('city not str')
    else:
        #errors.append('city not present')
        incident['city'] = ''    
    return (True, incident,errors)
def clean(json_clean):  
    errors = []
    if('endTimeMillis' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['endTimeMillis'],int)):
            place_holder_variable = True
        else:
            json_clean['endTimeMillis'] = ''
            errors.append('endTimeMillis not int')
    else:
        #errors.append('endTimeMillis not present')
        json_clean['endTimeMillis'] = ''
    if('endTime' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['endTime'],str)):
            place_holder_variable = True
        else:
            json_clean['endTime'] = ''
            errors.append('endTime not str')
    else:
        #errors.append('endTime not present')
        json_clean['endTime'] = ''
    if('startTime' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['startTime'],str)):
            place_holder_variable = True
        else:
            json_clean['startTime'] = ''
            errors.append('startTime not str')
    else:
        #errors.append('startTime not present')
        json_clean['startTime'] = ''
    if('startTimeMillis' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['startTimeMillis'],int)):
            place_holder_variable = True
        else:
            json_clean['startTimeMillis'] = ''
            errors.append('startTimeMillis not int')
    else:
        #errors.append('startTimeMillis not present')
        json_clean['startTimeMillis'] = ''    
    if('data' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['data'],list)):
            for i in range(0,len(json_clean['data'])):
                clean_incident_result = clean_incident(json_clean['data'][i])
                if clean_incident_result[0] == True:   
                    json_clean['data'][i] = clean_incident_result[1]
                    if len(clean_incident_result[2])>0:
                        errors.append("for data "+str(json_clean['data'][i]['id'])+" "+str(clean_incident_result[2]))  
                else:
                    json_clean['alerts'][i] = False
                    errors.append("for data number "+str(i)+" id not correct or present")                    
                
        elif (isinstance(json_clean['data'],dict)):
            json_clean['data'] = [json_clean['data']]
            for i in range(0,len(json_clean['data'])):
                for key in json_clean['data'][i].keys():
                    if key not in ['id', 'street', 'trend', 'driversCount', 'delaySeconds', 'speed', 'line', 'detectionDate', 'type', 'jamLevel', 'highway', 'endNode', 'updateDate', 'alertsCount', 'city', 'seconds', 'nImages', 'detectionDateMillis', 'nComments', 'length', 'regularSpeed', 'severity', 'nThumbsUp', 'alerts', 'country', 'updateDateMillis']:
                        errors.append('new key '+str(key))
            for i in range(0,len(json_clean['data'])):
                clean_incident_result = clean_incident(json_clean['data'][i])
                if clean_incident_result[0] == True:   
                    json_clean['data'][i] = clean_incident_result[1]
                    if len(clean_incident_result[2])>0:
                        errors.append("for data "+str(json_clean['data'][i]['id'])+" "+str(clean_incident_result[2]))  
                else:
                    json_clean['alerts'][i] = False
                    errors.append("for data number "+str(i)+" id not correct or present")                   
        else:
            json_clean['data'] = ''
            errors.append('data not list or dict')
    else:
        errors.append('data not present')
        json_clean['data'] = []       
    return(True, 'holdover',json_clean,errors)
