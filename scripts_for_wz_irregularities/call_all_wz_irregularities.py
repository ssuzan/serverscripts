#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports
import apscheduler
from apscheduler.schedulers.blocking import BlockingScheduler
import sendemail
email_file = workspace_environ+'/serverscripts_production/globals/emails'
import requests
import smtplib
import clean_wz_irregularities
import fetch_wz_irregularities
import store_wz_irregularities
import hashlib
from automation_building_scripts import automate_build_clean
from automation_building_scripts import automate_build_new_elements
from email.mime.text import MIMEText
from os import listdir


type_name = 'waze_traffic_irregularities_lv'
wz_url = "https://na-georss.waze.com/rtserver/web/TGeoRSS?tk=ccp_partner&ccp_partner_name=WayCare&format=JSON&types=irregularities&polygon=-115.344000,36.492000;-115.580000,36.414000;-115.490000,35.918000;-115.390000,35.605000;-114.994000,35.636000;-114.505000,35.908000;-113.917000,36.929000;-114.680000,36.724000;-115.344000,36.492000;-115.344000,36.492000"
save_path = workspace_environ+'/DB_backups/data_from_wz_irregularities'
dump_output_to = "NONE"

#number of iterations before element is removed from ongoing duplicate check list
default_age = 5
#ongoing duplicate check list
hash_log = {}


def call_all_wz_irregularities():
    curr_t = universal_imports.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
    print("call_all_wz_irregularities() running at time: "+curr_t)
    #1st fetch 2nd clean 3rd store
    now = str(universal_imports.datetime.utcnow())
    now = now.split(" ")
    now[0] = now[0].split("-")
    now[1] = now[1].split(":")    
    filename = type_name+"_"+now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1]+"_"+now[1][2].replace(".","_")        
    y = fetch_wz_irregularities.downloadWaze(wz_url,save_path,filename)
    #y = fetch_wz.downloadWaze("https://oogle1233///3445425463y/t2wg4tw45q4gtwhew6t4.com")   
    text = open(email_file,'r')
    string = text.read()
    email_json = universal_imports.json.loads(string) 
    dead_hashes = []
    #automate_build_new_elements.automate_new_elements(y['jams'][0]['line'][0],"json_clean['jams'][i]['line'][n]")    
    #automate_build_new_elements.automate_new_elements(y['jams'][0],"json_clean['jams'][i]")
    #print(y['data'])
    #automate_build_clean.automate_clean(y['data']['line'][0],"json_clean['data']['line'][n]")
    if 'fetch_error' in y.keys():
        sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'fetch_wz_jam() error', message = "fetch_wz_irregularities() error "+str(y['error']), login = email_json["email"], password = email_json["password"])                        
    else:
        clean_results = clean_wz_irregularities.clean(y)
        if(clean_results[0] == True):
            #hashing starts here
            y = clean_results[2]
            if ('data not present' not in clean_results[3] and 'data not list' not in clean_results[3]):   
                if len(clean_results[3])>0:
                    sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'clean_wz_irregularities() warning', message = "clean_wz_jam() warning "+str(clean_results[3]), login = email_json["email"], password = email_json["password"])                                
                results = store_wz_irregularities.store(y, save_path,hash_log,dump_output_to,default_age,filename,clean_results[3]) 
                hashes = results[0]
                repeat_hashes = results[1]
                for key in hashes:
                    hash_log[key] = hashes[key]
                for key in hash_log.keys():
                    if key not in repeat_hashes and key not in hashes.keys():
                        hash_log[key][1]-=1
                        #print(key+" "+str(hash_log[key][1]))
                        if hash_log[key][1] == 0:
                            dead_hashes.append(key)
                    else:
                        hash_log[key][1] = default_age
                for i in range(0,len(dead_hashes)):
                    del hash_log[dead_hashes[i]]    
            else:
                for key in hash_log.keys():
                    hash_log[key][1]-=1
                    #print(key+" "+str(hash_log[key][1]))
                    if hash_log[key][1] == 0:
                        dead_hashes.append(key)
    
                for i in range(0,len(dead_hashes)):
                    del hash_log[dead_hashes[i]]                  
        else:
            sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'clean_wz_irregularities() error', message = "error in clean_wz_jam() "+filename, login = email_json["email"], password = email_json["password"])    
    print('call_all_wz_irregularities.py completed successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
        

def scheduled_call():
    print('call_all_wz_irregularities.scheduled_call() started successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
    sched = BlockingScheduler(timezone='UTC', coalesce=True)
    sched.add_job(call_all_wz_irregularities, 'interval', id='job_id1', seconds=10, coalesce=True)
    sched.start()


#call_all_wz_irregularities()
scheduled_call()
#print(hash_log)
