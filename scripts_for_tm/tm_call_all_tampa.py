#Shai Suzan
#shai@waycaretech.com
#main file
#required installations:
import sys,os
import os.path
sys.path.append(os.environ['WORKSPACE']+'/serverscripts/globals')
import universal_imports
from apscheduler.schedulers.blocking import BlockingScheduler
#import sendemail
from email.mime.text import MIMEText
#import xmltodict
import tm_fetch
import tm_store
import tm_clean

dire = os.path.dirname(__file__)
tm_file = os.environ['WORKSPACE']+'/serverscripts/globals/ticket_master_access.txt'
tm_api_text = open(tm_file,'r')
tm_api_string = tm_api_text.read().rstrip()
tm_url = 'https://app.ticketmaster.com/discovery/v2/events.json?dmaId=396'
venues_file = os.path.join(dire + '/venues_short_tampa.csv')
save_path = os.environ['WORKSPACE']+'/DB_backups/data_from_tampa_tm'

#original_backup_path = os.environ['WORKSPACE']+'/DB_backups/data_from_ndot/database_original/'
#email_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/emails'
type_name = 'tm_events_live_tampa'
#text = open(email_file,'r')
#email_string = text.read()
#email_json = universal_imports.json.loads(email_string)  
#is_development_file =  os.environ['WORKSPACE']+'/.is_development'
#is_productoin_file =   os.environ['WORKSPACE']+'/.is_production'
is_prod_flag= False 
is_dev_flag=  False
prod_fifo_ip = '10.0.3.58'
 
store_original = True 
dump_to_output = "FILE"
#number of iterations before element is removed from ongoing duplicate check list - set to 10 minutes
#default_age = 11 
#minutes between iterations (.5 for half minute)
#minutes_between_script_calls = 1


#Set to True if encryption is required before sending to FIFO
#encrypt_output= True

#n is for iterating through test files
#n = [0]

def tm_call_all():
    now = str(universal_imports.datetime.utcnow())
    now = now.split(" ")
    now[0] = now[0].split("-")
    now[1] = now[1].split(":")
    filename = type_name + "_" + now[0][2] + "_"+now[0][1] + "_"+now[0][0] + "_"+now[1][0] + "_"+now[1][1] + "_"+now[1][2].replace(".","_")
    curr_t = universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")  
    print('tm_call_all_tampa.py started! time is: '+curr_t+' UTC')

    #calls fetch
    doc = tm_fetch.tm_fetch(filename, store_original,tm_url, tm_api_string, venues_file, save_path)
    #print (doc)
    if(doc[0] == True):
 
        doc_json = doc[1]
        clean_results = tm_clean.tm_clean(doc_json, filename)
        #print (clean_results)
        if clean_results[0] == True:
            saving_result = tm_store.tm_store_clean(curr_t, clean_results[1], dump_to_output, filename, save_path, type_name, prod_fifo_ip, is_prod_flag, is_dev_flag)
            if saving_result:                
                print('tm_call_all.py completed successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')


#def scheduled_call():
#    print('tm_call_all.scheduled_call() started successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
#    #initialize new scheduler
#    sched = BlockingScheduler(timezone='UTC', coalesce=True)
#    sched.add_job(tm_call_all, 'interval', id='job_id1', seconds=600,coalesce=True)
#    sched.start()
   
if __name__ == "__main__":
        tm_call_all()
#        scheduled_call()

