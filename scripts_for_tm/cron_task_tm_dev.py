#Shai Suzan
#shai@waycaretech.com
#main file
#required installations:
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports
import apscheduler
from apscheduler.schedulers.blocking import BlockingScheduler
import tm_dev_call_all

def scheduled_call():
    print('tm_dev_scheduled_call started successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
    #initialize new scheduler
    sched = BlockingScheduler(timezone='UTC',coalesce=True)
    sched.add_job(tm_dev_call_all.tm_call_all, 'cron', id='job_id1', hour='0',coalesce=True)
    sched.start()

scheduled_call()
