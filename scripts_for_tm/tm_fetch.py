#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: tm database https request (url) 
#output: json of data given by https request or json containing error message

import universal_imports
import sendemail
from email.mime.text import MIMEText
import sys, os
import json
import csv

workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
email_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/emails'
import universal_imports
import requests
import tm_store
dire = os.path.dirname(__file__)



#function to fetch all pages of events
def tm_fetch(filename, store_original, call_url, api_key, venues_file, save_path):
    raw_json = []
    total_pages = 0
    size=198 #how many items on the page: max=199
    warn_report = []   
    
    #gets json from url
    def fetch_page(page, page_size, call_url, api_key):
        try:
            url = call_url + '&apikey=' + api_key + '&page' + str(page) + '&size='  + str(page_size)
            res = requests.get(url)
            res = res.json()
        except :
            res = {"error" : "request exception occured"}
        return res
    
    start_page = 0
    trial_pages = 10
    # try to fetch first page to get the size of query
    # if you get error trying to fetch trial number of pages - probably a bad day to fetch, try later 
    for i in range(trial_pages):
        r = fetch_page(start_page, size, call_url, api_key)
        try: 
            #if not possible to fetch first page, try next page, create warning
            r["error"]
            start_page += 1
            text = open(email_file,'r')
            email_string = text.read()
            email_json = universal_imports.json.loads(email_string)
            prefix = "tm fetch warnings: " + str(filename) + "_"
            w = 'tm_fetch failed to fetch page num=' + str(start_page) + ' containing '+str(size)+'events'
            message_text = prefix + w #... This is a report in form of string
            sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'tm warning report', message = message_text, login = email_json["email"], password = email_json["password"])
    
            if start_page == trial_pages - 1:
                return [False, []]
        except:       
            raw_json += r['_embedded']['events']
            total_pages = r["page"]["totalPages"]
            start_page += 1
            #print ('Total pages=', total_pages)
            break


    # fetch the rest of the pages
    if total_pages > 1:
        for i in range(start_page,3): #total_pages):
            r = fetch_page(i, size, call_url, api_key)
            try:
                #if wasn't possible to fetch page, pass and try new page. Don't append error object to result
                r["error"]
                warn_report.append('tm_fetch failed to fetch page No' + str(i)+', containing '+str(size)+'events')
            except:
                raw_json += r['_embedded']['events']
    
    # store raw_json object
    if (store_original):
        tm_store.tm_store_original(raw_json, filename, save_path)            
    
      
    # print('SAMPLE OF RAW ITEM', raw_json[0])
    # print('FETCH WARNINGS', warn_report)
   #   
   # input_json = os.path.join(dire + '/venues_short.json')
   # 
   # with open(input_json) as json_data:
   #     venues = json.load(json_data)
   #     
   #     for item in raw_json:
   #         try:
   #             v = list(filter(lambda x: x['id'] == item['_embedded']['venues'][0]['id'], venues))[0]
   #             item['_embedded']['venues'][0]['capacity'] = v['capacity']
   #         except:
   #             item['_embedded'].update({'venues':[{'capacity':0}]})
         
        #If necessary, complete list with events and capacity added to venue can be dumped from this spot.
        #Capacity is assigned according to pre-fetched file with venues and their match to manually collected data on sitting capacity.
               
  #Procedure to match venues and their capacity with venues csv file
    input_csv = venues_file
    with open(input_csv) as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=',')
        venues = []
        for line in csv_reader:
            if line[5] != 'capacity':
                #print(line)
                venues.append({'id': line[0],'capacity': int(line[5]), 'to_output': int(line[9])})
        
        for item in raw_json:
            try:
                v = list(filter(lambda x: x['id'] == item['_embedded']['venues'][0]['id'], venues))[0]
                item['_embedded']['venues'][0]['capacity'] = v['capacity']
                item['_embedded']['venues'][0]['to_output'] = v['to_output']
            except:
                item['_embedded'].update({'venues':[{'capacity':0, 'to_output':0}]})
                
        # Filter out items with capacity == 0,  so that only items of interest will be passed to clean and store_clean
        raw_json_filtered = list(filter(lambda x: x['_embedded']['venues'][0]['capacity'] > 0, raw_json))
        #### FILTER BY TO_OUTPUT
        # Filter out items with to_output == 0,  so that only items of interest will be passed to clean and store_clean
        raw_json_filtered = list(filter(lambda x: x['_embedded']['venues'][0]['to_output'] > 0, raw_json_filtered))
    for w in warn_report:
        text = open(email_file,'r')
        email_string = text.read()
        email_json = universal_imports.json.loads(email_string)
        prefix = "tm fetch warnings: " + str(filename) + "_"
        message_text = prefix + w #... This is a report in form of string
        sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'tm warning report', message = message_text, login = email_json["email"], password = email_json["password"]) 
    
      
    #print('SAMPLE OF FILTERED ITEM', raw_json_filtered[0])
    
    return [len(raw_json) > 0, raw_json_filtered]


