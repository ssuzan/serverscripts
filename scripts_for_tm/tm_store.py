#main file
#required installations:
import sys,os
import os.path
sys.path.append(os.environ['WORKSPACE']+'/serverscripts_production/globals')
import universal_imports
import sendemail
import push_to_fifo
import push_to_fifo_dev

from collections import OrderedDict
from email.mime.text import MIMEText

import json
#dire = os.path.dirname(__file__)

email_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/emails'

text = open(email_file,'r')
string = text.read()
email_json = universal_imports.json.loads(string) 


'''Store original raw_json file'''
def tm_store_original(raw_json, filename, save_path):  
    #saves orignal file
    with open(save_path+"/database_original/"+filename+'.json', 'w') as outfile:
        json.dump(raw_json, outfile)
    print('stored to: '+save_path+"/database_original/"+filename+'.json')
    #with open(os.path.join(dire + filename + '.json'), 'w') as outfile:
    #    json.dump(raw_json, outfile)

'''Store cleaned tm_json file'''
def tm_store_clean(curr_t, tm_json, dump_to_output, filename, save_path, type_name, prod_fifo_ip, is_prod, is_dev):  
    err_report = []

    for i in range(0,len(tm_json)):
        curr_filename = filename+'_event_'+tm_json[i]['id'] 

        #saves file (array of JSON objects each containing one clean event)    
        new_store_json = {'type':type_name,'date_time':str(curr_t).replace("-","_")+' UTC','suspicious':False,'data':tm_json[i],'error':False}
                   
        data_out = universal_imports.json.dumps(new_store_json, sort_keys=True, indent=4, separators=(',', ': '))

        if ((dump_to_output == "FILE") or (dump_to_output == "ALL")): 
            try:
                with open(save_path + "/database/" + curr_filename + '.json', 'w') as outfile:
                    json.dump(new_store_json, outfile)  
                #with open(os.path.join(dire + curr_filename + '.json'), 'w') as outfile:
                #    json.dump(tm_json, outfile)
            except:
                err_report.append('failed to store clean file' + curr_filename + '.json')
        
        #sends file to FIFO
        if ((dump_to_output == "FIFO") or (dump_to_output == "ALL")): 
            try:
                if (is_prod):
                    if prod_fifo_ip == '':
                        push_to_fifo.push_to_fifo(data_out)
                    else:
                        push_to_fifo.push_to_fifo(data_out, prod_fifo_ip)
            except:
                text = open(email_file,'r')
                email_string = text.read()
                email_json = universal_imports.json.loads(email_string)
                sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'error in tm_store', message = 'fifo error in tm_store', login = email_json['email'], password = email_json['password'])         
                err_report.append('fifo error in tm_store')
            
            #push to development FIFO
            try:
                if (is_dev):
                    push_to_fifo_dev.push_to_fifo_dev(data_out)
            except:
                text = open(email_file,'r')
                email_string = text.read()
                email_json = universal_imports.json.loads(email_string)
                sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'error in tm_store', message = 'fifo error in tm_store', login = email_json['email'], password = email_json['password'])         
                err_report.append('fifo error in tm_store')
                
    return len(err_report) == 0




