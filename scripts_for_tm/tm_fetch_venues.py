#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: tm database https request (url) 
#output: json of data given by https request or json containing error message
import sys
import os
#workspace_environ = str(os.environ['WORKSPACE'])
#sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports
import requests
import json
import csv
dire = os.path.dirname(__file__)


# Function to get all venues and add capacity value to known venues
# Dumps output files in json and csv
# Colnames = ['id', 'name', 'address', 'lon', 'lat', 'capacity', 'event_type','code', 'note']

def downloadTM_Venues():  
    raw_json = []
    total_pages = 0
    keys_of_interest = ['address', 'dmas', 'id', 'location', 'name', 'postalCode']
    
    #gets json from url
    def fetch_page(page):
        try:
            res = requests.get('https://app.ticketmaster.com/discovery/v2/venues.json?apikey=I2jBiTkT97tcDXFqPR9PRJ0SEkVAU5zz&stateCode=NV&page='+str(page)+'&size=199')
            res = res.json()
        except requests.exceptions.ConnectionError:
            res = universal_imports.json.loads('{"error" : "requests.exceptions.ConnectionError"}')
        return res
        
    r = fetch_page(0)
    try: 
        #if not possible to fetch first page, return
        r["error"]
        return r
    except:       
        raw_json += list(filter(lambda x: x['city']['name'] == "Las Vegas", r["_embedded"]["venues"]))
        total_pages = r["page"]["totalPages"]      
    
    if total_pages > 1:
        for i in range(1,total_pages):
            r = fetch_page(i)
            try:
                #if wasn't possible to fetch page, pass and try new page. Don't append error object to result
                r["error"]
                continue
            except:
                raw_json += list(filter(lambda x: x['city']['name'] == "Las Vegas", r["_embedded"]["venues"]))
    
    for j in raw_json:
        test_keys = list(j.keys())
        for k in test_keys:
            if not k in keys_of_interest:
                del(j[k])    
        test_keys = list(j.keys())
        if len(test_keys) < len(keys_of_interest):
            for t in keys_of_interest: 
                if t not in test_keys: 
                    if t != 'location': j.update({t: ''})    
                    else: j.update({t: {'longitude': '', 'latitude': ''}})      
    print(raw_json[:10])
    
    ''' Querying for known labels to match the sitting capacity.
        THIS IS THE PLACE TO ASSIGN CAPACITY TO KNOWN ITEMS. 
        In dev version query matches to several items in TM database according to the name of the venue.
    '''
    query_list = [{'MGM Grand Garden': 16800}, {'The Joint': 4000}, {'Desert Breeze': 0}, {'Downtown': 15000}, {'Luxor Festival Grounds': 85000}, {'Motor Speedway': 116000}, {'Paris Theater':1453}, {'Caesars Palace': 4298}, {'Mandalay Bay':12000}, {'Palms Casino Resort':2500}, {'Bunkhouse':800}, {'Las Vegas Convention Center':2500}, {'The Cosmopolitan of Las Vegas': 3000}, {'Paris Las Vegas':1200}, {'T-Mobile':20000}, {'Palazzo Theatre':1850}, {'Wynn':1630}, {'Rio Hotel':2500}, {'Planet Hollywood':7500}, {'Mirage Hotel':8650}, {'Orleans Hotel':9500}, {'Treasure Island':1200}, {'Flamingo Las Vegas':2400}, {'Hard Rock Hotel - Las Vegas':4000}, {'Luxor Hotel':2000}, {"Harrah's":1840}, {'Golden Nugget':2500}, {'The Linq':1670}, {'Sam Boyd Stadium':40000}, {'Fremont':15000}, {"Sunset Station":4750}, {'Starbright Theatre':144}, {'Brooklyn Bowl':3000}, {'Venetian Las Vegas':8500}, {'Sands Expo':2000}, {"The Cashman's Theater":1992}, {'Cashman Field':12500}]    
    
    for query in query_list:
        for venue in raw_json:
            q = list(query.keys())[0]
            val = list(query.values())[0]
            if q in venue['name']:
                venue['capacity'] = val
            
            # if capacity is already there, do nothing
            try:
                venue['capacity']
            
            # if venue is not on list and capacity is missing, assign 0
            # this is later used by tm_fetch to filter out items with unknown capacity
            except:
                venue['capacity'] = 0
    
    with open(os.path.join(dire + 'venues_short.json'), 'w') as outfile:
        json.dump(raw_json, outfile)
        
    with open(os.path.join(dire + 'venues_short.csv'), 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(['id', 'name', 'address', 'lon', 'lat', 'capacity', 'event_type','code', 'note']) 
        for venue in  raw_json:   
            address = ''
            if isinstance(venue['address'], dict):
                for v in venue['address'].values(): address += v
            elif isinstance(venue['address'], str): address += venue['address']
            else: print('address error ', venue['id'])
            writer.writerow([venue['id'], venue['name'], address, venue['location']['longitude'], venue['location']['latitude'], venue['capacity'], 'N/A', 'N/A', 'N/A'])
    
    #return raw_json

test = downloadTM_Venues()