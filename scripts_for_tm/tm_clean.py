#main file
#required installations:

import universal_imports
import sendemail
from email.mime.text import MIMEText
import sys, os
import json

sys.path.append(os.environ['WORKSPACE']+'/serverscripts_produciton/globals')
email_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/emails'

# The incoming JSONs to check categories & keys against existing set (look for new keys)
#dire = os.path.dirname(__file__)
#input_file = os.path.join(dire + 'tm.json')

#me - string
# Return type -> [bool, [{},{},...{}]
def tm_clean(raw_events, filename):
    
    ### CONSTANTS
    keys_of_interest = ['dates', 'images', 'id', 'locale', 'info', 'name', '_embedded']
    keys_of_interest_attractions = ['id', 'locale', 'name', 'upcomingEvents', 'url']
    keys_of_interest_venues = ['address', 'boxOfficeInfo', 'id', 'images', 'locale', 'location', 'name', 'postalCode', 'social', 'upcomingEvents', 'url', 'capacity']
    keys_of_interest_dates = ['dateTBA', 'dateTBD', 'dateTime', 'localDate', 'localTime', 'noSpecificTime', 'timeTBA']
    clean_jsons = []
    err_report = []
    warn_report = []
    
    #dire = os.path.dirname(__file__)
    #input_json = os.path.join(dire + 'venues_short.json')
    #
    #with open(input_json) as json_data:
    #    venues = json.load(json_data)
    #    
    #    for item in raw_json:
    #        try:
    #            v = list(filter(lambda x: x['id'] == item['venues'][0]['id'], venues))[0]
    #            item['venues'][0]['capacity'] = v['capacity']
    #            print ('!!!!!!!!!!!!!!!',item)
    #        except:
    #            continue
    #        
    #
    #print('SAMPLE OF CLEAN ITEM', raw_json[0])
    # Check new incoming samples against existing list of keys
    for event in raw_events: 
        
        # The initial structure of clean_json is created according to categories_stable_keys structure
        clean_json = {}
        try:
            # Check all keys of event
            for k,v in event.items():
                
                # If it is important key - add it to clean_json and clean it. If not - do nothing
                if k in keys_of_interest:
                    
                    if k != '_embedded':
                        clean_json.update({k:v})
                    
                    # Embedded contains attractions and venues which need more cleaning      
                    else:
                        # delete unnecessary keys from event.attractions, check for missing keys and add {key: ""} in case important key is missing; send warning report
                        try:
                            clean_json.update({'attractions': event['_embedded']['attractions']})                        
                            for n in range(len(event['_embedded']['attractions'])):
                                test_keys = list(clean_json['attractions'][n].keys())
                                for key in test_keys:
                                    if not key in keys_of_interest_attractions:
                                        del(clean_json['attractions'][n][key])
                                if len(test_keys) < len(keys_of_interest_attractions):
                                    for t in keys_of_interest_attractions: 
                                        if t not in test_keys:
                                            clean_json['attractions'][n].update({t: ''})
                                            #warn_report.append('missing key event.attractions.' + str(t) + ' in event id=' + str(event['id']))
                        except:
                            clean_json.update({'attractions': []})
                            #warn_report.append('missing key event.attractions in event id=' + str(event['id']))
                        
                        # delete unnecessary keys from event.venues, check for missing keys and add {key: ""} in case important key is missing; send warning report
                        try:
                            clean_json.update({'venues': event['_embedded']['venues']})
                            for n in range(len(event['_embedded']['venues'])):
                                test_keys = list(clean_json['venues'][n].keys())
                                for key in test_keys:
                                    if not key in keys_of_interest_venues:
                                        del(clean_json['venues'][n][key])
                                if len(test_keys) < len(keys_of_interest_venues):
                                    for t in keys_of_interest_venues: 
                                        if t not in test_keys:
                                            clean_json['venues'][n].update({t: ''})
                                            #warn_report.append('missing key event.venues.' + str(t) + ' in event id=' + str(event['id']))
                        except:
                            clean_json.update({'venues': []})
                            #warn_report.append('missing key event.venues in event id=' + str(event['id']))
                    
            # check for missing keys on top level and add {key: ""} in case important key is missing; send warning report       
            test_keys = list(clean_json.keys())
            if len(test_keys) < len(keys_of_interest):
                for t in keys_of_interest: 
                    if (t not in test_keys) and (t != '_embedded'):
                        clean_json.update({t: ''})
                        #warn_report.append('missing key event.venues.' + str(t) + ' in event id=' + str(event['id']))
            
            # check inner keys of event.dates
            try:                   
                test_keys = list(clean_json['dates']['start'].keys())
                for key in test_keys:
                    if not key in keys_of_interest_dates:
                        del(clean_json['dates']['start'][key])
                if len(test_keys) < len(keys_of_interest_dates):
                    for t in keys_of_interest_dates: 
                        if t not in test_keys:
                            clean_json['dates']['start'].update({t: ''})
                            #warn_report.append('missing key event.dates.' + str(t) + ' in event id=' + str(event['id']))
            except:
                place_holder=1
                #warn_report.append('missing key event.dates in event id=' + str(event['id']))           
                                                        
            # Create a list of clean json objects to be returned
            clean_jsons.append(clean_json)
        except:
            warn_report.append('failed to process raw event in ' + str(filename))
    
    # Sort events - those with largest capacity first
    clean_jsons = sorted(clean_jsons, key=lambda x: x['venues'][0]['capacity'], reverse = True)
    
    #print ('SAMPLE OF CLEAN JSONS', len(clean_jsons), clean_jsons[0] )   
    #print ('ERR REPORT', err_report)
    #print ('CLEAN WARNINGS', warn_report)
    
    # Send error and warning report contents - each item individually (if needed - it's possible to send one combined report)
    for e in err_report:
        text = open(email_file,'r')
        email_string = text.read()
        email_json = universal_imports.json.loads(email_string)
        prefix = "tm clean errors: "
        message_text = prefix + e #... This is a report in form of string
        sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'tm error report', message = message_text, login = email_json["email"], password = email_json["password"]) 
    for w in warn_report:
        text = open(email_file,'r')
        email_string = text.read()
        email_json = universal_imports.json.loads(email_string)
        prefix = "tm clean warnings: "
        message_text = prefix + w #... This is a report in form of string
        sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'tm warning report', message = message_text, login = email_json["email"], password = email_json["password"]) 

    return [len(err_report) == 0, clean_jsons]


#TEST SOME NEW TEST FILE

#with open(input_file) as json_data:
#    d = json.load(json_data)
#
#    clean_list = tm_clean(d, 'test.json')
    #print ('NO ERROR REPORT?', clean_list[0])
    #print ('SAMPLE OF EVENT ITEM -> tm_clean return [1][0]')
    #print (clean_list[1][0])
    #print ('Total items in file', len(clean_list[1]))
