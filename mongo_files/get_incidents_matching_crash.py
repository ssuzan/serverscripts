import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts/globals')
import universal_imports
import sendemail
from email.mime.text import MIMEText
from dateutil.parser import parse
from datetime import datetime
import googlemaps
from googlemaps import roads
from math import sin, cos, sqrt, atan2, radians
import mongo_fetch
import mongo_universal

#occational occurance of matches, often recurrent may indicate some unreliablity in googlemaps api
#changing radius effects street results: I-515 occur more ofter with larger radius (about 1/2 times w/ radius of 1000m), occurs very rarely if ever w/ radius of 100m
#I-515 I-515
#I-515 I-515

#US-93 US-93
#US-93 US-93

#US-95 US-95
#US-95 US-95

#Veterans Memorial Hwy Veterans Memorial Hwy
#Veterans Memorial Hwy Veterans Memorial Hwy

#acceptable distance is in m
acceptable_distance = 1500
#acceptable_time_lapse is in seconds
acceptable_time_lapse = 7200
gmaps = googlemaps.Client(key='AIzaSyC7U9h2KLqESB5j0tu50sf-9zr8BNMV448') 

def give_location(client,location, distance_m):
    a1 = roads.nearest_roads(client, location)
    if len(a1)>0:
        a1 = a1[0]['location']
        s1 = roads.snap_to_roads(client, location, interpolate=False)#[0]['location']
        #print('\n\n\n')
        s1 = s1[0]['location']
        street_location = (s1['latitude'],s1['longitude'])  
        reverse_geocode_result_a1 = client.reverse_geocode((a1['latitude'],a1['longitude']))
        #print(reverse_geocode_result_a1)
        reverse_geocode_result_a1 = reverse_geocode_result_a1[0]['address_components'][0]['short_name']
        return reverse_geocode_result_a1

    else:
        return "road cannot be found, coordinates entered are likely not near a marked road"   

        
        

    

    
def find_road_matches(filename_1,filename_2):
    matches = {}
    file_1_list = mongo_fetch.mongo_fetch(filename_1)
    file_2_list = mongo_fetch.mongo_fetch(filename_2)
    for i in range(0,len(file_1_list)):
        for n in range(0,len(file_2_list)):
            if (mongo_universal.close_enough_distance(file_1_list[i]['coordinates'],file_2_list[n]['coordinates'],acceptable_distance) == True):
                if (mongo_universal.close_enough_time(file_1_list[i]['creation_time'],file_2_list[n]['creation_time'],acceptable_time_lapse) == True):
                    coordinate_touple_other = (float(file_2_list[n]['coordinates'][1]),float(file_2_list[n]['coordinates'][0]))
                    coordinate_touple_crash = (float(file_1_list[i]['coordinates'][1]),float(file_1_list[i]['coordinates'][0]))  
                    high_name_other = give_location(gmaps,coordinate_touple_other, 300)
                    high_name_crash = give_location(gmaps,coordinate_touple_crash, 300)
                    if high_name_crash == high_name_other and high_name_other != "road cannot be found, coordinates entered are likely not near a marked road":
                        file_2_list[n]['confirmed_street'] = high_name_other
                        file_1_list[i]['confirmed_street'] = high_name_crash
                        print(high_name_other,high_name_crash) 
                        if file_1_list[i]['_id'] in matches.keys():
                            matches[file_1_list[i]['_id']][1].append(file_2_list[n])
                        else:
                            matches[file_1_list[i]['_id']]  = (file_1_list[i],[file_2_list[n]])   
    now = str(universal_imports.datetime.utcnow())
    now = now.split(" ")
    now[0] = now[0].split("-")
    now[1] = now[1].split(":")
    curr_t = universal_imports.datetime.utcnow().strftime("%d-%m-%Y %H:%M:%S")
    filename = "mongo"+"_"+now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1]              
    incidentFiletxt = open("examples/"+filename+'.json', 'w')
    incidentFiletxt.write(str(matches))  
    incidentFiletxt.close()
    
find_road_matches('high_crash_2.json','high_other_2.json')





