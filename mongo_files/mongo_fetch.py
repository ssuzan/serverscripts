import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts/globals')
import universal_imports
from dateutil.parser import parse
from datetime import datetime


#read in road type and road number
def calculate_time(time):
    time = time.split('T')
    time[0] = time[0].split('-')
    for i in range(0,len(time[0])):
        time[0][i] = int(time[0][i])
    time[1] = time[1].split(':')
    time[1][0] = int(time[1][0])
    time[1][1] = int(time[1][1])
    time = datetime(time[0][0], time[0][1], time[0][2], time[1][0], time[1][1],0)  
    return time

def load_json_multiple(segments):
    chunk = ""
    for segment in segments:
        chunk += segment
        try:
            yield universal_imports.json.loads(chunk)
            chunk = ""
        except ValueError:
            pass
        
def mongo_fetch(file_name):
    data_list_raw = [] 
    data_list = []
    with open(file_name) as f:
        for parsed_json in load_json_multiple(f):
            data_list_raw.append(parsed_json)
    #print(file_name+" "+str(len(data_list_raw)))
    for i in range(0,len(data_list_raw)):
        if 'location' in data_list_raw[i].keys() and '_id' in data_list_raw[i].keys() and 'creation_time' in data_list_raw[i].keys():
            data_list.append({"confirmed_street":"","type":data_list_raw[i]['type'],"_id":data_list_raw[i]['_id']['$oid'],"coordinates":data_list_raw[i]['location']['coordinates'],"creation_time":calculate_time(data_list_raw[i]['creation_time']['$date']),'end_time':calculate_time(data_list_raw[i]['last_update']['$date'])})
    #print(data_list)
    return data_list

def mongo_fetch_get_first_crash_indication_1(file_name):
    data_list_raw = [] 
    data_list = []
    with open(file_name) as f:
        for parsed_json in load_json_multiple(f):
            data_list_raw.append(parsed_json)
    #print(file_name+" "+str(len(data_list_raw)))
    #print(data_list_raw[0])
    for i in range(0,len(data_list_raw)):
        if 'location' in data_list_raw[i].keys() and '_id' in data_list_raw[i].keys() and 'creation_time' in data_list_raw[i].keys():
            data_list.append({"confirmed_street":"","type":data_list_raw[i]['type'],"_id":data_list_raw[i]['_id']['$oid'],"coordinates":data_list_raw[i]['location']['coordinates'],"start_time":calculate_time(data_list_raw[i]['creation_time']['$date']),'end_time':calculate_time(data_list_raw[i]['last_update']['$date'])})
    #print(data_list)
    return data_list


def mongo_fetch_spillman(file_name):
    data_list_raw = [] 
    data_list = []
    with open(file_name) as f:
        for parsed_json in load_json_multiple(f):
            data_list_raw.append(parsed_json)
    #print(file_name+" "+str(len(data_list_raw)))
    #print(data_list_raw[0])
    for i in range(0,len(data_list_raw)):
        if 'location' in data_list_raw[i].keys() and '_id' in data_list_raw[i].keys() and 'creation_time' in data_list_raw[i].keys():
            data_list.append({"road_type":data_list_raw[i]['section']['road_type'],"road_number":data_list_raw[i]['section']['road_number'],"spillman_id":data_list_raw[i]['based_on']['id_in_source'],"confirmed_street":"","type":data_list_raw[i]['type'],"_id":data_list_raw[i]['_id']['$oid'],"coordinates":data_list_raw[i]['location']['coordinates'],"start_time":calculate_time(data_list_raw[i]['creation_time']['$date']),'end_time':calculate_time(data_list_raw[i]['last_update']['$date'])})
    #print(data_list)
    return data_list


def mongo_fetch_gr(file_name):
    data_list_raw = [] 
    data_list = []
    with open(file_name) as f:
        for parsed_json in load_json_multiple(f):
            data_list_raw.append(parsed_json)
    #print(data_list_raw)
    #print(file_name+" "+str(len(data_list_raw)))
    #print(data_list_raw[0])
    for i in range(0,len(data_list_raw)):
        if 'data' in data_list_raw[i].keys():
            data_list.append({"event_type":data_list_raw[i]['data'][0]['EventType'],"confirmed_street":"","type":data_list_raw[i]['type'],"_id":data_list_raw[i]['_id']['$oid'],"coordinates":[data_list_raw[i]['data'][0]['Location']['Longitude'],data_list_raw[i]['data'][0]['Location']['Latitude']],"creation_time":calculate_time(data_list_raw[i]['data'][0]['StartTime']),'end_time':calculate_time(data_list_raw[i]['data'][0]['EndTime'])})
    #print(data_list[0])
    return data_list
