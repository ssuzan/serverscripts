import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts/globals')
import universal_imports
import sendemail
from email.mime.text import MIMEText
from dateutil.parser import parse
import datetime
from datetime import timedelta
import googlemaps
from googlemaps import roads
from math import sin, cos, sqrt, atan2, radians
import mongo_fetch
import mongo_universal

#create walkthrough (check)
#manuel input of filenames (check)
#time should be from start to finish for first list items, allow margin of time before starttime (default 0) (check)
#output 1. number of each spillman road type 2. gr event type count 3. number of spillman incidents with matches 4. cataloge matches
#step 1        
#generate files from mongo (spillman/waze)
#mongoexport --db waycare_db_backup --collection gr_alerts_live_lv_received --query ' {"data.StartTime": {$gt:"2017-10-01T01:00:00Z", $lt: "2017-11-01T00:00:00Z"}} ' --out gr_out.json
#mongoexport --db waycare_production_db --collection incidents --query '{"creation_time": {$gte:ISODate(1506805200000), $lt: ISODate(1509487200000) },"source":"spillman_incident","type":"crash"}' --out spillman_crashes.json
#step 2
#move files to desired location
#step 3
#select acceptable distance (m) global_start_offset (s) and global_end_offset (s)
#step 4
#input filenames
#step 5
#run script
#python3 mongo_gr

#acceptable distance is in m
acceptable_distance = 750
gmaps = googlemaps.Client(key='AIzaSyC7U9h2KLqESB5j0tu50sf-9zr8BNMV448') 
global_start_offset = 60
global_end_offset = 60

def give_location(client,location, distance_m):
    a1 = roads.nearest_roads(client, location)
    if len(a1)>0:
        a1 = a1[0]['location']
        s1 = roads.snap_to_roads(client, location, interpolate=False)#[0]['location']
        #print('\n\n\n')
        s1 = s1[0]['location']
        street_location = (s1['latitude'],s1['longitude'])  
        reverse_geocode_result_a1 = client.reverse_geocode((a1['latitude'],a1['longitude']))
        #print(reverse_geocode_result_a1)
        reverse_geocode_result_a1 = reverse_geocode_result_a1[0]['address_components'][0]['short_name']
        return reverse_geocode_result_a1

    else:
        return "road cannot be found, coordinates entered are likely not near a marked road"   

        
        
def close_enough_time_local(start_time_2,start_time_1,end_time_2,end_time_1):
    local_starttime_offset = start_time_2 - datetime.timedelta(0,global_start_offset)
    local_endtime_offset = end_time_2 + datetime.timedelta(0,global_end_offset)
    if start_time_1>local_starttime_offset and start_time_1<local_endtime_offset:
        #print(str(spellman_starttime_offset)+" "+str(start_time_1)+" "+str(spellman_endtime_offset)+"\n\n\n\n")
        return True
    else:
        return False


    
def find_road_matches(filename_1,filename_2):
    matches = {}
    file_1_list = mongo_fetch.mongo_fetch_spillman(filename_1)
    file_1_list = sorted(file_1_list, key=lambda x: x['start_time'])
    file_2_list = mongo_fetch.mongo_fetch_gr(filename_2) 
    file_2_list = sorted(file_2_list, key=lambda x: x['creation_time'])
    
    for i in range(0,len(file_1_list)):        
        for n in range(0,len(file_2_list)):
            if (mongo_universal.close_enough_distance(file_1_list[i]['coordinates'],file_2_list[n]['coordinates'],acceptable_distance) == True):
                if (close_enough_time_local(file_1_list[i]['start_time'],file_2_list[n]['creation_time'],file_1_list[i]['end_time'],file_2_list[n]['end_time'])):
                    coordinate_touple_other = (float(file_2_list[n]['coordinates'][1]),float(file_2_list[n]['coordinates'][0]))
                    coordinate_touple_crash = (float(file_1_list[i]['coordinates'][1]),float(file_1_list[i]['coordinates'][0]))  
                    if file_1_list[i]['_id'] in matches.keys():
                        matches[file_1_list[i]['_id']][1].append(file_2_list[n])
                    else:
                        matches[file_1_list[i]['_id']]  = (file_1_list[i],[file_2_list[n]])                      
                    #high_name_other = give_location(gmaps,coordinate_touple_other, 300)
                    #high_name_crash = give_location(gmaps,coordinate_touple_crash, 300)
                    #if high_name_crash == high_name_other and high_name_other != "road cannot be found, coordinates entered are likely not near a marked road":
                    #    file_2_list[n]['confirmed_street'] = high_name_other
                    #    file_1_list[i]['confirmed_street'] = high_name_crash
                    #    print(high_name_other,high_name_crash) 
                    #    if file_1_list[i]['_id'] in matches.keys():
                    #        matches[file_1_list[i]['_id']][1].append(file_2_list[n])
                    #    else:
                    #        matches[file_1_list[i]['_id']]  = (file_1_list[i],[file_2_list[n]]) 
    print(len(matches))
    event_type_map = {}
    for i in range(0,len(file_2_list)):
        if(file_2_list[i]['event_type'] in event_type_map.keys()):
            event_type_map[file_2_list[i]['event_type']]+=1
        else:
            event_type_map[file_2_list[i]['event_type']]=1
    road_type_map = {}
    for key in matches.keys():
        if matches[key][0]['road_type'] in road_type_map.keys():
            road_type_map[matches[key][0]['road_type']]+=1
        else:
            road_type_map[matches[key][0]['road_type']]=1  
    now = str(universal_imports.datetime.utcnow())
    now = now.split(" ")
    now[0] = now[0].split("-")
    now[1] = now[1].split(":")
    curr_t = universal_imports.datetime.utcnow().strftime("%d-%m-%Y %H:%M:%S")
    filename = "mongo_gr"+"_"+now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1]              
    incidentFiletxt = open("examples/"+filename+'.json', 'w')
    incidentFiletxt.write(str({"spillman road_type count":road_type_map,"gr event_type count":event_type_map,"number of matches":len(matches),"matches":matches}))  
    incidentFiletxt.close()
#'high_crash_2.json','gr_out.json'
first_file_name = input("enter first filename: ")
second_file_name = input("enter second filename: ")
find_road_matches(first_file_name,second_file_name)
#find_road_matches('high_crash_2.json','gr_out.json')
