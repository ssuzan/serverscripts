import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts/globals')
import universal_imports
import sendemail
from email.mime.text import MIMEText
from dateutil.parser import parse
import datetime
from datetime import timedelta
import googlemaps
from googlemaps import roads
from math import sin, cos, sqrt, atan2, radians
import mongo_fetch
import mongo_universal

#acceptable distance is in m
acceptable_distance = 450
#acceptable_time_lapse is in seconds
acceptable_time_lapse = 7200

#step 1        
#generate files from mongo (spillman/waze)
#mongoexport --db waycare_production_db --collection incidents --query '{"creation_time": {$gte:ISODate(1506805200000), $lt: ISODate(1509487200000) },"source":"waze_traffic_alert","type":"crash"}' --out waycare_crashes.json
#mongoexport --db waycare_production_db --collection incidents --query '{"creation_time": {$gte:ISODate(1506805200000), $lt: ISODate(1509487200000) },"source":"spillman_incident","type":"crash"}' --out spillman_crashes.json

#mongoexport -h 10.0.3.44:27017 -d waycare_db -c incidents --query '{ $and: [{"creation_time": {$gte: {"$date":"2017-11-01T00:00:00.000Z"}, $lt: {"$date":"2017-12-01T00:00:00.000Z"}}},{"type":"crash"},{"source":"waze_traffic_alert"}]}' --type json --out waycare_crashes.json

#set start date in UTC format (7 hours ahead of PDT):
#step 2
#move files to desired location
#step 3
#select acceptable distance (m) and time (s)
#step 4
#input filenames
#step 5
#run script
#python3 get_first_crash_indication

    
def which_is_first(start_time_1,start_time_2):
    time_difference = abs((start_time_2-start_time_1).total_seconds())
    if (start_time_1 <start_time_2):
        return ["spillman",time_difference]
    elif (start_time_1 > start_time_2):
        return ["waycare",time_difference]
    else:
        return ["tie",time_difference]

    
def close_enough_time_local(start_time_1,start_time_2,end_time_1,end_time_2):
    if start_time_1 <start_time_2 and start_time_2 < end_time_1:
        return True
    else:
        return False

    
def close_enough_time_waze(start_time_1,start_time_2,end_time_1,end_time_2):
    spellman_starttime_offset = start_time_2 - datetime.timedelta(0,1800)
    spellman_endtime_offset = end_time_2 + datetime.timedelta(0,1800)
    if start_time_1>spellman_starttime_offset and start_time_1<spellman_endtime_offset:
        #print(str(spellman_starttime_offset)+" "+str(start_time_1)+" "+str(spellman_endtime_offset)+"\n\n\n\n")
        return True
    else:
        return False
    
def waze_lowest_date(waze_dates):
    lowest_date_iterater_number = None
    for key in waze_dates.keys():
        if lowest_date_iterater_number == None:
            lowest_date_iterater_number = key
        elif waze_dates[key]<waze_dates[lowest_date_iterater_number]:
            lowest_date_iterater_number = key
    return lowest_date_iterater_number

def get_spillman_duplicates(file_1_list):
    spillman_duplicates = []
    for i in range(0,len(file_1_list)-1):
        if i not in spillman_duplicates:
            start_time = file_1_list[i]['start_time']
            end_time = file_1_list[i]['end_time']  
            for n in range(i+1,len(file_1_list)):
                if mongo_universal.close_enough_distance(file_1_list[i]["coordinates"],file_1_list[n]["coordinates"],acceptable_distance) == True:
                    if (close_enough_time_local(start_time,file_1_list[n]['start_time'],end_time,file_1_list[n]['end_time']) == True):                
                        spillman_duplicates.append(i) 
    return spillman_duplicates

def get_matches(spillman_unique,file_1_list,file_2_list):
    matches = {}
    for i in range(0,len(spillman_unique)):
        start_time = file_1_list[spillman_unique[i]]["start_time"]
        end_time = file_1_list[spillman_unique[i]]["end_time"]  
        for n in range(0,len(file_2_list)):
            if mongo_universal.close_enough_distance(file_2_list[n]["coordinates"],file_1_list[spillman_unique[i]]["coordinates"],acceptable_distance) == True:
                if close_enough_time_waze(file_2_list[n]['start_time'],start_time,file_2_list[n]['end_time'],end_time) == True:
                    if spillman_unique[i] in matches.keys():
                        matches[spillman_unique[i]].append(n)
                    else:
                        matches[spillman_unique[i]] = [n]
    return matches

#output file headers
#print structure and summary to file
#print info for max waze incident and corresponding spillman incident
#create structure with each unique spillman and correspoding waze
#print max element
#add longterm caller id for spillman

def find_road_matches(filename_1,filename_2):
    # creating query from mongo_db:
    os_command='mongoexport -h 10.0.3.44:27017 -d waycare_db -c incidents --query \'{ $and: [{\"creation_time\": {$gte: {\"$date\":\"'+date_1+'\"}, $lt: {\"$date\":\"'+date_2+'\"}}},{\"type\":\"crash\"},{\"source\":\"waze_traffic_alert\"}]}\' --type json --out waycare_crashes.json'

    print(os_command)
    os.system(os_command)
    os_command='mongoexport -h 10.0.3.44:27017 -d waycare_db -c incidents --query \'{ $and: [{\"creation_time\": {$gte: {\"$date\":\"'+date_1+'\"}, $lt: {\"$date\":\"'+date_2+'\"}}},{\"type\":\"crash\"},{\"source\":\"spillman_incident\"}]}\' --type json --out spillman_crashes.json'


    print(os_command)
    os.system(os_command)



    output_struct = {"summary":{},"max":{},"all unique":{}}
    matches = {}
    start_time = 0
    end_time = 0
    file_1_list = mongo_fetch.mongo_fetch_spillman(filename_1)
    file_2_list = mongo_fetch.mongo_fetch_get_first_crash_indication_1(filename_2)
    file_1_list.sort(key=lambda file_1_list: file_1_list['start_time'], reverse=False)
    file_2_list.sort(key=lambda file_2_list: file_2_list['start_time'], reverse=False)    
    spillman_unique = []
    spillman_duplicates = []
    waze_matches = []
    waycare_first = []
    waycare_first_elements = []
    spillman_first = []
    ties = []
    spillman_duplicates = get_spillman_duplicates(file_1_list)
    for i in range(0,len(file_1_list)):
        if i not in spillman_duplicates:
            spillman_unique.append(i)
    matches = get_matches(spillman_unique,file_1_list,file_2_list)
    for i in matches.keys():
        #print(str(file_1_list[i]['start_time'])+" "+file_1_list[i]['_id']+" "+str(file_1_list[i]['end_time']))
        waze_matches_i = {}
        for n in range(0,len(matches[i])):
            #print(matches[i][n])
            waze_matches_i[matches[i][n]] = file_2_list[matches[i][n]]['start_time']
            
        earliest_waze = waze_lowest_date(waze_matches_i)
        #print(str(file_2_list[matches[i][n]]['start_time'])+" "+file_2_list[matches[i][n]]['_id'])
        result = which_is_first(file_1_list[i]['start_time'],file_2_list[earliest_waze]['start_time'])          
        if result[0] == "waycare":
            waycare_first.append(result[1])
            waycare_first_elements.append([file_1_list[i],file_2_list[earliest_waze]])
        if result[0] == "spillman":
            spillman_first.append(result[1])
        if result[0] == "tie":
            ties.append(result[1])
    for i in matches.keys():
        waze_matches_struct = []
        for n in range(0,len(matches[i])):
            #print(matches[i][n])
            waze_matches_struct.append(file_2_list[matches[i][n]])
        output_struct["all unique"][file_1_list[i]['_id']] = [file_1_list[i],waze_matches_struct]
    waycare_max_element = None
    waycare_max = max(waycare_first)
    for i in range(0,len(waycare_first)):
        if waycare_first[i] == waycare_max:
            waycare_max_element = waycare_first_elements[i]            
    print("Spillman total: "+str(len(file_1_list)))
    print("Spillman unique total: "+str(len(spillman_unique)))
    print("Waycare first: "+str(len(waycare_first)))
    print("Spillman first: "+str(len(spillman_first)))
    print("Created at same time: "+str(len(ties)))
    print("Average waycare difference: "+str(sum(waycare_first)/len(waycare_first)) +" seconds")
    print("Average spillman difference: "+str(sum(spillman_first)/len(spillman_first))+" seconds")   
    print("Max waycare difference: "+str(max(waycare_first))+" seconds")
    print("Max spillman difference: "+str(max(spillman_first))+" seconds")
    print("Waycare max: "+str(waycare_max_element))
    output_struct['summary']["Spillman total"] = len(file_1_list)
    output_struct['summary']["Spillman unique total"] = len(spillman_unique)
    output_struct['summary']["Waycare first"]=len(waycare_first)
    output_struct['summary']["Spillman first"]=len(spillman_first)
    output_struct['summary']["Created at same time"]=len(ties)
    output_struct['summary']["Average waycare difference"]=(sum(waycare_first)/len(waycare_first))
    output_struct['summary']["Average spillman difference"]=(sum(spillman_first)/len(spillman_first)) 
    output_struct['summary']["Max waycare difference"]=max(waycare_first)
    output_struct['summary']["Max spillman difference"]=max(spillman_first)
    output_struct["max"] = waycare_max_element
    #print(output_struct)
    now = str(universal_imports.datetime.utcnow())
    now = now.split(" ")
    now[0] = now[0].split("-")
    now[1] = now[1].split(":")
    curr_t = universal_imports.datetime.utcnow().strftime("%d-%m-%Y %H:%M:%S")
    filename = "mongo_time_diff"+"_"+now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1]              
    incidentFiletxt = open("examples/"+filename+'.json', 'w')
    incidentFiletxt.write(str(output_struct))  
    incidentFiletxt.close()
    
#x = 'spillman_crashes.json'
#y = 'waycare_crashes.json'
#first_file_name = input("enter first filename: ")
#second_file_name = input("enter second filename: ")
#find_road_matches(first_file_name,second_file_name)
date_1= "2017-11-01T07:00:00.000Z"
date_2= "2017-12-01T07:00:00.000Z"

#date_1 = input("enter first date (e.g. 2017-11-01T07:00:00.000Z) : ")
#date_2 = input("enter last date  (e.g. 2017-12-01T07:00:00.000Z) : ")
first_file_name = 'spillman_crashes.json'
second_file_name= 'waycare_crashes.json'
find_road_matches(first_file_name,second_file_name)
