#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports
import APScheduler
import sendemail
email_file = workspace_environ+'/serverscripts/globals/emails'
import jsonlib_python3
from apscheduler.schedulers.blocking import BlockingScheduler
import requests
import smtplib
import clean_wz
import fetch_wz
import store_wz
import hashlib
from email.mime.text import MIMEText


wz_url = "https://na-georss.waze.com/rtserver/web/TGeoRSS?tk=ccp_partner&ccp_partner_name=WayCare&format=JSON&types=alerts&polygon=-115.344000,36.492000;-115.580000,36.414000;-115.490000,35.918000;-115.390000,35.605000;-114.994000,35.636000;-114.505000,35.908000;-113.917000,36.929000;-114.680000,36.724000;-115.344000,36.492000;-115.344000,36.492000"
save_path = workspace_environ+'/DB_backups/data_from_wz'
dump_output_to = "FILE"
from os import listdir

#number of iterations before element is removed from ongoing duplicate check list
default_age = 5
#minutes between iterations (.5 for half minute)
minutes_between_script_calls = 1
#ongoing duplicate check list
hash_log = {}

def initialize_new_schedule():
    back_log = os.listdir(save_path+"/database_original")
    text = open(save_path+"/database_original/"+max(back_log),'r')
    string = text.read()
    string = universal_imports.json.loads(string) 
    if(clean_wz.clean(string)[0] == True):
        print("initializing: backup file is clean")
        for i in range(0,len(string['alerts'])):
            hash_uuid = hashlib.md5()
            encoded_uuid = str(string['alerts'][i])
            encoded_uuid = encoded_uuid.encode("utf-8")
            hash_uuid.update(encoded_uuid)
            hash_log[string['alerts'][i]['uuid']] = [hash_uuid.hexdigest(),default_age] 

def call_all_wz():
    curr_t = universal_imports.datetime.utcnow().strftime("%d-%m-%Y %H:%M:%S")
    y = fetch_wz.downloadWaze(wz_url)
    #y = fetch_wz.downloadWaze("https://oogle1233///3445425463y/t2wg4tw45q4gtwhew6t4.com")   
    text = open(email_file,'r')
    string = text.read()
    email_json = universal_imports.json.loads(string) 
    dead_hashes = []
    if(clean_wz.clean(y)[0] == True and clean_wz.clean(y)[1] == 'alerts not present'):
        original = y
        store_wz.store(original,original, save_path,hash_log,dump_output_to,default_age,True) 
        for key in hash_log.keys():
            hash_log[key][1]-=1
            #print(key+" "+str(hash_log[key][1]))
            if hash_log[key][1] == 0:
                dead_hashes.append(key)
        for i in range(0,len(dead_hashes)):
            del hash_log[dead_hashes[i]]      
        print('call_all_wz.py completed successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
        
    elif(clean_wz.clean(y)[0] == True and clean_wz.clean(y)[1] == 'alerts present'):
        #hashing starts here
        original = y
        original_cleaned = clean_wz.clean(y)[2]
        results = store_wz.store(original,original_cleaned, save_path,hash_log,dump_output_to,default_age,False) 
        hashes = results[0]
        repeat_hashes = results[1]
        for key in hashes:
            hash_log[key] = hashes[key]
        for key in hash_log.keys():
            if key not in repeat_hashes and key not in hashes.keys():
                hash_log[key][1]-=1
                #print(key+" "+str(hash_log[key][1]))
                if hash_log[key][1] == 0:
                    dead_hashes.append(key)
            else:
                hash_log[key][1] = default_age
        for i in range(0,len(dead_hashes)):
            del hash_log[dead_hashes[i]]      
        print('call_all_wz.py completed successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
    else:
        sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'error', message = clean_wz.clean(y)[1], login = email_json["email"], password = email_json["password"])    
        

def scheduled_call():
    print('call_all_wz.scheduled_call() started successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
    #do not intialize with -not_initialize
    try:
        if (str(sys.argv[1]) == '-not_initialize'):
            place_holder_variable = "holding a place"
    except:
        try:
            initialize_new_schedule()
        except:
            place_holder_variable = "holding a place"
    #initialize new scheduler
    sched = BlockingScheduler(timezone='UTC')
    sched.add_job(call_all_wz, 'interval', id='job_id1', seconds=60)
    sched.start()
#call_all_wz()
scheduled_call()
#print(hash_log)