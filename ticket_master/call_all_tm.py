#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports
import apscheduler
from apscheduler.schedulers.blocking import BlockingScheduler
import sendemail
email_file = workspace_environ+'/serverscripts_production/globals/emails'
import requests
import smtplib
import clean_tm
import fetch_tm
import store_tm
import hashlib
from email.mime.text import MIMEText
import json


tm_url = "https://app.ticketmaster.com/discovery/v2/events.json?dmaId=319&apikey=I2jBiTkT97tcDXFqPR9PRJ0SEkVAU5zz"
save_path = workspace_environ+'/DB_backups/data_from_tm'
dump_output_to = "FILE"

def initialize_new_schedule():
    back_log = os.listdir(save_path+"/database_original")
    text = open(save_path+"/database_original/"+max(back_log),'r')
    string = text.read()
    string = universal_imports.json.loads(string) 
    if(clean_tm.clean(string)[0] == True):
        print("initializing: backup file is clean")
        for i in range(0,len(string['alerts'])):
            hash_uuid = hashlib.md5()
            encoded_uuid = str(string['alerts'][i])
            encoded_uuid = encoded_uuid.encode("utf-8")
            hash_uuid.update(encoded_uuid)
            hash_log[string['alerts'][i]['uuid']] = [hash_uuid.hexdigest(),default_age] 

def call_all_tm():
    curr_t = universal_imports.datetime.utcnow().strftime("%d-%m-%Y %H:%M:%S")
    print("call_all_tm() running at time: "+curr_t) 
    curr_t = str(curr_t).replace(":","_") 
    curr_t = str(curr_t).replace(" ","_") 
    curr_t = str(curr_t).replace("-","_")     

    filename = "tm_lv_events_"+curr_t 
    text = open(email_file,'r')
    string = text.read()
    email_json = universal_imports.json.loads(string) 
#

    y = fetch_tm.downloadTM(tm_url)
    #print(json.dumps(y, sort_keys=True,indent=4, separators=(',', ': ')))
    store_tm.store_original(filename, y, save_path) 
    resp= clean_tm.clean(y)

    if(resp[0] == True):
        y = resp[2]
        store_tm.store(curr_t, y, save_path, dump_output_to, filename)
        print('call_all_tm.py completed successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
        
    else:
        sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'call_all_tm() error ', message = resp[1], login = email_json["email"], password = email_json["password"])    
        

def scheduled_call():
    print('call_all_tm.scheduled_call() started successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
    #do not intialize with -not_initialize
  #  try:
  #      if (str(sys.argv[1]) == '-not_initialize'):
  #          place_holder_variable = "holding a place"
  #  except:
  #      try:
  #          initialize_new_schedule()
  #      except:
  #          place_holder_variable = "holding a place"
  #  #initialize new scheduler
    sched = BlockingScheduler(timezone='UTC', coalesce=True)
    sched.add_job(call_all_tm, 'interval', id='job_id1', seconds=60,coalesce=True)
    sched.start()

if __name__ == '__main__':
    call_all_tm()
    #scheduled_call()
