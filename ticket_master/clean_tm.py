#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: json containing data from waze database
#output: properly formatted json containing data from waze database
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports
import requests
import sendemail
email_file = workspace_environ+'/serverscripts_production/globals/emails'
from email.mime.text import MIMEText


def clean_incident(incident):
    if('type' in incident.keys()):
        if(isinstance(incident['type'],str)):
            place_holder_variable = True
        else:
            print(incident['type'])
            return(False,'type not str')
    else:
        incident['type'] = '' 
    if('subtype' in incident.keys()):
        if(isinstance(incident['subtype'],str)):
            place_holder_variable = True
        else:
            print(incident['subtype'])
            return(False,'subtype not str')
    else:
        incident['subtype'] = ''
    if('reliability' in incident.keys()):
        if(isinstance(incident['reliability'],int)):
            place_holder_variable = True
        else:
            print(incident['reliability'])
            return(False,'reliability not int')
    else:
        incident['reliability'] = ''
    if('confidence' in incident.keys()):
        if(isinstance(incident['confidence'],int)):
            place_holder_variable = True
        else:
            print(incident['confidence'])
            return(False,'confidence not int')
    else:
        return(False,'confidence not present')     
    if('street' in incident.keys()):
        if(isinstance(incident['street'],str)):
            place_holder_variable = True
        else:
            print(incident['street'])
            return(False,'street not str')
    else:
        incident['street'] = ''
    if('uuid' in incident.keys()):
        if(isinstance(incident['uuid'],str)):
            place_holder_variable = True
        else:
            print(incident['uuid'])
            return(False,'uuid not str')
    else:
        incident['uuid'] = 'unknown uuid %i'%i  
    if('magvar' in incident.keys()):
        if(isinstance(incident['magvar'],int)):
            place_holder_variable = True
        else:
            print(incident['magvar'])
            return(False,'magvar not int')
    else:
        incident['magvar'] = ''   
    if('country' in incident.keys()):
        if(isinstance(incident['country'],str)):
            place_holder_variable = True
        else:
            print(incident['country'])
            return(False,'country not str')
    else:
        incident['country'] = ''       
    if('city' in incident.keys()):
        place_holder_variable = True
    else:
        incident['city'] = ''     
    if('pubMillis' in incident.keys()):
        place_holder_variable = True
    else:
        incident['pubMillis'] = '' 
    if('location' in incident.keys()):
        place_holder_variable = True
    else:
        incident['location'] = '' 
    if('reportRating' in incident.keys()):
        place_holder_variable = True
    else:
        incident['reportRating'] = ''      
    if('reportDescription' in incident.keys()):
        place_holder_variable = True
    else:
        incident['reportDescription'] = '' 
    if('jamUuid' in incident.keys()):
        place_holder_variable = True
    else:
        incident['jamUuid'] = ''    
    if('reportByMunicipalityUser' in incident.keys()):
        place_holder_variable = True
    else:
        incident['reportByMunicipalityUser'] = ''     

    known_elements = {'type': 'type', 'subtype': 'subtype', 'reliability': 'reilability', 'confidence':'confidence','street': 'street', 'uuid':'uuid','magvar':'magvar', 'city':'city', 'country':'country', 'location':'location', 'pubMillis':'pubMillis', 'reportRating':'reportRating', 'jamUuid' : 'jamUuid', 'reportByMunicipalityUser': 'reportByMunicipalityUser' , 'reportDescription': 'reportDescription', 'roadType':'roadType', 'nThumbsUp':'nThumbsUp' }
    for incident_key in incident.keys():
        if incident_key not in known_elements.keys():
            #print(incident_key)
            text = open(email_file,'r')
            email_string = text.read()
            email_json = universal_imports.json.loads(email_string)
            sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'tm alerts warning', message = "tm "+"clean warning unknown incident_key "+incident_key, login = email_json["email"], password = email_json["password"])             
    return (True, incident)

def clean(json_clean):
#    #print(json_clean.keys())
#    if('endTime' in json_clean.keys()):
#        if(isinstance(json_clean['endTime'],str)):
#            place_holder_variable = True
#        else:
#            print(json_clean['endTime'])
#            return(False,'endTime not str')
#    else:
#        return(False,'endTime not present')   
#    if('startTime' in json_clean.keys()):
#        if(isinstance(json_clean['startTime'],str)):
#            place_holder_variable = True
#        else:
#            print(json_clean['startTime'])
#            return(False,'startTime not str')
#    else:
#        return(False,'startTime not present')    
#    if('endTimeMillis' in json_clean.keys()):
#        if(isinstance(json_clean['endTimeMillis'],int)):
#            place_holder_variable = True
#        else:
#            print(json_clean['endTimeMillis'])
#            return(False,'endTimeMillis not int')
#    else:
#        return(False,'endTimeMillis not present')    
#    if('startTimeMillis' in json_clean.keys()):
#        if(isinstance(json_clean['startTimeMillis'],int)):
#            place_holder_variable = True
#        else:
#            print(json_clean['startTimeMillis'])
#            return(False,'startTimeMillis not int')
#    else:
#        return(False,'startTimeMillis not present')      
#    if('alerts' in json_clean.keys()):
#        if(isinstance(json_clean['alerts'],list)):
#            place_holder_variable = True
#            for i in range(0,len(json_clean['alerts'])):
#                clean = clean_incident(json_clean['alerts'][i])
#                #print(clean)
#                if(clean[0] == False):    
#                    return(clean)
#        else:
#            print(json_clean['alerts'])
#            return(False,'alerts not list')
#    else:
#        return(True,'alerts not present',json_clean)         
    return(True, 'all clean',json_clean)    
