#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: tm database https request (url) 
#output: json of data given by https request or json containing error message
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports
import requests


def downloadTM(url_tm):  
    try:
        #gets json from url
        r = requests.get(url_tm)
        r = r.json()
    except requests.exceptions.ConnectionError:
        r = universal_imports.json.loads('{"error" : "requests.exceptions.ConnectionError"}')  
    return r["_embedded"]["events"]
     
