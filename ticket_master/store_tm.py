#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: json input and file directory path
#output: json stored to directory
#import hashlib
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports
#import jsonlib_python3
import requests
import hashlib
import sendemail
from email.mime.text import MIMEText
import push_to_fifo
import push_to_fifo_dev
import json
type_name = 'tm_events_lv'
email_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/emails'

def store_original(filename,r,save_path):
    #storing source and result
    original_file = open(save_path+"/database_original/"+filename+'.json', 'w')
    r=json.dumps(r, sort_keys=True,indent=4, separators=(',', ': '))
    original_file.write(r)
    original_file.close()

def store(curr_t, cleaned_store_json, save_path, dump_output_to, filename):
    text = open(email_file,'r')
    email_string = text.read()
    email_json = universal_imports.json.loads(email_string)

    for i in range(0,len(cleaned_store_json)):
        new_store_json =cleaned_store_json[i]["_embedded"]
        curr_id = 'event_'+str(i)
        if ((dump_output_to == "FILE") or (dump_output_to == "ALL")):
            lastFiletxt = open(save_path+'/database/'+filename+'_'+curr_id+'.json', 'w')
            lastFiletxt.write(universal_imports.json.dumps(new_store_json, sort_keys=True, indent=4, separators=(',', ': ')))
        #sends file to FIFO
        if ((dump_output_to == "FIFO") or (dump_output_to == "ALL")):
            try:
                push_to_fifo.push_to_fifo(universal_imports.json.dumps(new_store_json))
            except:
                text = open(email_file,'r')
                email_string = text.read()
                email_json = universal_imports.json.loads(email_string)
                sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'error in store_tm', message = 'fifo error in store_tm', login = email_json['email'], password = email_json['password'])
            # pushing to dev fifo:
            try:
                push_to_fifo_dev.push_to_fifo_dev(universal_imports.json.dumps(new_store_json))
            except:
                text = open(email_file,'r')
                email_string = text.read()
                email_json = universal_imports.json.loads(email_string)
                sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'warning in store_tm', message = 'dev fifo error in store_tm', login = email_json['email'], password = email_json['password'])
 
        if(dump_output_to == "NONE"):
            print("call_all_tm() writing output to NONE")

