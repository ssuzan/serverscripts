#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: json input and file directory path
#output: json stored to directory
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports
import jsonlib_python3
import random
import requests
full_file_path = 'backup_full_files'
type_name = 'waze_traffic_jams_lv'

dump_output_to = "FIFO"

def store(store_json, path):
    #creates folder for files
    if not universal_imports.os.path.exists(path):
        universal_imports.os.makedirs(path)
    save_path = path
    #gets date for filename as now
    now = str(universal_imports.datetime.now())
    now = now.split(" ")
    now[0] = now[0].split("-")
    now[1] = now[1].split(":")
    #print(store_json['jams'])
    curr_t = universal_imports.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
    filename = type_name+"_"+now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1]       
    #saves original files
    lastFiletxt = open(path+"/"+full_file_path+"/"+filename+'.json', 'w')
    lastFiletxt.write(universal_imports.json.dumps(store_json, sort_keys=True, indent=4, separators=(',', ': ')))     
    lastFiletxt.close()

    #dumps updated files
    if 'jams' in store_json.keys():
        for i in range(0,len(store_json['jams'])):
            filename = type_name+"_"+now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1]+"_incident_"+str(i)
            #saves file
            new_store_json = {'type':type_name,'date_time':curr_t,'suspicious':False,'data':store_json['jams'][i]}
            if  (dump_output_to == "FILE"):
                lastFiletxt = open(save_path+"/"+filename+'.json', 'w')
                lastFiletxt.write(universal_imports.json.dumps(new_store_json, sort_keys=True, indent=4, separators=(',', ': ')))  
            elif (dump_output_to == "FIFO"):
                print("TBD implementation of FIFO")

       
