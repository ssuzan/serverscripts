#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: json input and file directory path
#output: json stored to directory
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports
#import jsonlib_python3
import requests
import hashlib
import sendemail
from email.mime.text import MIMEText
import push_to_fifo
full_file_path = 'backup_full_files'
type_name = 'waze_traffic_jam_lv'
email_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/emails'

def store(original, original_cleaned, save_path,hash_log,dump_output_to,default_age,is_empty):
    #gets date for filename as now
    now = str(universal_imports.datetime.utcnow())
    now = now.split(" ")
    now[0] = now[0].split("-")
    now[1] = now[1].split(":")
    curr_t = universal_imports.datetime.utcnow().strftime("%d-%m-%Y %H:%M:%S")
    filename = type_name+"_"+now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1]+"_"+now[1][2].replace(".","_")       
    #saves file
    lastFiletxt = open(save_path+"/database_original/"+filename+'.json', 'w')
    lastFiletxt.write(universal_imports.json.dumps(original, sort_keys=True, indent=4, separators=(',', ': ')))  
    if (is_empty == False):
        hashes = {}
        repeat_hashes=[]
        for i in range(0,len(original_cleaned['jams'])):
            hash_uuid = hashlib.md5()
            encoded_uuid = str(original_cleaned['jams'][i])
            #encoded_uuid.pop(original_cleaned['jams'][i]))
            #print(encoded_uuid)
            encoded_uuid = encoded_uuid.encode("utf-8")
            hash_uuid.update(encoded_uuid)
            if original_cleaned['jams'][i]['uuid'] in hash_log.keys():
                if hash_log[original_cleaned['jams'][i]['uuid']][0]!=hash_uuid.hexdigest():
                    hashes[original_cleaned['jams'][i]['uuid']] = [hash_uuid.hexdigest(),default_age]
                else:
                    repeat_hashes.append(hash_log[original_cleaned['jams'][i]['uuid']][0])
            else:
                hashes[original_cleaned['jams'][i]['uuid']] = [hash_uuid.hexdigest(),default_age]
            filename = type_name+"_"+str(now[0][2])+"_"+str(now[0][1])+"_"+str(now[0][0])+"_"+str(now[1][0])+"_"+str(now[1][1])+"_"+str(now[1][2]).replace(".","_")+"_incident_"+str(original_cleaned['jams'][i]['uuid'])
            #saves file
            if (original_cleaned['jams'][i]['uuid'] not in repeat_hashes):
                new_store_json = {'type':type_name,'date_time':str(curr_t).replace("-","_")+' UTC','suspicious':False,'data':original_cleaned['jams'][i]}        
                if (dump_output_to == "FILE"): 
                    lastFiletxt = open(save_path+"/database/"+filename+'.json', 'w')
                    lastFiletxt.write(universal_imports.json.dumps(new_store_json, sort_keys=True, indent=4, separators=(',', ': ')))  
                #sends file to FIFO
                elif (dump_output_to == "FIFO"):
                    try:
                        push_to_fifo.push_to_fifo(new_store_json, "waze_traffic_jams_queue")         
                    except:
                        text = open(email_file,'r')
                        email_string = text.read()
                        email_json = universal_imports.json.loads(email_string)                  
                        sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'error', message = 'fifo error in store_wz_jam', login = email_json['email'], password = email_json['password'])         
                
        return [hashes,repeat_hashes]
