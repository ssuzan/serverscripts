#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports
import sendemail
email_file = workspace_environ+'/serverscripts_production/globals/emails'
from automation_building_scripts import automate_build_clean
import jsonlib_python3
import requests
import smtplib
import clean_wz_jams
import fetch_wz_jams
import store_wz_jams
from email.mime.text import MIMEText

wz_url = "https://na-georss.waze.com/rtserver/web/TGeoRSS?tk=ccp_partner&ccp_partner_name=WayCare&format=JSON&types=traffic&polygon=-115.344000,36.492000;-115.580000,36.414000;-115.490000,35.918000;-115.390000,35.605000;-114.994000,35.636000;-114.505000,35.908000;-113.917000,36.929000;-114.680000,36.724000;-115.344000,36.492000;-115.344000,36.492000"
save_path = workspace_environ +'/DB_backups/data_from_wz_jams'

def call_all_wz_jams():
    curr_t = universal_imports.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
    print("call_all_wz_jams() running at time: "+curr_t)
    #1st fetch 2nd clean 3rd store
    y = fetch_wz_jams.downloadWaze(wz_url)  
    #print(y)
    #automate_build_clean.automate_clean({'pubMillis': 1501075200165, 'magvar': 49, 'nThumbsUp': 0, 'street': 'I-15 N', 'subtype': 'HAZARD_ON_SHOULDER_CAR_STOPPED', 'confidence': 0, 'reportRating': 3, 'uuid': '6ddf7028-953c-36a6-b5d3-bfd9f89cb296', 'location': {'x': -115.071521, 'y': 36.264511}, 'roadType': 3, 'country': 'US', 'reliability': 6, 'type': 'WEATHERHAZARD'},"json_clean['alerts'][i]")
    #print(clean_wz.clean(y))  
    if(clean_wz_jams.clean(y)[0] == True):
        store_wz_jams.store(y, save_path)    
    else:
        text = open(email_file,'r')
        string = text.read()
        email_json = universal_imports.json.loads(string)    
        sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'error', message = clean_wz_jams.clean(y)[1], login = email_json["email"], password = email_json["password"])    
        

call_all_wz_jams()
