#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: json containing data from waze database
#output: properly formatted json containing data from waze database
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
from automation_building_scripts import automate_build_clean
import universal_imports
import requests


def clean_incident(incident):
    if('length' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['length'],int)):
            place_holder_variable = True
        else:
            print(incident['length'])
            return(False,'length not int')
    else:
        incident['lenght'] = ''
    if('roadType' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['roadType'],int)):
            place_holder_variable = True
        else:
            print(incident['roadType'])
            return(False,'roadType not int')
    else:
        incident['roadType'] = ''
    if('type' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['type'],str)):
            place_holder_variable = True
        else:
            print(incident['type'])
            return(False,'type not str')
    else:
        incident['type'] = ''
    if('country' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['country'],str)):
            place_holder_variable = True
        else:
            print(incident['country'])
            return(False,'country not str')
    else:
        incident['country not present'] = ''
    if('turnType' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['turnType'],str)):
            place_holder_variable = True
        else:
            print(incident['turnType'])
            return(False,'turnType not str')
    else:
        incident['turnType'] = ''
    if('delay' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['delay'],int)):
            place_holder_variable = True
        else:
            print(incident['delay'])
            return(False,'delay not int')
    else:
        incident['delay'] = ''
    if('street' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['street'],str)):
            place_holder_variable = True
        else:
            print(incident['street'])
            return(False,'street not str')
    else:
        incident['street not present'] = ''
    if('pubMillis' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['pubMillis'],int)):
            place_holder_variable = True
        else:
            print(incident['pubMillis'])
            return(False,'pubMillis not int')
    else:
        incident['pubMillis'] = ''
    if('line' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['line'],list)):
            place_holder_variable = True
        else:
            print(incident['line'])
            return(False,'line not list')
    else:
        incident['line'] = ''
    if('id' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['id'],int)):
            place_holder_variable = True
        else:
            print(incident['id'])
            return(False,'id not int')
    else:
        incident['id'] = ''
    if('detectionDate' in incident.keys()):
        place_holder_variable = True
    else:
        incident['detectionDateMillis'] = '' 
    if('detectionDate' in incident.keys()):
        place_holder_variable = True
    else:
        incident['detectionDateMillis'] = '' 
    if('updatDate' in incident.keys()):
        place_holder_variable = True
    else:
        incident['updatDate'] = ''      
    if('trend' in incident.keys()):
        place_holder_variable = True
    else:
        incident['trend'] = ''          
    if('updatDateMillis' in incident.keys()):
        place_holder_variable = True
    else:
        incident['updatDateMillis'] = ''
    if('regularSpeed' in incident.keys()):
        place_holder_variable = True
    else:
        incident['regularSpeed'] = ''  
    if('severity' in incident.keys()):
        place_holder_variable = True
    else:
        incident['severity'] = ''         
    if('delaySeconds' in incident.keys()):
        place_holder_variable = True
    else:
        incident['delaySeconds'] = ''  
    if('jamLevel' in incident.keys()):
        place_holder_variable = True
    else:
        incident['jamLevel'] = ''          
    if('seconds' in incident.keys()):
        place_holder_variable = True
    else:
        incident['seconds'] = ''    
    if('driversCount' in incident.keys()):
        place_holder_variable = True
    else:
        incident['driversCount'] = ''     
    if('alertsCount' in incident.keys()):
        place_holder_variable = True
    else:
        incident['alertsCount'] = ''        
    if('segments' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['segments'],list)):
            place_holder_variable = True
        else:
            print(incident['segments'])
            return(False,'segments not list')
    else:
        incident['segments'] = ''
    if('uuid' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['uuid'],int)):
            place_holder_variable = True
        else:
            print(incident['uuid'])
            return(False,'uuid not int')
    else:
        incident['uuid'] = 'unknown uuid'
    if('city' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['city'],str)):
            place_holder_variable = True
        else:
            print(incident['city'])
            return(False,'city not str')
    else:
        incident['city'] = ''
    if('speed' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['speed'],float) or isinstance(incident['speed'],int)):
            place_holder_variable = True
        else:
            print(incident['speed'])
            return(False,'speed not float')
    else:
        incident['speed not present'] = ''
    if('level' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['level'],int)):
            place_holder_variable = True
        else:
            print(incident['level'])
            return(False,'level not int')
    else:
        incident['level'] = ''   
    return (True, incident)
def clean(json_clean):
    if('startTime' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['startTime'],str)):
            place_holder_variable = True
        else:
            print(json_clean['startTime'])
            return(False,'startTime not str')
    else:
        return(False,'startTime not present')
    if('endTimeMillis' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['endTimeMillis'],int)):
            place_holder_variable = True
        else:
            print(json_clean['endTimeMillis'])
            return(False,'endTimeMillis not int')
    else:
        return(False,'endTimeMillis not present')
    if('endTime' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['endTime'],str)):
            place_holder_variable = True
        else:
            print(json_clean['endTime'])
            return(False,'endTime not str')
    else:
        return(False,'endTime not present')
   
    if('startTimeMillis' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['startTimeMillis'],int)):
            place_holder_variable = True
        else:
            print(json_clean['startTimeMillis'])
            return(False,'startTimeMillis not int')
    else:
        return(False,'startTimeMillis not present')
    if('jams' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['jams'],list)):
            for i in range(0,len(json_clean['jams'])):
                clean = clean_incident(json_clean['jams'][i])
                if (clean[0] == False):
                    return clean_incident(json_clean['jams'][i])
                else:
                    json_clean['jams'][i]=clean[1]
                    
        else:
            print(json_clean['jams'])
            return(False,'jams not list')
    else:
        return(True,'jams not present',json_clean)    
    return(True, 'jams present',json_clean)     
