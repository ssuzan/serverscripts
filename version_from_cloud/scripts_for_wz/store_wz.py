#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: json input and file directory path
#output: json stored to directory
#import hashlib
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports
#import jsonlib_python3
import requests
import hashlib
import sendemail
from email.mime.text import MIMEText
import push_to_fifo
full_file_path = 'backup_full_files'
type_name = 'waze_traffic_alerts_lv'
email_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/emails'

def store(store_json, save_path,hash_log,dump_output_to,default_age,is_empty):
    #gets date for filename as now
    now = str(universal_imports.datetime.utcnow())
    now = now.split(" ")
    now[0] = now[0].split("-")
    now[1] = now[1].split(":")
    curr_t = universal_imports.datetime.utcnow().strftime("%d-%m-%Y %H:%M:%S")
    filename = type_name+"_"+now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1]+"_"+now[1][2].replace(".","_")       
#    hashlogFiletxt = open(save_path+"/hash_log"+filename+'.txt', 'w')
#    hashlogFiletxt.write(universal_imports.json.dumps(hash_log))
 
    #saves file
    lastFiletxt = open(save_path+"/database_original/"+filename+'.json', 'w')
    lastFiletxt.write(universal_imports.json.dumps(store_json, sort_keys=True, indent=4, separators=(',', ': ')))  
    if (is_empty == False):
        hashes = {}
        repeat_hashes=[]
        #StorylogFiletxt = open(save_path+"/hash_story_"+filename+'.txt', 'w')
        for i in range(0,len(store_json['alerts'])):
            hash_match_flag= False
            hash_uuid = hashlib.md5()
            encoded_uuid = str(store_json['alerts'][i])
            #encoded_uuid.pop(store_json['alerts'][i]))
            #print(encoded_uuid)
            encoded_uuid = encoded_uuid.encode("utf-8")
            hash_uuid.update(encoded_uuid)
            hex_dig= hash_uuid.hexdigest()
            if store_json['alerts'][i]['uuid'] in hash_log.keys():
                if hash_log[store_json['alerts'][i]['uuid']][0]!=hex_dig:
                    hashes[store_json['alerts'][i]['uuid']] = [hex_dig,default_age]
                    hash_match_flag= False 
                    #StorylogFiletxt.write('hash not matching for uuid: '+store_json['alerts'][i]['uuid']+' new hex_dig is: '+hex_dig+'\n')  
                else: 
                    repeat_hashes.append(hash_log[store_json['alerts'][i]['uuid']][0])
                    hash_match_flag= True
                    #StorylogFiletxt.write('hash matching for uuid: '+hash_log[store_json['alerts'][i]['uuid']][0]+'\n')  
            else:
                hashes[store_json['alerts'][i]['uuid']] = [hex_dig,default_age]
                hash_match_flag= False 
                #StorylogFiletxt.write('hash first time for uuid: '+store_json['alerts'][i]['uuid']+' new hex_dig is: '+hex_dig+'\n')  
            out_filename = type_name+"_"+now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1]+"_"+now[1][2].replace(".","_")+"_incident_"+store_json['alerts'][i]['uuid']

            #saves file

            #StorylogFiletxt.write('------------------------starting store phase: \n')  
            #StorylogFiletxt.write('repeat_hashes: '+universal_imports.json.dumps(repeat_hashes)+'\n')  
            #StorylogFiletxt.write('searching for: '+universal_imports.json.dumps(store_json['alerts'][i]['uuid'])+'\n')  
            if (hash_match_flag == False):
#            if (store_json['alerts'][i]['uuid'] not in repeat_hashes):
                #StorylogFiletxt.write('I am not in repeat_hashes: '+store_json['alerts'][i]['uuid']+'\n')  
                new_store_json = {'type':type_name,'date_time':str(curr_t).replace("-","_")+' UTC','suspicious':False,'error':False,'data':store_json['alerts'][i]}        
                data_out = universal_imports.json.dumps(new_store_json, sort_keys=True, indent=4, separators=(',', ': '))
                #print(data_out)
                if ((dump_output_to == "FILE") or (dump_output_to == "ALL")): 
                    lastFiletxt = open(save_path+"/database/"+out_filename+'.json', 'w')
                    lastFiletxt.write(data_out)  
                #sends file to FIFO
                if ((dump_output_to == "FIFO") or (dump_output_to == "ALL")):
                    try:
                        push_to_fifo.push_to_fifo(data_out,"fifo_name")
                    except Exception as e:
                        text = open(email_file,'r')
                        email_string = text.read()
                        email_json = universal_imports.json.loads(email_string)                  
                        sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'error', message = 'fifo error in store_wz '+e, login = email_json['email'], password = email_json['password'])         
            #else:
            #    StorylogFiletxt.write('I am in repeat_hashes: '+store_json['alerts'][i]['uuid']+'\n')  
             

        return [hashes,repeat_hashes]
    
