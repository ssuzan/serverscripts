#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import os
import sys
sys.path.append(os.environ['WORKSPACE']+'/serverscripts_production/globals')
import universal_imports
import sendemail
email_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/emails'
weather_underground_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/weather_underground_access.txt'
import requests
import smtplib
import weather_underground_clean
import weather_underground_fetch
import weather_underground_store
#from automation_building_scripts import automate_build_clean
from email.mime.text import MIMEText
weather_api_text = open(weather_underground_file,'r')
weather_api_string = weather_api_text.read()

dump_output_to = "FIFO"
pws = ['KLAS']
call_url = 'http://api.wunderground.com/api/c9fec3ad2f01d208/conditions/q/KLAS.json'

#call_url = 'http://api.wunderground.com/api/%s/conditions/q/pws:%s.json'
call_url = 'http://api.wunderground.com/api/c9fec3ad2f01d208/conditions/q/KLAS.json'
save_path = os.environ['WORKSPACE']+'/DB_backups/data_from_wu'

def weather_underground_call_all():
    curr_t = universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")
    curr_t = str(curr_t).replace(":","_")
    curr_t = str(curr_t).replace(" ","_")
    curr_t = str(curr_t).replace("-","_")    
    print("weather_underground_call_all() running at time: "+curr_t)
    text = open(email_file,'r')
    email_string = text.read()
    email_json = universal_imports.json.loads(email_string)
    for i in range(0,len(pws)):
        filename = pws[i]+"_"+curr_t
        r_touple = weather_underground_fetch.weather_underground_fetch(weather_api_string,pws[i],call_url,filename,save_path)
        if(r_touple[0] == True):
            r = r_touple[1]
            #automate_build_clean.automate_clean(r,"weather_data")
            #cleaning data sets
            #r['current_observation'].pop('windchill_c')
            results = weather_underground_clean.weather_underground_clean(r,filename)
            if(results[0] == True):
                r_cleaned = results[1]
                if(len(results[2]) >0):
                    sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'weather_underground error', message = "weather_underground clean warning "+filename+" "+str(results[2]), login = email_json["email"], password = email_json["password"])                                       
                weather_underground_store.weather_underground_store(filename,r_cleaned,save_path,dump_output_to,'weather_live_lv',results[2])
                
            else:
                sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'weather_underground error', message = "weather_underground clean error "+filename+" "+results[1], login = email_json["email"], password = email_json["password"])                    
        else:
            sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'weather_underground error', message = "weather_underground fetch error "+filename+" "+r_touple[1], login = email_json["email"], password = email_json["password"])                                
    print('weather_underground_call_all.py completed successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')            

weather_underground_call_all()

