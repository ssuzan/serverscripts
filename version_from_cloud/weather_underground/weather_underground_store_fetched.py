#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import os
import sys
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports
import sendemail
import push_to_fifo
from email.mime.text import MIMEText

email_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/emails'

def weather_underground_store_fetched(filename,r,save_path,type_name):
    #storing source and result
    original_file = open(save_path+"/database_original/"+filename+'.json', 'w')
    original_file.write(universal_imports.json.dumps(r, sort_keys=True, indent=4, separators=(',', ': ')))  
    original_file.close()  
 
