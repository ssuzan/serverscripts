#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports
import apscheduler
from apscheduler.schedulers.blocking import BlockingScheduler
import weather_underground_call_all

def scheduled_call():
    print('weather_underground_scheduled_call started successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
    #initialize new scheduler
    sched = BlockingScheduler(timezone='UTC')
    sched.add_job(weather_underground_call_all.weather_underground_call_all, 'interval', id='job_id1', seconds=300)
    sched.start()

scheduled_call()
#manage_transaction_gr
