#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import os
import sys
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports
import sendemail
email_file = workspace_environ+'/serverscripts_production/globals/emails'
weather_underground_file = workspace_environ+'/serverscripts_production/globals/weather_underground_access.txt'
import jsonlib_python3
import requests
import smtplib
import weather_underground_clean
import weather_underground_fetch
import weather_underground_store
from email.mime.text import MIMEText
weather_api_text = open(weather_underground_file,'r')
weather_api_string = weather_api_text.read()

dump_output_to = "FIFO"
pws = ['KNVBOULD17']  
curr_t = universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")
curr_t = str(curr_t).replace(":","_")
curr_t = str(curr_t).replace(" ","_")
curr_t = str(curr_t).replace("-","_")

save_path = workspace_environ+'/DB_backups/data_from_wu'

def weather_underground_call_all():
    
    print("weather_underground_call_all() running at time: "+curr_t)
    text = open(email_file,'r')
    email_string = text.read()
    email_json = universal_imports.json.loads(email_string)

    for i in range(0,len(pws)):
        r_touple = weather_underground_fetch.weather_underground_fetch(weather_api_string,pws[i])
        if(r_touple[0] == True):
            r = r_touple[1]
            #cleaning data sets
            results = weather_underground_clean.weather_underground_clean(r)
            if(results[0] == True):
                filename = pws[i]+"_"+curr_t
                weather_underground_store.weather_underground_store(filename,r,save_path,dump_output_to)
                print('weather_underground_call_all.py completed successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
            else:
                sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'error', message = "weather_underground_error : "+pws[i]+" : "+results[1], login = email_json["email"], password = email_json["password"])                    
        else:
            sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'error', message = "weather_underground_error : requests.exceptions.ConnectionError", login = email_json["email"], password = email_json["password"])                                
weather_underground_call_all()

