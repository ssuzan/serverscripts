#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import sys
import os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts_produciton/globals')
import universal_imports

def clean(weather_data):
    errors = []
    #automate_build_new_elements.automate_new_elements(weather_data,'weather_data')     
    if('response' in weather_data.keys()):
        place_holder_variable = True
        if(isinstance(weather_data['response'],dict)):
            place_holder_variable = True
            if('features' in weather_data['response'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['response']['features'],dict)):
                    place_holder_variable = True
                    if('hourly' in weather_data['response']['features'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['response']['features']['hourly'],int)):
                            place_holder_variable = True
                        else:
                            weather_data['response']['features']['hourly'] = ''
                            errors.append('hourly not int')
                    else:
                        errors.append('hourly not present')
                        weather_data['response']['features']['hourly'] = ''
                else:
                    weather_data['response']['features'] = ''
                    errors.append('features not dict')
            else:
                errors.append('features not present')
                weather_data['response']['features'] = ''
            if('version' in weather_data['response'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['response']['version'],str)):
                    place_holder_variable = True
                else:
                    weather_data['response']['version'] = ''
                    errors.append(False,'version not str')
            else:
                errors.append('version not present')
                weather_data['response']['version'] = ''
            if('termsofService' in weather_data['response'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['response']['termsofService'],str)):
                    place_holder_variable = True
                else:
                    weather_data['response']['termsofService'] = ''
                    errors.append(False,'termsofService not str')
            else:
                errors.append('termsofService not present')
                weather_data['response']['termsofService'] = ''
        else:
            weather_data['response'] = ''
            errors.append('response not dict')
    else:
        errors.append('response not present')
        weather_data['response'] = ''
    if('hourly_forecast' in weather_data.keys()):
        place_holder_variable = True
        for i in range(0,len(weather_data['hourly_forecast'])):
            if('dewpoint' in weather_data['hourly_forecast'][i].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['hourly_forecast'][i]['dewpoint'],dict)):
                    place_holder_variable = True
                    if('metric' in weather_data['hourly_forecast'][i]['dewpoint'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['dewpoint']['metric'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['dewpoint']['metric'] = ''
                            errors.append(False,'metric not str')
                    else:
                        errors.append('metric not present')
                        weather_data['hourly_forecast'][i]['dewpoint']['metric'] = ''
                    if('english' in weather_data['hourly_forecast'][i]['dewpoint'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['dewpoint']['english'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['dewpoint']['english'] = ''
                            errors.append(False,'english not str')
                    else:
                        errors.append('english not present')
                        weather_data['hourly_forecast'][i]['dewpoint']['english'] = ''
                else:
                    weather_data['hourly_forecast'][i]['dewpoint'] = ''
                    errors.append('dewpoint not dict')
            else:
                errors.append('dewpoint not present')
                weather_data['hourly_forecast'][i]['dewpoint'] = ''
            if('snow' in weather_data['hourly_forecast'][i].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['hourly_forecast'][i]['snow'],dict)):
                    place_holder_variable = True
                    if('metric' in weather_data['hourly_forecast'][i]['snow'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['snow']['metric'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['snow']['metric'] = ''
                            errors.append(False,'metric not str')
                    else:
                        errors.append('metric not present')
                        weather_data['hourly_forecast'][i]['snow']['metric'] = ''
                    if('english' in weather_data['hourly_forecast'][i]['snow'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['snow']['english'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['snow']['english'] = ''
                            errors.append(False,'english not str')
                    else:
                        errors.append('english not present')
                        weather_data['hourly_forecast'][i]['snow']['english'] = ''
                else:
                    weather_data['hourly_forecast'][i]['snow'] = ''
                    errors.append('snow not dict')
            else:
                errors.append('snow not present')
                weather_data['hourly_forecast'][i]['snow'] = ''
            if('humidity' in weather_data['hourly_forecast'][i].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['hourly_forecast'][i]['humidity'],str)):
                    place_holder_variable = True
                else:
                    weather_data['hourly_forecast'][i]['humidity'] = ''
                    errors.append(False,'humidity not str')
            else:
                errors.append('humidity not present')
                weather_data['hourly_forecast'][i]['humidity'] = ''
            if('heatindex' in weather_data['hourly_forecast'][i].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['hourly_forecast'][i]['heatindex'],dict)):
                    place_holder_variable = True
                    if('metric' in weather_data['hourly_forecast'][i]['heatindex'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['heatindex']['metric'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['heatindex']['metric'] = ''
                            errors.append(False,'metric not str')
                    else:
                        errors.append('metric not present')
                        weather_data['hourly_forecast'][i]['heatindex']['metric'] = ''
                    if('english' in weather_data['hourly_forecast'][i]['heatindex'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['heatindex']['english'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['heatindex']['english'] = ''
                            errors.append(False,'english not str')
                    else:
                        errors.append('english not present')
                        weather_data['hourly_forecast'][i]['heatindex']['english'] = ''
                else:
                    weather_data['hourly_forecast'][i]['heatindex'] = ''
                    errors.append('heatindex not dict')
            else:
                errors.append('heatindex not present')
                weather_data['hourly_forecast'][i]['heatindex'] = ''
            if('wx' in weather_data['hourly_forecast'][i].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['hourly_forecast'][i]['wx'],str)):
                    place_holder_variable = True
                else:
                    weather_data['hourly_forecast'][i]['wx'] = ''
                    errors.append(False,'wx not str')
            else:
                errors.append('wx not present')
                weather_data['hourly_forecast'][i]['wx'] = ''
            if('qpf' in weather_data['hourly_forecast'][i].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['hourly_forecast'][i]['qpf'],dict)):
                    place_holder_variable = True
                    if('metric' in weather_data['hourly_forecast'][i]['qpf'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['qpf']['metric'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['qpf']['metric'] = ''
                            errors.append(False,'metric not str')
                    else:
                        errors.append('metric not present')
                        weather_data['hourly_forecast'][i]['qpf']['metric'] = ''
                    if('english' in weather_data['hourly_forecast'][i]['qpf'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['qpf']['english'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['qpf']['english'] = ''
                            errors.append(False,'english not str')
                    else:
                        errors.append('english not present')
                        weather_data['hourly_forecast'][i]['qpf']['english'] = ''
                else:
                    weather_data['hourly_forecast'][i]['qpf'] = ''
                    errors.append('qpf not dict')
            else:
                errors.append('qpf not present')
                weather_data['hourly_forecast'][i]['qpf'] = ''
            if('icon_url' in weather_data['hourly_forecast'][i].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['hourly_forecast'][i]['icon_url'],str)):
                    place_holder_variable = True
                else:
                    weather_data['hourly_forecast'][i]['icon_url'] = ''
                    errors.append(False,'icon_url not str')
            else:
                errors.append('icon_url not present')
                weather_data['hourly_forecast'][i]['icon_url'] = ''
            if('temp' in weather_data['hourly_forecast'][i].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['hourly_forecast'][i]['temp'],dict)):
                    place_holder_variable = True
                    if('metric' in weather_data['hourly_forecast'][i]['temp'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['temp']['metric'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['temp']['metric'] = ''
                            errors.append(False,'metric not str')
                    else:
                        errors.append('metric not present')
                        weather_data['hourly_forecast'][i]['temp']['metric'] = ''
                    if('english' in weather_data['hourly_forecast'][i]['temp'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['temp']['english'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['temp']['english'] = ''
                            errors.append(False,'english not str')
                    else:
                        errors.append('english not present')
                        weather_data['hourly_forecast'][i]['temp']['english'] = ''
                else:
                    weather_data['hourly_forecast'][i]['temp'] = ''
                    errors.append('temp not dict')
            else:
                errors.append('temp not present')
                weather_data['hourly_forecast'][i]['temp'] = ''
            if('FCTTIME' in weather_data['hourly_forecast'][i].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME'],dict)):
                    place_holder_variable = True
                    if('sec' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['sec'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['sec'] = ''
                            errors.append(False,'sec not str')
                    else:
                        errors.append('sec not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['sec'] = ''
                    if('mon_padded' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['mon_padded'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['mon_padded'] = ''
                            errors.append(False,'mon_padded not str')
                    else:
                        errors.append('mon_padded not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['mon_padded'] = ''
                    if('min_unpadded' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['min_unpadded'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['min_unpadded'] = ''
                            errors.append(False,'min_unpadded not str')
                    else:
                        errors.append('min_unpadded not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['min_unpadded'] = ''
                    if('UTCDATE' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['UTCDATE'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['UTCDATE'] = ''
                            errors.append(False,'UTCDATE not str')
                    else:
                        errors.append('UTCDATE not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['UTCDATE'] = ''
                    if('weekday_name_abbrev' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['weekday_name_abbrev'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['weekday_name_abbrev'] = ''
                            errors.append(False,'weekday_name_abbrev not str')
                    else:
                        errors.append('weekday_name_abbrev not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['weekday_name_abbrev'] = ''
                    if('mon' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['mon'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['mon'] = ''
                            errors.append(False,'mon not str')
                    else:
                        errors.append('mon not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['mon'] = ''
                    if('ampm' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['ampm'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['ampm'] = ''
                            errors.append(False,'ampm not str')
                    else:
                        errors.append('ampm not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['ampm'] = ''
                    if('weekday_name' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['weekday_name'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['weekday_name'] = ''
                            errors.append(False,'weekday_name not str')
                    else:
                        errors.append('weekday_name not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['weekday_name'] = ''
                    if('hour_padded' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['hour_padded'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['hour_padded'] = ''
                            errors.append(False,'hour_padded not str')
                    else:
                        errors.append('hour_padded not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['hour_padded'] = ''
                    if('min' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['min'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['min'] = ''
                            errors.append(False,'min not str')
                    else:
                        errors.append('min not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['min'] = ''
                    if('yday' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['yday'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['yday'] = ''
                            errors.append(False,'yday not str')
                    else:
                        errors.append('yday not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['yday'] = ''
                    if('epoch' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['epoch'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['epoch'] = ''
                            errors.append(False,'epoch not str')
                    else:
                        errors.append('epoch not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['epoch'] = ''
                    if('hour' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['hour'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['hour'] = ''
                            errors.append(False,'hour not str')
                    else:
                        errors.append('hour not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['hour'] = ''
                    if('year' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['year'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['year'] = ''
                            errors.append(False,'year not str')
                    else:
                        errors.append('year not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['year'] = ''
                    if('month_name_abbrev' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['month_name_abbrev'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['month_name_abbrev'] = ''
                            errors.append(False,'month_name_abbrev not str')
                    else:
                        errors.append('month_name_abbrev not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['month_name_abbrev'] = ''
                    if('tz' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['tz'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['tz'] = ''
                            errors.append(False,'tz not str')
                    else:
                        errors.append('tz not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['tz'] = ''
                    if('age' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['age'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['age'] = ''
                            errors.append(False,'age not str')
                    else:
                        errors.append('age not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['age'] = ''
                    if('mon_abbrev' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['mon_abbrev'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['mon_abbrev'] = ''
                            errors.append(False,'mon_abbrev not str')
                    else:
                        errors.append('mon_abbrev not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['mon_abbrev'] = ''
                    if('weekday_name_night' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['weekday_name_night'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['weekday_name_night'] = ''
                            errors.append(False,'weekday_name_night not str')
                    else:
                        errors.append('weekday_name_night not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['weekday_name_night'] = ''
                    if('mday_padded' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['mday_padded'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['mday_padded'] = ''
                            errors.append(False,'mday_padded not str')
                    else:
                        errors.append('mday_padded not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['mday_padded'] = ''
                    if('pretty' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['pretty'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['pretty'] = ''
                            errors.append(False,'pretty not str')
                    else:
                        errors.append('pretty not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['pretty'] = ''
                    if('month_name' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['month_name'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['month_name'] = ''
                            errors.append(False,'month_name not str')
                    else:
                        errors.append('month_name not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['month_name'] = ''
                    if('isdst' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['isdst'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['isdst'] = ''
                            errors.append(False,'isdst not str')
                    else:
                        errors.append('isdst not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['isdst'] = ''
                    if('weekday_name_night_unlang' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['weekday_name_night_unlang'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['weekday_name_night_unlang'] = ''
                            errors.append(False,'weekday_name_night_unlang not str')
                    else:
                        errors.append('weekday_name_night_unlang not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['weekday_name_night_unlang'] = ''
                    if('civil' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['civil'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['civil'] = ''
                            errors.append(False,'civil not str')
                    else:
                        errors.append('civil not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['civil'] = ''
                    if('mday' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['mday'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['mday'] = ''
                            errors.append(False,'mday not str')
                    else:
                        errors.append('mday not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['mday'] = ''
                    if('weekday_name_unlang' in weather_data['hourly_forecast'][i]['FCTTIME'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['FCTTIME']['weekday_name_unlang'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['FCTTIME']['weekday_name_unlang'] = ''
                            errors.append(False,'weekday_name_unlang not str')
                    else:
                        errors.append('weekday_name_unlang not present')
                        weather_data['hourly_forecast'][i]['FCTTIME']['weekday_name_unlang'] = ''
                else:
                    weather_data['hourly_forecast'][i]['FCTTIME'] = ''
                    errors.append('FCTTIME not dict')
            else:
                errors.append('FCTTIME not present')
                weather_data['hourly_forecast'][i]['FCTTIME'] = ''
            if('uvi' in weather_data['hourly_forecast'][i].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['hourly_forecast'][i]['uvi'],str)):
                    place_holder_variable = True
                else:
                    weather_data['hourly_forecast'][i]['uvi'] = ''
                    errors.append(False,'uvi not str')
            else:
                errors.append('uvi not present')
                weather_data['hourly_forecast'][i]['uvi'] = ''
            if('wspd' in weather_data['hourly_forecast'][i].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['hourly_forecast'][i]['wspd'],dict)):
                    place_holder_variable = True
                    if('metric' in weather_data['hourly_forecast'][i]['wspd'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['wspd']['metric'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['wspd']['metric'] = ''
                            errors.append(False,'metric not str')
                    else:
                        errors.append('metric not present')
                        weather_data['hourly_forecast'][i]['wspd']['metric'] = ''
                    if('english' in weather_data['hourly_forecast'][i]['wspd'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['wspd']['english'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['wspd']['english'] = ''
                            errors.append(False,'english not str')
                    else:
                        errors.append('english not present')
                        weather_data['hourly_forecast'][i]['wspd']['english'] = ''
                else:
                    weather_data['hourly_forecast'][i]['wspd'] = ''
                    errors.append('wspd not dict')
            else:
                errors.append('wspd not present')
                weather_data['hourly_forecast'][i]['wspd'] = ''
            if('fctcode' in weather_data['hourly_forecast'][i].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['hourly_forecast'][i]['fctcode'],str)):
                    place_holder_variable = True
                else:
                    weather_data['hourly_forecast'][i]['fctcode'] = ''
                    errors.append(False,'fctcode not str')
            else:
                errors.append('fctcode not present')
                weather_data['hourly_forecast'][i]['fctcode'] = ''
            if('condition' in weather_data['hourly_forecast'][i].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['hourly_forecast'][i]['condition'],str)):
                    place_holder_variable = True
                else:
                    weather_data['hourly_forecast'][i]['condition'] = ''
                    errors.append(False,'condition not str')
            else:
                errors.append('condition not present')
                weather_data['hourly_forecast'][i]['condition'] = ''
            if('sky' in weather_data['hourly_forecast'][i].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['hourly_forecast'][i]['sky'],str)):
                    place_holder_variable = True
                else:
                    weather_data['hourly_forecast'][i]['sky'] = ''
                    errors.append(False,'sky not str')
            else:
                errors.append('sky not present')
                weather_data['hourly_forecast'][i]['sky'] = ''
            if('wdir' in weather_data['hourly_forecast'][i].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['hourly_forecast'][i]['wdir'],dict)):
                    place_holder_variable = True
                    if('dir' in weather_data['hourly_forecast'][i]['wdir'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['wdir']['dir'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['wdir']['dir'] = ''
                            errors.append(False,'dir not str')
                    else:
                        errors.append('dir not present')
                        weather_data['hourly_forecast'][i]['wdir']['dir'] = ''
                    if('degrees' in weather_data['hourly_forecast'][i]['wdir'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['wdir']['degrees'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['wdir']['degrees'] = ''
                            errors.append(False,'degrees not str')
                    else:
                        errors.append('degrees not present')
                        weather_data['hourly_forecast'][i]['wdir']['degrees'] = ''
                else:
                    weather_data['hourly_forecast'][i]['wdir'] = ''
                    errors.append('wdir not dict')
            else:
                errors.append('wdir not present')
                weather_data['hourly_forecast'][i]['wdir'] = ''
            if('icon' in weather_data['hourly_forecast'][i].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['hourly_forecast'][i]['icon'],str)):
                    place_holder_variable = True
                else:
                    weather_data['hourly_forecast'][i]['icon'] = ''
                    errors.append(False,'icon not str')
            else:
                errors.append('icon not present')
                weather_data['hourly_forecast'][i]['icon'] = ''
            if('windchill' in weather_data['hourly_forecast'][i].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['hourly_forecast'][i]['windchill'],dict)):
                    place_holder_variable = True
                    if('metric' in weather_data['hourly_forecast'][i]['windchill'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['windchill']['metric'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['windchill']['metric'] = ''
                            errors.append(False,'metric not str')
                    else:
                        errors.append('metric not present')
                        weather_data['hourly_forecast'][i]['windchill']['metric'] = ''
                    if('english' in weather_data['hourly_forecast'][i]['windchill'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['windchill']['english'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['windchill']['english'] = ''
                            errors.append(False,'english not str')
                    else:
                        errors.append('english not present')
                        weather_data['hourly_forecast'][i]['windchill']['english'] = ''
                else:
                    weather_data['hourly_forecast'][i]['windchill'] = ''
                    errors.append('windchill not dict')
            else:
                errors.append('windchill not present')
                weather_data['hourly_forecast'][i]['windchill'] = ''
            if('feelslike' in weather_data['hourly_forecast'][i].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['hourly_forecast'][i]['feelslike'],dict)):
                    place_holder_variable = True
                    if('metric' in weather_data['hourly_forecast'][i]['feelslike'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['feelslike']['metric'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['feelslike']['metric'] = ''
                            errors.append(False,'metric not str')
                    else:
                        errors.append('metric not present')
                        weather_data['hourly_forecast'][i]['feelslike']['metric'] = ''
                    if('english' in weather_data['hourly_forecast'][i]['feelslike'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['feelslike']['english'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['feelslike']['english'] = ''
                            errors.append(False,'english not str')
                    else:
                        errors.append('english not present')
                        weather_data['hourly_forecast'][i]['feelslike']['english'] = ''
                else:
                    weather_data['hourly_forecast'][i]['feelslike'] = ''
                    errors.append('feelslike not dict')
            else:
                errors.append('feelslike not present')
                weather_data['hourly_forecast'][i]['feelslike'] = ''
            if('mslp' in weather_data['hourly_forecast'][i].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['hourly_forecast'][i]['mslp'],dict)):
                    place_holder_variable = True
                    if('metric' in weather_data['hourly_forecast'][i]['mslp'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['mslp']['metric'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['mslp']['metric'] = ''
                            errors.append(False,'metric not str')
                    else:
                        errors.append('metric not present')
                        weather_data['hourly_forecast'][i]['mslp']['metric'] = ''
                    if('english' in weather_data['hourly_forecast'][i]['mslp'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['hourly_forecast'][i]['mslp']['english'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['hourly_forecast'][i]['mslp']['english'] = ''
                            errors.append(False,'english not str')
                    else:
                        errors.append('english not present')
                        weather_data['hourly_forecast'][i]['mslp']['english'] = ''
                else:
                    weather_data['hourly_forecast'][i]['mslp'] = ''
                    errors.append('mslp not dict')
            else:
                errors.append('mslp not present')
                weather_data['hourly_forecast'][i]['mslp'] = ''
            if('pop' in weather_data['hourly_forecast'][i].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['hourly_forecast'][i]['pop'],str)):
                    place_holder_variable = True
                else:
                    weather_data['hourly_forecast'][i]['pop'] = ''
                    errors.append(False,'pop not str')
            else:
                errors.append('pop not present')
                weather_data['hourly_forecast'][i]['pop'] = ''
    else:
        errors.append('hourly_forecast not present')
        weather_data['hourly_forecast'] = ''




    for key in weather_data:
        if key not in ['hourly_forecast', 'response']:
            errors.append('new key '+key+' in weather_data')
    for i in range(0,len(weather_data['hourly_forecast'])):
        for key_0 in weather_data['hourly_forecast'][i]:
            if key_0 not in ['icon', 'icon_url', 'feelslike', 'windchill', 'uvi', 'wspd', 'qpf', 'FCTTIME', 'fctcode', 'humidity', 'wx', 'dewpoint', 'mslp', 'sky', 'snow', 'temp', 'pop', 'condition', 'wdir', 'heatindex']:
                errors.append('new key '+key_1+' in weather_data[hourly_forecast][%s][feelslike]'%str(i))            
        for key_1 in weather_data['hourly_forecast'][i]['feelslike']:
            if key_1 not in ['metric', 'english']:
                    errors.append('new key '+key_1+' in weather_data[hourly_forecast][%s][feelslike]')%str(i)
        for key_1 in weather_data['hourly_forecast'][i]['wspd']:
            if key_1 not in ['metric', 'english']:
                    errors.append('new key '+key_1+' in weather_data[hourly_forecast][%s][wspd]')%str(i)
        for key_1 in weather_data['hourly_forecast'][i]['mslp']:
            if key_1 not in ['metric', 'english']:
                    errors.append('new key '+key_1+' in weather_data[hourly_forecast][%s][mslp]')%str(i)
        for key_1 in weather_data['hourly_forecast'][i]['qpf']:
            if key_1 not in ['metric', 'english']:
                    errors.append('new key '+key_1+' in weather_data[hourly_forecast][%s][qpf]')%str(i)
        for key_1 in weather_data['hourly_forecast'][i]['temp']:
            if key_1 not in ['metric', 'english']:
                    errors.append('new key '+key_1+' in weather_data[hourly_forecast][i][temp]')%str(i)
        for key_1 in weather_data['hourly_forecast'][i]['windchill']:
            if key_1 not in ['metric', 'english']:
                    errors.append('new key '+key_1+' in weather_data[hourly_forecast][%s][windchill]')%str(i)
        for key_1 in weather_data['hourly_forecast'][i]['heatindex']:
            if key_1 not in ['metric', 'english']:
                    errors.append('new key '+key_1+' in weather_data[hourly_forecast][%s][heatindex]')%str(i)
        for key_1 in weather_data['hourly_forecast'][i]['wdir']:
            if key_1 not in ['degrees', 'dir']:
                    errors.append('new key '+key_1+' in weather_data[hourly_forecast][%s][wdir]')%str(i)
        for key_1 in weather_data['hourly_forecast'][i]['dewpoint']:
            if key_1 not in ['metric', 'english']:
                    errors.append('new key '+key_1+' in weather_data[hourly_forecast][%s][dewpoint]')%str(i)
        for key_1 in weather_data['hourly_forecast'][i]['snow']:
            if key_1 not in ['metric', 'english']:
                    errors.append('new key '+key_1+' in weather_data[hourly_forecast][%s][snow]')%str(i)
        for key_1 in weather_data['hourly_forecast'][i]['FCTTIME']:
            if key_1 not in ['month_name_abbrev', 'weekday_name_unlang', 'pretty', 'min_unpadded', 'tz', 'mday', 'civil', 'mon', 'hour', 'month_name', 'epoch', 'mday_padded', 'ampm', 'yday', 'age', 'mon_abbrev', 'weekday_name_abbrev', 'year', 'sec', 'UTCDATE', 'weekday_name_night_unlang', 'mon_padded', 'isdst', 'weekday_name_night', 'min', 'hour_padded', 'weekday_name']:
                    errors.append('new key '+key_1+' in weather_data[hourly_forecast][%s][FCTTIME]')%str(i)
    for key_0 in weather_data['response']:
        if key_0 not in ['termsofService', 'features', 'version']:
            errors.append('new key '+key_0+' in weather_data[response]')
        for key_1 in weather_data['response']['features']:
            if key_1 not in ['hourly']:
                errors.append('new key '+key_1+' in weather_data[response][features]')
                        
    return(True,weather_data,errors)
