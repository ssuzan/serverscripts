#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import os
import sys
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports
import sendemail
email_file = workspace_environ+'/serverscripts/globals/emails'
import fetch_sensors
import store_sensors
import clean_sensors
import getopt
import smtplib
from email.mime.text import MIMEText
from automation_building_scripts import automate_build_clean

download_url = 'http://bugatti.nvfast.org/realtimexml/FMSRealtimeData.xml'
save_path = workspace_environ+'/DB_backups/data_from_sensors'
dump_output_to = "FIFO"
email_file = os.environ['WORKSPACE']+'/serverscripts/globals/emails'


    
def call_all():
    curr_t = universal_imports.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
    print("call_all_sensors() running at time: "+curr_t)  
    returned = fetch_sensors.fetch(download_url)
    if ('error' in returned.keys()):
        text = open(email_file,'r')
        email_string = text.read()
        email_json = universal_imports.json.loads(email_string)                  
        sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'error', message = "sensors "+returned['error'], login = email_json['email'], password = email_json['password'])                  
    else:
        clean_return = clean_sensors.clean(returned)
        if (clean_return[0] == True):
            store_sensors.store(returned,clean_return[2],clean_return[1],save_path,dump_output_to,'sensor_data')
        else:
            text = open(email_file,'r')
            email_string = text.read()
            email_json = universal_imports.json.loads(email_string)                  
            sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'error', message = "clean_sensors "+clean_return[1], login = email_json['email'], password = email_json['password'])  
    print('call_all_wz.py completed successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
        
    
call_all()
