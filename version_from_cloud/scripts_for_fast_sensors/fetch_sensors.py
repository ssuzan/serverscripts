#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports
#import jsonlib_python3
import xmltodict
import requests

def fetch(url_sensors):
    try:
        #gets json from url
        r = requests.get(url_sensors)
        r = xmltodict.parse(r.content)
        r = universal_imports.json.dumps(r, sort_keys=True, indent=4, separators=(',', ': '))
        r = universal_imports.json.loads(r)
    except requests.exceptions.ConnectionError:
        r = universal_imports.json.loads('{"error" "requests.exceptions.ConnectionError"}')  
    return r    