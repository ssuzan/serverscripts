#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: json containing data from waze database
#output: properly formatted json containing data from waze database
from datetime import datetime
import time
import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts_production/globals')
import universal_imports
import json, requests
from email.mime.text import MIMEText


email_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/emails'


def clean_incident(incident_clean):
    if('signId' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['signId'],str)):
            place_holder_variable = True
        else:
            print(incident_clean['signId'])
            return(False,'signId not str')
    else:
        incident_clean['signId'] = ''
    if('currentStatus' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['currentStatus'],dict)):
            place_holder_variable = True
            if('message' in incident_clean['currentStatus'].keys()):
                place_holder_variable = True
                if(isinstance(incident_clean['currentStatus']['message'],dict)):
                    place_holder_variable = True
                    if('messageText' in incident_clean['currentStatus']['message'].keys()):
                        place_holder_variable = True
                        if(isinstance(incident_clean['currentStatus']['message']['messageText'],str) or incident_clean['currentStatus']['message']['messageText'] == None):
                            place_holder_variable = True
                        else:
                            print(incident_clean['currentStatus']['message']['messageText'])
                            return(False,'messageText not str')
                    else:
                        incident_clean['currentStatus']['message']['messageText'] = ''
                    if('messageBeacon' in incident_clean['currentStatus']['message'].keys()):
                        place_holder_variable = True
                        if(isinstance(incident_clean['currentStatus']['message']['messageBeacon'],str)):
                            place_holder_variable = True
                        else:
                            print(incident_clean['currentStatus']['message']['messageBeacon'])
                            return(False,'messageBeacon not str')
                    else:
                        incident_clean['currentStatus']['message']['messageBeacon'] = ''                
                else:
                    print(incident_clean['currentStatus']['message'])
                    return(False,'message not dict')        
            else:
                incident_clean['currentStatus']['message'] = ''              
            if('status' in incident_clean['currentStatus'].keys()):
                place_holder_variable = True
                if(isinstance(incident_clean['currentStatus']['status'],str)):
                    place_holder_variable = True
                else:
                    print(incident_clean['currentStatus']['status'])
                    return(False,'status not str')
            else:
                incident_clean['currentStatus']['status'] = ''
        else:
            print(incident_clean['currentStatus'])
            return(False,'currentStatus not dict')                 
                     
    else:
        incident_clean['currentStatus'] = '' 
    return(True, incident_clean)

def clean(json_clean):
    place = 1
    compromised_ids = []
    #compomised_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110,111]    
    if('DMSDevicesStatus' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['DMSDevicesStatus'],dict)):
            place_holder_variable = True
            if('DMSDeviceStatus' in json_clean['DMSDevicesStatus'].keys()):
                place_holder_variable = True
                if(isinstance(json_clean['DMSDevicesStatus']['DMSDeviceStatus'],list)):
                    place_holder_variable = True
                    for i in range(0,len(json_clean['DMSDevicesStatus']['DMSDeviceStatus'])):
                        returned = clean_incident(json_clean['DMSDevicesStatus']['DMSDeviceStatus'][i])
                        if returned[0] == True:
                            json_clean['DMSDevicesStatus']['DMSDeviceStatus'][i] = returned[1]
                        else:
                            #if individual instance has error
                            universal_imports.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = "error", message = str("dms clean device "+str(i+1)+" "+returned[1]), login = email_json["email"], password = email_json["password"])    
                            compromised_ids.append(int(json_clean['DMSDevicesStatus']['DMSDeviceStatus'][i]['signId']))
                else:
                    print(json_clean['DMSDevicesStatus']['DMSDeviceStatus'])
                    return(False,'DMSDeviceStatus not list')
            else:
                json_clean['DMSDevicesStatus']['DMSDeviceStatus'] = ''            
        else:
            print(json_clean['DMSDevicesStatus'])
            return(False,'DMSDevicesStatus not dict')
        
    else:
        json_clean['DMSDevicesStatus'] = ''
        return(True,'DMSDevicesStatus not present',json_clean,compomised_ids)    
    return(True,'DMSDevicesStatus present',json_clean,compromised_ids)
