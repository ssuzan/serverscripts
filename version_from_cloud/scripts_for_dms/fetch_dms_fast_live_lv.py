#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: waze database https request (url) 
#output: json of data given by https request or json containing error message
import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts_production/globals')
import universal_imports
from datetime import datetime
import time
import requests
import xmltodict
import collections


def downloadWaze(url):  
    try:
        #gets json from url
        r = requests.get(url)
        try:
            r = xmltodict.parse(r.content)
            r = universal_imports.json.dumps(r, sort_keys=True, indent=4, separators=(',', ': '))
            r = universal_imports.json.loads(r)
        except:
            r = universal_imports.json.loads('{"error" : "fetch_dms_live_lv failure to convert to json"}')
    except requests.exceptions.ConnectionError:
        r = json.loads('{"error" : "fetch_dms_live_lv requests.exceptions.ConnectionError"}')  
        r = universal_imports.json.loads('{"error" : "fetch_dms_live_lv requests.exceptions.ConnectionError"}')  
    except:
        r = universal_imports.json.loads('{"error" : "fetch_dms_live_lv error"}')  
    return r
     
