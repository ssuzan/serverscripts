#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: json input and file directory path
#output: json stored to directory
from datetime import datetime
import time
import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts_production/globals')
import universal_imports
import json, requests
import push_to_fifo
import sendemail
from email.mime.text import MIMEText


dms_type_name = 'dms_fast_live_lv'
dms_tt_type_name = 'dms_tt_fast_live_lv'

email_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/emails'

def store_original(original_store_json, save_path,dump_to_output):
     now = str(datetime.utcnow())
     now = now.split(" ")
     now[0] = now[0].split("-")
     now[1] = now[1].split(":")
     filename = "data_from_dms_"+now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1] 
     DeviceFile = open(save_path+"/database_original/"+filename+'.json', 'w')
     DeviceFile.write(json.dumps(original_store_json, sort_keys=True, indent=4, separators=(',', ': ')))  
     DeviceFile.close()


def store(original_store_json,cleaned_store_json, save_path,dump_to_output,compromised_ids):
    #gets date for filename as now
    dms_fast_live_lv = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,60,61,62,63,64,65,66,67,68,73,74,75,76,77,78,83,86,87,88,89,90,93,94,95,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111]
    dms_tt_fast_live_lv = [24,53,59,69,70,71,72,79,80,81,82,84,85,91,92,96]

    now = str(datetime.utcnow())
    now = now.split(" ")
    now[0] = now[0].split("-")
    now[1] = now[1].split(":")
    filename = "data_from_dms_"+now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1] 
    DeviceFile = open(save_path+"/database_original/"+filename+'.json', 'w')
    DeviceFile.write(json.dumps(original_store_json, sort_keys=True, indent=4, separators=(',', ': ')))  
    DeviceFile.close()    
    #saves file
    #if the entire list was damaged:
    if len(compromised_ids) == 111:
        cleaned_store_json['DMSDevicesStatus'] = {}
        cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'] = []
        for i in range(1,111):
            cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'].append({'signId':str(i)})
        for i in range(0,110):
            if int(cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'][i]['signId']) in dms_fast_live_lv:
                i_type = 'dms_fast_live_lv'
            elif int(cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'][i]['signId']) in dms_tt_fast_live_lv:
                i_type = 'dms_tt_fast_live_lv'
            else:
                sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'Error in store_dms_fast_live_lv', message = "The following dms ID is not defined"+int(cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'][i]['signId']), login = email_json["email"], password = email_json["password"])    

            json_struct = {"type":i_type,"error":True,"date_time":now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1] +" UTC","suspicious":False,"data":cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'][i]}
            json_struct = json_struct
            json_save = universal_imports.json.dumps(json_struct)            
            if dump_to_output == 'FIFO':
                push_to_fifo.push_to_fifo(json_save,"fifo_names")
            else:
                DeviceFile = open(save_path+"/database/"+filename+"_"+cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'][i]['signId']+'.json', 'w')
                DeviceFile.write(json.dumps(json_struct, sort_keys=True, indent=4, separators=(',', ': ')))  
                DeviceFile.close()  
    #if some of the list is good      
    else:
        for i in range(0,len(cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'])):
            i_type = ''
            error = False
            if i+1 in compromised_ids:
                cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'][i] = {'signId':str(i+1)}
                error = True
            if int(cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'][i]['signId']) in dms_fast_live_lv:
                i_type = 'dms_fast_live_lv'
            elif int(cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'][i]['signId']) in dms_tt_fast_live_lv:
                i_type = 'dms_tt_fast_live_lv'
            else:
                sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'Error in store_dms_fast_live_lv', message = "The following dms ID is not defined"+int(cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'][i]['signId']), login = email_json["email"], password = email_json["password"])    


            json_struct = {"type":i_type,"error":error,"date_time":now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1] +" UTC","suspicious":False,"data":cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'][i]}
            json_struct = json_struct
            json_save = universal_imports.json.dumps(json_struct)            
            if dump_to_output == 'FIFO':
                push_to_fifo.push_to_fifo(json_save, "input queue %s"%i_type) 
            else:
                DeviceFile = open(save_path+"/database/"+filename+"_"+cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'][i]['signId']+'.json', 'w')
                DeviceFile.write(json.dumps(json_struct, sort_keys=True, indent=4, separators=(',', ': ')))  
                DeviceFile.close()
