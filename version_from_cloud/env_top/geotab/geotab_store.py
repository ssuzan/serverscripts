#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import json

from serverscripts_production.mandark.geotab.geotab_clean import geotab_clean

from serverscripts_production.mandark.globals.sendemail import sendemail


def geotab_store(curr_t,result, is_dev, email_json, save_path, original_backup_path,
                 rabbit_connection, queue_name):
    if not is_dev:
        Filetxt_original = open(original_backup_path+"geotab_fsp_location_live_lv_"+str(curr_t).replace("-","_")+'.txt', 'w')
        Filetxt_original.write(str(result))
        Filetxt_original.close()
    #parsing and saving json file:
    for i in range(0,len(result)):        
        if(geotab_clean(result[0])[0] == True):
            json_struct = {"type":"geotab_fsp_location_live_lv","date_time":curr_t+" UTC","suspicious":False,"data":{"id":result[i]["device"]["id"],"coordinates":(result[i]['latitude'],result[i]['longitude']),"bearing":result[i]['bearing']}}
            json_struct = json_struct
            json_save = json.dumps(json_struct)
            if not is_dev:
                Filetxt = open(save_path+"geotab_fsp_location_live_lv_"+str(curr_t).replace("-","_")+"_file_%s"%result[i]['device']['id']+'.json', 'w')
                Filetxt.write(json_save)
                Filetxt.close()
            elif is_dev:
                try:
                    rabbit_connection.publish(json_save, queue_name)
                except:
                    sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'error', message = 'geotab:fifo error '+geotab_clean(result[0])[1]+" file "+str(i), login = email_json['email'], password = email_json['password'])
                
        elif(geotab_clean(result[0])[0] == False):
            sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'error', message = 'file not clean '+geotab_clean(result[0])[1]+" file "+str(i), login = email_json['email'], password = email_json['password'])
        else:
            sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'error', message = "file not clean unknown error"+" file "+str(i), login = email_json['email'], password = email_json['password'])
