#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
#sudo pip3 install mygeotab
import json
from datetime import datetime

from serverscripts_production.mandark.geotab.geotab_fetch import geotab_fetch
from serverscripts_production.mandark.geotab.geotab_store import geotab_store

from serverscripts_production.mandark.globals.sendemail import sendemail


def geotab_call_all(is_dev, email_file, save_path, original_backup_path,
                    access_file, rabbit_connection, queue_name):
    """
    Fetches fsp geotab data

    :param bool is_dev: defines if we are in development mode or production
    :param str email_file: path
    :param str save_path: save file backup path
    :param str original_backup_path: save file original backup path
    :param str access_file: the access file path
    :param RabbitAdapter rabbit_connection: the rabbit adapter instance
    :param str queue_name: the rabbit queue name
    """
    text = open(email_file,'r')
    email_string = text.read()
    email_json = json.loads(email_string)
    fetched = geotab_fetch(access_file)
    curr_t = datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")
    if (fetched[0] == True):
        result = fetched[1]
        geotab_store(curr_t, result, is_dev, email_json, save_path, original_backup_path,
                     rabbit_connection, queue_name)
    else:
        if not is_dev:
            sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'error', message = fetched[1], login = email_json["email"], password = email_json["password"])
        return(False)
       

def get_geotab(*args):
    geotab_call_all(args[0], args[1], args[2], args[3], args[4], args[5], args[6])
    print('geotab_call_all.py completed successfuly! time is: ' +
          datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S") + ' UTC')

