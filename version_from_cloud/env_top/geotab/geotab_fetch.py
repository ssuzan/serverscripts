#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import json

import mygeotab


def geotab_fetch(access_file):
    #connecting to geotab and authentication  
    text = open(access_file,'r')
    access_string = text.read()
    access_json = json.loads(access_string)
    try:
        api = mygeotab.API(username=access_json["username"], password=access_json["password"], database=access_json["database"])
    except:
        return(False,'could not access database')
    try:
        api.authenticate()
    except:
        return(False,'could not authenticate')
    try:
        result = api.get('DeviceStatusInfo')
        return(True,result)     
    except:
        return(False,'Could not pull')   
    return(False, 'unknown error in geotab_fetch')
