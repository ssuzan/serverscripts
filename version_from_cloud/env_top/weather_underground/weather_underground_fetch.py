# Ethan Gellman
# esgellman@hotmail.com 954-610-6868
# main file
# required installations:

import requests


def weather_underground_fetch(weather_underground_url, weather_api_string, pws_curr):
    try:
        # gets json from url
        weather_underground_url_complete = weather_underground_url % \
                                  (weather_api_string.strip("\n"), pws_curr)
        r = requests.get(weather_underground_url_complete)
        r = r.json()
        return True, r
    except:
        return False, "false"
