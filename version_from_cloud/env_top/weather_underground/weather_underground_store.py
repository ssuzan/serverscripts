#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import json
import os
from datetime import datetime
from logging import getLogger

from serverscripts_production.mandark.globals.sendemail import sendemail

logger = getLogger(__name__)


def weather_underground_store(filename, r, email_json, save_path, is_dev, rabbit_connection, queue_name):
    # storing source and result
    new_store_json = {'type': 'weather_underground',
                      'date_time': str(datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")).replace(
                          "-", "_") + " UTC", 'suspicious': False, 'data': r}
    if not is_dev:
        lastFiletxt = open(os.path.abspath(save_path) + "/" + filename + '.json', 'w')
        lastFiletxt.write(
            json.dumps(new_store_json, sort_keys=True, indent=4, separators=(',', ': ')))
        lastFiletxt.close()
    elif is_dev:
        try:
            rabbit_connection.publish(new_store_json, queue_name)
        except Exception as e:
            logger.error('failed publishing to ' + queue_name + 'with error:{}'.format(e))
            sendemail(from_addr=email_json["email"], to_addr_list=email_json["send_emails"],
                      cc_addr_list=[''], subject='error',
                      message="weather_underground_error : fifo error", login=email_json["email"],
                      password=email_json["password"])
