# Ethan Gellman
# esgellman@hotmail.com 954-610-6868
# main file
# required installations:
import datetime
import json
from logging import getLogger

from serverscripts_production.mandark.globals.sendemail import sendemail
from serverscripts_production.mandark.weather_underground.weather_underground_clean import \
    weather_underground_clean
from serverscripts_production.mandark.weather_underground.weather_underground_fetch import \
    weather_underground_fetch

from serverscripts_production.mandark.weather_underground.weather_underground_store import \
    weather_underground_store

logger = getLogger(__name__)


def weather_underground_call_all(is_dev, email_file, pws, save_path, weather_underground_url,
                                 weather_underground_file, rabbit_connection, queue_name):
    """
    Fetches weather data

    :param bool is_dev: defines if we are in development mode or production
    :param str email_file: path
    :param str pws: weather password
    :param str save_path: save file backup path
    :param str weather_underground_url: the url used to fetch the data
    :param str weather_underground_file: path
    :param RabbitAdapter rabbit_connection: the rabbit adapter instance
    :param str queue_name: the rabbit queue name
    """
    curr_t = datetime.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")
    curr_t = str(curr_t).replace(":", "_")
    curr_t = str(curr_t).replace(" ", "_")
    curr_t = str(curr_t).replace("-", "_")
    logger.info("weather_underground_call_all() running at time: " + curr_t)
    weather_api_text = open(weather_underground_file, 'r')
    weather_api_string = weather_api_text.read()

    print("weather_underground_call_all() running at time: " + curr_t)
    text = open(email_file, 'r')
    email_string = text.read()
    email_json = json.loads(email_string)

    for i in range(0, len(pws)):
        r_touple = weather_underground_fetch(weather_underground_url, weather_api_string, pws[i])
        if r_touple[0] == True:
            r = r_touple[1]
            # cleaning data sets
            results = weather_underground_clean(r)
            if results[0] == True:
                filename = pws[i] + "_" + curr_t
                weather_underground_store(filename, r, email_json, save_path, is_dev,
                                          rabbit_connection, queue_name)
                print('weather_underground_call_all.py completed successfuly! time is: ' +
                      datetime.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S") + ' UTC')
            else:
                if not is_dev:
                    sendemail(from_addr=email_json["email"], to_addr_list=email_json["send_emails"],
                              cc_addr_list=[''], subject='error',
                              message="weather_underground_error : " + pws[i] + " : " + results[1],
                              login=email_json["email"], password=email_json["password"])
        else:
            if not is_dev:
                sendemail(from_addr=email_json["email"], to_addr_list=email_json["send_emails"],
                          cc_addr_list=[''], subject='error',
                          message="weather_underground_error : requests.exceptions.ConnectionError",
                          login=email_json["email"], password=email_json["password"])


def get_weather(*args):
    weather_underground_call_all(args[0], args[1], args[2], args[3], args[4], args[5], args[6],
                                 args[7])
