#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: json input and file directory path
#output: json stored to directory
import json
import os
from datetime import datetime


def store(store_json, path, full_file_path, type_name, is_dev, rabbit_connection, queue_name):
    #creates folder for files
    if not os.path.exists(path):
        os.makedirs(path)
    save_path = path
    #gets date for filename as now
    now = str(datetime.now())
    now = now.split(" ")
    now[0] = now[0].split("-")
    now[1] = now[1].split(":")
    #print(store_json['jams'])
    curr_t = datetime.now().strftime("%d-%m-%Y %H:%M:%S")
    if not is_dev:
        filename = type_name+"_"+now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1]
        #saves original files
        lastFiletxt = open(path+"/"+full_file_path+"/"+filename+'.json', 'w')
        lastFiletxt.write(json.dumps(store_json, sort_keys=True, indent=4, separators=(',', ': ')))
        lastFiletxt.close()

    #dumps updated files
    if 'jams' in store_json.keys():
        for i in range(0,len(store_json['jams'])):
            filename = type_name+"_"+now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1]+"_incident_"+str(i)
            #saves file
            new_store_json = {'type':type_name,'date_time':curr_t,'suspicious':False,'data':store_json['jams'][i]}
            if not is_dev:
                lastFiletxt = open(save_path+"/"+filename+'.json', 'w')
                lastFiletxt.write(json.dumps(new_store_json, sort_keys=True, indent=4, separators=(',', ': ')))
            elif is_dev:
                rabbit_connection.publish(new_store_json, queue_name)
