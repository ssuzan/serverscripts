# Ethan Gellman
# esgellman@hotmail.com 954-610-6868
# main file
# required installations:
import json
from datetime import datetime

from serverscripts_production.mandark.globals.sendemail import sendemail
from serverscripts_production.mandark.scripts_for_wz_jams.clean_wz_jams import clean
from serverscripts_production.mandark.scripts_for_wz_jams.fetch_wz_jams import downloadWaze

from serverscripts_production.mandark.scripts_for_wz_jams.store_wz_jams import store


def call_all_wz_jams(is_dev, email_file, save_path, full_file_path, type_name,
                     wz_url, rabbit_connection, queue_name):
    """
    Fetches waze traffic jams data

    :param bool is_dev: defines if we are in development mode or production
    :param str email_file: path
    :param str save_path: save file backup path
    :param str wz_url: the url used to fetch the data
    :param RabbitAdapter rabbit_connection: the rabbit adapter instance
    :param str queue_name: the rabbit queue name
    """
    curr_t = datetime.now().strftime("%d-%m-%Y %H:%M:%S")
    print("call_all_wz_jams() running at time: " + curr_t)
    # 1st fetch 2nd clean 3rd store
    y = downloadWaze(wz_url)
    if clean(y)[0] == True:
        store(y, save_path, full_file_path, type_name, is_dev, rabbit_connection, queue_name)
    else:
        if not is_dev:
            text = open(email_file, 'r')
            string = text.read()
            email_json = json.loads(string)
            sendemail(from_addr=email_json["email"], to_addr_list=email_json["send_emails"],
                      cc_addr_list=[''], subject='error', message=clean(y)[1],
                      login=email_json["email"], password=email_json["password"])


def get_waze_traffic_jam(*args):
    call_all_wz_jams(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7])
