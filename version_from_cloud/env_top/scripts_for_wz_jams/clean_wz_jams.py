#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: json containing data from waze database
#output: properly formatted json containing data from waze database


def clean_incident(incident):
    if('type' in incident.keys()):
        if(isinstance(incident['type'],str)):
            place_holder_variable = True
        else:
            print(incident['type'])
            return(False,'type not str')
    else:
        return(False,'type not present')   
    if('subtype' in incident.keys()):
        if(isinstance(incident['subtype'],str)):
            place_holder_variable = True
        else:
            print(incident['subtype'])
            return(False,'subtype not str')
    else:
        return(False,'subtype not present')    
    if('reliability' in incident.keys()):
        if(isinstance(incident['reliability'],int)):
            place_holder_variable = True
        else:
            print(incident['reliability'])
            return(False,'reliability not int')
    else:
        return(False,'confidence not present') 
    if('confidence' in incident.keys()):
        if(isinstance(incident['confidence'],int)):
            place_holder_variable = True
        else:
            print(incident['confidence'])
            return(False,'confidence not int')
    else:
        return(False,'confidence not present')     
    if('street' in incident.keys()):
        if(isinstance(incident['street'],str)):
            place_holder_variable = True
        else:
            print(incident['street'])
            return(False,'street not str')
        
    if('uuid' in incident.keys()):
        if(isinstance(incident['uuid'],str)):
            place_holder_variable = True
        else:
            print(incident['uuid'])
            return(False,'uuid not str')
    else:
        return(False,'uuid not present')     
    if('magvar' in incident.keys()):
        if(isinstance(incident['magvar'],int)):
            place_holder_variable = True
        else:
            print(incident['magvar'])
            return(False,'magvar not int')
    else:
        return(False,'magvar not present')    
    if('country' in incident.keys()):
        if(isinstance(incident['country'],str)):
            place_holder_variable = True
        else:
            print(incident['country'])
            return(False,'country not str')
    else:
        return(False,'country not present')       
    return (True, 'no errors')
def clean(json_clean):
    if('endTimeMillis' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['endTimeMillis'],int)):
            place_holder_variable = True
        else:
            print(json_clean['endTimeMillis'])
            return(False,'endTimeMillis not int')
    else:
        return(False,'endTimeMillis not present')
    if('jams' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['jams'],list)):
            place_holder_variable = True
        else:
            print(json_clean['jams'])
            return(False,'jams not list')
    else:
        return(False,'jams not present')
    if('startTimeMillis' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['startTimeMillis'],int)):
            place_holder_variable = True
        else:
            print(json_clean['startTimeMillis'])
            return(False,'startTimeMillis not int')
    else:
        return(False,'startTimeMillis not present')
    if('jams' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['jams'],list)):
            place_holder_variable = True
            for i in range(0,len(json_clean['jams'])):
                if('uuid' in json_clean['jams'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(json_clean['jams'][i]['uuid'],str)):
                        place_holder_variable = True
                    else:
                        print(json_clean['jams'][i]['uuid'])
                        return(False,'uuid not str')
                else:
                    return(False,'uuid not present')
                if('roadType' in json_clean['jams'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(json_clean['jams'][i]['roadType'],int)):
                        place_holder_variable = True
                    else:
                        print(json_clean['jams'][i]['roadType'])
                        return(False,'roadType not int')
                if('pubMillis' in json_clean['jams'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(json_clean['jams'][i]['pubMillis'],int)):
                        place_holder_variable = True
                    else:
                        print(json_clean['jams'][i]['pubMillis'])
                        return(False,'pubMillis not int')
                else:
                    return(False,'pubMillis not present')
                if('confidence' in json_clean['jams'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(json_clean['jams'][i]['confidence'],int)):
                        place_holder_variable = True
                    else:
                        print(json_clean['jams'][i]['confidence'])
                        return(False,'confidence not int')
                else:
                    return(False,'confidence not present')
                if('type' in json_clean['jams'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(json_clean['jams'][i]['type'],str)):
                        place_holder_variable = True
                    else:
                        print(json_clean['jams'][i]['type'])
                        return(False,'type not str')
                else:
                    return(False,'type not present')
                if('subtype' in json_clean['jams'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(json_clean['jams'][i]['subtype'],str)):
                        place_holder_variable = True
                    else:
                        print(json_clean['jams'][i]['subtype'])
                        return(False,'subtype not str')
                else:
                    return(False,'subtype not present')
                if('nThumbsUp' in json_clean['jams'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(json_clean['jams'][i]['nThumbsUp'],int)):
                        place_holder_variable = True
                    else:
                        print(json_clean['jams'][i]['nThumbsUp'])
                        return(False,'nThumbsUp not int')
                else:
                    return(False,'nThumbsUp not present')
                if('reliability' in json_clean['jams'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(json_clean['jams'][i]['reliability'],int)):
                        place_holder_variable = True
                    else:
                        print(json_clean['jams'][i]['reliability'])
                        return(False,'reliability not int')
                else:
                    return(False,'reliability not present')
                if('location' in json_clean['jams'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(json_clean['jams'][i]['location'],dict)):
                        place_holder_variable = True
                    else:
                        print(json_clean['jams'][i]['location'])
                        return(False,'location not dict')
                    if('y' in json_clean['jams'][i]['location'].keys()):
                        place_holder_variable = True
                        if(isinstance(json_clean['jams'][i]['location']['y'],float)):
                            place_holder_variable = True
                        else:
                            print(json_clean['jams'][i]['location']['y'])
                            return(False,'y not float')
                    else:
                        return(False,'y not present')
                    if('x' in json_clean['jams'][i]['location'].keys()):
                        place_holder_variable = True
                        if(isinstance(json_clean['jams'][i]['location']['x'],float)):
                            place_holder_variable = True
                        else:
                            print(json_clean['jams'][i]['location']['x'])
                            return(False,'x not float')
                    else:
                        return(False,'x not present')
                else:
                    return(False,'location not present')
                if('magvar' in json_clean['jams'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(json_clean['jams'][i]['magvar'],int)):
                        place_holder_variable = True
                    else:
                        print(json_clean['jams'][i]['magvar'])
                        return(False,'magvar not int')
                else:
                    return(False,'magvar not present')
                if('reportRating' in json_clean['jams'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(json_clean['jams'][i]['reportRating'],int)):
                        place_holder_variable = True
                    else:
                        print(json_clean['jams'][i]['reportRating'])
                        return(False,'reportRating not int')
                else:
                    return(False,'reportRating not present')
                if('street' in json_clean['jams'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(json_clean['jams'][i]['street'],str)):
                        place_holder_variable = True
                    else:
                        print(json_clean['jams'][i]['street'])
                        return(False,'street not str')
                if('country' in json_clean['jams'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(json_clean['jams'][i]['country'],str)):
                        place_holder_variable = True
                    else:
                        print(json_clean['jams'][i]['country'])
                        return(False,'country not str')
                else:
                    return(False,'country not present')
        else:
            print(json_clean['jams'])
            return(False,'jams not list')
    else:
        return(False,'jams not present')
    if('startTime' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['startTime'],str)):
            place_holder_variable = True
        else:
            print(json_clean['startTime'])
            return(False,'startTime not str')
    else:
        return(False,'startTime not present')
    if('endTime' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['endTime'],str)):
            place_holder_variable = True
        else:
            print(json_clean['endTime'])
            return(False,'endTime not str')
    else:
        return(False,'endTime not present')   
    return(True, 'no errors')     
