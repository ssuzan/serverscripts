#!/bin/bash

echo "Create folder for logs"
sudo mkdir -p -m 777 /var/log/wc

echo "Start & Create rabbit docker"
if [ ! "$(docker ps -q -f name=waycare-rabbit)" ]; then
    if [ "$(docker ps -aq -f status=exited -f name=waycare-rabbit)" ]; then
        # cleanup
        echo "Remove old container - rabbit"
        docker rm waycare-rabbit
    fi
    # run your container
    echo "Start new container - rabbbit"
	sudo docker run -d --hostname waycare-rabbit-host -v /tmp/rabbit-home:/home --name waycare-rabbit -p 8080:15672 -p 5672:5672 rabbitmq:3-management
fi
