# Format Service #
Service that fetches data from different sources

How to run:
-- Optional if want to run rabbit fifos:
a. Install docker sudo apt install docker.io
b. Make a parent directory
c. Git clone be_rabbit_adapter under the parent directory

-- Mandatory
1. Git clone be_common under the parent directory
2. Git clone serverscripts_production under the parent directory
3. Change directory to env_top(cd mandark)
** optional if want to run RABIT: Run env_setup.sh to setup rabbit
4. Run pip3 install -r requirements.txt
5. Change directory back to parent dir (cd ..)
6. Export: export PYTHONPATH=/absolute/path/to/parent/dir (e.g. /home/username/serversecripts_production)
7. Run: python3 serverscripts_production/env_top/env_top_service.py start --mode (dev or prod)
8. Stop: python3 serverscripts_production/mandark/env_top_service.py stop
9. Check if running: python3 env_top/env_top_service.py status
