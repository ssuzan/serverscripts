#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: json containing data from waze database
#output: properly formatted json containing data from waze database


def clean_incident(incident):
    if('type' in incident.keys()):
        if(isinstance(incident['type'],str)):
            place_holder_variable = True
        else:
            print(incident['type'])
            return(False,'type not str')
    else:
        return(False,'type not present')   
    if('subtype' in incident.keys()):
        if(isinstance(incident['subtype'],str)):
            place_holder_variable = True
        else:
            print(incident['subtype'])
            return(False,'subtype not str')
    else:
        return(False,'subtype not present')    
    if('reliability' in incident.keys()):
        if(isinstance(incident['reliability'],int)):
            place_holder_variable = True
        else:
            print(incident['reliability'])
            return(False,'reliability not int')
    else:
        return(False,'confidence not present') 
    if('confidence' in incident.keys()):
        if(isinstance(incident['confidence'],int)):
            place_holder_variable = True
        else:
            print(incident['confidence'])
            return(False,'confidence not int')
    else:
        return(False,'confidence not present')     
    if('street' in incident.keys()):
        if(isinstance(incident['street'],str)):
            place_holder_variable = True
        else:
            print(incident['street'])
            return(False,'street not str')
        
    if('uuid' in incident.keys()):
        if(isinstance(incident['uuid'],str)):
            place_holder_variable = True
        else:
            print(incident['uuid'])
            return(False,'uuid not str')
    else:
        return(False,'uuid not present')     
    if('magvar' in incident.keys()):
        if(isinstance(incident['magvar'],int)):
            place_holder_variable = True
        else:
            print(incident['magvar'])
            return(False,'magvar not int')
    else:
        return(False,'magvar not present')    
    if('country' in incident.keys()):
        if(isinstance(incident['country'],str)):
            place_holder_variable = True
        else:
            print(incident['country'])
            return(False,'country not str')
    else:
        return(False,'country not present')       
    return (True, 'no errors')
def clean(json_clean):
    print(json_clean.keys())
    if('endTime' in json_clean.keys()):
        if(isinstance(json_clean['endTime'],str)):
            place_holder_variable = True
        else:
            print(json_clean['endTime'])
            return(False,'endTime not str')
    else:
        return(False,'endTime not present')   
    if('startTime' in json_clean.keys()):
        if(isinstance(json_clean['startTime'],str)):
            place_holder_variable = True
        else:
            print(json_clean['startTime'])
            return(False,'startTime not str')
    else:
        return(False,'startTime not present')    
    if('endTimeMillis' in json_clean.keys()):
        if(isinstance(json_clean['endTimeMillis'],int)):
            place_holder_variable = True
        else:
            print(json_clean['endTimeMillis'])
            return(False,'endTimeMillis not int')
    else:
        return(False,'endTimeMillis not present')    
    if('startTimeMillis' in json_clean.keys()):
        if(isinstance(json_clean['startTimeMillis'],int)):
            place_holder_variable = True
        else:
            print(json_clean['startTimeMillis'])
            return(False,'startTimeMillis not int')
    else:
        return(False,'startTimeMillis not present')      
    if('alerts' in json_clean.keys()):
        if(isinstance(json_clean['alerts'],list)):
            place_holder_variable = True
            for i in range(0,len(json_clean['alerts'])):
                clean = clean_incident(json_clean['alerts'][i])
                #print(clean)
                if(clean[0] == False):
                    return(clean)
        else:
            print(json_clean['alerts'])
            return(False,'alerts not list')
    else:
        return(False,'alerts not present')         
    return(True, 'no errors')    