#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: json input and file directory path
#output: json stored to directory
import hashlib
import json

import os
from datetime import datetime


def store(store_json, path, full_file_path, type_name, is_dev, rabbit_connection, queue_name):
    #creates folder for files
    if not is_dev and not os.path.exists(path):
        os.makedirs(path)
    save_path = path
    #gets date for filename as now
    now = str(datetime.now())
    now = now.split(" ")
    now[0] = now[0].split("-")
    now[1] = now[1].split(":")
    #print(store_json['alerts'])
    curr_t = datetime.now().strftime("%d-%m-%Y %H:%M:%S")
    if not is_dev:
        filename = type_name+"_"+now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1]
        #saves file
        lastFiletxt = open(path+"/"+full_file_path+"/"+filename+'.json', 'w')
        lastFiletxt.write(json.dumps(store_json, sort_keys=True, indent=4, separators=(',', ': ')))
    else:
        rabbit_connection.publish(store_json, queue_name)
    for i in range(0,len(store_json['alerts'])):
        hash_uuid = hashlib.md5()
        encoded_uuid = store_json['alerts'][i]['uuid']
        encoded_uuid = encoded_uuid.encode("utf-8")
        hash_uuid.update(encoded_uuid)
        hash_uuid.hexdigest()
        print(hash_uuid.hexdigest())
        #print(store_json['alerts'][i]['uuid'])
        #filename = type_name+"_"+now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1]+"_incident_"+str(i)
        #saves file
        #new_store_json = {'type':type_name,'date_time':curr_t,'suspicious':False,'data':store_json['alerts'][i]}
        #lastFiletxt = open(save_path+"/"+filename+'.json', 'w')
        #lastFiletxt.write(universal_imports.json.dumps(new_store_json, sort_keys=True, indent=4, separators=(',', ': ')))  
	#if (dump_output_to == "FIFO"):
        #   print("TBD implementation of FIFO")
