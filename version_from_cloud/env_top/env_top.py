import argparse
import sys
from logging import getLogger

import yaml
from apscheduler.schedulers.background import BackgroundScheduler
from pytz import utc
from be_common.utils.waycare_logger import logging_setup
from be_rabbit_adapter.rabbit_adapter import RabbitAdapter
from service import Service

#from serverscripts_production.geotab.geotab_call_all import get_geotab
from serverscripts_production.scripts_for_wz_jams.call_all_wz_jams import \
    get_waze_traffic_jam
#from serverscripts_production.weather_underground.weather_underground import get_weather

from serverscripts_production.mandark.scripts_for_wz.call_all_wz import get_waze_traffic_alert

PROD = 'prod'
DEV = 'dev'


class DataServerService(Service):

    def __init__(self, mode, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._is_dev_mode = True if mode == DEV else False
        self._setup_configuration()
        self._setup_logging(self._cfg['logger']['log_config_file'])

    def run(self):
        self._logger.info('Mandark service started')

        self._setup_rabbit(self._cfg['rabbitmq'])

        self._setup_scheduler(self._is_dev_mode)

        self._scheduler.start()

        self.wait_for_sigterm()

        self._scheduler.shutdown()

        self._logger.info('Mandark service stopped')

    def _setup_rabbit(self, cfg):
        self._rabbit_connection = RabbitAdapter(cfg['host'], cfg['port'])

    def _setup_scheduler(self, is_dev):
        self._scheduler = BackgroundScheduler(timezone=utc)

        weather_params = [
            is_dev,
            self._cfg['email']['email_config_file'],
            self._cfg['weather']['passwd'],
            self._cfg['weather']['save_path'],
            self._cfg['weather']['weather_underground_url'],
            self._cfg['weather']['weather_underground_file'],
            self._rabbit_connection,
            self._cfg['rabbitmq']['queue_name']['outcoming']['weather']
        ]
#        self._scheduler.add_job(get_weather, 'cron', weather_params,
#                                minute=self._cfg['scheduler']['weather']['minutes'])

        waze_jam_params = [
            is_dev,
            self._cfg['email']['email_config_file'],
            self._cfg['waze_traffic_jam']['save_path'],
            self._cfg['waze_traffic_jam']['full_file_path'],
            self._cfg['waze_traffic_jam']['type_name'],
            self._cfg['waze_traffic_jam']['waze_url'],
            self._rabbit_connection,
            self._cfg['rabbitmq']['queue_name']['outcoming']['waze_traffic_jam']
        ]
        self._scheduler.add_job(get_waze_traffic_jam, 'cron', waze_jam_params,
                                minute=self._cfg['scheduler']['waze_traffic_jam']['minutes'])

        waze_alert_params = [
            is_dev,
            self._cfg['email']['email_config_file'],
            self._cfg['waze_traffic_alert']['save_path'],
            self._cfg['waze_traffic_alert']['full_file_path'],
            self._cfg['waze_traffic_alert']['type_name'],
            self._cfg['waze_traffic_alert']['waze_url'],
            self._rabbit_connection,
            self._cfg['rabbitmq']['queue_name']['outcoming']['waze_traffic_alert']
        ]
#        self._scheduler.add_job(get_waze_traffic_alert, 'cron', waze_alert_params,
#                                minute=self._cfg['scheduler']['waze_traffic_alert']['minutes'])

        geotab_params = [
            is_dev,
            self._cfg['email']['email_config_file'],
            self._cfg['geotab']['save_path'],
            self._cfg['geotab']['original_backup_path'],
            self._cfg['geotab']['access_file'],
            self._rabbit_connection,
            self._cfg['rabbitmq']['queue_name']['outcoming']['fsp']
        ]
#        self._scheduler.add_job(get_geotab, 'cron', geotab_params, minute=self._cfg['scheduler']['fsp']['minutes'])

    def _setup_configuration(self, default_path='serverscripts_production/mandark/settings.yaml'):
        """
        Setup misc configurations
        """
        try:
            with open(default_path, 'r') as ymlfile:
                self._cfg = yaml.safe_load(ymlfile)
        except IOError as e:
            sys.exit('Could not load yaml configuration file:{}' % default_path)

    def _setup_logging(self, log_file_path):
        """
        Setup logging configuration
        """
        logging_setup(log_file_path)
        self._logger = getLogger(__name__)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    sub_parser = parser.add_subparsers(dest='command')
    parser_start = sub_parser.add_parser('start')
    parser_stop = sub_parser.add_parser('stop')
    parser_status = sub_parser.add_parser('status')

    parser_start.add_argument('--mode', type=str, choices=[PROD, DEV], default=DEV)

    args = parser.parse_args()

    if args.command == 'start':
        service = DataServerService(args.mode, DataServerService.__name__, pid_dir='/tmp')
        # service.start()
        service.run()
    elif args.command == 'stop':
        service = DataServerService(None, DataServerService.__name__, pid_dir='/tmp')
        service.stop()
    elif args.command == 'status':
        service = DataServerService(None, DataServerService.__name__, pid_dir='/tmp')
        if service.is_running():
            print("Service is running.")
        else:
            print("Service is not running.")
    else:
        sys.exit('Unknown command "%s".' % args.command)
