#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts_production/globals')
import universal_imports
import xmltodict
import requests
import urllib
import urllib.request
from urllib import request
#import socket
#from socket import socket
import nhp_store

access_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/spillman_access.txt'
url = 'http://10.10.55.8:9975/DataExchange/REST'



#x = ['sp_output2.xml','sp_output3.xml','sp_output3.xml','sp_output2.xml']
def nhp_fetch(filename):
    headers = {
        'Content-Type': 'text/xml;charset=UTF-8',
    }
    text = open(access_file,'r')
    access_string = text.read()
    access_json = universal_imports.json.loads(access_string)      
    user = access_json["user"]
    password=access_json["password"]
    data = open('soap_get.xml')

    #connecting to Spillman and authentication 
    r= requests.post('http://10.10.55.5:4081/DataExchangeWayCare/REST', headers=headers, data=data, auth=(user, password))
    if (r.status_code== 200):
        nhp_store.nhp_store_original(r.content,filename)
        try:
            doc = xmltodict.parse(r.content)
#            r = universal_imports.json.dumps(r, sort_keys=True, indent=4, separators=(',', ': '))
#            r = universal_imports.json.loads(r)
        except:
            return [False,'nhp failed to convert to json '+filename]
        return [True, doc]
    else:
        return [False,'nhp failed to fetch '+filename]

