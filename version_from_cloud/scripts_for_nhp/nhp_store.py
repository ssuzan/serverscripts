#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts_production/globals')
import universal_imports
import sendemail
import push_to_fifo
import hashlib
from collections import OrderedDict
from email.mime.text import MIMEText


save_path = os.environ['WORKSPACE']+'/DB_backups/data_from_nhp'
email_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/emails'
text = open(email_file,'r')
string = text.read()
email_json = universal_imports.json.loads(string) 
type_name = 'road_incidents_nhp_live_lv'
def nhp_store_original(nhp_xml,filename):  
    #saves orignal file
    lastFiletxt = open(save_path+"/database_original/"+filename+'.xml', 'wb')
    lastFiletxt.write(nhp_xml)  
    lastFiletxt.close()
    #lastFiletxt.write(universal_imports.json.dumps(nhp_xml, sort_keys=True, indent=4, separators=(',', ': ')))  
    
def nhp_store(curr_t,nhp_json,hash_log,default_age,dump_to_output,filename):  
    hashes = {}
    repeat_hashes=[]
    #saves new files
    for i in range(0,len(nhp_json)):
        if ('compromised' not in nhp_json[i].keys()): 
            nhp_json[i] = OrderedDict(sorted(nhp_json[i].items()))
            if isinstance(nhp_json[i]['MainRadioLogTable'],list):
                for n in range(0,len(nhp_json[i]['MainRadioLogTable'])):
                    nhp_json[i]['MainRadioLogTable'][n] = OrderedDict(sorted(nhp_json[i]['MainRadioLogTable'][n].items()))
                nhp_json[i]['MainRadioLogTable'] = sorted(nhp_json[i]['MainRadioLogTable'], key=lambda k: k['TimeDateOfEntry']) 
    
            hash_uuid = hashlib.md5()
            encoded_uuid = str(nhp_json[i])
            encoded_uuid = encoded_uuid.encode("utf-8")
            hash_uuid.update(encoded_uuid)
            if nhp_json[i]['LongTermCallID'] in hash_log.keys():
                if hash_log[nhp_json[i]['LongTermCallID']][0]!=hash_uuid.hexdigest():
                    hashes[nhp_json[i]['LongTermCallID']] = [hash_uuid.hexdigest(),default_age]
                else:
                    repeat_hashes.append(nhp_json[i]['LongTermCallID'])
            else:
                hashes[nhp_json[i]['LongTermCallID']] = [hash_uuid.hexdigest(),default_age]
            curr_filename = filename+"_incident_"+nhp_json[i]['LongTermCallID']            
            #saves file
            if (nhp_json[i]['LongTermCallID'] not in repeat_hashes):
                #print(nhp_json[i]['LongTermCallID'],hashes(nhp_json[i]['LongTermCallID']))
                new_store_json = {'type':type_name,'date_time':str(curr_t).replace("-","_")+' UTC','suspicious':False,'data':nhp_json[i],'error':False}        
                if (dump_to_output == "FILE"): 
                    lastFiletxt = open(save_path+"/database/"+curr_filename+'.json', 'w')
                    lastFiletxt.write(universal_imports.json.dumps(new_store_json, sort_keys=True, indent=4, separators=(',', ': ')))  
                #sends file to FIFO
                elif (dump_to_output == "FIFO"):
                    try:
                        push_to_fifo.push_to_fifo(new_store_json, "nhp_queue")         
                    except:
                        text = open(email_file,'r')
                        email_string = text.read()
                        email_json = universal_imports.json.loads(email_string)                  
                        #sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'error', message = 'fifo error in store_wz', login = email_json['email'], password = email_json['password'])         
    return [hashes,repeat_hashes]
