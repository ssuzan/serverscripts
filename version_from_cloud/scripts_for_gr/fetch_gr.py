#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: waze database https request (url) 
#output: json of data given by https request or json containing error message
import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts_production/globals')
import universal_imports
from datetime import datetime
import time
import requests
import collections


def fetch(incoming_path, type, curr_t, incoming_files):  
    r=[]
    element={}
    for filename in incoming_files:
        #with open(incoming_path+filename, encoding='utf-8') as data_file:
        #    data = universal_imports.json.loads(data_file.read())
        #data_file.close()

        filetext =open(incoming_path+filename)
        d = filetext.read()
        data =universal_imports.json.loads(d)
        filetext.close()

        element['data']=data
        element['suspicious']=False
        element['error']=False
        element['ID']=filename
        element['type']= type
        element['date_time']= curr_t+' UTC'
        r.append(dict(element))

    return ([True,r])
     
