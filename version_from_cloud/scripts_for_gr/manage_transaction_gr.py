#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts_production/globals')
import logging
import universal_imports
import sendemail
from email.mime.text import MIMEText
import push_to_fifo
#import geotab_fetch
#import geotab_store

save_path = os.environ['WORKSPACE']+'/DB_backups/data_from_gr/database/'
original_backup_path = os.environ['WORKSPACE']+'/DB_backups/data_from_gr/database_original/'
email_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/emails'

#local paths:
local_path='/home/ubuntu/transactions/'
trans_save_path = os.environ['WORKSPACE']+'/DB_backups/data_from_gr/transactions/'
# remote paths:
key_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/.KeyForFetchingData.pem'
remote_path='ubuntu@ec2-52-29-25-49.eu-central-1.compute.amazonaws.com:/home/ubuntu/workspaces/DB_backups/data_from_gr/transactions/'

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
log_name= '/home/ubuntu/workspaces/cron_log.log'
handler = logging.FileHandler(log_name)
logger.addHandler(handler)

#dump_output_to = "FIFO"

def manage_trans():
    text = open(email_file,'r')
    email_string = text.read()
    email_json = universal_imports.json.loads(email_string)  
    curr_t = universal_imports.datetime.utcnow().strftime("%d_%m_%Y_%H_%M_%S")  
    print('manage_trans() is running! time is: '+curr_t+' UTC')
    logger.info('manage_trans() is running! time is: '+curr_t+' UTC')
    for filename in os.listdir(local_path):
        #print (filename)

        #sending to remote machine
        try:
            command1='scp -i '+key_file+' '+local_path+filename+' '+remote_path        
            #print (command1)
            os.system(command1)
        except:
            sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'error managed_transaction_gr.py', message = "error failed to send file- "+filename, login = email_json["email"], password = email_json["password"])    


	#backup localy
        command2='mv '+local_path+filename+' '+trans_save_path        
        #print (command2)
        os.system(command2)
    

       
    
#manage_trans()
#print('manage_transactions_gr.py completed successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d_%m_%Y_%H_%M_%S")+' UTC')
