#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: json containing data from waze database
#output: properly formatted json containing data from gr database
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports
import sendemail
from email.mime.text import MIMEText

email_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/emails'
text = open(email_file,'r')
email_string = text.read()
email_json = universal_imports.json.loads(email_string)  


def clean_incident(incident_clean):
    if('EventTime' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['EventTime'],str)):
            place_holder_variable = True
        else:
            incident_clean['EventTime'] = ''
            return(False,'EventTime wrong type')
    else:
        incident_clean['EventTime'] = ''
        return(False,'EventTime not present')
    if('EventType' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['EventType'],int)):
            place_holder_variable = True
        else:
            incident_clean['EventType'] = ''
            return(False,'EventType wrong type')
    else:
        incident_clean['EventType'] = ''
        return(False,'EventType not present')
    if('EndSpeedKmh' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['EndSpeedKmh'],float)):
            place_holder_variable = True
        else:
            incident_clean['EndSpeedKmh'] = ''
            return(False,'EndSpeedKmh wrong type')
    else:
        incident_clean['EndSpeedKmh'] = ''
        return(False,'EndSpeedKmh not present')
    if('EndSpeedMph' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['EndSpeedMph'],float)):
            place_holder_variable = True
        else:
            incident_clean['EndSpeedMph'] = ''
            return(False,'EndSpeedMph wrong type')
    else:
        incident_clean['EndSpeedMph'] = ''
        return(False,'EndSpeedMph not present')
    if('EndTime' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['EndTime'],str)):
            place_holder_variable = True
        else:
            incident_clean['EndTime'] = ''
            return(False,'EndTime wrong type')
    else:
        incident_clean['EndTime'] = ''
        return(False,'EndTime not present')
    if('MaxSpeedKmh' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['MaxSpeedKmh'],float)):
            place_holder_variable = True
        else:
            incident_clean['MaxSpeedKmh'] = ''
            return(False,'MaxSpeedKmh wrong type')
    else:
        incident_clean['MaxSpeedKmh'] = ''
        return(False,'MaxSpeedKmh not present')
    if('MaxSpeedMph' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['MaxSpeedMph'],float)):
            place_holder_variable = True
        else:
            incident_clean['MaxSpeedMph'] = ''
            return(False,'MaxSpeedMph wrong type')
    else:
        incident_clean['MaxSpeedMph'] = ''
        return(False,'MaxSpeedMph not present')
    if('Severity' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['Severity'],int)):
            place_holder_variable = True
        else:
            incident_clean['Severity'] = ''
            return(False,'Severity wrong type')
    else:
        incident_clean['Severity'] = ''
        return(False,'Severity not present')
    if('StartSpeedKmh' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['StartSpeedKmh'],float)):
            place_holder_variable = True
        else:
            incident_clean['StartSpeedKmh'] = ''
            return(False,'StartSpeedKmh wrong type')
    else:
        incident_clean['StartSpeedKmh'] = ''
        return(False,'StartSpeedKmh not present')
    if('StartSpeedMph' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['StartSpeedMph'],float)):
            place_holder_variable = True
        else:
            incident_clean['StartSpeedMph'] = ''
            return(False,'StartSpeedMph wrong type')
    else:
        incident_clean['StartSpeedMph'] = ''
        return(False,'StartSpeedMph not present')
    if('StartTime' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['StartTime'],str)):
            place_holder_variable = True
        else:
            incident_clean['StartTime'] = ''
            return(False,'StartTime wrong type')
    else:
        incident_clean['StartTime'] = ''
        return(False,'StartTime not present')
    if('TripId' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['TripId'],int)):
            place_holder_variable = True
        else:
            incident_clean['TripId'] = ''
            return(False,'TripId wrong type')
    else:
        incident_clean['TripId'] = ''
        return(False,'TripId not present')
      #"EndLocation": null,
      #"MaxLocation": null,
      #"StartLocation": null,
      #"EventData": null,
      #"Location": {
      #  "Heading": 40,
      #  "Latitude": 34.045018,
      #  "Longitude": -118.454319,
      #  "SpeedKMH": 8,
      #  "SpeedMPH": 4.9709696
      #}
    return ([True, incident_clean])



def clean(json_clean):
    #print(len(json_clean))
    for i in range(0,len(json_clean)):       
        clean_r = clean_incident(json_clean[i])
        if (clean_r[0] == False):
            #if thereis an error                           
            sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = "clean_gr error", message = str("clean_gr error likely device "+clean_r[1]), login = email_json["email"], password = email_json["password"]) 
            return ('error') 
        else:
            json_clean[i]=clean_r[1]
    return json_clean

