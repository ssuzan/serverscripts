#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
#sudo pip3 install mygeotab
import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts_production/globals')
import universal_imports
import mygeotab
import sendemail
import push_to_fifo
from email.mime.text import MIMEText
import geotab_fetch
import geotab_store

save_path = os.environ['WORKSPACE']+'/DB_backups/data_from_geotab/database/'
original_backup_path = os.environ['WORKSPACE']+'/DB_backups/data_from_geotab/database_original/'
email_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/emails'

dump_output_to = "FIFO"

def geotab_call_all():
    text = open(email_file,'r')
    email_string = text.read()
    email_json = universal_imports.json.loads(email_string)  
    fetched = geotab_fetch.geotab_fetch()
    curr_t = universal_imports.datetime.utcnow().strftime("%d_%m_%Y_%H_%M_%S")  
    if (fetched[0] == True):
        result = fetched[1]
        geotab_store.geotab_store(curr_t,result, dump_output_to, False)
    else:
        geotab_store.geotab_store(curr_t,result, dump_output_to, True)
        sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'error', message = fetched[1], login = email_json["email"], password = email_json["password"])    
        return(False)
       
    
#Calling geotab_call_all
geotab_call_all()
print('geotab_call_all.py completed successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d_%m_%Y_%H_%M_%S")+' UTC')
