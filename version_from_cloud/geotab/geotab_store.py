#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts_production/globals')
import universal_imports
import sendemail
import geotab_clean
import push_to_fifo
from email.mime.text import MIMEText


save_path =  os.environ['WORKSPACE']+'/DB_backups/data_from_geotab/database/'
original_backup_path = os.environ['WORKSPACE']+'/DB_backups/data_from_geotab/database_original/'
email_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/emails'

def geotab_store(curr_t,result, dump_output_to, encountered_error_flag):
    Filetxt_original = open(original_backup_path+"geotab_fsp_location_live_lv_"+str(curr_t).replace("-","_")+'.txt', 'w')
    Filetxt_original.write(str(result))
    Filetxt_original.close()              
    #parsing and saving json file:
    if(encountered_error_flag == True):
        for i in range(0,89):       
            print('error in client id ..')
            json_struct = {"type":"geotab_fsp_location_live_lv","date_time":curr_t+" UTC","suspicious":False,"error":True,"data":{"id":i}}
            json_struct = json_struct
            json_save = universal_imports.json.dumps(json_struct)

            if (dump_output_to == "FILE"): 
                Filetxt = open(save_path+"geotab_fsp_location_live_lv_"+str(curr_t).replace("-","_")+"_file_%s"%i+'.json', 'w')
                Filetxt.write(json_save)
                Filetxt.close()
            elif (dump_output_to == "FIFO"):
                try:
                    push_to_fifo.push_to_fifo(json_save, "fsp_queue")         
                except:
                    sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'error', message = 'geotab_store: fifo error in geotab for file id '+i, login = email_json['email'], password = email_json['password'])    


    else:
        for i in range(0,len(result)):       
            if(geotab_clean.geotab_clean(result[0])[0] == True):
                json_struct = {"type":"geotab_fsp_location_live_lv","date_time":curr_t+" UTC","suspicious":False,"error":False,"data":{"id":result[i]["device"]["id"],"coordinates":(result[i]['latitude'],result[i]['longitude']),"bearing":result[i]['bearing']}}
                json_struct = json_struct
                json_save = universal_imports.json.dumps(json_struct)

                if (dump_output_to == "FILE"): 
                    Filetxt = open(save_path+"geotab_fsp_location_live_lv_"+str(curr_t).replace("-","_")+"_file_%s"%result[i]['device']['id']+'.json', 'w')
                    Filetxt.write(json_save)
                    Filetxt.close()
                elif (dump_output_to == "FIFO"):
                    try:
                        push_to_fifo.push_to_fifo(json_save, "fsp_queue")         
                    except:
                        sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'error', message = 'geotab_store: fifo error in geotab for file '+result[i]['device']['id'], login = email_json['email'], password = email_json['password'])    


            elif(geotab_clean.geotab_clean(result[0])[0] == False):
                sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'error', message = 'geotab_store: geotab file not clean '+geotab_clean.geotab_clean(result[0])[1]+" file "+result[i]['device']['id'], login = email_json['email'], password = email_json['password']) 

            else:
                sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'error', message = "geotab_store: file not clean unknown error"+" file "+str(i), login = email_json['email'], password = email_json['password'])     
