#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts_production/globals')
import universal_imports
import mygeotab

access_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/geotab_access.txt'

def geotab_fetch():
    #connecting to geotab and authentication  
    text = open(access_file,'r')
    access_string = text.read()
    access_json = universal_imports.json.loads(access_string)      
    try:
        api = mygeotab.API(username=access_json["username"], password=access_json["password"], database=access_json["database"])
    except:
        return(False,'geotab_fetch: could not access database')
    try:
        api.authenticate()
    except:
        return(False,'geotab_fetch: Could not authenticate')
    try:
        result = api.get('DeviceStatusInfo')
        return(True,result)     
    except:
        return(False,'geotab_fetch: Could not pull')   
    return(False, 'geotab_fetch: unknown error')
