import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts/globals')
import universal_imports
import sendemail
from email.mime.text import MIMEText
from dateutil.parser import parse
import datetime
from datetime import timedelta
from math import sin, cos, sqrt, atan2, radians
import mongo_fetch
import mongo_universal
import forecast_print
import random
#from pytz import timezone
#import pytz
import csv
from shapely.geometry import Point, shape


#create walkthrough 
#manuel input of filenames (check)
#time should be from start to finish for first list items, allow margin of time before starttime (default 0) (check)
#output 1. number of each spillman road type 2. gr event type count 3. number of spillman incidents with matches 4. cataloge matches
#step 1        
#generate files from mongo (spillman/waze)
#mongoexport --db waycare_db_backup --collection gr_alerts_live_lv_received --query ' {"data.StartTime": {$gt:"2017-10-01T01:00:00Z", $lt: "2017-11-01T00:00:00Z"}} ' --out gr_out.json
#mongoexport --db waycare_production_db --collection incidents --query '{"creation_time": {$gte:ISODate(1506805200000), $lt: ISODate(1509487200000) },"source":"spillman_incident","type":"crash"}' --out spillman_crashes.json
#generate lv data (optional) and forecast data
#step 2
#move files to desired location
#step 3 
#choose option you want to run (lv,spillman_waycare,waycare,spillman), include which ones you want in do_all_the_stuff() parameter list
#step 4
#select time slot in hours,month as number string
#step 5
#run script
#python3 forecast_main
#step 6
#results are snur_<time>.txt and <data_type_name>_<time>.txt

prediction_filter_file_name = 'res/table_filtered.csv'
forecast_file_name = 'res/forecasts_out.json'
crashes_file_name = 'res/Waycare-LV_Crashes.csv'
street_polygon_file_name = 'polygons.json'
prediction_filter_input = []
prediction_filter_struct = []
time_slot_size = 2 #hours
US95_segments_list = ['HLA10']
I15_segments_list = ['HLC30','HLC31', 'HLC32']
month = '10'
time_offset_from_udt = 7
#street name unprocessed repository
snur = []
forecast_calender_struct = []

    

with open(street_polygon_file_name) as f:
    roads_json = universal_imports.json.load(f)  
def get_refined_lv(lv_input):
    lv_output = {}
    #print(lv_input)
    stage_1_successful = False
    street_string = lv_input[8]+" "+lv_input[9]
    segment_string = lv_input[4]
    #check for both us95 and i15 at same time, use first
    if 'I15' in street_string or 'IR15' in street_string:
        if segment_string in I15_segments_list:
            if any(ext in street_string for ext in ['I15 (SB','I15 SB','I15SB','I15 S','I15 S/','IR15 (SB','IR15 SB','IR15SB','IR15 S','IR15 S/','SOUTHBOUND','SOUTBOUND','S/B',' SB ','SOUTH',' SB/']):
                #print(street_string)
                lv_output['street'] = 'I15S'
                stage_1_successful = True
            elif any(ext in street_string for ext in ['I15 (NB','I15 NB','I15NB','I15 N','I15 N/','IR15 (NB','IR15 NB','IR15NB','IR15 N','IR15 N/','NORTHBOUND','NORTBOUND','N/B',' NB ','NORTH',' NB/','NORHTBOUND']):
                #print(street_string)
                lv_output['street'] = 'I15N'
                stage_1_successful = True                
            #else:
                #print(street_string)
    elif 'US95' in street_string or 'US 95' in street_string or '95' in street_string:
        if segment_string in US95_segments_list:
            if any(ext in street_string for ext in ['US95 (SB','US95 SB','US95SB','US95 S','US95 S/','95 (SB','95 SB','95SB','95 S','95 S/','US 95 (SB','US 95 SB','US 95SB','US 95 S','US 95 S/','SOUTHBOUND','SOUTBOUND','S/B',' SB ','SOUTH',' SB/']):
                #print(street_string)
                lv_output['street'] = 'US95S'
                stage_1_successful = True
            elif any(ext in street_string for ext in ['US95 (NB','US95 NB','US95NB','US95 N','US95 N/','95 (NB','95 NB','95NB','95 N','95 N/','US 95 (NB','US 95 NB','US 95NB','US 95 N','US 95 N/','NORTHBOUND','NORTBOUND','N/B',' NB ','NORTH',' NB/','NORHTBOUND']):
                #print(street_string)
                lv_output['street'] = 'US95N'
                stage_1_successful = True                
            #else:
                #print(street_string)

    if stage_1_successful == False:
        lv_output['street'] = 'NA'
        snur.append(lv_input)
    #print(lv_input[9])
    #if (len(lv_input[9].strip('I15')) < len(lv_input[9])):
    #    print(lv_input[9])      
    date_str = lv_input[1].strip('"').strip(" ")+'-10-'+lv_input[0][1]+"T"+lv_input[5][0]+lv_input[5][1]+":"+lv_input[5][2]+lv_input[5][3]+":0.000Z"
    lv_output['start_time'] = mongo_fetch.calculate_time(date_str)    
    return lv_output


def create_time_slot_struct(forecast_data,filter_parameters,time_slot):
    filter_parameters_struct = {}
    for i in range(1,len(filter_parameters)):
        filter_parameters_struct[int(filter_parameters[i][0])] = filter_parameters[i]
    forecast_json={'I15N_CRASHES':[],'I15S_CRASHES':[],'US95S_CRASHES':[],'US95N_CRASHES':[]}
    two_count = 0
    for x in range(0,len(forecast_data)):
        #print(forecast_json[i]['payload']['shifts'][0]['segments'][x])
        if forecast_data[x]['id'] == '0':
            forecast_json['I15N_PREDICTION'] = forecast_data[x]['score']
            if(forecast_data[x]['score'] == 2.0):
                two_count+=1
        if forecast_data[x]['id'] == '1':
            forecast_json['I15S_PREDICTION'] = forecast_data[x]['score']
            if(forecast_data[x]['score'] == 2.0):
                two_count+=1            
        if forecast_data[x]['id'] == '2':
            forecast_json['US95N_PREDICTION'] = forecast_data[x]['score'] 
            if(forecast_data[x]['score'] == 2.0):
                two_count+=1            
        if forecast_data[x]['id'] == '3':
            forecast_json['US95S_PREDICTION'] = forecast_data[x]['score'] 
            if(forecast_data[x]['score'] == 2.0):
                two_count+=1   
    #if two_count >2 :
    #    print(filter_parameters_struct)
    return forecast_json

def get_roads_by_location(location):
    road_names = []

    point = Point(location[0], location[1])
    for road_name in roads_json:
        road_polygon = shape(roads_json[road_name])

        if road_polygon.contains(point):
            road_names.append(road_name)

    return road_names

def get_direction(road_name):
    direction = ''
    if any(ext in road_name for ext in ['15N',' NB ',' nb ',' NB',' N ',';NB',' N/','; N','I15 NB','US95 NB']):
        direction = 'N'
    elif any(ext in road_name for ext in ['15S',' SB ',' sb ',' SB',' S ',';SB',' S/','; S','I15 SB','US95 SB']):
        direction = 'S'
    else:
        direction = ''
        #print(road_name)
    return(direction)
def input_locations(input_struct):
    return_struct = []
    for i in range(0,len(input_struct)):
        i_road_name = get_roads_by_location(input_struct[i]['street'])
        if len(i_road_name)>0:
            x = get_direction(input_struct[i]['street_name'])
            if len(x)>0:
                #print(input_struct[i])
                return_struct.append({"street": str(i_road_name[0]+x),'start_time': input_struct[i]['start_time']})
            else:
                snur.append(input_struct[i])
        else:
            snur.append(input_struct[i])         
    return return_struct
def make_calender_struct():
    for i in range(0,32):
        forecast_calender_struct.append({0:{},2:{},4:{},6:{},8:{},10:{},12:{},14:{},16:{},18:{},20:{},22:{}})
    forecast_json = mongo_fetch.mongo_fetch_forecast(forecast_file_name)
    for i in range(0,len(forecast_json)):
        local_time = mongo_fetch.calculate_time(forecast_json[i]['creation_time']['$date'])- datetime.timedelta(0,time_offset_from_udt*3600)
        local_time_struct = str(local_time).split(' ')
        local_time_struct[0] = local_time_struct[0].split('-')
        local_time_struct[1] = local_time_struct[1].split(':')
        if local_time_struct[0][1] == month:
            for x in range(0,len(local_time_struct)):
                for n in range(0,len(local_time_struct[x])):
                    local_time_struct[x][n] = int(local_time_struct[x][n])
            #print(forecast_calender_struct[local_time_struct[0][2]])
            #print(local_time_struct)
            current_time_struct = forecast_calender_struct[local_time.day]#[local_time_struct[0][2]-1]
    
            for key in current_time_struct.keys():
                #print(key)
                if local_time_struct[1][0] >= int(key) and local_time_struct[1][0] < int(key)+time_slot_size:
                    #print(local_time_struct,local_time_struct[0][2]-1,key)
                    #forecast_calender_struct[local_time_struct[0][2]-1][key]={'I15N_CRASHES':[],'I15S_CRASHES':[],'US95S_CRASHES':[],'US95N_CRASHES':[]}#forecast_json[i]['payload']['shifts'][0]['segments'] 
                    #print(forecast_json[i]['payload']['shifts'][0]['segments'])
                    forecast_calender_struct[local_time.day][key]=create_time_slot_struct(forecast_json[i]['payload']['shifts'][0]['segments'],prediction_filter_struct,key)
    
        
def set_prediction_filter():
    with open(prediction_filter_file_name, 'rb') as csvfile:
        for row in csvfile:
            prediction_filter_input.append(row)  
        
    for i in range(0,len(prediction_filter_input)):
        prediction_filter_struct.append(str(prediction_filter_input[i]).strip("b'").strip("\\n'").split(','))
    

def add_data_to_calender(data):
    for i in range(0,len(data)):
        for key in forecast_calender_struct[data[i]['start_time'].day].keys():
            if data[i]['start_time'].hour>=key and data[i]['start_time'].hour<key+time_slot_size:
                #print(forecast_calender_struct[data[i]['start_time'].day])
                if data[i]['street'] == 'I15N':
                    if 'I15N_CRASHES' in forecast_calender_struct[data[i]['start_time'].day][key].keys():
                        forecast_calender_struct[data[i]['start_time'].day][key]['I15N_CRASHES'].append(data[i])
                    else:
                        forecast_calender_struct[data[i]['start_time'].day][key]['I15N_CRASHES'] = [data[i]]
                if data[i]['street'] == 'I15S':
                    if 'I15S_CRASHES' in forecast_calender_struct[data[i]['start_time'].day][key].keys():
                        forecast_calender_struct[data[i]['start_time'].day][key]['I15S_CRASHES'].append(data[i])
                    else:
                        forecast_calender_struct[data[i]['start_time'].day][key]['I15S_CRASHES'] = [data[i]]
                if data[i]['street'] == 'US95N':
                    if 'US95N_CRASHES' in forecast_calender_struct[data[i]['start_time'].day][key].keys():
                        forecast_calender_struct[data[i]['start_time'].day][key]['US95N_CRASHES'].append(data[i])
                    else:
                        forecast_calender_struct[data[i]['start_time'].day][key]['US95N_CRASHES'] = [data[i]]                       
                if data[i]['street'] == 'US95S':
                    if 'US95S_CRASHES' in forecast_calender_struct[data[i]['start_time'].day][key].keys():
                        forecast_calender_struct[data[i]['start_time'].day][key]['US95S_CRASHES'].append(data[i])
                    else:
                        forecast_calender_struct[data[i]['start_time'].day][key]['US95S_CRASHES'] = [data[i]]               
                #forecast_calender_struct[lvsdf[i]['start_time'].day][key]
           
def do_all_the_stuff(files_to_analyze):
    
    set_prediction_filter()        
    if ('waycare' in files_to_analyze):
        make_calender_struct()
        waycare_crashes_json = mongo_fetch.mongo_fetch('res/waycare_crashes.json')
        waycare_crashes_json = input_locations(waycare_crashes_json)
        for i in range(0,len(waycare_crashes_json)):
            waycare_crashes_json[i]['start_time'] = waycare_crashes_json[i]['start_time']- datetime.timedelta(0,time_offset_from_udt*3600)
        add_data_to_calender(waycare_crashes_json)
        forecast_print.print_calender_charts(forecast_calender_struct,'waycare')
        #print(waycare_crashes_json[0])
        #forecast_calender_struct = []
        forecast_calender_struct.clear()
    if ('spillman' in files_to_analyze):
        make_calender_struct()        
        spillman_crashes_json = mongo_fetch.mongo_fetch('res/spillman_crashes.json')
        spillman_crashes_json = input_locations(spillman_crashes_json)
        for i in range(0,len(spillman_crashes_json)):
            spillman_crashes_json[i]['start_time'] = spillman_crashes_json[i]['start_time']- datetime.timedelta(0,time_offset_from_udt*3600)        
        add_data_to_calender(spillman_crashes_json)
        forecast_print.print_calender_charts(forecast_calender_struct,'spillman')        
        #print(forecast_calender_struct)        
        forecast_calender_struct.clear()
    if ('spillman_waycare' in files_to_analyze):
        make_calender_struct()
        waycare_crashes_json = mongo_fetch.mongo_fetch('res/waycare_crashes.json')
        waycare_crashes_json = input_locations(waycare_crashes_json)
        for i in range(0,len(waycare_crashes_json)):
            waycare_crashes_json[i]['start_time'] = waycare_crashes_json[i]['start_time']- datetime.timedelta(0,time_offset_from_udt*3600)          
        add_data_to_calender(waycare_crashes_json)
        #make_calender_struct()        
        spillman_crashes_json = mongo_fetch.mongo_fetch('res/spillman_crashes.json')
        spillman_crashes_json = input_locations(spillman_crashes_json)
        for i in range(0,len(spillman_crashes_json)):
            spillman_crashes_json[i]['start_time'] = spillman_crashes_json[i]['start_time']- datetime.timedelta(0,time_offset_from_udt*3600)                
        add_data_to_calender(spillman_crashes_json)
        #print(forecast_calender_struct)
        forecast_print.print_calender_charts(forecast_calender_struct,'spillman_waycare')        
        #print(forecast_calender_struct)        
        forecast_calender_struct.clear()    
        
    if ('lv' in files_to_analyze):
        make_calender_struct()        
        lv_struct = []
        #lvsdf = lv struct date filtered (lv's that are being used and have been trimmed down to only useful data)
        lvsdf = []
        
        with open(crashes_file_name, 'rb') as csvfile:
            for row in csvfile:
                lv_struct.append(row)    
        for i in range(2,len(lv_struct)):
            lv_struct[i] = str(lv_struct[i]).strip("b\"'").split(',')
            lv_struct[i][0] = lv_struct[i][0].split(' ')
            if lv_struct[i][0][0] == 'Oct':
                lv_refined = get_refined_lv(lv_struct[i])
                #print(lv_refined)
                if(lv_refined['street'] != 'NA'):
                    lvsdf.append(lv_refined)
        #print(lvsdf[0])            
        add_data_to_calender(lvsdf)
        forecast_print.print_calender_charts(forecast_calender_struct,'lv')
        forecast_calender_struct.clear()
    lastFiletxt = open('snur_file'+"_"+str(universal_imports.datetime.utcnow())+'.txt', 'w')    
    for i in range(0,len(snur)):
        lastFiletxt.write(str(snur[i])+"\n")       
    lastFiletxt.close()
    #print(snur)

#check that spillman/waycare times are correct       
    

do_all_the_stuff(['spillman_waycare','spillman'])
#for i in range(0,len(forecast_calender_struct)):
    #for key in forecast_calender_struct[i].keys():
        #print("day "+str(i)+" time slot "+str(key))
        #if('I15N_PREDICTION' in forecast_calender_struct[i][key].keys()):
            #print (str(forecast_calender_struct[i][key]['I15N_PREDICTION'])+" "+str(forecast_calender_struct[i][key]['I15S_PREDICTION'])+ " "+str(forecast_calender_struct[i][key]['US95S_PREDICTION'])+" "+str(forecast_calender_struct[i][key]['US95N_PREDICTION']))


            
#forecast_print.forecast_print(forecast_calender_struct)
#print(forecast_calender_struct)
#print(snur)
#lastFiletxt = open('snur.txt', 'w')
#lastFiletxt.write(str(snur))
#lastFiletxt.close()
#print(lvsdf)
#print(lv_struct[0])
#print(lv_struct[1])
#print(lv_struct[2])
#print(lv_struct[3])

#print(lvsdf[1])
#print(lvsdf[2])
#print(lvsdf[3])
