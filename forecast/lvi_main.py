import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts/globals')
import universal_imports
import sendemail
from email.mime.text import MIMEText
from dateutil.parser import parse
import datetime
from datetime import timedelta
import googlemaps
from googlemaps import roads
from math import sin, cos, sqrt, atan2, radians
import mongo_fetch
import mongo_universal

def process_struct_2(struct_2):
    for i in range(1,len(struct_2)):
        data = str(struct_2[i]).strip("b',").split(',')
        data.append({4:0,10:0,16:0,22:0,'total':0})
        data[3] = data[3].strip('\\n')
        data[2] = float(data[2])
        data[3] = float(data[3])
        #struct_2[i] = struct_2[i].pop(0)
        struct_2[i] = data
    return struct_2
def process_struct_1(struct_1):
    for i in range(1,len(struct_1)):
        data = str(struct_1[i])
        data = data.split(',')
        data[0] = int(data[0].strip("b'"))
        data[1] = float(data[1])
        data[2] = float(data[2])
        data[3] = int(data[3].strip("\\n'"))        
        struct_1[i] = data
    return struct_1
main_struct_1 = []
main_struct_2 = []
with open('LasVegasIncidentsByDayAndHour_geotab.csv', 'rb') as csvfile:
    for row in csvfile:
        main_struct_1.append(row)   
with open('LasVegasIncidentsByDayAndHour_geotab_2.csv', 'rb') as csvfile:
    for row in csvfile:
        main_struct_2.append(row)   
main_struct_1 = process_struct_1(main_struct_1)
main_struct_2 = process_struct_2(main_struct_2)

print(main_struct_1[2])
print(main_struct_2[2])
for i in range(0,len(main_struct_2)):
    coord_2= (main_struct_2[i][2],main_struct_2[i][3])    
    #do calculations by 4-9,10-15,16-21,22-0 & 1-3
    for n in range(0,len(main_struct_1)):
        coord_1=(main_struct_1[n][1],main_struct_1[n][2])
        
        if mongo_universal.close_enough_distance(coord_1 , coord_2 ,750) == True:
            if main_struct_1[n][0] >= 4 and main_struct_1[n][0] < 10:
                main_struct_2[i][4][4] += main_struct_1[n][3]
            if main_struct_1[n][0] >= 10 and main_struct_1[n][0] < 16:
                main_struct_2[i][4][10] += main_struct_1[n][3]
            if main_struct_1[n][0] >= 16 and main_struct_1[n][0] < 22:
                main_struct_2[i][4][16] += main_struct_1[n][3]    
            if main_struct_1[n][0] >= 22 and main_struct_1[n][0] <= 23:
                main_struct_2[i][4][22] += main_struct_1[n][3]      
            if main_struct_1[n][0] >= 0 and main_struct_1[n][0] <= 3:
                main_struct_2[i][4][22] += main_struct_1[n][3]   
            main_struct_2[i][4]['total']+= main_struct_1[n][3]                
lastFiletxt = open("lvi"+"_"+str(universal_imports.datetime.utcnow())+'.txt', 'w')  
for i in range(0,len(main_struct_1)):
    lastFiletxt.write(str(main_struct_1[i])+"\n")                 
#for i in range(0,len(main_struct_2)):
#    print(main_struct_2[i])
                #main_struct_2[i][3]{''} = main_struct_2[i][3]{''} + main_struct_1[n][3]
    #sum up struct 2 by row and put under 'total'       
#print results (which is struct_2)
    


