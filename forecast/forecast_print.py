import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports

def forecast_print(forecast_calender_struct):
    results_list = []
    start_time_slots = 6
    end_time_slots = 22
    
    
    total_accidents_slots_predicted_I15N = 0
    total_accidents_slots_predicted_I15S = 0
    total_accidents_slots_predicted_US95N = 0
    total_accidents_slots_predicted_US95S = 0
    
    total_number_of_accidents_predicted_correctly_I15N = 0
    total_number_of_accidents_predicted_correctly_I15S = 0
    total_number_of_accidents_predicted_correctly_US95N = 0
    total_number_of_accidents_predicted_correctly_US95S = 0
    
    total_crashes_in_slots_I15N = 0
    total_crashes_in_slots_I15S = 0
    total_crashes_in_slots_US95N = 0
    total_crashes_in_slots_US95S = 0
    
    total_accident_slots_correct_I15N = 0
    total_accident_slots_correct_I15S = 0
    total_accident_slots_correct_US95N = 0
    total_accident_slots_correct_US95S = 0
    
    total_accident_slots_I15N = 0
    total_accident_slots_I15S = 0
    total_accident_slots_US95N = 0
    total_accident_slots_US95S = 0
    
    total_accidents_I15N = 0
    total_accidents_I15S = 0
    total_accidents_US95N = 0
    total_accidents_US95S = 0
    
    total_slots_I15N = 0
    total_slots_I15S = 0
    total_slots_US95N = 0
    total_slots_US95S = 0
    
    total_nonaccident_slots_predicted_I15N = 0
    total_nonaccident_slots_predicted_I15S = 0
    total_nonaccident_slots_predicted_US95N = 0
    total_nonaccident_slots_predicted_US95S = 0
    
    nonaccident_slots_predicted_correctly_I15N = 0
    nonaccident_slots_predicted_correctly_I15S = 0
    nonaccident_slots_predicted_correctly_US95N = 0
    nonaccident_slots_predicted_correctly_US95S = 0
    
    
    
    for i in range(0,len(forecast_calender_struct)):
        #print(forecast_calender_struct[i])
        for key in forecast_calender_struct[i].keys():
            if key >= start_time_slots and key < end_time_slots:
                total_slots_I15N += 1
                total_slots_I15S += 1
                total_slots_US95N += 1
                total_slots_US95S += 1            
                #crashes are only while predictions are happening, not before or after            
                if 'I15N_PREDICTION' in forecast_calender_struct[i][key].keys():
                    total_slots_I15N+=1
                    if forecast_calender_struct[i][key]['I15N_PREDICTION'] == 2.0:
                        total_accidents_slots_predicted_I15N+=1
                        if(len(forecast_calender_struct[i][key]['I15N_CRASHES'])>0):
                            total_accident_slots_correct_I15N+=1
                            total_number_of_accidents_predicted_correctly_I15N += len(forecast_calender_struct[i][key]['I15N_CRASHES'])
                    else:
                        total_nonaccident_slots_predicted_I15N+=1
                        if ('I15N_CRASHES' not in forecast_calender_struct[i][key].keys() or len(forecast_calender_struct[i][key]['I15N_CRASHES'])==0):
                            nonaccident_slots_predicted_correctly_I15N+=1
                    if(len(forecast_calender_struct[i][key]['I15N_CRASHES'])>0):
                        total_accident_slots_I15N+=1
                        total_crashes_in_slots_I15N += len(forecast_calender_struct[i][key]['I15N_CRASHES'])       
    
    
                if 'I15S_PREDICTION' in forecast_calender_struct[i][key].keys():
                    total_slots_I15S+=1
                    if forecast_calender_struct[i][key]['I15S_PREDICTION'] == 2.0:
                        total_accidents_slots_predicted_I15S+=1
                        if(len(forecast_calender_struct[i][key]['I15S_CRASHES'])>0):
                            total_accident_slots_correct_I15S+=1
                            total_number_of_accidents_predicted_correctly_I15S += len(forecast_calender_struct[i][key]['I15S_CRASHES'])
                    else:
                        total_nonaccident_slots_predicted_I15S+=1
                        if ('I15S_CRASHES' not in forecast_calender_struct[i][key].keys() or len(forecast_calender_struct[i][key]['I15S_CRASHES'])==0):
                            nonaccident_slots_predicted_correctly_I15S+=1                    
                    if(len(forecast_calender_struct[i][key]['I15S_CRASHES'])>0):
                        total_accident_slots_I15S+=1
                        total_crashes_in_slots_I15S += len(forecast_calender_struct[i][key]['I15S_CRASHES']) 
    
    
                if 'US95N_PREDICTION' in forecast_calender_struct[i][key].keys():
                    total_slots_US95N +=1
                    if forecast_calender_struct[i][key]['US95N_PREDICTION'] == 2.0:
                        total_accidents_slots_predicted_US95N+=1
                        if(len(forecast_calender_struct[i][key]['US95N_CRASHES'])>0):
                            total_accident_slots_correct_US95N+=1
                            total_number_of_accidents_predicted_correctly_US95N += len(forecast_calender_struct[i][key]['US95N_CRASHES'])
                    else:
                        total_nonaccident_slots_predicted_US95N+=1
                        if ('US95N_CRASHES' not in forecast_calender_struct[i][key].keys() or len(forecast_calender_struct[i][key]['US95N_CRASHES'])==0):
                            nonaccident_slots_predicted_correctly_US95N+=1                    
                    if(len(forecast_calender_struct[i][key]['US95N_CRASHES'])>0):
                        total_accident_slots_US95N+=1
                        total_crashes_in_slots_US95N += len(forecast_calender_struct[i][key]['US95N_CRASHES'])
    
    
                if 'US95S_PREDICTION' in forecast_calender_struct[i][key].keys():
                    total_slots_US95S+=1
                    if forecast_calender_struct[i][key]['US95S_PREDICTION'] == 2.0:
                        total_accidents_slots_predicted_US95S+=1
                        if(len(forecast_calender_struct[i][key]['US95S_CRASHES'])>0):
                            total_accident_slots_correct_US95S+=1
                            total_number_of_accidents_predicted_correctly_US95S += len(forecast_calender_struct[i][key]['US95S_CRASHES'])
                    else:
                        total_nonaccident_slots_predicted_US95S+=1
                        if ('US95S_CRASHES' not in forecast_calender_struct[i][key].keys() or len(forecast_calender_struct[i][key]['US95S_CRASHES'])==0):
                            nonaccident_slots_predicted_correctly_US95S+=1                    
                    if(len(forecast_calender_struct[i][key]['US95S_CRASHES'])>0):
                        total_accident_slots_US95S+=1
                        total_crashes_in_slots_US95S += len(forecast_calender_struct[i][key]['US95S_CRASHES']) 
            if('I15S_CRASHES' in forecast_calender_struct[i][key].keys()):
                total_accidents_I15S += len(forecast_calender_struct[i][key]['I15S_CRASHES'])           
            if('I15N_CRASHES' in forecast_calender_struct[i][key].keys()):
                total_accidents_I15N += len(forecast_calender_struct[i][key]['I15N_CRASHES'])   
            if('US95S_CRASHES' in forecast_calender_struct[i][key].keys()):
                total_accidents_US95S += len(forecast_calender_struct[i][key]['US95S_CRASHES'])           
            if('US95N_CRASHES' in forecast_calender_struct[i][key].keys()):
                total_accidents_US95N += len(forecast_calender_struct[i][key]['US95N_CRASHES'])             
    
    
    
    results_list.append("I15N:")
    results_list.append("total_number_of_accidents_predicted_correctly_I15N: "+str(total_number_of_accidents_predicted_correctly_I15N))
    results_list.append("total_crashes_in_slots_I15N: "+str(total_crashes_in_slots_I15N))
    results_list.append("accident hit rate I15N (accidents predicted correctly/total crashes in slots)(%): "+str(total_number_of_accidents_predicted_correctly_I15N/total_crashes_in_slots_I15N*100))
    results_list.append("Total accidents I15N: "+str(total_accidents_I15N))    
    results_list.append("accident hit rate I15N (accidents predicted correctly/total crashes)(%): "+str(total_number_of_accidents_predicted_correctly_I15N/total_accidents_I15N*100))        
    results_list.append("Total accident slots predicted correctly I15N: "+str(total_accident_slots_correct_I15N))    
    results_list.append("Total accident slots I15N: "+str(total_accident_slots_I15N))    
    results_list.append("accident slot hit rate I15N (accident slots predicted correctly/total accident slots)(%): "+str(total_accident_slots_correct_I15N/total_accident_slots_I15N*100))
    results_list.append("total_accidents_slots_predicted_I15N: "+str(total_accidents_slots_predicted_I15N))    
    results_list.append("Total slots in time range I15N: "+str(total_slots_I15N))
    results_list.append("Non-Accident hit-rate (predicted correct noncrash slots/total noncrash slots) I15N: "+str(nonaccident_slots_predicted_correctly_I15N/total_nonaccident_slots_predicted_I15N*100))
    results_list.append("Average slots hit rate I15N: "+str(((nonaccident_slots_predicted_correctly_I15N/total_nonaccident_slots_predicted_I15N*100)+(total_accident_slots_correct_I15N/total_accident_slots_I15N*100))/2))
    results_list.append("accident miss rate I15N (accidents predicted correctly/total accidents)(%): "+str((1-(total_number_of_accidents_predicted_correctly_I15N/total_crashes_in_slots_I15N))*100))
    results_list.append("accident slot miss rate I15N (accident slots predicted correctly/total accident slots)(%): "+str((1-(total_accident_slots_correct_I15N/total_accident_slots_I15N))*100))
    
    results_list.append("I15S:")
    results_list.append("total_number_of_accidents_predicted_correctly_I15S: "+str(total_number_of_accidents_predicted_correctly_I15S))
    results_list.append("total_crashes_in_slots_I15S: "+str(total_crashes_in_slots_I15S))
    results_list.append("accident hit rate I15S (accidents predicted correctly/total crashes in slots)(%): "+str(total_number_of_accidents_predicted_correctly_I15S/total_crashes_in_slots_I15S*100))
    results_list.append("Total accidents I15S: "+str(total_accidents_I15S))    
    results_list.append("accident hit rate I15S (accidents predicted correctly/total crashes)(%): "+str(total_number_of_accidents_predicted_correctly_I15S/total_accidents_I15S*100))        
    results_list.append("Total accident slots predicted correctly I15S: "+str(total_accident_slots_correct_I15S))    
    results_list.append("Total accident slots I15S: "+str(total_accident_slots_I15S))    
    results_list.append("accident slot hit rate I15S (accident slots predicted correctly/total accident slots)(%): "+str(total_accident_slots_correct_I15S/total_accident_slots_I15S*100))
    results_list.append("total_accidents_slots_predicted_I15S: "+str(total_accidents_slots_predicted_I15S))    
    results_list.append("Total slots in time range I15S: "+str(total_slots_I15S))
    results_list.append("Non-Accident hit-rate (predicted correct noncrash slots/total noncrash slots) I15S: "+str(nonaccident_slots_predicted_correctly_I15S/total_nonaccident_slots_predicted_I15S*100))
    results_list.append("Average slots hit rate I15S: "+str(((nonaccident_slots_predicted_correctly_I15S/total_nonaccident_slots_predicted_I15S*100)+(total_accident_slots_correct_I15S/total_accident_slots_I15S*100))/2))
    results_list.append("accident miss rate I15S (accidents predicted correctly/total accidents)(%): "+str((1-(total_number_of_accidents_predicted_correctly_I15S/total_crashes_in_slots_I15S))*100))
    results_list.append("accident slot miss rate I15S (accident slots predicted correctly/total accident slots)(%): "+str((1-(total_accident_slots_correct_I15S/total_accident_slots_I15S))*100))
    
    results_list.append("US95S:")
    results_list.append("total_number_of_accidents_predicted_correctly_US95S: "+str(total_number_of_accidents_predicted_correctly_US95S))
    results_list.append("total_crashes_in_slots_US95S: "+str(total_crashes_in_slots_US95S))
    results_list.append("accident hit rate US95S (accidents predicted correctly/total crashes in slots)(%): "+str(total_number_of_accidents_predicted_correctly_US95S/total_crashes_in_slots_US95S*100))
    results_list.append("Total accidents US95S: "+str(total_accidents_US95S))    
    results_list.append("accident hit rate US95S (accidents predicted correctly/total crashes)(%): "+str(total_number_of_accidents_predicted_correctly_US95S/total_accidents_US95S*100))        
    results_list.append("Total accident slots predicted correctly US95S: "+str(total_accident_slots_correct_US95S))    
    results_list.append("Total accident slots US95S: "+str(total_accident_slots_US95S))    
    results_list.append("accident slot hit rate US95S (accident slots predicted correctly/total accident slots)(%): "+str(total_accident_slots_correct_US95S/total_accident_slots_US95S*100))
    results_list.append("total_accidents_slots_predicted_US95S: "+str(total_accidents_slots_predicted_US95S))    
    results_list.append("Total slots in time range US95S: "+str(total_slots_US95S))
    results_list.append("Non-Accident hit-rate (predicted correct noncrash slots/total noncrash slots) US95S: "+str(nonaccident_slots_predicted_correctly_US95S/total_nonaccident_slots_predicted_US95S*100))
    results_list.append("Average slots hit rate US95S: "+str(((nonaccident_slots_predicted_correctly_US95S/total_nonaccident_slots_predicted_US95S*100)+(total_accident_slots_correct_US95S/total_accident_slots_US95S*100))/2))
    results_list.append("accident miss rate US95S (accidents predicted correctly/total accidents)(%): "+str((1-(total_number_of_accidents_predicted_correctly_US95S/total_crashes_in_slots_US95S))*100))
    results_list.append("accident slot miss rate US95S (accident slots predicted correctly/total accident slots)(%): "+str((1-(total_accident_slots_correct_US95S/total_accident_slots_US95S))*100))
    
    
    results_list.append("US95N:")
    results_list.append("total_number_of_accidents_predicted_correctly_US95N: "+str(total_number_of_accidents_predicted_correctly_US95N))
    results_list.append("total_crashes_in_slots_US95N: "+str(total_crashes_in_slots_US95N))
    results_list.append("accident hit rate US95N (accidents predicted correctly/total crashes in slots)(%): "+str(total_number_of_accidents_predicted_correctly_US95N/total_crashes_in_slots_US95N*100))
    results_list.append("Total accidents US95N: "+str(total_accidents_US95N))    
    results_list.append("accident hit rate US95N (accidents predicted correctly/total crashes)(%): "+str(total_number_of_accidents_predicted_correctly_US95N/total_accidents_US95N*100))        
    results_list.append("Total accident slots predicted correctly US95N: "+str(total_accident_slots_correct_US95N))    
    results_list.append("Total accident slots US95N: "+str(total_accident_slots_US95N))    
    results_list.append("accident slot hit rate US95N (accident slots predicted correctly/total accident slots)(%): "+str(total_accident_slots_correct_US95N/total_accident_slots_US95N*100))
    results_list.append("total_accidents_slots_predicted_US95N: "+str(total_accidents_slots_predicted_US95N))    
    results_list.append("Total slots in time range US95N: "+str(total_slots_US95N))
    results_list.append("Non-Accident hit-rate (predicted correct noncrash slots/total noncrash slots) US95N: "+str(nonaccident_slots_predicted_correctly_US95N/total_nonaccident_slots_predicted_US95N*100))
    results_list.append("Average slots hit rate US95N: "+str(((nonaccident_slots_predicted_correctly_US95N/total_nonaccident_slots_predicted_US95N*100)+(total_accident_slots_correct_US95N/total_accident_slots_US95N*100))/2))
    results_list.append("accident miss rate US95N (accidents predicted correctly/total accidents)(%): "+str((1-(total_number_of_accidents_predicted_correctly_US95N/total_crashes_in_slots_US95N))*100))
    results_list.append("accident slot miss rate US95N (accident slots predicted correctly/total accident slots)(%): "+str((1-(total_accident_slots_correct_US95N/total_accident_slots_US95N))*100))
    
    return results_list
def print_calender_charts(forecast_calender_struct,data_name):
    lastFiletxt = open(data_name+"_"+str(universal_imports.datetime.utcnow())+'.txt', 'w')  
    summary = forecast_print(forecast_calender_struct)
    for i in range(0,len(summary)):
        lastFiletxt.write(summary[i]+"\n")                      
    #print('\n')
    US_95N_CRASHES_STRING_LIST = []
    US_95N_PREDICTIONS_STRING_LIST = []
    US_95N_PREDICTIONS_STRING_LIST.append('Hr>| 6   | 8   |10   |12   |14   |16   |18   |20   |\nDay|')
    US_95N_CRASHES_STRING_LIST.append('Hr>|0  |2  |4  |6  |8  |10 |12 |14 |16 |18 |20 |22 |\nDay|')    
    for i in range(0,len(forecast_calender_struct)):
        if len(str(i))>1:
            line_string_crashes_US95N = str(i)+' |'        
        else:
            line_string_crashes_US95N = str(i)+'  |'        
        for key_1 in forecast_calender_struct[i].keys():
            if key_1>=6 and key_1<22:
                if "US95N_PREDICTION" in forecast_calender_struct[i][key_1]:
                    line_string_crashes_US95N+=str(forecast_calender_struct[i][key_1]["US95N_PREDICTION"])+"  |"
                else:
                    line_string_crashes_US95N+="     |"
        US_95N_PREDICTIONS_STRING_LIST.append(line_string_crashes_US95N)
        #print(forecast_calender_struct[i])
    for i in range(0,len(forecast_calender_struct)):
        if len(str(i))>1:
            line_string_crashes_US95N = str(i)+' |'        
        else:
            line_string_crashes_US95N = str(i)+'  |'        
        for key_1 in forecast_calender_struct[i].keys():
            if "US95N_CRASHES" in forecast_calender_struct[i][key_1]:
                if len(forecast_calender_struct[i][key_1]["US95N_CRASHES"])>0:
                    if len(forecast_calender_struct[i][key_1]["US95N_CRASHES"])>1:
                        line_string_crashes_US95N+=str(len(forecast_calender_struct[i][key_1]["US95N_CRASHES"]))+"  |"
                    else:
                        line_string_crashes_US95N+=str(len(forecast_calender_struct[i][key_1]["US95N_CRASHES"]))+"  |"                    
                else:
                    line_string_crashes_US95N+="   |"                    
            else:
                line_string_crashes_US95N+="   |"
        US_95N_CRASHES_STRING_LIST.append(line_string_crashes_US95N)  
    lastFiletxt.write(data_name+' US95N predictions:\n')       
    for i in range(0,len(US_95N_PREDICTIONS_STRING_LIST)):
        lastFiletxt.write(US_95N_PREDICTIONS_STRING_LIST[i]+"\n")   
    lastFiletxt.write(data_name+' US95N crashes:\n')              
    for i in range(0,len(US_95N_CRASHES_STRING_LIST)):
        lastFiletxt.write(US_95N_CRASHES_STRING_LIST[i]+"\n")              
        
    US_95S_CRASHES_STRING_LIST = []
    US_95S_PREDICTIONS_STRING_LIST = []
    US_95S_PREDICTIONS_STRING_LIST.append('Hr>| 6   | 8   |10   |12   |14   |16   |18   |20   |\nDay|')
    US_95S_CRASHES_STRING_LIST.append('Hr>|0  |2  |4  |6  |8  |10 |12 |14 |16 |18 |20 |22 |\nDay|')    
    for i in range(0,len(forecast_calender_struct)):
        if len(str(i))>1:
            line_string_crashes_US95S = str(i)+' |'        
        else:
            line_string_crashes_US95S = str(i)+'  |'        
        for key_1 in forecast_calender_struct[i].keys():
            if key_1>=6 and key_1<22:
                if "US95S_PREDICTION" in forecast_calender_struct[i][key_1]:
                    line_string_crashes_US95S+=str(forecast_calender_struct[i][key_1]["US95S_PREDICTION"])+"  |"
                else:
                    line_string_crashes_US95S+="     |"
        US_95S_PREDICTIONS_STRING_LIST.append(line_string_crashes_US95S)
        #print(forecast_calender_struct[i])
    for i in range(0,len(forecast_calender_struct)):
        if len(str(i))>1:
            line_string_crashes_US95S = str(i)+' |'        
        else:
            line_string_crashes_US95S = str(i)+'  |'        
        for key_1 in forecast_calender_struct[i].keys():
            if "US95S_CRASHES" in forecast_calender_struct[i][key_1]:
                if len(forecast_calender_struct[i][key_1]["US95S_CRASHES"])>0:
                    if len(forecast_calender_struct[i][key_1]["US95S_CRASHES"])>1:
                        line_string_crashes_US95S+=str(len(forecast_calender_struct[i][key_1]["US95S_CRASHES"]))+"  |"
                    else:                        
                        line_string_crashes_US95S+=str(len(forecast_calender_struct[i][key_1]["US95S_CRASHES"]))+"  |"

                else:
                    line_string_crashes_US95S+="   |"                    
            else:
                line_string_crashes_US95S+="   |"
        US_95S_CRASHES_STRING_LIST.append(line_string_crashes_US95S)  
    lastFiletxt.write(data_name+' US95S predictions:\n')                          
    for i in range(0,len(US_95S_PREDICTIONS_STRING_LIST)):
        lastFiletxt.write(US_95S_PREDICTIONS_STRING_LIST[i]+'\n')
    lastFiletxt.write(data_name+' US95S crashes:\n')                                  
    for i in range(0,len(US_95S_CRASHES_STRING_LIST)):
        lastFiletxt.write(US_95S_CRASHES_STRING_LIST[i]+'\n')                                         
        
    I15N_CRASHES_STRING_LIST = []
    I15N_PREDICTIONS_STRING_LIST = []
    I15N_PREDICTIONS_STRING_LIST.append('Hr>| 6   | 8   |10   |12   |14   |16   |18   |20   |\nDay|')
    I15N_CRASHES_STRING_LIST.append('Hr>|0  |2  |4  |6  |8  |10 |12 |14 |16 |18 |20 |22 |\nDay|')    
    for i in range(0,len(forecast_calender_struct)):
        if len(str(i))>1:
            line_string_crashes_I15N = str(i)+' |'        
        else:
            line_string_crashes_I15N = str(i)+'  |'        
        for key_1 in forecast_calender_struct[i].keys():
            if key_1>=6 and key_1<22:
                if "I15N_PREDICTION" in forecast_calender_struct[i][key_1]:
                    line_string_crashes_I15N+=str(forecast_calender_struct[i][key_1]["I15N_PREDICTION"])+"  |"
                else:
                    line_string_crashes_I15N+="     |"
        I15N_PREDICTIONS_STRING_LIST.append(line_string_crashes_I15N)
        #print(forecast_calender_struct[i])
    for i in range(0,len(forecast_calender_struct)):
        if len(str(i))>1:
            line_string_crashes_I15N = str(i)+' |'        
        else:
            line_string_crashes_I15N = str(i)+'  |'        
        for key_1 in forecast_calender_struct[i].keys():
            if "I15N_CRASHES" in forecast_calender_struct[i][key_1]:
                if len(forecast_calender_struct[i][key_1]["I15N_CRASHES"])>0:
                    if len(forecast_calender_struct[i][key_1]["I15N_CRASHES"])>1:
                        line_string_crashes_I15N+=str(len(forecast_calender_struct[i][key_1]["I15N_CRASHES"]))+"  |"
                        
                    else:
                        line_string_crashes_I15N+=str(len(forecast_calender_struct[i][key_1]["I15N_CRASHES"]))+"  |"
                else:
                    line_string_crashes_I15N+="   |"                    
            else:
                line_string_crashes_I15N+="   |"
        I15N_CRASHES_STRING_LIST.append(line_string_crashes_I15N) 
    lastFiletxt.write(data_name+' I15N predictions:\n')                  
    for i in range(0,len(I15N_PREDICTIONS_STRING_LIST)):
        lastFiletxt.write(I15N_PREDICTIONS_STRING_LIST[i]+'\n')    
    lastFiletxt.write(data_name+' I15N crashes:\n')                  
    for i in range(0,len(I15N_CRASHES_STRING_LIST)):
        lastFiletxt.write(I15N_CRASHES_STRING_LIST[i]+'\n')                  
        
        
    
    I15S_CRASHES_STRING_LIST = []
    I15S_PREDICTIONS_STRING_LIST = []
    I15S_PREDICTIONS_STRING_LIST.append('Hr>| 6   | 8   |10   |12   |14   |16   |18   |20   |\nDay|')
    I15S_CRASHES_STRING_LIST.append('Hr>|0  |2  |4  |6  |8  |10 |12 |14 |16 |18 |20 |22 |\nDay|')    
    for i in range(0,len(forecast_calender_struct)):
        if len(str(i))>1:
            line_string_crashes_I15S = str(i)+' |'        
        else:
            line_string_crashes_I15S = str(i)+'  |'        
        for key_1 in forecast_calender_struct[i].keys():
            if key_1>=6 and key_1<22:
                if "I15S_PREDICTION" in forecast_calender_struct[i][key_1]:
                    #print("x"+str(forecast_calender_struct[i][key_1]["I15S_PREDICTION"])+"x")                    
                    line_string_crashes_I15S+=str(forecast_calender_struct[i][key_1]["I15S_PREDICTION"])+"  |"
                else:
                    line_string_crashes_I15S+="     |"
        I15S_PREDICTIONS_STRING_LIST.append(line_string_crashes_I15S)
        #print(forecast_calender_struct[i])
    for i in range(0,len(forecast_calender_struct)):
        if len(str(i))>1:
            line_string_crashes_I15S = str(i)+' |'        
        else:
            line_string_crashes_I15S = str(i)+'  |'        
        for key_1 in forecast_calender_struct[i].keys():
            if "I15S_CRASHES" in forecast_calender_struct[i][key_1]:
                if len(forecast_calender_struct[i][key_1]["I15S_CRASHES"])>0:
                    line_string_crashes_I15S+=str(len(forecast_calender_struct[i][key_1]["I15S_CRASHES"]))+"  |"
                else:
                    line_string_crashes_I15S+="   |"                    
            else:
                line_string_crashes_I15S+="   |"
        I15S_CRASHES_STRING_LIST.append(line_string_crashes_I15S)  
    lastFiletxt.write(data_name+' I15S predictions:\n')     
    
    for i in range(0,len(I15S_PREDICTIONS_STRING_LIST)):
        lastFiletxt.write(I15S_PREDICTIONS_STRING_LIST[i]+"\n")     
    lastFiletxt.write(data_name+' I15S crashes:\n')                 
    for i in range(0,len(I15S_CRASHES_STRING_LIST)):
        lastFiletxt.write(I15S_CRASHES_STRING_LIST[i]+"\n")                 
    lastFiletxt.close()