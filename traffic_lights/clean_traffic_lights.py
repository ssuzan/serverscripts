#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: json containing data from waze database
#output: properly formatted json containing data from waze database
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
from automation_building_scripts import automate_build_clean
import universal_imports
import jsonlib_python3
import requests


def clean_incident(incident):
    if('length' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['length'],int)):
            place_holder_variable = True
        else:
            print(incident['length'])
            return(False,'length not int')
    else:
        return(False,'length not present')
    if('roadType' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['roadType'],int)):
            place_holder_variable = True
        else:
            print(incident['roadType'])
            return(False,'roadType not int')
    else:
        return(False,'roadType not present')
    if('type' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['type'],str)):
            place_holder_variable = True
        else:
            print(incident['type'])
            return(False,'type not str')
    else:
        return(False,'type not present')
    if('country' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['country'],str)):
            place_holder_variable = True
        else:
            print(incident['country'])
            return(False,'country not str')
    else:
        return(False,'country not present')
    if('turnType' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['turnType'],str)):
            place_holder_variable = True
        else:
            print(incident['turnType'])
            return(False,'turnType not str')
    else:
        return(False,'turnType not present')
    if('delay' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['delay'],int)):
            place_holder_variable = True
        else:
            print(incident['delay'])
            return(False,'delay not int')
    else:
        return(False,'delay not present')
    if('street' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['street'],str)):
            place_holder_variable = True
        else:
            print(incident['street'])
            return(False,'street not str')
    else:
        return(False,'street not present')
    if('pubMillis' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['pubMillis'],int)):
            place_holder_variable = True
        else:
            print(incident['pubMillis'])
            return(False,'pubMillis not int')
    else:
        return(False,'pubMillis not present')
    if('line' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['line'],list)):
            place_holder_variable = True
        else:
            print(incident['line'])
            return(False,'line not list')
    else:
        return(False,'line not present')
    if('id' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['id'],int)):
            place_holder_variable = True
        else:
            print(incident['id'])
            return(False,'id not int')
    else:
        return(False,'id not present')
    if('segments' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['segments'],list)):
            place_holder_variable = True
        else:
            print(incident['segments'])
            return(False,'segments not list')
    else:
        return(False,'segments not present')
    if('uuid' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['uuid'],int)):
            place_holder_variable = True
        else:
            print(incident['uuid'])
            return(False,'uuid not int')
    else:
        return(False,'uuid not present')
    if('city' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['city'],str)):
            place_holder_variable = True
        else:
            print(incident['city'])
            return(False,'city not str')
    if('speed' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['speed'],float) or isinstance(incident['speed'],int)):
            place_holder_variable = True
        else:
            print(incident['speed'])
            return(False,'speed not float')
    else:
        return(False,'speed not present')
    if('level' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['level'],int)):
            place_holder_variable = True
        else:
            print(incident['level'])
            return(False,'level not int')
    else:
        return(False,'level not present')    
    return (True, 'no errors')
def clean(json_clean):
    if('startTime' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['startTime'],str)):
            place_holder_variable = True
        else:
            print(json_clean['startTime'])
            return(False,'startTime not str')
    else:
        return(False,'startTime not present')
    if('endTimeMillis' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['endTimeMillis'],int)):
            place_holder_variable = True
        else:
            print(json_clean['endTimeMillis'])
            return(False,'endTimeMillis not int')
    else:
        return(False,'endTimeMillis not present')
    if('endTime' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['endTime'],str)):
            place_holder_variable = True
        else:
            print(json_clean['endTime'])
            return(False,'endTime not str')
    else:
        return(False,'endTime not present')
   
    if('startTimeMillis' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['startTimeMillis'],int)):
            place_holder_variable = True
        else:
            print(json_clean['startTimeMillis'])
            return(False,'startTimeMillis not int')
    else:
        return(False,'startTimeMillis not present')
    if('jams' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['jams'],list)):
            for i in range(0,len(json_clean['jams'])):
                if clean_incident(json_clean['jams'][i])[0] == False:
                    return clean_incident(json_clean['jams'][i])
        else:
            print(json_clean['jams'])
            return(False,'jams not list')
    else:
        return(True,'jams not present')    
    return(True, 'jams present')     