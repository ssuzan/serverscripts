#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: waze database https request (url) 
#output: json of data given by https request or json containing error message
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports
#import jsonlib_python3
import requests


def downloadWaze(url_waze):  
    try:
        #gets json from url
        r = requests.get(url_waze)
        r = r.json()
    except requests.exceptions.ConnectionError:
        r = universal_imports.json.loads('{"error" : "requests.exceptions.ConnectionError"}')  
    return r
     
