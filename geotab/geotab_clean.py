import sys,os
sys.path.append("os.environ['WORKSPACE']+'/serverscripts/globals'")
import universal_imports

def geotab_clean(geotab_data):
    #print(geotab_data)
    #print(geotab_data.keys())
    if ('bearing' in geotab_data.keys()):
        placeholder_variable = True
        if(isinstance(geotab_data['bearing'], int)):
            placeholder_variable = True
        else:
            return(False,'bearing not int')
    else:
        return(False,'bearing not present')
    if ('longitude' in geotab_data.keys()):
        placeholder_variable = True
        if(isinstance(geotab_data['longitude'], float)):
            placeholder_variable = True
        else:
            return(False,'longitude not float')        
    else:
        return(False,'longitude not present')
    if ('latitude' in geotab_data.keys()):
        placeholder_variable = True
        if(isinstance(geotab_data['latitude'], float)):
            placeholder_variable = True
        else:
            return(False,'latitude not float')        
    else:
        return(False,'latitude not present')
    if ('groups' in geotab_data.keys()):
        placeholder_variable = True
        if(isinstance(geotab_data['groups'], list)):
            placeholder_variable = True  
            #print(geotab_data['groups'][0].keys())
            if(isinstance(geotab_data['groups'][0], dict)):
                placeholder_variable = True 
                if('children' in geotab_data['groups'][0].keys()):
                    placeholder_variable = True          
                else:
                    return(False,'children not in groups[0]')    
                if('id' in geotab_data['groups'][0].keys()):
                    placeholder_variable = True          
                else:
                    return(False, 'id not in groups[0]')                    
            else:
                return(False,'groups[0] not dict')                
        else:
            return(False, 'groups not list')    
    else:
        return(False, 'groups not present')
    if ('dateTime' in geotab_data.keys()):
        placeholder_variable = True
        if(isinstance(geotab_data['dateTime'], universal_imports.datetime)):
            placeholder_variable = True
        else:
            return(False, 'dateTime not universal_imports.datetime')         
    else:
        return(False, 'dateTime not present')
    if ('exceptionEvents' in geotab_data.keys()):
        placeholder_variable = True
        if(isinstance(geotab_data['exceptionEvents'], list)):
            placeholder_variable = True
        else:
            return(False, 'exceptionEvents not list')        
    else:
        return(False, 'exceptionEvents not present')
    if ('driver' in geotab_data.keys()):
        placeholder_variable = True
        if(isinstance(geotab_data['driver'], dict) or isinstance(geotab_data['driver'],str)):
            placeholder_variable = True
        else:
            return(False, 'driver not dict')           
    else:
        return(False, 'driver not present')
    if ('speed' in geotab_data.keys()):
        placeholder_variable = True
        if(isinstance(geotab_data['speed'], float)):
            placeholder_variable = True
        else:
            return(False, 'speed not float')         
    else:
        return(False, 'speed not present')
    if ('device' in geotab_data.keys()):
        placeholder_variable = True
        if(isinstance(geotab_data['device'], dict)):
            placeholder_variable = True
        else:
            return(False, 'device not dict')         
    else:
        return(False, 'device not present')
    if ('isDriving' in geotab_data.keys()):
        placeholder_variable = True
        if(isinstance(geotab_data['isDriving'], bool)):
            placeholder_variable = True
        else:
            return(False, 'isDriving not bool')        
    else:
        return(False, 'isDriving not present')
    if ('currentStateDuration' in geotab_data.keys()):
        placeholder_variable = True
        if(isinstance(geotab_data['currentStateDuration'], str)):
            placeholder_variable = True
        else:
            return(False, 'currentStateDuration not str')           
    else:
        return(False,'currentStateDuration not present')
    if ('isDeviceCommunicating' in geotab_data.keys()):
        placeholder_variable = True
        if(isinstance(geotab_data['isDeviceCommunicating'], bool)):
            placeholder_variable = True
        else:
            return(False, 'isDeviceCommunicating not bool')         
    else:
        return(False,'isDeviceCommunicating not present')
    return(True, 'no error')
