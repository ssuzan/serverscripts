#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: json containing data from waze database
#output: properly formatted json containing data from waze database
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
from automation_building_scripts import automate_build_clean
import universal_imports
import jsonlib_python3
import requests
  

def clean_incident(incident):
    errors=[]  
    for key in incident:
        if key not in ["description", "id", "location","schedule","severity","speedAt","speedBefore","speedDelta","version"]:
            errors.append('new key '+key+' in dangerousSlowdowns')         
    if('id' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['id'],str)):
            place_holder_variable = True
        else:
            return([False])
    else:
        return([False])
    if('schedule' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['schedule'],dict)):
            for key in incident['schedule']:
                if key not in ["occurrenceStartTimeUTC"]:
                    errors.append('new key '+key+' in schedule')             
            if('occurrenceStartTimeUTC' in incident['schedule'].keys()):
                place_holder_variable = True
                if(isinstance(incident['schedule']['occurrenceStartTimeUTC'],str)):
                    place_holder_variable = True
                else:
                    incident['schedule']['occurrenceStartTimeUTC'] = ''
                    errors.append('occurrenceStartTimeUTC not str')
            else:
                errors.append('occurrenceStartTimeUTC not present')
                incident['schedule']['occurrenceStartTimeUTC'] = ''
        else:
            incident['schedule'] = ''
            errors.append('schedule not dict')
    else:
        errors.append('schedule not present')
        incident['schedule'] = ''
    if('version' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['version'],int)):
            place_holder_variable = True
        else:
            incident['version'] = ''
            errors.append('version not int')
    else:
        errors.append('version not present')
        incident['version'] = ''
    if('speedAt' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['speedAt'],int)):
            place_holder_variable = True
        else:
            incident['speedAt'] = ''
            errors.append('speedAt not int')
    else:
        errors.append('speedAt not present')
        incident['speedAt'] = ''
    if('location' in incident.keys()):
        place_holder_variable = True                    
        if(isinstance(incident['location'],dict)):
            for key in incident['location']:
                if key not in ["geometry", "segment","geoIds"]:
                    errors.append('new key '+key+' in location')              
            if('geometry' in incident['location'].keys()):
                place_holder_variable = True
                if(isinstance(incident['location']['geometry'],dict)):
                    for key in incident['location']['geometry']:
                        if key not in ["coordinates", "type"]:
                            errors.append('new key '+key+' in geometry')          
                    if('coordinates' in incident['location']['geometry'].keys()):
                        place_holder_variable = True
                        if(isinstance(incident['location']['geometry']['coordinates'],list)):
                            for n in range(0,len(incident['location']['geometry']['coordinates'])):
                                if(isinstance(incident['location']['geometry']['coordinates'][n],float)):
                                    place_holder_variable = True
                                else:
                                    incident['location']['geometry']['coordinates'][n] = ''
                                    errors.append('a coordinates element not float')
                        else:
                            errors.append('coordinates not list')
                            incident['location']['geometry']['coordinates'] = []                           
                    else:
                        errors.append('coordinates not present')
                        incident['location']['geometry']['coordinates'] = []
                    if('type' in incident['location']['geometry'].keys()):
                        place_holder_variable = True
                        if(isinstance(incident['location']['geometry']['type'],str)):
                            place_holder_variable = True
                        else:
                            incident['location']['geometry']['type'] = ''
                            errors.append('type not str')
                    else:
                        errors.append('type not present')
                        incident['location']['geometry']['type'] = ''
                else:
                    incident['location']['geometry'] = ''
                    errors.append('geometry not dict')
            else:
                errors.append('geometry not present')
                incident['location']['geometry'] = ''
            if('segment' in incident['location'].keys()):
                place_holder_variable = True
                if(isinstance(incident['location']['segment'],dict)):
                    for key in incident['location']['segment']:
                        if key not in ["code", "offset","type"]:
                            errors.append('new key '+key+' in segment')                              
                    if('offset' in incident['location']['segment'].keys()):
                        place_holder_variable = True
                        if(isinstance(incident['location']['segment']['offset'],int)):
                            place_holder_variable = True
                        else:
                            incident['location']['segment']['offset'] = ''
                            errors.append('offset not int')
                    else:
                        errors.append('offset not present')
                        incident['location']['segment']['offset'] = ''
                    if('code' in incident['location']['segment'].keys()):
                        place_holder_variable = True
                        if(isinstance(incident['location']['segment']['code'],str)):
                            place_holder_variable = True
                        else:
                            incident['location']['segment']['code'] = ''
                            errors.append('code not str')
                    else:
                        errors.append('code not present')
                        incident['location']['segment']['code'] = ''
                    if('type' in incident['location']['segment'].keys()):
                        place_holder_variable = True
                        if(isinstance(incident['location']['segment']['type'],str)):
                            place_holder_variable = True
                        else:
                            incident['location']['segment']['type'] = ''
                            errors.append('type not str')
                    else:
                        errors.append('type not present')
                        incident['location']['segment']['type'] = ''
                else:
                    incident['location']['segment'] = ''
                    errors.append('segment not dict')
            else:
                errors.append('segment not present')
                incident['location']['segment'] = ''
            if('geoIds' in incident['location'].keys()):
                place_holder_variable = True
                if(isinstance(incident['location']['geoIds'],list)):
                    for i in range(0,len(incident['location']['geoIds'])):
                        if(isinstance(incident['location']['geoIds'][i],int)):
                            place_holder_variable = True
                        else:
                            errors.append('element in geoIds not int')
                            incident['location']['geoIds'] = ''                             
                else:
                    errors.append('geoIds not list')
                    incident['location']['geoIds'] = ''                   
            else:
                errors.append('geoIds not present')
                incident['location']['geoIds'] = []
        else:
            incident['location'] = ''
            errors.append('location not dict')
    else:
        errors.append('location not present')
        incident['location'] = ''
    if('speedDelta' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['speedDelta'],int)):
            place_holder_variable = True
        else:
            incident['speedDelta'] = ''
            errors.append('speedDelta not int')
    else:
        errors.append('speedDelta not present')
        incident['speedDelta'] = ''
    if('id' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['id'],str)):
            place_holder_variable = True
        else:
            incident['id'] = ''
            errors.append('id not str')
    else:
        errors.append('id not present')
        incident['id'] = ''
    if('speedBefore' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['speedBefore'],int)):
            place_holder_variable = True
        else:
            incident['speedBefore'] = ''
            errors.append('speedBefore not int')
    else:
        errors.append('speedBefore not present')
        incident['speedBefore'] = ''
    if('description' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['description'],dict)):
            for key in incident['description']:
                if key not in ["direction", "mileMarker", "roadName"]:
                    errors.append('new key '+key+' in description')            
            if('roadName' in incident['description'].keys()):
                place_holder_variable = True
                if(isinstance(incident['description']['roadName'],str)):
                    place_holder_variable = True
                else:
                    incident['description']['roadName'] = ''
                    errors.append('roadName not str')
            else:
                errors.append('roadName not present')
                incident['description']['roadName'] = ''
            if('mileMarker' in incident['description'].keys()):
                place_holder_variable = True
                if(isinstance(incident['description']['mileMarker'],str)):
                    place_holder_variable = True
                else:
                    incident['description']['mileMarker'] = ''
                    errors.append('mileMarker not str')
            else:
                errors.append('mileMarker not present')
                incident['description']['mileMarker'] = ''
            if('direction' in incident['description'].keys()):
                place_holder_variable = True
                if(isinstance(incident['description']['direction'],int)):
                    place_holder_variable = True
                else:
                    incident['description']['direction'] = ''
                    errors.append('direction not int')
            else:
                errors.append('direction not present')
                incident['description']['direction'] = ''
        else:
            incident['description'] = ''
            errors.append('description not dict')
    else:
        errors.append('description not present')
        incident['description'] = ''
    if('severity' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['severity'],int)):
            place_holder_variable = True
        else:
            incident['severity'] = ''
            errors.append('severity not int')
    else:
        errors.append('severity not present')
        incident['severity'] = ''    
    return (True, incident,errors)


def clean(json_clean):  
    errors = []
    if('createdDate' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['createdDate'],str)):
            place_holder_variable = True
        else:
            json_clean['createdDate'] = ''
            errors.append('createdDate not str')
    else:
        errors.append('createdDate not present')
        json_clean['createdDate'] = ''
    if('responseId' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['responseId'],str)):
            place_holder_variable = True
        else:
            json_clean['responseId'] = ''
            errors.append('responseId not str')
    else:
        errors.append('responseId not present')
        json_clean['responseId'] = ''
    if('versionNumber' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['versionNumber'],str)):
            place_holder_variable = True
        else:
            json_clean['versionNumber'] = ''
            errors.append('versionNumber not str')
    else:
        errors.append('versionNumber not present')
        json_clean['versionNumber'] = ''
    if('copyright' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['copyright'],str)):
            place_holder_variable = True
        else:
            json_clean['copyright'] = ''
            errors.append('copyright not str')
    else:
        errors.append('copyright not present')
        json_clean['copyright'] = ''    
    if ('result' in json_clean.keys()):
        if(isinstance(json_clean['result'],dict)):
            if('dangerousSlowdowns' in json_clean['result'].keys()):
                place_holder_variable = True
                if(isinstance(json_clean['result']['dangerousSlowdowns'],list)): 
                    for i in range(0,len(json_clean['result']['dangerousSlowdowns'])):                       
                        clean_out = clean_incident(json_clean['result']['dangerousSlowdowns'][i])
                        if clean_out[0] == True:   
                            json_clean['result']['dangerousSlowdowns'][i] = clean_out[1]
                            if len(clean_out[2])>0:
                                errors.append("for dangerousSlowdowns "+str(json_clean['result']['dangerousSlowdowns'][i]['id'])+" "+str(clean_out[2]))  
                        else:
                            json_clean['result']['dangerousSlowdowns'][i] = False
                            errors.append("for dangerousSlowdowns number "+str(i)+" uuid not correct or present")
                elif(isinstance(json_clean['result']['dangerousSlowdowns'],dict)): 
                    json_clean['result']['dangerousSlowdowns'] = [json_clean['result']['dangerousSlowdowns']]
                    for i in range(0,len(json_clean['result']['dangerousSlowdowns'])):
                        clean_out = clean_incident(json_clean['result']['dangerousSlowdowns'][i])
                        if clean_out[0] == True:   
                            json_clean['result']['dangerousSlowdowns'][i] = clean_out[1]
                            if len(clean_out[2])>0:
                                errors.append("for dangerousSlowdowns "+str(json_clean['result']['dangerousSlowdowns'][i]['id'])+" "+str(clean_out[2]))  
                        else:
                            json_clean['result']['dangerousSlowdowns'][i] = False
                            errors.append("for dangerousSlowdowns number "+str(i)+" id not correct or present")                    
        
                else:
                    json_clean['result']['dangerousSlowdowns'] = []
                    errors.append('dangerousSlowdowns not dict or list')        
        
            else:
                errors.append('dangerousSlowdowns not present')
                json_clean['result']['dangerousSlowdowns'] = []   
        else:
            errors.append('result not dict')
            json_clean['result'] = {}
    else:
        errors.append('result not present')
        json_clean['result'] = {}
    #    for i in range(0,len(incident['segments']):
    #        for i in range(0,len(incident['line']):    
    return(True, 'placeholder',json_clean,errors)