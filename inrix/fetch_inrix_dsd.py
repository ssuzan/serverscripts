#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: waze database https request (url) 
#output: json of data given by https request or json containing error message
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports
import jsonlib_python3
import requests
import store_inrix_dsd


def downloadInrix(url_inrix,save_path,filename,l): 
    #test_list = ['waze_traffic_jam_lv_18_10_2017_11_12_08_523532.json','waze_traffic_jam_lv_18_10_2017_11_12_08_523532.json','waze_traffic_jam_lv_18_10_2017_11_12_08_523532.json',
    #test_list = ['waze_traffic_jam_lv_18_10_2017_11_26_28_709656.json','waze_traffic_jam_lv_18_10_2017_11_12_08_523532.json','waze_traffic_jam_lv_18_10_2017_11_26_28_709656.json']
    #text = open(workspace_environ+'/DB_backups/data_from_inrix/database_original/'+test_list[l],'r')
    #string = text.read()
    #email_json = universal_imports.json.loads(string)     
    #r = email_json
    try:
        r2 = requests.get(url_inrix)
        
        try:
            r2 = r2.json()
            try:
                store_inrix_dsd.store_original(r2,save_path,filename)
                r = r2
            except:
                r = universal_imports.json.loads('{"error" : "unable to save orignal"}')                     
        except:
            r = universal_imports.json.loads('{"error" : "unable to convert response to json"}') 
    except:
        r = universal_imports.json.loads('{"error" : "requests.exceptions.ConnectionError"}') 
     
    return r

