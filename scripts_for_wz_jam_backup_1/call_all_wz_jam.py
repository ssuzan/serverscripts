#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports
import sendemail
email_file = workspace_environ+'/serverscripts/globals/emails'
from automation_building_scripts import automate_build_clean
#import jsonlib_python3
from apscheduler.schedulers.blocking import BlockingScheduler
import requests
import smtplib
import clean_wz_jam
import fetch_wz_jam
import store_wz_jam
from os import listdir
from automation_building_scripts import automate_build_clean
from automation_building_scripts import automate_build_new_elements
from email.mime.text import MIMEText

wz_url = "https://na-georss.waze.com/rtserver/web/TGeoRSS?tk=ccp_partner&ccp_partner_name=WayCare&format=JSON&types=traffic&polygon=-115.344000,36.492000;-115.580000,36.414000;-115.490000,35.918000;-115.390000,35.605000;-114.994000,35.636000;-114.505000,35.908000;-113.917000,36.929000;-114.680000,36.724000;-115.344000,36.492000;-115.344000,36.492000"
save_path = workspace_environ+'/DB_backups/data_from_wz_jam'
dump_output_to = "FILE"
text = open(email_file,'r')
string = text.read()
email_json = universal_imports.json.loads(string)    

#number of iterations before element is removed from ongoing duplicate check list
default_age = 5
#minutes between iterations (.5 for half minute)
minutes_between_script_calls = 1
#ongoing duplicate check list
hash_log = {}
type_name = 'waze_traffic_jam_lv'


def initialize_new_schedule():
    back_log = os.listdir(save_path+"/database_original")
    text = open(save_path+"/database_original/"+max(back_log),'r')
    string = text.read()
    string = universal_imports.json.loads(string) 
    if(clean_wz.clean(string)[0] == True):
        print("initializing: backup file is clean")
        for i in range(0,len(string['jams'])):
            hash_uuid = hashlib.md5()
            encoded_uuid = str(string['jams'][i])
            encoded_uuid = encoded_uuid.encode("utf-8")
            hash_uuid.update(encoded_uuid)
            hash_log[string['jams'][i]['uuid']] = [hash_uuid.hexdigest(),default_age] 

def call_all_wz():
    curr_t = universal_imports.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
    print("call_all_wz() running at time: "+curr_t)
    #1st fetch 2nd clean 3rd store
    now = str(universal_imports.datetime.utcnow())
    now = now.split(" ")
    now[0] = now[0].split("-")
    now[1] = now[1].split(":")
    curr_t = universal_imports.datetime.utcnow().strftime("%d-%m-%Y %H:%M:%S")
    filename = type_name+"_"+now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1]+"_"+now[1][2].replace(".","_")     
    y = fetch_wz_jam.downloadWaze(wz_url,filename,save_path)  
    automate_build_clean.automate_clean(y['jams'][0]['segments'][0],"json_clean['jams'][i][segments][n]")
    #automate_build_clean.automate_clean(ex2,"json_clean['irregularities'][i]")
    #print(clean_wz.clean(y))   
    clean_results = clean_wz_jam.clean(y)
    if(clean_results[0] == True):
        original = y
        dead_hashes = []
        store_wz_jam.store(original, original, save_path,hash_log,dump_output_to,default_age,True) 
        for key in hash_log.keys():
            hash_log[key][1]-=1
            #print(key+" "+str(hash_log[key][1]))
            if hash_log[key][1] == 0:
                dead_hashes.append(key)
        for i in range(0,len(dead_hashes)):
            del hash_log[dead_hashes[i]]      
        print('call_all_wz.py completed successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
    else:
        #print("error in wz_jams "+clean_wz_jam.clean(y)[1])
        sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'error', message = "error in wz_jams "+str(clean_results[1]), login = email_json["email"], password = email_json["password"])    
        

def scheduled_call():
    print('call_all_wz.scheduled_call() started successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
    #do not intialize with -not_initialize
    try:
        if (str(sys.argv[1]) == '-not_initialize'):
            place_holder_variable = "holding a place"
    except:
        try:
            initialize_new_schedule()
        except:
            place_holder_variable = "holding a place"
    #initialize new scheduler
    sched = BlockingScheduler(timezone='UTC', coalesce=True)
    sched.add_job(call_all_wz, 'interval', id='job_id1', seconds=5, coalesce=True)
    sched.start()
    
text = open(save_path+"/database_original/waze_traffic_jam_lv_09_08_2017_11_08_21_361765.json",'r')
string = text.read()
string = universal_imports.json.loads(string) 
#print(string)
#automate_build_clean.automate_clean(string['jams'][0],"incident")
#call_all_wz()
scheduled_call()
#print(hash_log)