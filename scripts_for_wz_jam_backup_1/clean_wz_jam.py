#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: json containing data from waze database
#output: properly formatted json containing data from waze database
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
from automation_building_scripts import automate_build_clean
import universal_imports
import jsonlib_python3
import requests
  

def clean(json_clean):
    errors = []
    if('endTimeMillis' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['endTimeMillis'],int)):
            place_holder_variable = True
        else:
            json_clean['endTimeMillis'] = ''
            errors.append('endTimeMillis not int')
    else:
        errors.append('endTimeMillis not present')
        json_clean['endTimeMillis'] = ''
    if('startTimeMillis' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['startTimeMillis'],int)):
            place_holder_variable = True
        else:
            json_clean['startTimeMillis'] = ''
            errors.append('startTimeMillis not int')
    else:
        errors.append('startTimeMillis not present')
        json_clean['startTimeMillis'] = ''
    if('jams' in json_clean.keys()):
        if(isinstance(json_clean['jams'],list)):
            for i in range(0,len(json_clean['startTime'])):
                    if('street' in json_clean['jams'][i].keys()):
                        place_holder_variable = True
                        if(isinstance(json_clean['jams'][i]['street'],str)):
                            place_holder_variable = True
                        else:
                            json_clean['jams'][i]['street'] = ''
                            errors.append('street not str')
                    else:
                        errors.append('street not present')
                        json_clean['jams'][i]['street'] = ''
                    if('uuid' in json_clean['jams'][i].keys()):
                        place_holder_variable = True
                        if(isinstance(json_clean['jams'][i]['uuid'],int)):
                            place_holder_variable = True
                        else:
                            json_clean['jams'][i]['uuid'] = ''
                            errors.append('uuid not int')
                    else:
                        errors.append('uuid not present')
                        json_clean['jams'][i]['uuid'] = ''
                    if('turnType' in json_clean['jams'][i].keys()):
                        place_holder_variable = True
                        if(isinstance(json_clean['jams'][i]['turnType'],str)):
                            place_holder_variable = True
                        else:
                            json_clean['jams'][i]['turnType'] = ''
                            errors.append('turnType not str')
                    else:
                        errors.append('turnType not present')
                        json_clean['jams'][i]['turnType'] = ''
                    if('country' in json_clean['jams'][i].keys()):
                        place_holder_variable = True
                        if(isinstance(json_clean['jams'][i]['country'],str)):
                            place_holder_variable = True
                        else:
                            json_clean['jams'][i]['country'] = ''
                            errors.append('country not str')
                    else:
                        errors.append('country not present')
                        json_clean['jams'][i]['country'] = ''
                    if('id' in json_clean['jams'][i].keys()):
                        place_holder_variable = True
                        if(isinstance(json_clean['jams'][i]['id'],int)):
                            place_holder_variable = True
                        else:
                            json_clean['jams'][i]['id'] = ''
                            errors.append('id not int')
                    else:
                        errors.append('id not present')
                        json_clean['jams'][i]['id'] = ''
                    if('roadType' in json_clean['jams'][i].keys()):
                        place_holder_variable = True
                        if(isinstance(json_clean['jams'][i]['roadType'],int)):
                            place_holder_variable = True
                        else:
                            json_clean['jams'][i]['roadType'] = ''
                            errors.append('roadType not int')
                    else:
                        errors.append('roadType not present')
                        json_clean['jams'][i]['roadType'] = ''
                    if('speed' in json_clean['jams'][i].keys()):
                        place_holder_variable = True
                        if(isinstance(json_clean['jams'][i]['speed'],int)):
                            place_holder_variable = True
                        else:
                            json_clean['jams'][i]['speed'] = ''
                            errors.append('speed not int')
                    else:
                        errors.append('speed not present')
                        json_clean['jams'][i]['speed'] = ''
                    if('delay' in json_clean['jams'][i].keys()):
                        place_holder_variable = True
                        if(isinstance(json_clean['jams'][i]['delay'],int)):
                            place_holder_variable = True
                        else:
                            json_clean['jams'][i]['delay'] = ''
                            errors.append('delay not int')
                    else:
                        errors.append('delay not present')
                        json_clean['jams'][i]['delay'] = ''
                    if('line' in json_clean['jams'][i].keys()):
                        place_holder_variable = True
                        if(isinstance(json_clean['jams'][i]['line'],list)):
                            for n in range(0,len(json_clean['jams'][i]['line'])):
                                if('x' in json_clean['jams'][i]['line'][n].keys()):
                                    place_holder_variable = True
                                    if(isinstance(json_clean['jams'][i]['line'][n]['x'],float)):
                                        place_holder_variable = True
                                    else:
                                        json_clean['jams'][i]['line'][n]['x'] = ''
                                        errors.append('x not float')
                                else:
                                    errors.append('x not present')
                                    json_clean['jams'][i]['line'][n]['x'] = ''
                                if('y' in json_clean['jams'][i]['line'][n].keys()):
                                    place_holder_variable = True
                                    if(isinstance(json_clean['jams'][i]['line'][n]['y'],float)):
                                        place_holder_variable = True
                                    else:
                                        json_clean['jams'][i]['line'][n]['y'] = ''
                                        errors.append('y not float')
                                else:
                                    errors.append('y not present')
                                    json_clean['jams'][i]['line'][n]['y'] = ''                                
                        else:
                            json_clean['jams'][i]['delay'] = ''
                            errors.append('delay not int')
                        
                    else:
                        errors.append('line not present')
                        json_clean['jams'][i]['line'] = ''
                    if('type' in json_clean['jams'][i].keys()):
                        place_holder_variable = True
                        if(isinstance(json_clean['jams'][i]['type'],str)):
                            place_holder_variable = True
                        else:
                            json_clean['jams'][i]['type'] = ''
                            errors.append('type not str')
                    else:
                        errors.append('type not present')
                        json_clean['jams'][i]['type'] = ''
                    if('blockingAlertUuid' in json_clean['jams'][i].keys()):
                        place_holder_variable = True
                        if(isinstance(json_clean['jams'][i]['blockingAlertUuid'],str)):
                            place_holder_variable = True
                        else:
                            json_clean['jams'][i]['blockingAlertUuid'] = ''
                            errors.append('blockingAlertUuid not str')
                    else:
                        errors.append('blockingAlertUuid not present')
                        json_clean['jams'][i]['blockingAlertUuid'] = ''
                    if('length' in json_clean['jams'][i].keys()):
                        place_holder_variable = True
                        if(isinstance(json_clean['jams'][i]['length'],int)):
                            place_holder_variable = True
                        else:
                            json_clean['jams'][i]['length'] = ''
                            errors.append('length not int')
                    else:
                        errors.append('length not present')
                        json_clean['jams'][i]['length'] = ''
                    if('city' in json_clean['jams'][i].keys()):
                        place_holder_variable = True
                        if(isinstance(json_clean['jams'][i]['city'],str)):
                            place_holder_variable = True
                        else:
                            json_clean['jams'][i]['city'] = ''
                            errors.append('city not str')
                    else:
                        errors.append('city not present')
                        json_clean['jams'][i]['city'] = ''
                    if('level' in json_clean['jams'][i].keys()):
                        place_holder_variable = True
                        if(isinstance(json_clean['jams'][i]['level'],int)):
                            place_holder_variable = True
                        else:
                            json_clean['jams'][i]['level'] = ''
                            errors.append('level not int')
                    else:
                        errors.append('level not present')
                        json_clean['jams'][i]['level'] = ''
                    if('endNode' in json_clean['jams'][i].keys()):
                        place_holder_variable = True
                        if(isinstance(json_clean['jams'][i]['endNode'],str)):
                            place_holder_variable = True
                        else:
                            json_clean['jams'][i]['endNode'] = ''
                            errors.append('endNode not str')
                    else:
                        errors.append('endNode not present')
                        json_clean['jams'][i]['endNode'] = ''
                    if('segments' in json_clean['jams'][i].keys()):
                        if(isinstance(json_clean['jams'][i]['segments'],list)):
                            place_holder_variable = True
                        else:
                            json_clean['jams'][i]['segments'] = ''
                            errors.append('segments not list')
                    else:
                        errors.append('segments not present')
                        json_clean['jams'][i]['segments'] = ''
                    if('pubMillis' in json_clean['jams'][i].keys()):
                        place_holder_variable = True
                        if(isinstance(json_clean['jams'][i]['pubMillis'],int)):
                            place_holder_variable = True
                        else:
                            json_clean['jams'][i]['pubMillis'] = ''
                            errors.append('pubMillis not int')
                    else:
                        errors.append('pubMillis not present')
                        json_clean['jams'][i]['pubMillis'] = ''
            else:
                json_clean['startTime'] = ''
                errors.append('startTime not str')
        else:
            errors.append('jams not list')
            json_clean['jams'] = ''            
    else:
        errors.append('jams not present')
        json_clean['jams'] = ''
    if('startTime' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['startTime'],str)):
            place_holder_variable = True
        else:
            json_clean['startTime'] = ''
            errors.append('startTime not str')
    else:
        errors.append('startTime not present')
        json_clean['startTime'] = ''
    if('endTime' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['endTime'],str)):
            place_holder_variable = True
        else:
            json_clean['endTime'] = ''
            errors.append('endTime not str')
    else:
        errors.append('endTime not present')
        json_clean['endTime'] = ''
    return(True,json_clean,errors) 