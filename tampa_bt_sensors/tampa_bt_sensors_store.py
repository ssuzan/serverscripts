#main file
#required installations:
import sys,os
import os.path
sys.path.append(os.environ['WORKSPACE']+'/serverscripts_production/globals')
import universal_imports
import sendemail
import push_to_fifo
import push_to_fifo_dev

from collections import OrderedDict
from email.mime.text import MIMEText

import json
dire = os.path.dirname(__file__)

save_path = os.environ['WORKSPACE']+'/DB_backups/data_from_tampa_bt_sensors'
email_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/emails'
# key_file = os.environ['WORKSPACE']+'link to encryption key file'

# text = open(email_file,'r')
# f=open(key_file,"r")
# string = text.read()
# email_json = universal_imports.json.loads(string) 
# type_name = 'events_tampa_bt_sensors_live_lv'
# key= f.read().replace("\n", "")


#Store original raw_json file
def tampa_bt_sensors_store_original(raw_data,filename,is_prod):  
    if(is_prod):
        
        # PROD environment
        with open(save_path+"/database_original/"+filename+'_original_feed.txt', 'wb') as fd:
            for chunk in raw_data.iter_content(chunk_size=50000, decode_unicode=False):
                fd.write(chunk)
        # DEV environment
        #with open(os.path.join(dire + filename + "_original_feed.txt"), 'wb') as fd:
        #    for chunk in raw_data.iter_content(chunk_size=50000, decode_unicode=False):
        #        fd.write(chunk)



#Store cleaned tampa_bt_sensors_json file

def tampa_bt_sensors_store_clean(curr_t,tampa_bt_sensors_json,dump_to_output,filename,is_prod,is_dev):  
    err_report = []
    curr_filename = filename+"_clean_"  
    
    #saves file (array of JSON objects each containing one clean event)    
    #new_store_json = {'type':type_name,'date_time':str(curr_t).replace("-","_")+' UTC','suspicious':False,'data':tampa_bt_sensors_json,'error':False}
               
    if ((dump_to_output == "FILE") or (dump_to_output == "ALL")): 
        try:
            #with open(save_path+"/database_original/"+curr_filename+'.json', 'w') as outfile:
            #    json.dump(tampa_bt_sensors_json, outfile)  
            with open(os.path.join(dire + curr_filename + '.json'), 'w') as outfile:
                json.dump(tampa_bt_sensors_json, outfile)
        except:
            err_report.append('failed to store clean file'+curr_filename+'.json')
    
    #sends file to FIFO
    if ((dump_to_output == "FIFO") or (dump_to_output == "ALL")): 
        try:
            if (is_prod):
                push_to_fifo.push_to_fifo(new_store_json,'10.0.3.44',queue_name='spillman_queue')         
        except:
            text = open(email_file,'r')
            email_string = text.read()
            email_json = universal_imports.json.loads(email_string)                  
            sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'error in tampa_bt_sensors_store', message = 'fifo error in tampa_bt_sensors_store', login = email_json['email'], password = email_json['password'])         
            err_report.append('fifo error in tampa_bt_sensors_store')
        
        #push to development FIFO
        try:
            if (is_dev):
                push_to_fifo_dev.push_to_fifo_dev(new_store_json,'35.157.104.189',queue_name='spillman_queue')         
        except:
            text = open(email_file,'r')
            email_string = text.read()
            email_json = universal_imports.json.loads(email_string)                  
            sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'error in tampa_bt_sensors_store', message = 'fifo error in tampa_bt_sensors_store', login = email_json['email'], password = email_json['password'])         
            err_report.append('fifo error in tampa_bt_sensors_store')
            
    return len(err_report) == 0




