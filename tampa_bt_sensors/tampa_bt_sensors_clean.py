#main file
#required installations:

import universal_imports
import sendemail
from email.mime.text import MIMEText
import sys, os
import lxml.etree as etree
from pprint import pprint

sys.path.append(os.environ['WORKSPACE']+'/serverscripts/globals')
email_file = os.environ['WORKSPACE']+'/serverscripts/globals/emails'

# The incoming XML file to check categories & keys against existing set (look for new keys)
# DEV enviromnent
#dire = os.path.dirname(__file__)
#input_file = os.path.join(dire + 'tampa_output_new.txt')

# Function to parse inbound XML Tree and convert it to JSON
def etree_to_dict(t):
    try:
        d = {t.tag : [i for i in map(etree_to_dict, t.iterchildren())]}
        [d[t.tag].append({'@' + k: v}) for k, v in t.attrib.iteritems()]
        d['text'] = t.text
        return d
    except:
        return False


# filename - string
# Return type -> [bool, [{},{},...{}]
def tampa_bt_sensors_clean(input_file, filename):
    # for input from fetch directly
    new_categories = []
    item_order = 0
    clean_jsons = []
    err_report = []
    warn_report = []
    doc = etree_to_dict(etree.fromstring(input_file))
    if doc: # If etree_to_dict succeed
        # for input from file
        #doc = etree_to_dict(etree.parse(input_file).getroot())
        pairs_list = doc['BlueTOAD_DATA'][1]['Pairs']
        #pprint(pairs_list[0])
        #### CONSTANTS:
        #### Stable categories obtained from pre-fetch
        categories_stable = [{'Pair': ['PairID', 'Name', 'SymbolicID', 'FromDevice', 'FromDevice', 'DeviceID', 'DeviceName', 'ToDevice', 'DeviceID', 'DeviceName', 'Distance', 'Direction', 'SpeedLimit', 'RoadClass', 'Method', 'Status', 'XF1', 'XF2', 'Speed', 'HistSpeed', 'TravelTime', '@stale', 'LastMatch', 'ActiveAlarm']}]
        #### Keys of stable categories
        categories_stable_keys = [k for i in categories_stable for k in i.keys()]
        #### Some intermediary keys that will be skipped in flat JSON object returned by the function (to make output more concise)
        skip_lst = ['FromDevice', 'ToDevice', '@count', '@units', '@groupID', '@groupName', '@groupPrefix']
        
        def parse_dict(d, cat, order,parent):
                if isinstance(d, list):
                    for i in d:
                        parse_dict(i, cat, order,parent)
                elif isinstance(d, dict):
                    for k,v in d.items():
                        if k != 'text' and not k.startswith('@'):                
                            try:                        
                                # Check for the base case and extract error reports
                                isinstance(v, str)                       
                                test_keys[0][cat].append(k)
                                
                                # If key is not in stable categories (new key), put it into new categories list, create warning report 
                                # and remove the key from output
                                if k not in categories_stable[0][cat]:                            
                                    new_categories.append({cat: [k]})
                                    warn_report.append([filename, order, k, 'new_category', 'New category: ' + str(cat) + ' > '+ str(k) + ' in ' + filename + ', item num=' + str(order)])
                                    return
                                    
                                # If PairID is missing, stop parsing and create error
                                if k == 'PairID': 
                                    # Check for None in PairID field
                                    if d['text'] is None or type(d['text']) == type(None) or d['text'] == 'None' or d['text'] == '':
                                        err_report.append([filename, order, '', 'pair_id_missing', 'Pair ID is missing in ' + filename + ', item num=' + str(order)])
                                        return  
                                    else: clean_json.append({k: d['text']})
                                
                                # If this is not the base case, continue                        
                                if len(v) > 0:
                                    parse_dict(v,cat,order,k)
                                
                                # If we're done with parsing text - return
                                if k not in skip_lst:
                                    clean_json[0][cat].append({str(parent)+'-'+str(k):d['text']})
                                return
                            
                            except:
                                # If this is not the base case, continue
                                parse_dict(v,cat,order,k)
                        
                        # general case with attributes - create new field based on parent name and attribute name:
                        elif k.startswith('@'):
                            clean_json[0][cat].append({str(parent)+'-'+str(k[1:]): v})
                                            
        
        ### MAIN LOOP - TRAVERSING PAIRS ###              
        for pair in pairs_list:
            test_keys = []
            clean_json = [{k: []} for k in categories_stable_keys]
            for k,v in pair.items():
                if k != 'text' and k not in skip_lst:
                    test_keys.append({k: []})
                    
                    # Check for the new possible category on top level, if it exists - report it
                    if sum([1 for cat in categories_stable if k in cat.keys()]) == 0:
                        new_categories.append({k: []})
                        warn_report.append([filename, item_order, k, 'new_category', 'New category '+ str(k) + ' in ' + filename + ', item num=' + str(item_order)])                      
                    
                    # Parse list of keys of Pair
                    if len(v) > 0:
                        parse_dict(v,k,item_order,k)               
                    else: 
                        warn_report.append([filename, item_order, k, 'new_category', 'New category '+ str(k) + ' in ' + filename + ', item num=' + str(item_order)])                       
            
            # check for missing keys and report
            # append missing key to result object
            if len(test_keys) > 0:
                for k in test_keys[0]['Pair']:
                    if k not in categories_stable[0]['Pair']:
                        clean_json[0]['Pair'].append({k: ''})
                        warn_report.append([filename, item_order, k, 'missing_key', 'Missing key '+ str(k) + ' in ' + filename + ', item num=' + str(item_order)])
            
            # Clean special symbols and keys
            clean_res = {}
            for j in clean_json[0]['Pair']:
                k = list(j.keys())[0]
                v = list(j.values())[0]
                
                if isinstance(v, str):
                    v = v.replace("&amp;", "and")
                    
                if k.startswith('Pair-'):
                    clean_res.update({k[5:]: v})
                else: clean_res.update({k: v})
            
            if len(clean_res) > 0: clean_jsons.append(clean_res)
            item_order += 1
    else: # Here if etree_to_dict failed
        err_report.append([filename, '', '', '', filename])
        
    
    #print(item_order)        
    #print ('STABLE=', categories_stable)
    #print ('NEW=', new_categories)
    #print ('ERR_', err_report)
    #print ('WARN_', warn_report)
    #pprint (clean_jsons[0])


    # Send error and warning report contents - each item individually (if needed - it's possible to send one combined report)
    for e in err_report:
        text = open(email_file,'r')
        email_string = text.read()
        email_json = universal_imports.json.loads(email_string)
        prefix = "tampa_bt_sensors clean errors: "
        message_text = prefix + e[4] #... This is a report in form of string, but error_report has also other fields that can be used to automate the process
        sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'tampa_bt_sensors error report', message = message_text, login = email_json["email"], password = email_json["password"]) 
    for w in warn_report:
        text = open(email_file,'r')
        email_string = text.read()
        email_json = universal_imports.json.loads(email_string)
        prefix = "m_tampa clean warnings: "
        message_text = prefix + w[4] #... This is a report in form of string, but error_report has also other fields that can be used to automate the process
        #print (message_text)
        sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'tampa_bt_sensors warning report', message = message_text, login = email_json["email"], password = email_json["password"]) 

    return [len(err_report) == 0, clean_jsons]

#test = tampa_bt_sensors_clean(input_file, 'filename')
