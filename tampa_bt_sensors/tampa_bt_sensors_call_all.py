#Shai Suzan
#shai@waycaretech.com
#main file
#required installations:
import sys,os
import os.path
sys.path.append(os.environ['WORKSPACE']+'/serverscripts/globals')
import universal_imports
#from apscheduler.schedulers.blocking import BlockingScheduler
import sendemail
from email.mime.text import MIMEText
#import xmltodict
import tampa_bt_sensors_fetch
import tampa_bt_sensors_store
import tampa_bt_sensors_clean

#tampa_bt_sensors_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/ticket_master_access.txt'
#tampa_bt_sensors_api_text = open(tampa_bt_sensors_file,'r')
#tampa_bt_sensors_api_string = tampa_bt_sensors_api_text.read().rstrip()
#tampa_bt_sensors_url = ''
#save_path = os.environ['WORKSPACE']+'/DB_backups/data_from_ndot/database/'
#original_backup_path = os.environ['WORKSPACE']+'/DB_backups/data_from_ndot/database_original/'
email_file = os.environ['WORKSPACE']+'/serverscripts/globals/emails'
type_name = 'tampa_bt_sensors_pairs_live'
#text = open(email_file,'r')
#email_string = text.read()
#email_json = universal_imports.json.loads(email_string)  
#is_development_file =  os.environ['WORKSPACE']+'/.is_development'
#is_productoin_file =   os.environ['WORKSPACE']+'/.is_production'
is_prod_flag= True 
is_dev_flag=  False
 
store_original = True 
dump_to_output = "FILE"
#number of iterations before element is removed from ongoing duplicate check list - set to 10 minutes
#default_age = 11 
#minutes between iterations (.5 for half minute)
#minutes_between_script_calls = 1


#Set to True if encrppyption is required before sending to FIFO
#encrypt_output= True

#n is for iterating through test files
#n = [0]

def tampa_bt_sensors_call_all():
    try:
        now = str(universal_imports.datetime.utcnow())
        now = now.split(" ")
        now[0] = now[0].split("-")
        now[1] = now[1].split(":")
        filename = type_name+"_"+now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1]+"_"+now[1][2].replace(".","_")           
        curr_t = universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")  
        print('tampa_bt_sensors_call_all.py started! time is: '+curr_t+' UTC')
        
        #calls fetch
        doc = tampa_bt_sensors_fetch.tampa_bt_sensors_fetch(filename, store_original, is_prod_flag)

        # print(blaglk)

        if(doc[0] == True):
    
            doc_xml = doc[1]

            # test if empty: doc_xml = 'push_to_fifo.py'
            clean_results = tampa_bt_sensors_clean.tampa_bt_sensors_clean(doc_xml, filename)
            if clean_results[0] == True:
                saving_result = tampa_bt_sensors_store.tampa_bt_sensors_store_clean(curr_t,clean_results[1],dump_to_output,filename,is_prod_flag,is_dev_flag)
                if saving_result:                
                    print('tampa_bt_sensors_call_all.py completed successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
        else:
                text = open(email_file,'r')
                email_string = text.read()
                email_json = universal_imports.json.loads(email_string)  
                sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'error in tampa_bt_sensors_fetch', message = doc[1], login = email_json['email'], password = email_json['password'])         
    except:
            text = open(email_file,'r')
            email_string = text.read()
            email_json = universal_imports.json.loads(email_string)  
            sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'error in tampa_bt_sensors', message = 'error caught in tampa_bt_sensors_call_all.py', login = email_json['email'], password = email_json['password'])         



#def scheduled_call():
#    print('tampa_bt_sensors_call_all.scheduled_call() started successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
#    try:
#        initialize_new_schedule()
#    except:
#        place_holder_variable = "holding a place"
#    #initialize new scheduler
#    sched = BlockingScheduler(timezone='UTC', coalesce=True)
#    sched.add_job(tampa_bt_sensors_call_all, 'interval', id='job_id1', seconds=30,coalesce=True)
#    sched.start()
    
if __name__ == "__main__":
        tampa_bt_sensors_call_all()

