import sys, os
import requests
import tampa_bt_sensors_store


user = 'Waycare'
password = 'Waycare2017!'
#user_email = 'support@waycaretech.com'

#URL schema
SCHEMA = "https://"
#Domain being accessed to get XML feed
DOMAIN1 = "bluetoad.trafficcast.com"
DOMAIN2 = "bluetoadfeeds.trafficcast.com"
#Path of the login script
LOGIN_PATH = "/user/login"
#Path of the XML feed
XML_PATH = "/xml/bt_pairs/live"

auth_url = SCHEMA + DOMAIN2 + LOGIN_PATH
xml_url = SCHEMA + DOMAIN2 + XML_PATH
headers = {
    'Content-Type': "application/x-www-form-urlencoded",
    'credentials': 'include',
}

dire = os.path.dirname(__file__)

#connecting to BlueToad and authentication 
def tampa_bt_sensors_fetch(filename,store_original, is_prod):
    
    # Authorization request
    payload = "op=Log+in&form_id=user_login&name="+str(user)+"&pass="+str(password)
    s = requests.Session()
    s.auth = (user, password)
    
    r = s.post(auth_url, data=payload, headers=headers)    
     
    if (r.status_code == 200):        
        
        # XML feed request (you need to be logged to site to get the cookies from Chrome browser with browser_cookie3 lib)
        r2 = s.get(xml_url) #, stream=True)                
        if (r2.status_code == 200):
            
            # store original
            if store_original:
                tampa_bt_sensors_store.tampa_bt_sensors_store_original(r2,filename,is_prod)
            return [True, r2.content]
        
        else:
            return [False,'tampa_bt_sensors failed to fetch xml'+ filename]        
    else:
        return [False,'tampa_bt_sensors failed to authorize '+ filename]
        
test = tampa_bt_sensors_fetch('test_filename',True, True)