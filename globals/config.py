
env_config = {
    "base_name": "traffic_lights",
    "prod":{
        "scripts_path":"serverscripts_production",
        "user":"prod"
    },
    "dev":{
        "scripts_path":"serverscripts",
        "user":"dev"
    }
}
