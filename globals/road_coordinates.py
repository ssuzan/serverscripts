import googlemaps
from datetime import datetime
from googlemaps import roads
from math import sin, cos, sqrt, atan2, radians

#checks if location is inside city limits
def in_City(city_limits, coordinates):
    x = coordinates[0]
    y = coordinates[1]
    n = len(city_limits)
    inside = False 
    p1x,p1y = city_limits[0]
    for i in range(n+1):
        p2x,p2y = city_limits[i % n]
        if y > min(p1y,p2y):
            if y <= max(p1y,p2y):
                if x <= max(p1x,p2x):
                    if p1y != p2y:
                        xinters = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                    if p1x == p2x or x <= xinters:
                        inside = True
        p1x,p1y = p2x,p2y
    return inside

#returns distance in meters
def close_enough(street,coordinates):
    R = 6373.0
    lat1 = radians(street[0])
    lon1 = radians(street[1])
    lat2 = radians(coordinates[0])
    lon2 = radians(coordinates[1])

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance_m = R * c*.001
    return distance_m


#returns name of road if it is legal or reason why it is not
#I began by using nearest_roads but switched to snap_to_roads, a1 is only used to get name of closest road and if a closest road can be found, s1 is used for all else
def give_location(client,location, distance_m):
    a1 = roads.nearest_roads(client, location)
    if len(a1)>0:
        if in_City(city_limits, location)==True:
            a1 = a1[0]['location']
            s1 = roads.snap_to_roads(client, location, interpolate=False)[0]['location']
            street_location = (s1['latitude'],s1['longitude'])
            if(close_enough(street_location, location) <= distance_m): 
                reverse_geocode_result_a1 = client.reverse_geocode((a1['latitude'],a1['longitude']))[0]['address_components'][0]['short_name']
                return [0,reverse_geocode_result_a1]
            else:
                return [1,"road cannot be found, insufficient proximity, distance: %i"%close_enough(street_location,location)]
        else:
            return [2,"Outside city limits"]
    else:
        return [3,"road cannot be found, coordinates entered are likely not near a marked road"]    



if __name__=="__main__":
    city_limits = [(36.321845, -114.967701),(36.320529, -115.344954), (35.943293, -115.346587), (35.947732, -114.910310)]
    #locations for testing
    test_locations = [(36.146100, -115.169285), (36.061706, -115.147339), (36.032330, -115.127024), (39.535761, -119.807623)]
    #max distance from closest road in km
    distance = 500
    #opens list of locations
    file_open = open('gps_example.txt','r')
    text = file_open.read()
    #processes list of locations
    text = text.replace(" ","")
    text = text.split('\n')
    text.remove('')
    for i in range(0,len(text)):
        text[i] = text[i].split(',')
        text[i][1] = text[i][1].split(':')
        text[i][2] = text[i][2].split(':')
    #google api key
    gmaps = googlemaps.Client(key='AIzaSyDL_BdqpHWQsEizx6vOrqjsneRbWfl58XE')
    #makes list of coordinates
    locations = []
    for i in range(0,len(text)):
        locations.append((float(text[i][1][1]),float(text[i][2][1])))
    for i in range (0,len(locations)):
        print(give_location(gmaps, locations[i], distance))  