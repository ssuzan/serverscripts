import sys
#path depends on computer
sys.path.append('/home/esgellman/waycare_bitbucket/serverscripts/globals')
import universal_imports
import requests
email_file = '/home/esgellman/waycare_bitbucket/serverscripts/globals/emails'
save_path = ""
download_url = 'ftp://bugatti.nvfast.org/RealtimeXML/'
import smtplib
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
#send email if error
# enable sending from your own email: https://stackoverflow.com/a/27515833/3914099

def sendemail(from_addr, to_addr_list, cc_addr_list, subject, message, login, password, smtpserver='smtp.gmail.com:587'):
    # try:
        message = message
        server = smtplib.SMTP(smtpserver)
        server.starttls()
        server.login(login,password)
        #building the message
        msg = ("From: %s\r\nTo: %s\r\nSubject: %s\r\n\r\n"
           % (from_addr, ", ".join(to_addr_list),subject))
        msg = msg + message
        
        #sending the message
        server.sendmail(from_addr, to_addr_list, msg)
        server.quit()
    # except:
        # print("could not send email")

def sendemail_picture(email_file, subject, message, image_bytes, smtpserver='smtp.gmail.com:587'):
    """Send mail and attach a picture. In contrast to previous func it gets the email_file 
    as input and then parses the settings.

    IN:
    email_file - as kept on globals directory
    subject - email subject
    message - email message
    image_bytes - image attached as byte code
    """

    # unpack settings
    from_addr = email_file['email']
    to_addr_list = email_file['send_emails']
    login = email_file['email']
    password = email_file['password']
    
    # generate message
    msg = MIMEMultipart()
    
    msg['Subject'] = subject
    msg['From'] = from_addr
    msg['To'] = to_addr_list
    mime_text = MIMEText(message)
    msg.attach(mime_text)

    # attach picture
    if image_bytes is not None:
        mime_img = MIMEImage(image_bytes, _subtype="jpeg")
        msg.attach(mime_img)

    # connect and send
    s = smtplib.SMTP(smtpserver)
    s.starttls()
    s.login(login, password)
    s.send_message(msg)
    s.quit()    
