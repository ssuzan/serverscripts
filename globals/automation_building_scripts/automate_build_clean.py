import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports
import sendemail
email_file = workspace_environ+'/serverscripts/globals/emails'
import requests
import smtplib
from email.mime.text import MIMEText
#add expected elements list


#example automate_clean(json_file,'name_of_dictionary_used_in_method')
#how to:
#1 call automate_clean
#2 copy into new method
#3 add ending True return
#4 bugtest
#5 check for lists and unknowns, if there is a list with dictionaries put the first element into automate_clean
#6 add specialized checks for unique elements
def automate_clean(dict_to_clean,name):
    #print(name)
    lines = []
    keys = dict_to_clean.keys()
    #print(keys)
    #print(name.count("["))
    indents = "\t"*name.count("[")
    for key in dict_to_clean.keys():
        #print(dict_to_clean[key])
        print(indents+"if('%s' in %s.keys()):"%(key,name))
        print(indents+"\tplace_holder_variable = True")
        if(isinstance(dict_to_clean[key],str)):
            print(indents+"\tif(isinstance(%s['%s'],str)):"%(name,key))
            print(indents+"\t\tplace_holder_variable = True")
            print(indents+"\telse:")
            print(indents+"\t\t%s['%s'] = ''"%(name,key))
            print(indents+"\t\terrors.append('%s not str')"%(key))
        elif(isinstance(dict_to_clean[key],int)):
            print(indents+"\tif(isinstance(%s['%s'],int)):"%(name,key))
            print(indents+"\t\tplace_holder_variable = True")
            print(indents+"\telse:")
            print(indents+"\t\t%s['%s'] = ''"%(name,key))
            print(indents+"\t\terrors.append('%s not int')"%(key))   
        elif(isinstance(dict_to_clean[key],float)):
            print(indents+"\tif(isinstance(%s['%s'],float)):"%(name,key))
            print(indents+"\t\tplace_holder_variable = True")
            print(indents+"\t\telse:")
            print(indents+"\t\t%s['%s'] = ''"%(name,key))
            print(indents+"\t\terrors.append('%s not float')"%(key))
        elif(isinstance(dict_to_clean[key],list)):
            print("list")
        elif(isinstance(dict_to_clean[key],dict)):
            print(indents+"\tif(isinstance(%s['%s'],dict)):"%(name,key))
            print(indents+"\t\tplace_holder_variable = True")
            automate_clean(dict_to_clean[key],name+"['%s']"%(key))
            print(indents+"\telse:")
            print(indents+"\t\t%s['%s'] = ''"%(name,key))
            print(indents+"\t\terrors.append('%s not dict')"%(key))
        else:
            print(indents+"%s UNKNOWN TYPE:MANUAL INPUT REQUIRED"%(key))
            
        print(indents+"else:")
        print(indents+"\terrors.append('%s not present')"%(key))
        print(indents+"\t%s['%s'] = ''"%(name,key))
      
