
def automate_spellman_output(hiarchy,name):
    indents = "\t"*name.count("[")
    for key in hiarchy.keys():
        print(indents+"if('%s' in %s.keys()):"%(key,name))
        print(indents+"\tplace_holder_variable = True")        
        if(isinstance(hiarchy[key],dict)):
            if 'LIST' in hiarchy[key].keys():
                #print(key) 
                print(indents+"\tif(isinstance(%s['%s'],list)):"%(name,key))
                print(indents+"\t\tfor i in range(0,len(%s)):"%key)
                automate_spellman_output(hiarchy[key],'%s[%s][i]'%(name,key))
                print(indents+"\telse:")
                print(indents+"\t\tprint(%s['%s'])"%(name,key))   
                print(indents+"\t\treturn(False,'%s not present')"%key)                  
            else:
                print(indents+"\tif(isinstance(%s['%s'],dict)):"%(name,key))
                automate_spellman_output(hiarchy[key],'%s[%s]'%(name,key))
                print(indents+"\telse:")
                print(indents+"\t\tprint(%s['%s'])"%(name,key))   
                print(indents+"\t\treturn(False,'%s not present')"%key)                
        else:
            print(indents+"\tif(isinstance(%s['%s'],%s)):"%(name,key,hiarchy[key]))
            print(indents+"\t\tplace_holder_variable = True")
            print(indents+"\telse:")
            print(indents+"\t\tprint(%s['%s'])"%(name,key))   
            print(indents+"\t\treturn(False,'%s not present')"%key)
            place_holder_variable = 1
        print(indents+"else:")
        print(indents+"\t%s['%s'] = ''"%(name,key))        
        
    
def automate_spellman(doc):
    doc_hiarchy_and_types = {}
    list_elements = {}
    #print(doc.keys())
    if(isinstance(doc,dict)): 
        for key in doc.keys():
            if(isinstance(doc[key],dict)):
                doc_hiarchy_and_types[key] = automate_spellman(doc[key])
            elif(isinstance(doc[key],list)):
                doc_hiarchy_and_types[key] = automate_spellman(doc[key])
            else:
                doc_hiarchy_and_types[key] = []
                if(isinstance(doc[key],str)):
                    if 'str' not in doc_hiarchy_and_types[key]:
                        doc_hiarchy_and_types[key].append('str')
                elif(isinstance(doc[key],int)):
                    if 'int' not in doc_hiarchy_and_types[key]:
                        doc_hiarchy_and_types[key].append('int')
                elif(isinstance(doc[key],float)):
                    if 'float' not in doc_hiarchy_and_types[key]:
                        doc_hiarchy_and_types[key].append('float')                    
                else:
                    doc_hiarchy_and_types[key].append('UNKNOWN TYPE')                    
    elif(isinstance(doc,list)):
        doc_hiarchy_and_types = {'LIST':'LIST'}
        for i in range(0,len(doc)):
            for subkey in doc[i]:
                if (isinstance(doc[i][subkey],dict)):
                    doc_hiarchy_and_types[subkey] = automate_spellman(doc[i][subkey])
                elif (isinstance(doc[i][subkey],list)):
                    doc_hiarchy_and_types[subkey] = automate_spellman(doc[i][subkey])
                else:
                    if(subkey not in doc_hiarchy_and_types.keys()):
                        doc_hiarchy_and_types[subkey] = []
                    if(isinstance(doc[i][subkey],str)):
                        if 'str' not in doc_hiarchy_and_types[subkey]:
                            doc_hiarchy_and_types[subkey].append('str')
                    elif(isinstance(doc[i][subkey],int)):
                        if 'int' not in doc_hiarchy_and_types[subkey]:
                            doc_hiarchy_and_types[subkey].append('int')
                    elif(isinstance(doc[i][subkey],float)):
                        if 'float' not in doc_hiarchy_and_types[subkey]:
                            doc_hiarchy_and_types[subkey].append('float')                    
                    else:
                        doc_hiarchy_and_types[subkey].append('UNKNOWN TYPE')                               
    return(doc_hiarchy_and_types)
