import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports
import sendemail
email_file = workspace_environ+'/serverscripts/globals/emails'
import requests
import smtplib
from email.mime.text import MIMEText
#add expected elements list


#example automate_clean(json_file,'name_of_dictionary_used_in_method')
#how to:
#1 call automate_clean
#2 copy into new method
#3 add ending True return
#4 bugtest
#5 check for lists and unknowns, if there is a list with dictionaries put the first element into automate_clean
#6 add specialized checks for unique elements

def automate_new_elements_helper(dict_to_clean,name,i):
    key_log = {}
    keys = dict_to_clean.keys()
    for key in dict_to_clean.keys():
        if(isinstance(dict_to_clean[key],dict)):          
            print("\t"*i+"for key_%s in %s['%s']:"%(str(i),name,key))
            print("\t"*i+"\tif key_%s not in %s:"%(str(i),str(dict_to_clean[key].keys())))
            print("\t"*i+"\t\terrors.append('new key '+key_%s+' in %s[%s]')"%(str(i),name,key))
            automate_new_elements_helper(dict_to_clean[key],name+"['%s']"%key,i+1)            
        elif(isinstance(dict_to_clean[key],list)):
            print("\t"*i+"for i in range(0,len(%s['%s']):"%(name,key))
            automate_new_elements_helper(dict_to_clean[key][0],name+"['%s']"%key+"[i]",i+1)
    
def automate_new_elements(dict_to_clean,name):
    #print(name)
    print("for key in %s:"%name)
    print("\tif key not in %s:"%(str(dict_to_clean.keys())))    
    print("\t\terrors.append('new key '+key+' in %s')"%(name))
    x = automate_new_elements_helper(dict_to_clean,name,0)
    print(x)
    
      
