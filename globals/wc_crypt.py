
from Crypto.Cipher import AES
# Encryption
def encrypt(key,text):
    encryption_suite = AES.new(key, AES.MODE_CBC, 'This is an IV456')
    data= text.encode("utf-8")
    # padding:
    length = 16 - (len(data) % 16)
    data += bytes([length])*length
    cipher_text = encryption_suite.encrypt(data)
    return(cipher_text)


def decrypt(key,enc_text):
    # Decryption
    decryption_suite = AES.new(key, AES.MODE_CBC, 'This is an IV456')
    plain_text = decryption_suite.decrypt(enc_text)
    data=plain_text  
    data = data[:-data[-1]]
    data = data.decode("utf-8")
    return(data)


#t1=encrypt('This is a key123',"shai12345678910111211314151617181920\n21222324252627282930\n\n")
#print(t1)
#t2=decrypt('This is a key123',t1)
#print(t2)

