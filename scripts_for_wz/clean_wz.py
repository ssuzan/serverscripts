#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: json containing data from waze database
#output: properly formatted json containing data from waze database
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports
import requests

def clean_incident(incident):
    errors=[]
    if('nThumbsUp' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['nThumbsUp'],int)):
            place_holder_variable = True
        else:
            incident['nThumbsUp'] = ''
            errors.append('nThumbsUp not int')
    else:
        errors.append('nThumbsUp not present')
        incident['nThumbsUp'] = ''
    if('city' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['city'],str)):
            place_holder_variable = True
        else:
            incident['city'] = ''
            errors.append('city not str')
    else:
        errors.append('city not present')
        incident['city'] = ''  
    if('roadType' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['roadType'],str)):
            place_holder_variable = True
        else:
            incident['roadType'] = ''
            errors.append('roadType not str')
    else:
        errors.append('roadType not present')
        incident['roadType'] = ''      
    if('uuid' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['uuid'],str)):
            place_holder_variable = True
        else:
            return([False])
    else:
        return([False])
    if('pubMillis' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['pubMillis'],int)):
            place_holder_variable = True
        else:
            incident['pubMillis'] = ''
            errors.append('pubMillis not int')
    else:
        errors.append('pubMillis not present')
        incident['pubMillis'] = ''
    if('location' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['location'],dict)):
            place_holder_variable = True
            if('x' in incident['location'].keys()):
                place_holder_variable = True
                if(isinstance(incident['location']['x'],float)):
                    place_holder_variable = True
                else:
                    incident['location']['x'] = ''
                    errors.append('x not float')
            else:
                errors.append('x not present')
                incident['location']['x'] = ''
            if('y' in incident['location'].keys()):
                place_holder_variable = True
                if(isinstance(incident['location']['y'],float)):
                    place_holder_variable = True
                else:
                    incident['location']['y'] = ''
                    errors.append('y not float')
            else:
                errors.append('y not present')
                incident['location']['y'] = ''
        else:
            incident['location'] = ''
            errors.append('location not dict')
    else:
        errors.append('location not present')
        incident['location'] = ''
    if('confidence' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['confidence'],int)):
            place_holder_variable = True
        else:
            incident['confidence'] = ''
            errors.append('confidence not int')
    else:
        errors.append('confidence not present')
        incident['confidence'] = ''
    if('subtype' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['subtype'],str)):
            place_holder_variable = True
        else:
            incident['subtype'] = ''
            errors.append('subtype not str')
    else:
        errors.append('subtype not present')
        incident['subtype'] = ''
    if('country' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['country'],str)):
            place_holder_variable = True
        else:
            incident['country'] = ''
            errors.append('country not str')
    else:
        errors.append('country not present')
        incident['country'] = ''
    if('reportRating' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['reportRating'],int)):
            place_holder_variable = True
        else:
            incident['reportRating'] = ''
            errors.append('reportRating not int')
    else:
        errors.append('reportRating not present')
        incident['reportRating'] = ''
    if('magvar' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['magvar'],int)):
            place_holder_variable = True
        else:
            incident['magvar'] = ''
            errors.append('magvar not int')
    else:
        errors.append('magvar not present')
        incident['magvar'] = ''
    if('reportDescription' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['reportDescription'],str)):
            place_holder_variable = True
        else:
            incident['reportDescription'] = ''
            errors.append('reportDescription not str')
    else:
        errors.append('reportDescription not present')
        incident['reportDescription'] = ''
    if('type' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['type'],str)):
            place_holder_variable = True
        else:
            incident['type'] = ''
            errors.append('type not str')
    else:
        errors.append('type not present')
        incident['type'] = ''
    if('reliability' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['reliability'],int)):
            place_holder_variable = True
        else:
            incident['reliability'] = ''
            errors.append('reliability not int')
    else:
        errors.append('reliability not present')
        incident['reliability'] = ''    
    return (True, incident,errors)

def clean(json_clean):
    #print(json_clean.keys())
    errors = []
    if('startTimeMillis' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['startTimeMillis'],int)):
            place_holder_variable = True
        else:
            json_clean['startTimeMillis'] = ''
            errors.append('startTimeMillis not int')
    else:
        errors.append('startTimeMillis not present')
        json_clean['startTimeMillis'] = ''
    if('startTime' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['startTime'],str)):
            place_holder_variable = True
        else:
            json_clean['startTime'] = ''
            errors.append('startTime not str')
    else:
        errors.append('startTime not present')
        json_clean['startTime'] = ''
    if('endTime' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['endTime'],str)):
            place_holder_variable = True
        else:
            json_clean['endTime'] = ''
            errors.append('endTime not str')
    else:
        errors.append('endTime not present')
        json_clean['endTime'] = ''
    if('endTimeMillis' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['endTimeMillis'],int)):
            place_holder_variable = True
        else:
            json_clean['endTimeMillis'] = ''
            errors.append('endTimeMillis not int')
    else:
        errors.append('endTimeMillis not present')
        json_clean['endTimeMillis'] = ''
    if('alerts' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['alerts'],list)):
            for i in range(0,len(json_clean['alerts'])):
                clean_out = clean_incident(json_clean['alerts'][i])
                if clean_out[0] == True:
                    json_clean['alerts'][i] = clean_out[1]
                    if len(clean_out[2])>0:
                        errors.append("for incident "+json_clean['alerts'][i]['uuid']+" "+str(clean_out[2]))
                    for key in json_clean['alerts'][i]:
                        if key not in ['roadType','city','subtype', 'location', 'reliability', 'reportRating', 'country', 'pubMillis', 'nThumbsUp', 'street', 'reportDescription', 'confidence', 'type', 'magvar', 'uuid']:
                            errors.append('new key '+key+' in json_clean[alerts] '+json_clean['alerts'][i]['uuid'])                    
                else:
                    json_clean['alerts'][i] = False
                    errors.append("for incident number "+str(i)+" uuid not correct or present")
        else:
            errors.append('alerts not list')
            json_clean['alerts'] = []
    else:
        errors.append('alerts not present')
        json_clean['alerts'] = []        
    for key in json_clean:
        if key not in ['startTimeMillis', 'startTime', 'endTime', 'endTimeMillis', 'alerts']:
            errors.append('new key '+key+' in json_clean')
    return(True,json_clean,errors)    
