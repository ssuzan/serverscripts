#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: waze database https request (url) 
#output: json of data given by https request or json containing error message
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports
import jsonlib_python3
import requests
import store_wz


def downloadWaze(url_waze,save_path,filename):  
    try:
        #gets json from url
        r = requests.get(url_waze)
        try:
            r = r.json()
            try:
                store_wz.store_original(r,save_path,filename)
            except:
                r = universal_imports.json.loads('{"error" : "unable to save orignal"}') 
        except:
            r = universal_imports.json.loads('{"error" : "unable to convert to json"}') 
    except requests.exceptions.ConnectionError:
        r = universal_imports.json.loads('{"error" : "requests.exceptions.ConnectionError"}')  
    return r
     
