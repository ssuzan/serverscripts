#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: waze database https request (url) 
#output: json of data given by https request or json containing error message
import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts/globals')
import universal_imports
from datetime import datetime
import time
import jsonlib_python3
import random
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.schedulers.blocking import BlockingScheduler
import requests
import xmltodict
import collections
import store_dms_fast_live_lv

def downloadWaze(url_waze,save_path, dump_output_to,filename):  
    try:
        #gets json from url
        r = requests.get(url_waze)
        if (r.status_code== 200):
        
            #print(r.content)
            store_dms_fast_live_lv.store_original(r.content,save_path,dump_output_to,filename) 
            try:
                r = xmltodict.parse(r.content)
                r = universal_imports.json.dumps(r, sort_keys=True, indent=4, separators=(',', ': '))
                r = universal_imports.json.loads(r)
            except:
                r = universal_imports.json.loads('{"error" : "fetch_dms_live_lv failure to convert to json %s"}'%filename)
        else:
                r = universal_imports.json.loads('{"error" : "fetch_dms_live_lv incorrect status_code %s}'%filename)  
    except requests.exceptions.ConnectionError:
        r = universal_imports.json.loads('{"error" : "fetch_dms_live_lv requests.exceptions.ConnectionError %s"}'%filename)  
    except:
        r = universal_imports.json.loads('{"error" : "fetch_dms_live_lv error %s"}'%filename)  
    #print(r)
    return r
     
