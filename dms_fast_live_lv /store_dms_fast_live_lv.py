#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: json input and file directory path
#output: json stored to directory
from datetime import datetime
import time
import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts_production/globals')
import universal_imports
import json, requests
import push_to_fifo

dms_type_name = 'dms_fast_live_lv'
dms_tt_type_name = 'dms_tt_fast_live_lv'

def store_original(original_store_xml, save_path,dump_to_output,filename):
    DeviceFile = open(save_path+"/database_original/"+filename+'.xml', 'wb')
    DeviceFile.write(original_store_xml)  
    DeviceFile.close()    
    
def store(cleaned_store_json, save_path,dump_to_output,compromised_ids,filename,excluded):
    dms_fast_live_lv = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,60,61,62,63,64,65,66,67,68,73,74,75,76,77,78,83,86,87,88,89,90,93,94,95,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111]
    dms_tt_fast_live_lv = [24,53,59,69,70,71,72,79,80,81,82,84,85,91,92,96]
    #saves file
    now = str(datetime.utcnow())   
    now = now.split(" ")
    if len(compromised_ids) == 111:
        cleaned_store_json['DMSDevicesStatus'] = {}
        cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'] = []
        for i in range(1,111):
            cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'].append({'signId':str(i)})
        for i in range(0,110):
            if int(cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'][i]['signId']) in dms_fast_live_lv:
                i_type = 'dms_fast_live_lv'
            elif int(cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'][i]['signId']) in dms_tt_fast_live_lv:
                i_type = 'dms_tt_fast_live_lv'
            json_struct = {"type":i_type,"error":True,"date_time":now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1] +" UTC","suspicious":False,"data":cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'][i]}
            json_struct = json_struct
            json_save = universal_imports.json.dumps(json_struct)            
            if dump_to_output == 'FIFO':
                push_to_fifo.push_to_fifo(json_save, "input queue %s"%i_type) 
            else:
                DeviceFile = open(save_path+"/database/"+filename+"_"+cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'][i]['signId']+'.json', 'w')
                DeviceFile.write(json.dumps(json_struct, sort_keys=True, indent=4, separators=(',', ': ')))  
                DeviceFile.close()        
    else:
        
        for i in range(0,len(cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'])):
            i_type = ''
            error = False
            if int(cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'][i]['signId']) in compromised_ids:
                error = True            
            if int(cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'][i]['signId']) not in excluded:        
                if int(cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'][i]['signId']) in dms_fast_live_lv:
                    i_type = 'dms_fast_live_lv'
                elif int(cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'][i]['signId']) in dms_tt_fast_live_lv:
                    i_type = 'dms_tt_fast_live_lv'
                json_struct = {"type":i_type,"error":error,"date_time":now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1] +" UTC","suspicious":False,"data":cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'][i]}
                json_struct = json_struct
                json_save = universal_imports.json.dumps(json_struct)            
                if dump_to_output == 'FIFO':
                    push_to_fifo.push_to_fifo(json_save, "input queue %s"%i_type) 
                else:
                    DeviceFile = open(save_path+"/database/"+filename+"_"+cleaned_store_json['DMSDevicesStatus']['DMSDeviceStatus'][i]['signId']+'.json', 'w')
                    DeviceFile.write(json.dumps(json_struct, sort_keys=True, indent=4, separators=(',', ': ')))  
                    DeviceFile.close()
