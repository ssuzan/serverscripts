#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: json containing data from waze database
#output: properly formatted json containing data from waze database
from datetime import datetime
import time
import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts/globals')
import universal_imports
import jsonlib_python3
import random
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.schedulers.blocking import BlockingScheduler
import json, requests
from firebase import firebase
import sendemail
from email.mime.text import MIMEText


email_file = os.environ['WORKSPACE']+'/serverscripts/globals/emails'


def clean_incident(incident_clean,num):
    errors = []
    text = open(email_file,'r')
    email_string = text.read()
    email_json = universal_imports.json.loads(email_string)                            
    for key in incident_clean.keys():
        if key not in ['signId','currentStatus']:
            sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = "error", message = str("dms clean device new item "+key+" "+str(num+1)), login = email_json["email"], password = email_json["password"])        
    
    if('signId' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['signId'],str)):
            place_holder_variable = True
        else:
            incident_clean['signId'] = ''
            return(False,'signId wrong type')
    else:
        incident_clean['signId'] = ''
        return(False,'signId not present')
    if('currentStatus' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['currentStatus'],dict)):
            place_holder_variable = True
            if('message' in incident_clean['currentStatus'].keys()):
                place_holder_variable = True
                if(isinstance(incident_clean['currentStatus']['message'],dict)):
                    place_holder_variable = True
                    if('messageText' in incident_clean['currentStatus']['message'].keys()):
                        place_holder_variable = True
                        if(isinstance(incident_clean['currentStatus']['message']['messageText'],str) or incident_clean['currentStatus']['message']['messageText'] == None):
                            place_holder_variable = True
                        else:
                            incident_clean['currentStatus']['message']['messageText'] = ''
                            errors.append('messageText wrong type')
                    else:
                        incident_clean['currentStatus']['message']['messageText'] = ''
                        errors.append('messageText not present')
                    if('messageBeacon' in incident_clean['currentStatus']['message'].keys()):
                        place_holder_variable = True
                        if(isinstance(incident_clean['currentStatus']['message']['messageBeacon'],str)):
                            place_holder_variable = True
                        else:
                            incident_clean['currentStatus']['message']['messageBeacon'] = ''
                            errors.append('messageBeacon wrong type')
                    else:
                        incident_clean['currentStatus']['message']['messageBeacon'] = ''
                        errors.append('messageBeacon not present')
                    for key in incident_clean['currentStatus']['message'].keys():
                        #if num == 2:
                        #    print(key)
                        if key not in ['messageBeacon','messageText']:
                            #print(num,key)
                            sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = "error", message = str("dms clean device message new item "+key+" "+str(num+1)), login = email_json["email"], password = email_json["password"])        
                                                
                elif incident_clean['currentStatus']['message'] == None:
                    incident_clean['currentStatus']['message'] = None  
                    print('MESSAGE NONE ' + str(num))
                else:
                    incident_clean['currentStatus']['message'] = '' 
                    errors.append('message wrong type')
            
            else:
                incident_clean['currentStatus']['message'] = ''
                errors.append('message not present')
            if('status' in incident_clean['currentStatus'].keys()):
                place_holder_variable = True
                if(isinstance(incident_clean['currentStatus']['status'],str)):
                    place_holder_variable = True
                else:
                    incident_clean['currentStatus']['status'] = ''
                    errors.append('status wrong type')
            else:
                incident_clean['currentStatus']['status'] = ''
                errors.append('status not present')
            for key in incident_clean['currentStatus'].keys():
                if key not in ['message','status']:
                    sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = "error", message = str("dms clean device currentStatus new item "+key+" "+str(num+1)), login = email_json["email"], password = email_json["password"])        
                                
        else:
            incident_clean['currentStatus'] = ''  
            errors.append('currentStatus wrong type')
                     
    else:
        incident_clean['currentStatus'] = ''
        errors.append('currentStatus not present')
    return(True, incident_clean,errors)

def clean(json_clean,excluded):
    text = open(email_file,'r')
    email_string = text.read()
    email_json = universal_imports.json.loads(email_string)     
    place = 1
    compromised_ids = []
    ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110,111]    
    if('DMSDevicesStatus' in json_clean.keys()):
        place_holder_variable = True
        if(isinstance(json_clean['DMSDevicesStatus'],dict)):
            place_holder_variable = True
            if('DMSDeviceStatus' in json_clean['DMSDevicesStatus'].keys()):
                place_holder_variable = True
                if(isinstance(json_clean['DMSDevicesStatus']['DMSDeviceStatus'],list)):
                    place_holder_variable = True
                    new_list = [None]*111
                    present_ids = []
                    for i in range(0,len(json_clean['DMSDevicesStatus']['DMSDeviceStatus'])):
                        returned = clean_incident(json_clean['DMSDevicesStatus']['DMSDeviceStatus'][i],i+1)
                        if returned[0] == True:
                            if len(returned[2])>0:
                                compromised_ids.append(int(json_clean['DMSDevicesStatus']['DMSDeviceStatus'][i]['signId']))
                                sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = "error", message = str("dms clean warning device "+json_clean['DMSDevicesStatus']['DMSDeviceStatus'][i]['signId']+" "+str(returned[2])), login = email_json["email"], password = email_json["password"])                                    
                            new_list[int(json_clean['DMSDevicesStatus']['DMSDeviceStatus'][i]['signId'])-1] = returned[1]
                            present_ids.append(int(json_clean['DMSDevicesStatus']['DMSDeviceStatus'][i]['signId']))
                    for i in range(0,len(ids)):
                        if i+1 not in present_ids:
                            compromised_ids.append(i+1)
                            new_list[i] = {'signId':str(i+1)}
                            if (i+1 not in excluded):
                                sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = "error", message = str("dms clean warning device "+str(i+1)+" not present "), login = email_json["email"], password = email_json["password"])    
                            
                    
                    json_clean['DMSDevicesStatus']['DMSDeviceStatus'] = new_list
                            
                else:
                    json_clean['DMSDevicesStatus']['DMSDeviceStatus'] = ''
                    return(True,'DMSDevicesStatus not present',json_clean,compomised_ids,'DMSDeviceStatus not correct type')  
            else:
                json_clean['DMSDevicesStatus']['DMSDeviceStatus'] = ''   
                return(True,'DMSDevicesStatus not present',json_clean,compomised_ids,'DMSDeviceStatus not present')   
        else:
            json_clean['DMSDevicesStatus'] = ''
            return(True,'DMSDevicesStatus not present',json_clean,compomised_ids,'DMSDevicesStatus not correct type')  
        
    else:
        json_clean['DMSDevicesStatus'] = ''
        return(True,'DMSDevicesStatus not present',json_clean,compomised_ids,'DMSDevicesStatus not present')   
    return(True,'DMSDevicesStatus present',json_clean,compromised_ids)
