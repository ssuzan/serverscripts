#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts/globals')
import universal_imports
import sendemail
from datetime import datetime
import time
from email.mime.text import MIMEText
import requests
import fetch_dms_fast_live_lv
import store_dms_fast_live_lv
import clean_dms_fast_live_lv
import smtplib
import xmltodict
import wget
from automation_building_scripts import automate_build_clean

save_path = os.environ['WORKSPACE']+'/DB_backups/data_from_fast_dms'
email_file = os.environ['WORKSPACE']+'/serverscripts/globals/emails'
dms_url = 'http://bugatti.nvfast.org/realtimexml/FASTDMSStatus.xml'
dump_output_to = "FILE"
excluded = [62,75]
x = ['data_from_dms_07_09_2017_08_07.xml']
def call_all_dms():
    now = str(datetime.utcnow())
    now = now.split(" ")
    now[0] = now[0].split("-")
    now[1] = now[1].split(":")
    filename = "data_from_dms_"+now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1]      
    compromised_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110,111]        
    print("call_all_dms_fast_live_lv() running at time: "+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+" UTC")
    text = open(email_file,'r')
    email_string = text.read()
    email_json = universal_imports.json.loads(email_string)  
    ex_a = open(save_path+'/database_original/'+x[0],'r')
    ex_b = ex_a.read()
    ex_json = xmltodict.parse(ex_b)
    download_file = ex_json
    #download_file = fetch_dms_fast_live_lv.downloadWaze(dms_url,save_path,dump_output_to,filename)
    #print(download_file)
    if ('error' in download_file.keys()):
        store_dms_fast_live_lv.store({},save_path,dump_output_to,compromised_ids,filename,excluded)                                
        sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'error', message = "dms fetch error "+download_file['error'], login = email_json["email"], password = email_json["password"])    
    else:
        #ex = {'currentStatus': {'status': 'on', 'message': {'messageBeacon': '0', 'messageText': '[fo1][jl3]FLAMINGO         7[nl][jl3]US-95           11[nl][jl3]CRAIG           17'}}, 'signId': '1'}        
        #automate_build_clean.automate_clean(ex,"json_clean['DMSDevicesStatus']['DMSDeviceStatus'][i]")
        
        clean_results = clean_dms_fast_live_lv.clean(download_file,excluded)
        if clean_results[0] == True:
            
            if (clean_results[1] == 'DMSDevicesStatus present' and clean_results[2]['DMSDevicesStatus']['DMSDeviceStatus'] !=''):
                compromised_ids = clean_results[3]
                store_dms_fast_live_lv.store(clean_results[2],save_path,dump_output_to,compromised_ids,filename,excluded)
            elif (clean_results[1] == 'DMSDevicesStatus not present'):
                store_dms_fast_live_lv.store({},save_path,dump_output_to,compromised_ids,filename,excluded) 
                sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'error', message = "dms file empty "+filename+" "+clean[3], login = email_json["email"], password = email_json["password"])    
            else:
                store_dms_fast_live_lv.store({},save_path,dump_output_to,compromised_ids,filename,excluded)                        
                sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'error', message = "dms unknown clean error "+filename, login = email_json["email"], password = email_json["password"])    
                   
        else:
            store_dms_fast_live_lv.store({},save_path,dump_output_to,compromised_ids,filename,excluded)                        
            sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'error', message = "dms clean error "+filename+" "+clean_results[1], login = email_json["email"], password = email_json["password"])    
    print('call_all_dms_fast_live_lv.py completed successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')            
if __name__ == '__main__':
    #print("call_all_dms_file_live_lv() running at time: "+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+" UTC")
    call_all_dms()