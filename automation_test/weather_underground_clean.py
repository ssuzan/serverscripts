import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports
#checks all except for image,estimated
def weather_underground_clean_current_observations_check(current_observations):
    #print(current_observations.keys())
    if('display_location' in current_observations.keys()):
        if(isinstance(current_observations['display_location'],dict)):
            #print(current_observations['display_location']) 
            if('full' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['full'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['full'])
                    return(False,'full not str')
            else:
                return(False,'full not present')      
            if('state_name' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['state_name'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['state_name'])
                    return(False,'state_name not str')
            else:
                return(False,'state_name not present')   
            if('magic' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['magic'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['magic'])
                    return(False,'magic not str')
            else:
                return(False,'magic not present')    
            if('country' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['country'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['country'])
                    return(False,'country not str')
            else:
                return(False,'country not present')   
            if('elevation' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['elevation'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['elevation'])
                    return(False,'elevation not str')
            else:
                return(False,'elevation not present') 
            if('country_iso3166' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['country_iso3166'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['country_iso3166'])
                    return(False,'country_iso3166 not str')
            else:
                return(False,'country_iso3166 not present')     
            if('longitude' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['longitude'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['longitude'])
                    return(False,'longitude not str')
            else:
                return(False,'longitude not present')  
            if('latitude' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['latitude'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['latitude'])
                    return(False,'latitude not str')
            else:
                return(False,'latitude not present')
            if('wmo' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['wmo'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['wmo'])
                    return(False,'wmo not str')
            else:
                return(False,'wmo not present')
            if('state' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['state'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['state'])
                    return(False,'state not str')
            else:
                return(False,'state not present')   
            if('city' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['city'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['city'])
                    return(False,'city not str')
            else:
                return(False,'city not present') 
            if('zip' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['zip'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['zip'])
                    return(False,'zip not str')
            else:
                return(False,'zip not present')             
        else:
            print(current_observations['display_location'])
            return(False,'display_location not dict')
    else:
        return(False,'display_location not present') 
    if('observation_location' in current_observations.keys()):
        if(isinstance(current_observations['observation_location'],dict)):
            #print(current_observations['observation_location']) 
            if('full' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['full'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['full'])
                    return(False,'full not str')
            else:
                return(False,'full not present')      
            if('state_name' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['state_name'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['state_name'])
                    return(False,'state_name not str')
            else:
                return(False,'state_name not present')   
            if('magic' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['magic'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['magic'])
                    return(False,'magic not str')
            else:
                return(False,'magic not present')    
            if('country' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['country'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['country'])
                    return(False,'country not str')
            else:
                return(False,'country not present')   
            if('elevation' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['elevation'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['elevation'])
                    return(False,'elevation not str')
            else:
                return(False,'elevation not present') 
            if('country_iso3166' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['country_iso3166'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['country_iso3166'])
                    return(False,'country_iso3166 not str')
            else:
                return(False,'country_iso3166 not present')     
            if('longitude' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['longitude'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['longitude'])
                    return(False,'longitude not str')
            else:
                return(False,'longitude not present')  
            if('latitude' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['latitude'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['latitude'])
                    return(False,'latitude not str')
            else:
                return(False,'latitude not present')
            if('wmo' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['wmo'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['wmo'])
                    return(False,'wmo not str')
            else:
                return(False,'wmo not present')
            if('state' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['state'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['state'])
                    return(False,'state not str')
            else:
                return(False,'state not present')   
            if('city' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['city'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['city'])
                    return(False,'city not str')
            else:
                return(False,'city not present') 
            if('zip' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['zip'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['zip'])
                    return(False,'zip not str')
            else:
                return(False,'zip not present')                
        else:
            print(current_observations['observation_location'])
            return(False,'observation_location not dict')
    else:
        return(False,'observation_location not present')  
    if('station_id' in current_observations.keys()):
        if(isinstance(current_observations['station_id'],str)):
            place_holder_variable = True
        else:
            print(current_observations['station_id'])
            return(False,'station_id not dict')        
    else:
        return(False,'station_id not present')
    if('dewpoint_c' in current_observations.keys()):
        if(isinstance(current_observations['dewpoint_c'],int) or isinstance(current_observations['dewpoint_c'],float)):
            place_holder_variable = True
        else:
            print(current_observations['dewpoint_c'])
            return(False,'dewpoint_c not int')  
    else:
        return(False,'dewpoint_c not present')
    if('heat_index_c' in current_observations.keys()):
        if(isinstance(current_observations['heat_index_c'],str)):
            place_holder_variable = True
        else:
            print(current_observations['heat_index_c'])
            return(False,'heat_index_c not str')
    else:
        return(False,'heat_index_c not present') 
    if('solarradiation' in current_observations.keys()):
        if(isinstance(current_observations['solarradiation'],str)):
            place_holder_variable = True
        else:
            print(current_observations['solarradiation'])
            return(False,'solarradiation not str')
    else:
        return(False,'solarradiation not present')  
   
    if('image' in current_observations.keys()):
        if(isinstance(current_observations['image'],dict)):
            place_holder_variable = True
        else:
            print(current_observations['image'])
            return(False,'image not dict')
    else:
        return(False,'image not present')      
    if('precip_today_string' in current_observations.keys()):
        if(isinstance(current_observations['precip_today_string'],str)):
            place_holder_variable = True
        else:
            print(current_observations['precip_today_string'])
            return(False,'precip_today_string not str')
    else:
        return(False,'precip_today_string not present')   
    if('precip_1hr_in' in current_observations.keys()):
        if(isinstance(current_observations['precip_1hr_in'],str)):
            place_holder_variable = True
        else:
            print(current_observations['precip_1hr_in'])
            return(False,'precip_1hr_in not str')
    else:
        return(False,'precip_1hr_in not present') 
    if('precip_today_in' in current_observations.keys()):
        if(isinstance(current_observations['precip_today_in'],str)):
            place_holder_variable = True
        else:
            print(current_observations['precip_today_in'])
            return(False,'precip_today_in not str')
    else:
        return(False,'precip_today_in not present')
    if('estimated' in current_observations.keys()):
        if(isinstance(current_observations['estimated'],dict)):
            place_holder_variable = True
        else:
            print(current_observations['estimated'])
            return(False,'estimated not dict')
    else:
        return(False,'estimated not present')    
    
    if('temperature_string' in current_observations.keys()):
        if(isinstance(current_observations['temperature_string'],str)):
            place_holder_variable = True
        else:
            print(current_observations['temperature_string'])
            return(False,'temperature_string not str')
    else:
        return(False,'temperature_string not present') 
    if('history_url' in current_observations.keys()):
        if(isinstance(current_observations['history_url'],str)):
            place_holder_variable = True
        else:
            print(current_observations['history_url'])
            return(False,'history_url not str')
    else:
        return(False,'history_url not present')    
    if('local_tz_short' in current_observations.keys()):
        if(isinstance(current_observations['local_tz_short'],str)):
            place_holder_variable = True
        else:
            print(current_observations['local_tz_short'])
            return(False,'local_tz_short not str')
    else:
        return(False,'local_tz_short not present')         
    if('visibility_km' in current_observations.keys()):
        if(isinstance(current_observations['visibility_km'],str)):
            place_holder_variable = True
        else:
            print(current_observations['visibility_km'])
            return(False,'visibility_km not str')
    else:
        return(False,'visibility_km not present')   
    if('wind_mph' in current_observations.keys()):
        if(isinstance(current_observations['wind_mph'],float)):
            place_holder_variable = True
        else:
            print(current_observations['wind_mph'])
            return(False,'wind_mph not float')
    else:
        return(False,'wind_mph not present')    
    if('precip_today_metric' in current_observations.keys()):
        if(isinstance(current_observations['precip_today_metric'],str)):
            place_holder_variable = True
        else:
            print(current_observations['precip_today_metric'])
            return(False,'precip_today_metric not str')
    else:
        return(False,'precip_today_metric not present')    
    if('precip_1hr_string' in current_observations.keys()):
        if(isinstance(current_observations['precip_1hr_string'],str)):
            place_holder_variable = True
        else:
            print(current_observations['precip_1hr_string'])
            return(False,'precip_1hr_string not str')
    else:
        return(False,'precip_1hr_string not present')     
    if('nowcast' in current_observations.keys()):
        if(isinstance(current_observations['nowcast'],str)):
            place_holder_variable = True
        else:
            print(current_observations['nowcast'])
            return(False,'nowcast not str')
    else:
        return(False,'nowcast not present')   
    if('wind_kph' in current_observations.keys()):
        if(isinstance(current_observations['wind_kph'],int)):
            place_holder_variable = True
        else:
            print(current_observations['wind_kph'])
            return(False,'wind_kph not int')
    else:
        return(False,'wind_kph not present')    
    if('windchill_f' in current_observations.keys()):
        if(isinstance(current_observations['windchill_f'],str)):
            place_holder_variable = True
        else:
            print(current_observations['windchill_f'])
            return(False,'windchill_f not str')
    else:
        return(False,'windchill_f not present')       
    if('windchill_string' in current_observations.keys()):
        if(isinstance(current_observations['windchill_string'],str)):
            place_holder_variable = True
        else:
            print(current_observations['windchill_string'])
            return(False,'windchill_string not str')
    else:
        return(False,'windchill_string not present')    
    if('wind_dir' in current_observations.keys()):
        if(isinstance(current_observations['wind_dir'],str)):
            place_holder_variable = True
        else:
            print(current_observations['wind_dir'])
            return(False,'wind_dir not str')
    else:
        return(False,'wind_dir not present')   
    if('temp_f' in current_observations.keys()):
        if(isinstance(current_observations['temp_f'],float)):
            place_holder_variable = True
        else:
            print(current_observations['temp_f'])
            return(False,'temp_f not float')
    else:
        return(False,'temp_f not present')    
    if('relative_humidity' in current_observations.keys()):
        if(isinstance(current_observations['relative_humidity'],str)):
            place_holder_variable = True
        else:
            print(current_observations['relative_humidity'])
            return(False,'relative_humidity not str')
    else:
        return(False,'relative_humidity not present')
    if('pressure_trend' in current_observations.keys()):
        if(isinstance(current_observations['pressure_trend'],str)):
            place_holder_variable = True
        else:
            print(current_observations['pressure_trend'])
            return(False,'pressure_trend not str')
    else:
        return(False,'pressure_trend not present')  
    if('heat_index_string' in current_observations.keys()):
        if(isinstance(current_observations['heat_index_string'],str)):
            place_holder_variable = True
        else:
            print(current_observations['heat_index_string'])
            return(False,'heat_index_string not str')
    else:
        return(False,'heat_index_string not present')    
    if('UV' in current_observations.keys()):
        if(isinstance(current_observations['UV'],str)):
            place_holder_variable = True
        else:
            print(current_observations['UV'])
            return(False,'UV not str')
    else:
        return(False,'UV not present')    
    if('local_epoch' in current_observations.keys()):
        if(isinstance(current_observations['local_epoch'],str)):
            place_holder_variable = True
        else:
            print(current_observations['local_epoch'])
            return(False,'local_epoch not str')
    else:
        return(False,'local_epoch not present')     
    if('heat_index_f' in current_observations.keys()):
        if(isinstance(current_observations['heat_index_f'],str)):
            place_holder_variable = True
        else:
            print(current_observations['heat_index_f'])
            return(False,'heat_index_f not str')
    else:
        return(False,'heat_index_f not present')     
    if('feelslike_string' in current_observations.keys()):
        if(isinstance(current_observations['feelslike_string'],str)):
            place_holder_variable = True
        else:
            print(current_observations['feelslike_string'])
            return(False,'feelslike_string not str')
    else:
        return(False,'feelslike_string not present')   
    if('wind_degrees' in current_observations.keys()):
        if(isinstance(current_observations['wind_degrees'],int)):
            place_holder_variable = True
        else:
            print(current_observations['wind_degrees'])
            return(False,'wind_degrees not int')
    else:
        return(False,'wind_degrees not present')   
    if('feelslike_f' in current_observations.keys()):
        if(isinstance(current_observations['feelslike_f'],str)):
            place_holder_variable = True
        else:
            print(current_observations['feelslike_f'])
            return(False,'feelslike_f not str')
    else:
        return(False,'feelslike_f not present')
    if('local_tz_offset' in current_observations.keys()):
        if(isinstance(current_observations['local_tz_offset'],str)):
            place_holder_variable = True
        else:
            print(current_observations['local_tz_offset'])
            return(False,'local_tz_offset not str')
    else:
        return(False,'local_tz_offset not present')  
    if('forecast_url' in current_observations.keys()):
        if(isinstance(current_observations['forecast_url'],str)):
            place_holder_variable = True
        else:
            print(current_observations['forecast_url'])
            return(False,'forecast_url not str')
    else:
        return(False,'forecast_url not present')  
    if('precip_1hr_metric' in current_observations.keys()):
        if(isinstance(current_observations['precip_1hr_metric'],str)):
            place_holder_variable = True
        else:
            print(current_observations['precip_1hr_metric'])
            return(False,'precip_1hr_metric not str')
    else:
        return(False,'precip_1hr_metric not present')     
    if('weather' in current_observations.keys()):
        if(isinstance(current_observations['weather'],str)):
            place_holder_variable = True
        else:
            print(current_observations['weather'])
            return(False,'weather not str')
    else:
        return(False,'weather not present')  
    if('observation_time' in current_observations.keys()):
        if(isinstance(current_observations['observation_time'],str)):
            place_holder_variable = True
        else:
            print(current_observations['observation_time'])
            return(False,'observation_time not str')
    else:
        return(False,'observation_time not present')    
    if('wind_string' in current_observations.keys()):
        if(isinstance(current_observations['wind_string'],str)):
            place_holder_variable = True
        else:
            print(current_observations['wind_string'])
            return(False,'wind_string not str')
    else:
        return(False,'wind_string not present')  
    if('feelslike_c' in current_observations.keys()):
        if(isinstance(current_observations['feelslike_c'],str)):
            place_holder_variable = True
        else:
            print(current_observations['feelslike_c'])
            return(False,'feelslike_c not str')
    else:
        return(False,'feelslike_c not present')
    if('local_tz_long' in current_observations.keys()):
        if(isinstance(current_observations['local_tz_long'],str)):
            place_holder_variable = True
        else:
            print(current_observations['local_tz_long'])
            return(False,'local_tz_long not str')
    else:
        return(False,'local_tz_long not present')   
    if('icon' in current_observations.keys()):
        if(isinstance(current_observations['icon'],str)):
            place_holder_variable = True
        else:
            print(current_observations['icon'])
            return(False,'icon not str')
    else:
        return(False,'icon not present') 
    if('wind_gust_kph' in current_observations.keys()):
        if(isinstance(current_observations['wind_gust_kph'],int)):
            place_holder_variable = True
        else:
            print(current_observations['wind_gust_kph'])
            return(False,'wind_gust_kph not int')
    else:
        return(False,'wind_gust_kph not present')  
    if('wind_gust_mph' in current_observations.keys()):
        if(isinstance(current_observations['wind_gust_mph'],int)):
            place_holder_variable = True
        else:
            print(current_observations['wind_gust_mph'])
            return(False,'wind_gust_mph not int')
    else:
        return(False,'wind_gust_mph not present')        
    if('temp_c' in current_observations.keys()):
        if(isinstance(current_observations['temp_c'],float)):
            place_holder_variable = True
        else:
            print(current_observations['temp_c'])
            return(False,'temp_c not float')
    else:
        return(False,'temp_c not present')   
    if('dewpoint_string' in current_observations.keys()):
        if(isinstance(current_observations['dewpoint_string'],str)):
            place_holder_variable = True
        else:
            print(current_observations['dewpoint_string'])
            return(False,'dewpoint_string not str')
    else:
        return(False,'dewpoint_string not present')     
    if('local_time_rfc822' in current_observations.keys()):
        if(isinstance(current_observations['local_time_rfc822'],str)):
            place_holder_variable = True
        else:
            print(current_observations['local_time_rfc822'])
            return(False,'local_time_rfc822 not str')
    else:
        return(False,'local_time_rfc822 not present')    
    if('pressure_in' in current_observations.keys()):
        if(isinstance(current_observations['pressure_in'],str)):
            place_holder_variable = True
        else:
            print(current_observations['pressure_in'])
            return(False,'pressure_in not str')
    else:
        return(False,'pressure_in not present')    
    if('pressure_mb' in current_observations.keys()):
        if(isinstance(current_observations['pressure_mb'],str)):
            place_holder_variable = True
        else:
            print(current_observations['pressure_mb'])
            return(False,'pressure_mb not str')
    else:
        return(False,'pressure_mb not present')      
    if('icon_url' in current_observations.keys()):
        if(isinstance(current_observations['icon_url'],str)):
            place_holder_variable = True
        else:
            print(current_observations['icon_url'])
            return(False,'icon_url not str')
    else:
        return(False,'icon_url not present')     
    if('observation_time_rfc822' in current_observations.keys()):
        if(isinstance(current_observations['observation_time_rfc822'],str)):
            place_holder_variable = True
        else:
            print(current_observations['observation_time_rfc822'])
            return(False,'observation_time_rfc822 not str')
    else:
        return(False,'observation_time_rfc822 not present')      
    if('windchill_c' in current_observations.keys()):
        if(isinstance(current_observations['windchill_c'],str)):
            place_holder_variable = True
        else:
            print(current_observations['windchill_c'])
            return(False,'windchill_c not str')
    else:
        return(False,'windchill_c not present')    
    if('dewpoint_f' in current_observations.keys()):
        if(isinstance(current_observations['dewpoint_f'],int)):
            place_holder_variable = True
        else:
            print(current_observations['dewpoint_f'])
            return(False,'dewpoint_f not int')
    else:
        return(False,'dewpoint_f not present')   
    if('visibility_mi' in current_observations.keys()):
        if(isinstance(current_observations['visibility_mi'],str)):
            place_holder_variable = True
        else:
            print(current_observations['visibility_mi'])
            return(False,'visibility_mi not str')
    else:
        return(False,'visibility_mi not present')   
    if('observation_epoch' in current_observations.keys()):
        if(isinstance(current_observations['observation_epoch'],str)):
            place_holder_variable = True
        else:
            print(current_observations['observation_epoch'])
            return(False,'observation_epoch not str')
    else:
        return(False,'observation_epoch not present')      
    if('ob_url' in current_observations.keys()):
        if(isinstance(current_observations['ob_url'],str)):
            place_holder_variable = True
        else:
            print(current_observations['ob_url'])
            return(False,'ob_url not str')
    else:
        return(False,'ob_url not present')          
    return(True,'no errors')
    
    
def weather_underground_clean(weather_file):
    #print(weather_file.keys())
    if ('response' in weather_file.keys()):
        #print(weather_file['response'].keys())
        if('features' in weather_file['response'].keys()):
            if(isinstance(weather_file['response']['features'],dict)):
                if(isinstance(weather_file['response']['features']['conditions'],int)):
                    place_holder_variable = True
                else:
                    print(weather_file['response']['features']['conditions'])
                    return(False,'conditions not int')
            else:
                print(weather_file['response']['features'])
                return(False,'features not dict')
        else:
            return(False,'features not present')
        if('version' in weather_file['response'].keys()):
            if(isinstance(weather_file['response']['version'],str)):
                place_holder_variable = True
            else:
                print(weather_file['response']['version'])
                return(False,'version not float')
        else:
            return(False,'version not present')
        if('termsofService' in weather_file['response'].keys()):
            if(isinstance(weather_file['response']['termsofService'],str)):
                place_holder_variable = True
            else:
                print(weather_file['response']['termsofService'])
                return(False,'termsofService not str')
        else:
            return(False,'termsofService not present')
    else:
        return(False,'response not present')
    if('current_observation' in weather_file.keys()):
        return(weather_underground_clean_current_observations_check(weather_file['current_observation']))
    else:
        return(False,'current_observation not present')
    return(False,'unknown error')