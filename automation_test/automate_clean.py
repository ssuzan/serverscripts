import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports
import sendemail
email_file = workspace_environ+'/serverscripts/globals/emails'
import jsonlib_python3
import requests
import smtplib
from email.mime.text import MIMEText

def automate_clean(dict_to_clean,name):
    #print(name)
    lines = []
    keys = dict_to_clean.keys()
    #print(keys)
    #print(name.count("["))
    indents = "\t"*name.count("[")
    for key in dict_to_clean.keys():
        #print(dict_to_clean[key])
        print(indents+"if('%s' in %s.keys()):"%(key,name))
        print(indents+"\tplace_holder_variable = True")
        if(isinstance(dict_to_clean[key],str)):
            print(indents+"\tif(isinstance(%s['%s'],str)):"%(name,key))
            print(indents+"\t\tplace_holder_variable = True")
            print(indents+"\telse:")
            print(indents+"\t\tprint(%s['%s'])"%(name,key))
            print(indents+"\t\treturn(False,'%s not str')"%(key))
        elif(isinstance(dict_to_clean[key],int)):
            print(indents+"\tif(isinstance(%s['%s'],int)):"%(name,key))
            print(indents+"\t\tplace_holder_variable = True")
            print(indents+"\telse:")
            print(indents+"\t\tprint(%s['%s'])"%(name,key))
            print(indents+"\t\treturn(False,'%s not int')"%(key))   
        elif(isinstance(dict_to_clean[key],float)):
            print(indents+"\tif(isinstance(%s['%s'],float)):"%(name,key))
            print(indents+"\t\tplace_holder_variable = True")
            print(indents+"\telse:")
            print(indents+"\t\tprint(%s['%s'])"%(name,key))
            print(indents+"\t\treturn(False,'%s not float')"%(key))
        elif(isinstance(dict_to_clean[key],list)):
            print(indents+"\tif(isinstance(%s[%s],list)):"%(name,key))
            print(indents+"\t\tplace_holder_variable = True")
            print(indents+"\telse:")
            print(indents+"\t\tprint(%s['%s'])"%(name,key))
            print(indents+"\t\treturn(False,'%s not list')"%(key))
        elif(isinstance(dict_to_clean[key],dict)):
            print(indents+"\tif(isinstance(%s['%s'],dict)):"%(name,key))
            print(indents+"\t\tplace_holder_variable = True")
            print(indents+"\telse:")
            print(indents+"\t\tprint(%s['%s'])"%(name,key))
            print(indents+"\t\treturn(False,'%s not dict')"%(key))
            automate_clean(dict_to_clean[key],name+"['%s']"%(key))
        else:
            print(indents+"%s \tUNKNOWN TYPE:MANUAL INPUT REQUIRED"%(key))
            
        print(indents+"else:")
        print(indents+"\treturn(False,'%s not present')"%(key))
      
