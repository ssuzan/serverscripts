import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports
import sendemail
email_file = workspace_environ+'/serverscripts/globals/emails'
import jsonlib_python3
import requests
import smtplib
from email.mime.text import MIMEText

def automate_clean(dict_to_clean,name):
    #print(name)
    lines = []
    keys = dict_to_clean.keys()
    #print(keys)
    for key in dict_to_clean.keys():
        #print(dict_to_clean[key])
        print("if('%s' in %s.keys()):\n\tplace_holder_variable = True"%(key,name))
        if(isinstance(dict_to_clean[key],str)):
            print("\tif(isinstance(%s['%s'],str)):"%(name,key))
            print("\t\tplace_holder_variable = True")
            print("\telse:")
            print("\t\tprint(%s['%s'])"%(name,key))
            print("\t\treturn(False,'%s not str')"%(key))
        elif(isinstance(dict_to_clean[key],int)):
            print("\tif(isinstance(%s['%s'],int)):"%(name,key))
            print("\t\tplace_holder_variable = True")
            print("\telse:")
            print("\t\tprint(%s['%s'])"%(name,key))
            print("\t\treturn(False,'%s not int')"%(key))   
        elif(isinstance(dict_to_clean[key],float)):
            print("\tif(isinstance(%s['%s'],float)):"%(name,key))
            print("\t\tplace_holder_variable = True")
            print("\telse:")
            print("\t\tprint(%s['%s'])"%(name,key))
            print("\t\treturn(False,'%s not float')"%(key))
        elif(isinstance(dict_to_clean[key],list)):
            print("\tif(isinstance(%s[%s],list)):"%(name,key))
            print("\t\tplace_holder_variable = True")
            print("\telse:")
            print("\t\tprint(%s['%s'])"%(name,key))
            print("\t\treturn(False,'%s not list')"%(key))
        elif(isinstance(dict_to_clean[key],dict)):
            print("\tif(isinstance(%s[%s],dict)):"%(name,key))
            print("\t\tplace_holder_variable = True")
            print("\telse:")
            print("\t\tprint(%s['%s'])"%(name,key))
            print("\t\treturn(False,'%s not dict')"%(key))
            automate_clean(dict_to_clean[key],name+"['%s']"%(key))
        else:
            print("%s UNKNOWN TYPE:MANUAL INPUT REQUIRED"%(key))
            
        print("else:\n\treturn(False,'%s not present)"%(key))