import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports
import sendemail
email_file = workspace_environ+'/serverscripts/globals/emails'
import jsonlib_python3
import requests
import smtplib
from email.mime.text import MIMEText

def automate_clean(dict_to_clean,name):
    #print(name)
    lines = []
    keys = dict_to_clean.keys()
    #print(keys)
    #print(name.count("["))
    indents = "\t"*name.count("[")
    for key in dict_to_clean.keys():
        #print(dict_to_clean[key])
        print(indents+"if('%s' in %s.keys()):"%(key,name))
        print(indents+"\tplace_holder_variable = True")
        if(isinstance(dict_to_clean[key],str)):
            print(indents+"\tif(isinstance(%s['%s'],str)):"%(name,key))
            print(indents+"\t\tplace_holder_variable = True")
            print(indents+"\telse:")
            print(indents+"\t\tprint(%s['%s'])"%(name,key))
            print(indents+"\t\treturn(False,'%s not str')"%(key))
        elif(isinstance(dict_to_clean[key],int)):
            print(indents+"\tif(isinstance(%s['%s'],int)):"%(name,key))
            print(indents+"\t\tplace_holder_variable = True")
            print(indents+"\telse:")
            print(indents+"\t\tprint(%s['%s'])"%(name,key))
            print(indents+"\t\treturn(False,'%s not int')"%(key))   
        elif(isinstance(dict_to_clean[key],float)):
            print(indents+"\tif(isinstance(%s['%s'],float)):"%(name,key))
            print(indents+"\t\tplace_holder_variable = True")
            print(indents+"\telse:")
            print(indents+"\t\tprint(%s['%s'])"%(name,key))
            print(indents+"\t\treturn(False,'%s not float')"%(key))
        elif(isinstance(dict_to_clean[key],list)):
            print(indents+"\tif(isinstance(%s[%s],list)):"%(name,key))
            print(indents+"\t\tplace_holder_variable = True")
            print(indents+"\telse:")
            print(indents+"\t\tprint(%s['%s'])"%(name,key))
            print(indents+"\t\treturn(False,'%s not list')"%(key))
        elif(isinstance(dict_to_clean[key],dict)):
            print(indents+"\tif(isinstance(%s['%s'],dict)):"%(name,key))
            print(indents+"\t\tplace_holder_variable = True")
            print(indents+"\telse:")
            print(indents+"\t\tprint(%s['%s'])"%(name,key))
            print(indents+"\t\treturn(False,'%s not dict')"%(key))
            automate_clean(dict_to_clean[key],name+"['%s']"%(key))
        else:
            print(indents+"%s \tUNKNOWN TYPE:MANUAL INPUT REQUIRED"%(key))
            
        print(indents+"else:")
        print(indents+"\treturn(False,'%s not present')"%(key))
        
def clean(r):
    if('response' in r.keys()):
        place_holder_variable = True
        if(isinstance(r['response'],dict)):
            place_holder_variable = True
        else:
            print(r['response'])
            return(False,'response not dict')
        if('termsofService' in r['response'].keys()):
            place_holder_variable = True
            if(isinstance(r['response']['termsofService'],str)):
                place_holder_variable = True
            else:
                print(r['response']['termsofService'])
                return(False,'termsofService not str')
        else:
            return(False,'termsofService not present')
        if('version' in r['response'].keys()):
            place_holder_variable = True
            if(isinstance(r['response']['version'],str)):
                place_holder_variable = True
            else:
                print(r['response']['version'])
                return(False,'version not str')
        else:
            return(False,'version not present')
        if('features' in r['response'].keys()):
            place_holder_variable = True
            if(isinstance(r['response']['features'],dict)):
                place_holder_variable = True
            else:
                print(r['response']['features'])
                return(False,'features not dict')
            if('conditions' in r['response']['features'].keys()):
                place_holder_variable = True
                if(isinstance(r['response']['features']['conditions'],int)):
                    place_holder_variable = True
                else:
                    print(r['response']['features']['conditions'])
                    return(False,'conditions not int')
            else:
                return(False,'conditions not present')
        else:
            return(False,'features not present')
    else:
        return(False,'response not present')
    if('current_observation' in r.keys()):
        place_holder_variable = True
        if(isinstance(r['current_observation'],dict)):
            place_holder_variable = True
        else:
            print(r['current_observation'])
            return(False,'current_observation not dict')
        if('local_epoch' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['local_epoch'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['local_epoch'])
                return(False,'local_epoch not str')
        else:
            return(False,'local_epoch not present')
        if('wind_string' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['wind_string'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['wind_string'])
                return(False,'wind_string not str')
        else:
            return(False,'wind_string not present')
        if('wind_degrees' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['wind_degrees'],int)):
                place_holder_variable = True
            else:
                print(r['current_observation']['wind_degrees'])
                return(False,'wind_degrees not int')
        else:
            return(False,'wind_degrees not present')
        if('local_tz_offset' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['local_tz_offset'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['local_tz_offset'])
                return(False,'local_tz_offset not str')
        else:
            return(False,'local_tz_offset not present')
        if('ob_url' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['ob_url'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['ob_url'])
                return(False,'ob_url not str')
        else:
            return(False,'ob_url not present')
        if('heat_index_string' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['heat_index_string'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['heat_index_string'])
                return(False,'heat_index_string not str')
        else:
            return(False,'heat_index_string not present')
        if('solarradiation' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['solarradiation'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['solarradiation'])
                return(False,'solarradiation not str')
        else:
            return(False,'solarradiation not present')
        if('local_tz_long' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['local_tz_long'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['local_tz_long'])
                return(False,'local_tz_long not str')
        else:
            return(False,'local_tz_long not present')
        if('dewpoint_c' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['dewpoint_c'],int)):
                place_holder_variable = True
            else:
                print(r['current_observation']['dewpoint_c'])
                return(False,'dewpoint_c not int')
        else:
            return(False,'dewpoint_c not present')
        if('wind_kph' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['wind_kph'],int)):
                place_holder_variable = True
            else:
                print(r['current_observation']['wind_kph'])
                return(False,'wind_kph not int')
        else:
            return(False,'wind_kph not present')
        if('pressure_trend' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['pressure_trend'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['pressure_trend'])
                return(False,'pressure_trend not str')
        else:
            return(False,'pressure_trend not present')
        if('display_location' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['display_location'],dict)):
                place_holder_variable = True
            else:
                print(r['current_observation']['display_location'])
                return(False,'display_location not dict')
            if('state' in r['current_observation']['display_location'].keys()):
                place_holder_variable = True
                if(isinstance(r['current_observation']['display_location']['state'],str)):
                    place_holder_variable = True
                else:
                    print(r['current_observation']['display_location']['state'])
                    return(False,'state not str')
            else:
                return(False,'state not present')
            if('city' in r['current_observation']['display_location'].keys()):
                place_holder_variable = True
                if(isinstance(r['current_observation']['display_location']['city'],str)):
                    place_holder_variable = True
                else:
                    print(r['current_observation']['display_location']['city'])
                    return(False,'city not str')
            else:
                return(False,'city not present')
            if('wmo' in r['current_observation']['display_location'].keys()):
                place_holder_variable = True
                if(isinstance(r['current_observation']['display_location']['wmo'],str)):
                    place_holder_variable = True
                else:
                    print(r['current_observation']['display_location']['wmo'])
                    return(False,'wmo not str')
            else:
                return(False,'wmo not present')
            if('latitude' in r['current_observation']['display_location'].keys()):
                place_holder_variable = True
                if(isinstance(r['current_observation']['display_location']['latitude'],str)):
                    place_holder_variable = True
                else:
                    print(r['current_observation']['display_location']['latitude'])
                    return(False,'latitude not str')
            else:
                return(False,'latitude not present')
            if('full' in r['current_observation']['display_location'].keys()):
                place_holder_variable = True
                if(isinstance(r['current_observation']['display_location']['full'],str)):
                    place_holder_variable = True
                else:
                    print(r['current_observation']['display_location']['full'])
                    return(False,'full not str')
            else:
                return(False,'full not present')
            if('state_name' in r['current_observation']['display_location'].keys()):
                place_holder_variable = True
                if(isinstance(r['current_observation']['display_location']['state_name'],str)):
                    place_holder_variable = True
                else:
                    print(r['current_observation']['display_location']['state_name'])
                    return(False,'state_name not str')
            else:
                return(False,'state_name not present')
            if('longitude' in r['current_observation']['display_location'].keys()):
                place_holder_variable = True
                if(isinstance(r['current_observation']['display_location']['longitude'],str)):
                    place_holder_variable = True
                else:
                    print(r['current_observation']['display_location']['longitude'])
                    return(False,'longitude not str')
            else:
                return(False,'longitude not present')
            if('magic' in r['current_observation']['display_location'].keys()):
                place_holder_variable = True
                if(isinstance(r['current_observation']['display_location']['magic'],str)):
                    place_holder_variable = True
                else:
                    print(r['current_observation']['display_location']['magic'])
                    return(False,'magic not str')
            else:
                return(False,'magic not present')
            if('zip' in r['current_observation']['display_location'].keys()):
                place_holder_variable = True
                if(isinstance(r['current_observation']['display_location']['zip'],str)):
                    place_holder_variable = True
                else:
                    print(r['current_observation']['display_location']['zip'])
                    return(False,'zip not str')
            else:
                return(False,'zip not present')
            if('country' in r['current_observation']['display_location'].keys()):
                place_holder_variable = True
                if(isinstance(r['current_observation']['display_location']['country'],str)):
                    place_holder_variable = True
                else:
                    print(r['current_observation']['display_location']['country'])
                    return(False,'country not str')
            else:
                return(False,'country not present')
            if('country_iso3166' in r['current_observation']['display_location'].keys()):
                place_holder_variable = True
                if(isinstance(r['current_observation']['display_location']['country_iso3166'],str)):
                    place_holder_variable = True
                else:
                    print(r['current_observation']['display_location']['country_iso3166'])
                    return(False,'country_iso3166 not str')
            else:
                return(False,'country_iso3166 not present')
            if('elevation' in r['current_observation']['display_location'].keys()):
                place_holder_variable = True
                if(isinstance(r['current_observation']['display_location']['elevation'],str)):
                    place_holder_variable = True
                else:
                    print(r['current_observation']['display_location']['elevation'])
                    return(False,'elevation not str')
            else:
                return(False,'elevation not present')
        else:
            return(False,'display_location not present')
        if('windchill_f' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['windchill_f'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['windchill_f'])
                return(False,'windchill_f not str')
        else:
            return(False,'windchill_f not present')
        if('dewpoint_f' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['dewpoint_f'],int)):
                place_holder_variable = True
            else:
                print(r['current_observation']['dewpoint_f'])
                return(False,'dewpoint_f not int')
        else:
            return(False,'dewpoint_f not present')
        if('weather' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['weather'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['weather'])
                return(False,'weather not str')
        else:
            return(False,'weather not present')
        if('heat_index_c' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['heat_index_c'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['heat_index_c'])
                return(False,'heat_index_c not str')
        else:
            return(False,'heat_index_c not present')
        if('observation_time_rfc822' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['observation_time_rfc822'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['observation_time_rfc822'])
                return(False,'observation_time_rfc822 not str')
        else:
            return(False,'observation_time_rfc822 not present')
        if('icon_url' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['icon_url'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['icon_url'])
                return(False,'icon_url not str')
        else:
            return(False,'icon_url not present')
        if('station_id' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['station_id'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['station_id'])
                return(False,'station_id not str')
        else:
            return(False,'station_id not present')
        if('icon' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['icon'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['icon'])
                return(False,'icon not str')
        else:
            return(False,'icon not present')
        if('feelslike_c' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['feelslike_c'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['feelslike_c'])
                return(False,'feelslike_c not str')
        else:
            return(False,'feelslike_c not present')
        if('observation_time' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['observation_time'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['observation_time'])
                return(False,'observation_time not str')
        else:
            return(False,'observation_time not present')
        if('precip_today_string' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['precip_today_string'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['precip_today_string'])
                return(False,'precip_today_string not str')
        else:
            return(False,'precip_today_string not present')
        if('precip_1hr_string' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['precip_1hr_string'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['precip_1hr_string'])
                return(False,'precip_1hr_string not str')
        else:
            return(False,'precip_1hr_string not present')
        if('observation_location' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['observation_location'],dict)):
                place_holder_variable = True
            else:
                print(r['current_observation']['observation_location'])
                return(False,'observation_location not dict')
            if('state' in r['current_observation']['observation_location'].keys()):
                place_holder_variable = True
                if(isinstance(r['current_observation']['observation_location']['state'],str)):
                    place_holder_variable = True
                else:
                    print(r['current_observation']['observation_location']['state'])
                    return(False,'state not str')
            else:
                return(False,'state not present')
            if('latitude' in r['current_observation']['observation_location'].keys()):
                place_holder_variable = True
                if(isinstance(r['current_observation']['observation_location']['latitude'],str)):
                    place_holder_variable = True
                else:
                    print(r['current_observation']['observation_location']['latitude'])
                    return(False,'latitude not str')
            else:
                return(False,'latitude not present')
            if('city' in r['current_observation']['observation_location'].keys()):
                place_holder_variable = True
                if(isinstance(r['current_observation']['observation_location']['city'],str)):
                    place_holder_variable = True
                else:
                    print(r['current_observation']['observation_location']['city'])
                    return(False,'city not str')
            else:
                return(False,'city not present')
            if('full' in r['current_observation']['observation_location'].keys()):
                place_holder_variable = True
                if(isinstance(r['current_observation']['observation_location']['full'],str)):
                    place_holder_variable = True
                else:
                    print(r['current_observation']['observation_location']['full'])
                    return(False,'full not str')
            else:
                return(False,'full not present')
            if('country' in r['current_observation']['observation_location'].keys()):
                place_holder_variable = True
                if(isinstance(r['current_observation']['observation_location']['country'],str)):
                    place_holder_variable = True
                else:
                    print(r['current_observation']['observation_location']['country'])
                    return(False,'country not str')
            else:
                return(False,'country not present')
            if('country_iso3166' in r['current_observation']['observation_location'].keys()):
                place_holder_variable = True
                if(isinstance(r['current_observation']['observation_location']['country_iso3166'],str)):
                    place_holder_variable = True
                else:
                    print(r['current_observation']['observation_location']['country_iso3166'])
                    return(False,'country_iso3166 not str')
            else:
                return(False,'country_iso3166 not present')
            if('elevation' in r['current_observation']['observation_location'].keys()):
                place_holder_variable = True
                if(isinstance(r['current_observation']['observation_location']['elevation'],str)):
                    place_holder_variable = True
                else:
                    print(r['current_observation']['observation_location']['elevation'])
                    return(False,'elevation not str')
            else:
                return(False,'elevation not present')
            if('longitude' in r['current_observation']['observation_location'].keys()):
                place_holder_variable = True
                if(isinstance(r['current_observation']['observation_location']['longitude'],str)):
                    place_holder_variable = True
                else:
                    print(r['current_observation']['observation_location']['longitude'])
                    return(False,'longitude not str')
            else:
                return(False,'longitude not present')
        else:
            return(False,'observation_location not present')
        if('temp_c' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['temp_c'],float)):
                place_holder_variable = True
            else:
                print(r['current_observation']['temp_c'])
                return(False,'temp_c not float')
        else:
            return(False,'temp_c not present')
        if('wind_gust_mph' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['wind_gust_mph'],int)):
                place_holder_variable = True
            else:
                print(r['current_observation']['wind_gust_mph'])
                return(False,'wind_gust_mph not int')
        else:
            return(False,'wind_gust_mph not present')
        if('history_url' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['history_url'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['history_url'])
                return(False,'history_url not str')
        else:
            return(False,'history_url not present')
        if('temp_f' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['temp_f'],float)):
                place_holder_variable = True
            else:
                print(r['current_observation']['temp_f'])
                return(False,'temp_f not float')
        else:
            return(False,'temp_f not present')
        if('precip_today_in' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['precip_today_in'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['precip_today_in'])
                return(False,'precip_today_in not str')
        else:
            return(False,'precip_today_in not present')
        if('observation_epoch' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['observation_epoch'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['observation_epoch'])
                return(False,'observation_epoch not str')
        else:
            return(False,'observation_epoch not present')
        if('local_tz_short' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['local_tz_short'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['local_tz_short'])
                return(False,'local_tz_short not str')
        else:
            return(False,'local_tz_short not present')
        if('wind_dir' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['wind_dir'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['wind_dir'])
                return(False,'wind_dir not str')
        else:
            return(False,'wind_dir not present')
        if('visibility_mi' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['visibility_mi'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['visibility_mi'])
                return(False,'visibility_mi not str')
        else:
            return(False,'visibility_mi not present')
        if('heat_index_f' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['heat_index_f'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['heat_index_f'])
                return(False,'heat_index_f not str')
        else:
            return(False,'heat_index_f not present')
        if('windchill_c' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['windchill_c'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['windchill_c'])
                return(False,'windchill_c not str')
        else:
            return(False,'windchill_c not present')
        if('pressure_in' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['pressure_in'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['pressure_in'])
                return(False,'pressure_in not str')
        else:
            return(False,'pressure_in not present')
        if('image' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['image'],dict)):
                place_holder_variable = True
            else:
                print(r['current_observation']['image'])
                return(False,'image not dict')
            if('link' in r['current_observation']['image'].keys()):
                place_holder_variable = True
                if(isinstance(r['current_observation']['image']['link'],str)):
                    place_holder_variable = True
                else:
                    print(r['current_observation']['image']['link'])
                    return(False,'link not str')
            else:
                return(False,'link not present')
            if('url' in r['current_observation']['image'].keys()):
                place_holder_variable = True
                if(isinstance(r['current_observation']['image']['url'],str)):
                    place_holder_variable = True
                else:
                    print(r['current_observation']['image']['url'])
                    return(False,'url not str')
            else:
                return(False,'url not present')
            if('title' in r['current_observation']['image'].keys()):
                place_holder_variable = True
                if(isinstance(r['current_observation']['image']['title'],str)):
                    place_holder_variable = True
                else:
                    print(r['current_observation']['image']['title'])
                    return(False,'title not str')
            else:
                return(False,'title not present')
        else:
            return(False,'image not present')
        if('feelslike_string' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['feelslike_string'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['feelslike_string'])
                return(False,'feelslike_string not str')
        else:
            return(False,'feelslike_string not present')
        if('precip_1hr_in' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['precip_1hr_in'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['precip_1hr_in'])
                return(False,'precip_1hr_in not str')
        else:
            return(False,'precip_1hr_in not present')
        if('relative_humidity' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['relative_humidity'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['relative_humidity'])
                return(False,'relative_humidity not str')
        else:
            return(False,'relative_humidity not present')
        if('forecast_url' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['forecast_url'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['forecast_url'])
                return(False,'forecast_url not str')
        else:
            return(False,'forecast_url not present')
        if('temperature_string' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['temperature_string'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['temperature_string'])
                return(False,'temperature_string not str')
        else:
            return(False,'temperature_string not present')
        if('windchill_string' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['windchill_string'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['windchill_string'])
                return(False,'windchill_string not str')
        else:
            return(False,'windchill_string not present')
        if('precip_today_metric' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['precip_today_metric'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['precip_today_metric'])
                return(False,'precip_today_metric not str')
        else:
            return(False,'precip_today_metric not present')
        if('wind_gust_kph' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['wind_gust_kph'],int)):
                place_holder_variable = True
            else:
                print(r['current_observation']['wind_gust_kph'])
                return(False,'wind_gust_kph not int')
        else:
            return(False,'wind_gust_kph not present')
        if('dewpoint_string' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['dewpoint_string'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['dewpoint_string'])
                return(False,'dewpoint_string not str')
        else:
            return(False,'dewpoint_string not present')
        if('precip_1hr_metric' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['precip_1hr_metric'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['precip_1hr_metric'])
                return(False,'precip_1hr_metric not str')
        else:
            return(False,'precip_1hr_metric not present')
        if('feelslike_f' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['feelslike_f'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['feelslike_f'])
                return(False,'feelslike_f not str')
        else:
            return(False,'feelslike_f not present')
        if('local_time_rfc822' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['local_time_rfc822'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['local_time_rfc822'])
                return(False,'local_time_rfc822 not str')
        else:
            return(False,'local_time_rfc822 not present')
        if('pressure_mb' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['pressure_mb'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['pressure_mb'])
                return(False,'pressure_mb not str')
        else:
            return(False,'pressure_mb not present')
        if('UV' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['UV'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['UV'])
                return(False,'UV not str')
        else:
            return(False,'UV not present')
        if('estimated' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['estimated'],dict)):
                place_holder_variable = True
            else:
                print(r['current_observation']['estimated'])
                return(False,'estimated not dict')
        else:
            return(False,'estimated not present')
        if('visibility_km' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['visibility_km'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['visibility_km'])
                return(False,'visibility_km not str')
        else:
            return(False,'visibility_km not present')
        if('nowcast' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['nowcast'],str)):
                place_holder_variable = True
            else:
                print(r['current_observation']['nowcast'])
                return(False,'nowcast not str')
        else:
            return(False,'nowcast not present')
        if('wind_mph' in r['current_observation'].keys()):
            place_holder_variable = True
            if(isinstance(r['current_observation']['wind_mph'],float)):
                place_holder_variable = True
            else:
                print(r['current_observation']['wind_mph'])
                return(False,'wind_mph not float')
        else:
            return(False,'wind_mph not present')
    else:
        return(False,'current_observation not present')
    return(True,'no errors')
    