#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports
import sendemail
email_file = workspace_environ+'/serverscripts/globals/emails'
weather_underground_file = workspace_environ+'/serverscripts/globals/weather_underground_access.txt'
import jsonlib_python3
import requests
import smtplib
import weather_underground_clean
import automation_test
from email.mime.text import MIMEText
weather_api_text = open(weather_underground_file,'r')
weather_api_string = weather_api_text.read()

pws = ['KNVBOULD17']  


def weather_underground_call_all():
    for i in range(0,len(pws)):
        try:
            #gets json from url
            weather_underground_url = 'http://api.wunderground.com/api/%s/conditions/q/pws:%s.json'%(weather_api_string.strip("\n"),pws[i])
            r = requests.get(weather_underground_url)
            r = r.json()
            r = {'current_observation': {'pressure_trend': '0', 'windchill_c': 'NA', 'heat_index_string': 'NA', 'windchill_string': 'NA', 'local_tz_offset': '-0700', 'visibility_mi': '10.0', 'icon_url': 'http://icons.wxug.com/i/c/k/nt_partlycloudy.gif', 'pressure_in': '29.99', 'precip_today_in': '', 'observation_time': 'Last Updated on July 24, 5:36 PM PDT', 'ob_url': 'http://www.wunderground.com/cgi-bin/findweather/getForecast?query=36.091877,-115.185905', 'heat_index_c': 'NA', 'precip_today_metric': '--', 'precip_1hr_metric': ' 0', 'dewpoint_string': '57 F (14 C)', 'wind_kph': 0, 'relative_humidity': '54%', 'wind_degrees': -9999, 'temp_f': 74.3, 'image': {'link': 'http://www.wunderground.com', 'title': 'Weather Underground', 'url': 'http://icons.wxug.com/graphics/wu2/logo_130x80.png'}, 'wind_string': 'Calm', 'windchill_f': 'NA', 'UV': '0', 'visibility_km': '16.1', 'estimated': {}, 'precip_1hr_string': '-999.00 in ( 0 mm)', 'observation_location': {'country_iso3166': 'US', 'state': 'Nevada', 'latitude': '36.091877', 'elevation': '0 ft', 'full': 'Las Vegas, Boulder Junction, Nevada', 'longitude': '-115.185905', 'city': 'Las Vegas, Boulder Junction', 'country': 'US'}, 'local_epoch': '1501061230', 'feelslike_f': '74.3', 'feelslike_string': '74.3 F (23.5 C)', 'display_location': {'country_iso3166': 'US', 'state': 'NV', 'longitude': '-115.185905', 'country': 'US', 'zip': '89118', 'elevation': '716.0', 'latitude': '36.091877', 'wmo': '99999', 'magic': '1', 'state_name': 'Nevada', 'full': 'Las Vegas, NV', 'city': 'Las Vegas'}, 'precip_today_string': ' in ( mm)', 'local_tz_short': 'PDT', 'weather': 'Scattered Clouds', 'temperature_string': '74.3 F (23.5 C)', 'feelslike_c': '23.5', 'wind_mph': -9999.0, 'forecast_url': 'http://www.wunderground.com/US/NV/Las_Vegas.html', 'local_tz_long': 'America/Los_Angeles', 'local_time_rfc822': 'Wed, 26 Jul 2017 02:27:10 -0700', 'station_id': 'KNVBOULD17', 'solarradiation': '--', 'history_url': 'http://www.wunderground.com/weatherstation/WXDailyHistory.asp?ID=KNVBOULD17', 'icon': 'partlycloudy', 'precip_1hr_in': '-999.00', 'dewpoint_c': 14, 'wind_gust_kph': 0, 'wind_gust_mph': 0, 'observation_epoch': '1500943013', 'pressure_mb': '1015', 'dewpoint_f': 57, 'temp_c': 23.5, 'wind_dir': 'North', 'heat_index_f': 'NA', 'observation_time_rfc822': 'Mon, 24 Jul 2017 17:36:53 -0700', 'nowcast': ''}, 'response': {'features': {'conditions': 1}, 'termsofService': 'http://www.wunderground.com/weather/api/d/terms.html', 'version': '0.1'}}
            #print(r)
            #automation_test.automate_clean(r,"r")
            #print(weather_underground_clean.weather_underground_clean(r)[1])
            print(automation_test.clean(r))
        except requests.exceptions.ConnectionError:
            r = universal_imports.json.loads('{"weather_underground_error" : "requests.exceptions.ConnectionError"}') 
            print(r)
weather_underground_call_all()

