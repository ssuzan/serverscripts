#!/usr/bin/python
# -*- coding: utf-8 -*-
from itertools import groupby

import sys
import os
import requests
from bs4 import BeautifulSoup as bs
import traceback

sys.path.append(os.environ['WORKSPACE'] + '/serverscripts/globals')

import universal_imports
import sendemail

email_file = os.environ['WORKSPACE'] + '/serverscripts/globals/emails'


def fetch(count, url):
    r = requests.get(url)
    # Emails if we have an error

    if r.status_code != 200:
        text = open(email_file, 'r')
        email_string = text.read()
        email_json = universal_imports.json.loads(email_string)
        prefix = "Twitter count " + str(count) + "_"
        w = 'twitter_tampa_fetch failed to fetch with url: ' + url + ' with ' + str(count) + ' tweets in memory' + \
            '. request returned code: ' + r.status_code + '. '

        message_text = prefix + w  # ... This is a report in form of string
        sendemail.sendemail(from_addr=email_json["email"], to_addr_list=email_json["send_emails"], cc_addr_list=[''],
                            subject='twitter_fetch_warning_report', message=message_text, login=email_json["email"],
                            password=email_json["password"])

        return [False, 'twitter_tampa_fetch failed to fetch with ' + str(count) + ' tweets in memory, '
                                                                                  'for the folowing url ' + url]
    return r
