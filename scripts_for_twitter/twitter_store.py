# <NAME>
# <EMAIL> <PHONE>
# store file

import sys
import os.path

sys.path.append(os.environ['WORKSPACE'] + '/serverscripts/globals')
import universal_imports
import sendemail
import traceback
# import push_to_fifo
# import push_to_fifo_dev

from collections import OrderedDict
from email.mime.text import MIMEText

email_file = os.environ['WORKSPACE'] + '/serverscripts/globals/emails'
# email_file = 'res/emails.json'
save_path = os.environ['WORKSPACE'] + '/DB_backups/data_from_twitter_tampa'
type_name = 'twitter_live_tampa'


def twitter_store_original(raw_data, source, count, url, dump_to_output, is_prod, is_dev, prod_fifo_ip):
    # saves orignal file:
    for tweet in raw_data:
        print('New Tweets')
        print(tweet['tweet_date'])
        print(tweet['tweet_id'])
        print(tweet['tweet_text'])
        print(tweet['inner_message'])
        bean = {
            'type': type_name,
            'suspicious': False,
            'date-time': universal_imports.datetime.utcnow().strftime('%d_%m_%Y_%H_%M_%S UTC'),
            'data': {
                'source': source,
                'tweet_post_time': tweet['tweet_date'],
                'tweet_item_id': tweet['tweet_id'],
                'tweet_text': tweet['tweet_text'],
                'original_post': tweet['inner_message']

            },
            'error': False
        }
        # File name -short url + tweet id
        now = str(universal_imports.datetime.utcnow())
        now = now.split(" ")
        now[0] = now[0].split("-")
        now[1] = now[1].split(":")
        filename = type_name + "_" + now[0][2] + "_" + now[0][1] + "_" + now[0][0] + "_" + now[1][0] + "_" + \
                   now[1][1] + "_" + now[1][2].replace(".", "_")
        # Store original tweets
        if (dump_to_output == "FILE") or (dump_to_output == "ALL"):
            try:
                with open(save_path + "/database_original/" + filename + '.json', 'w') as outfile:
                    universal_imports.json.dump(bean, outfile)
                print('stored to: ' + save_path + "/database_original/" + filename + '.json')
            except:
                error_message = traceback.format_exc()
                print(error_message)
                text = open(email_file, 'r')
                email_string = text.read()
                email_json = universal_imports.json.loads(email_string)
                prefix = "Twitter count " + str(count) + "_"
                w = 'twitter_store failed to fetch url: ' + url + '. TraceBack : ' + error_message + '.'
                message_text = prefix + w  # ... This is a report in form of string
                sendemail.sendemail(from_addr=email_json["email"], to_addr_list=email_json["send_emails"],
                                    cc_addr_list=[''],
                                    subject='twitter_tampa_fetch warning report', message=message_text,
                                    login=email_json["email"],
                                    password=email_json["password"])

                return [False, 'twitter_tampa_store failed to store file']

            if (dump_to_output == "FIFO") or (dump_to_output == "ALL"):
                try:
                    if (is_prod):
                        push_to_fifo.push_to_fifo(data_out, prod_fifo_ip)
                except:
                    text = open(email_file, 'r')
                    email_string = text.read()
                    email_json = universal_imports.json.loads(email_string)
                    sendemail.sendemail(from_addr=email_json['email'], to_addr_list=email_json['send_emails'],
                                        cc_addr_list=[''], subject='error in twitter_store',
                                        message='fifo error in twitter_store', login=email_json['email'],
                                        password=email_json['password'])
                    err_report.append('fifo error in twitter_store')
                    return [False, 'twitter_tampa_store failed to send to prod_Fifo']
                    # push to development FIFO
                try:
                    if (is_dev):
                        push_to_fifo_dev.push_to_fifo_dev(data_out)
                except:
                    text = open(email_file, 'r')
                    email_string = text.read()
                    email_json = universal_imports.json.loads(email_string)
                    sendemail.sendemail(from_addr=email_json['email'], to_addr_list=email_json['send_emails'],
                                        cc_addr_list=[''], subject='error in twitter_store',
                                        message='fifo error in twitter_store', login=email_json['email'],
                                        password=email_json['password'])
                    err_report.append('fifo error in twitter_store')
                    return [False, 'twitter_tampa_store failed to send to dev_Fifo']

    return True
