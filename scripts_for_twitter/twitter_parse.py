from itertools import groupby

import sys
import os
from bs4 import BeautifulSoup as bs
import traceback
sys.path.append(os.environ['WORKSPACE'] + '/serverscripts/globals')
import universal_imports
import sendemail

email_file = os.environ['WORKSPACE'] + '/serverscripts/globals/emails'


def parse(r, count, stored_tweets):
    # count = int(count)
    try:
        bs_data = bs(r.content, 'html.parser')
        tweets = bs_data.select('.js-original-tweet')
        tweets_array = []
        rng = 2 * count
        ids = []
        for i in range(0, rng):
            tweet_id = tweets[i].attrs['data-item-id']

            tweet_inner_id = tweets[i].find(class_='QuoteTweet-innerContainer u-cf js-permalink js-media-container')

            if tweet_inner_id is None:
                tweet_inner_id = 'None'
                inner_message = 'None'
            else:
                tweet_inner_id = tweet_inner_id['data-item-id']
                inner_message = tweets[i].find(class_='QuoteTweet-text tweet-text u-dir js-ellipsis').get_text()

            date = tweets[i].find(class_='time').find('span')['data-time']
            tweet_text = ([x.get_text() for x in tweets[i].find(class_='js-tweet-text-container').select('p')])
            # tweet_text = ([x.get_text() for x in tweets[i].select('p')])
            # print(tweet_id)
            # print(url)
            # print(tweet_inner_id)
            # print(date)
            # print(tweet_text)
            # print(inner_message)
            item_structure = {
                'timestamp': date,
                'tweet_id': tweet_id,
                'tweet_inner_id': tweet_inner_id,
                'tweet_date': universal_imports.datetime.utcfromtimestamp(int(date)).strftime('%Y_%m_%d_%H_%M_%S UTC'),
                'tweet_text': tweet_text,
                'inner_message': inner_message
            }
            ids.append(tweet_id)
            tweets_array.append(item_structure)
        tweets_array = tweets_array[0:count]
        ids = ids[0:count]

        # Remove if we have in the inner massage another tweet
        index_to_remove = []
        for tweet in tweets_array:
            if tweet['tweet_inner_id'] != 'None':
                if tweet['tweet_inner_id'] in ids:
                    index_to_remove.append(ids.index(tweet['tweet_inner_id']))

        # if we have a few updated messages with the same original message will be throw an exception because into Index_
        # to_remove will be a few same indices . SOLUTION : just keep unique elements in this array

        index_to_remove.sort(reverse=True)
        index_to_remove = [x for x, _ in groupby(index_to_remove)]
        for i in range(len(index_to_remove)):
            tweets_array.pop(index_to_remove[i])

        # Comparing with tweets in memory
        tweets_to_add = []
        if stored_tweets:
            last_post_time = int(stored_tweets[0]['timestamp'])
            for tweet in tweets_array:
                if int(tweet['timestamp']) > last_post_time:
                    tweets_to_add.append(tweet)
                else:
                    break
            if tweets_to_add is None:
                return
            new_tweets = tweets_to_add[:]
            # Add to tweets in memory new tweeters
            stored_tweets_array = stored_tweets
            tweets_to_add.extend(stored_tweets_array)

            # delete all elements after index(count)
            while len(tweets_to_add) > count:
                tweets_to_add.pop()
            stored_tweets = tweets_to_add
            return [stored_tweets, new_tweets]

        else:
            stored_tweets = tweets_array
            return [stored_tweets, '']
    except:
        error_message = traceback.format_exc()
        print(error_message)
        text = open(email_file, 'r')
        email_string = text.read()
        email_json = universal_imports.json.loads(email_string)
        prefix = "Twitter count " + str(count) + "_"
        w = 'twitter_store failed to fetch url: ' + url + '. TraceBack : ' + error_message + '.'
        message_text = prefix + w  # ... This is a report in form of string
        sendemail.sendemail(from_addr=email_json["email"], to_addr_list=email_json["send_emails"],
                            cc_addr_list=[''],
                            subject='twitter_tampa_fetch warning report', message=message_text,
                            login=email_json["email"],
                            password=email_json["password"])

        return [False, 'twitter_tampa_store failed to store file']
