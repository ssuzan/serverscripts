import twitter_fetch
import twitter_store
import twitter_parse
import sys
import os
from apscheduler.schedulers.blocking import BlockingScheduler
sys.path.append(os.environ['WORKSPACE'] + '/serverscripts/globals')
import universal_imports

dump_to_output = 'FILE'
is_prod = True
is_dev = False
prod_fifo_ip = '10.0.3.44'


with open('res/urls_for_requests.json', 'r') as reader:
    urls = universal_imports.json.load(reader)
# Structure of tweet in memory
stored_tweets = {x['url']: [] for x in urls['urls']}


def scheduled_call():
    print(
        'twitter_call_all.scheduled_call() started successfuly! time is: ' + universal_imports.datetime.utcnow().strftime(
            "%d-%m-%Y-%H-%M-%S") + ' UTC')
    sched = BlockingScheduler(timezone='UTC', coalesce=True)
    sched.add_job(twitter_call_all, 'interval', id='job_id1', seconds=120, coalesce=True)
    sched.start()


# Main Method ,call all child method by order in twitter fetch
def twitter_call_all():
    for url in urls['urls']:
        if url['count'] != '':
            count = url['count']
        else:
            count = 10

        r = twitter_fetch.fetch(count, url['url'])
        tweets = twitter_parse.parse(r, count, stored_tweets[url['url']])

        if tweets[1] == '':
            print(len(stored_tweets[url['url']]), url['url'])
            stored_tweets[url['url']] = tweets[0]
        else:
            print(len(stored_tweets[url['url']]), url['url'])
            stored_tweets[url['url']] = tweets[0]
            twitter_store.twitter_store_original(tweets[1], url['source'], count, url['url'],
                                                 dump_to_output, is_prod, is_dev, prod_fifo_ip)


if __name__ == '__main__':
        scheduled_call()


# Loop here. Done!
# Counts move to file urls.json. Done!
# source:short url add field. Done!
# remove urls for output. Done!
# instead delay put "crontask"
# call all : fetch, parse, store for each url. Done!
# Create lv script
