import sys
#path depends on computer
sys.path.append('/home/esgellman/waycare_bitbucket/serverscripts/globals')
import universal_imports
import sendemail
import requests
import xmltodict
import clean_road_incidents
email_file = '/home/esgellman/waycare_bitbucket/serverscripts/globals/emails'
save_path = ""
download_url = 'ftp://bugatti.nvfast.org/RealtimeXML/'
import smtplib
from email.mime.text import MIMEText
#send email if error

def ensure_constant_formatting(xml_in):
    try:
        x = xml_in['soap:Envelope']['soap:Body']['GetCurrentFASTIncidentsResponse']['GetCurrentFASTIncidentsResult']['Incident'][0]
    except:
        temp_storage_of_incident = xml_in['soap:Envelope']['soap:Body']['GetCurrentFASTIncidentsResponse']['GetCurrentFASTIncidentsResult']['Incident']
        xml_in['soap:Envelope']['soap:Body']['GetCurrentFASTIncidentsResponse']['GetCurrentFASTIncidentsResult']['Incident'] = []
        xml_in['soap:Envelope']['soap:Body']['GetCurrentFASTIncidentsResponse']['GetCurrentFASTIncidentsResult']['Incident'] = [temp_storage_of_incident]  
    return xml_in

def call_all():
    Host = 'bugatti.nvfast.org'
    url = "http://bugatti.nvfast.org/GetFASTIncidentWebService.asmx"
    headers = {'Content-Type': 'application/soap+xml; charset=utf-8'}
    body = """<?xml version="1.0" encoding="utf-8"?>
        <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
            <soap12:Body>
                <GetCurrentFASTIncidents xmlns="http://bugatti.nvfast.org/" />
            </soap12:Body>
        </soap12:Envelope>"""
    curr_t = universal_imports.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
    response = requests.post(url,data=body,headers=headers)
    with open(save_path+'fms_road_incidents'+("_"+curr_t.split(" ")[0]+"_"+curr_t.split(" ")[1])+'.xml','w') as f:
        f.write(response.text)         
    doc = xmltodict.parse(response.content) 
    #if road incidents is true download, if false and 'no current incidents' print 'no current incidents', otherwise send error email
    if (clean_road_incidents.clean_road_incidents(doc)[0] == True):
        doc = ensure_constant_formatting(doc)
        print(curr_t.split(" "))
        doc = universal_imports.json.dumps(doc, sort_keys=True, indent=4, separators=(',', ': '))
        lastFiletxt = open(save_path+'fms_road_incidents'+("_"+curr_t.split(" ")[0]+"_"+curr_t.split(" ")[1])+'.json','w')
        lastFiletxt.write(doc)  
        lastFiletxt.close()   
    elif (clean_road_incidents.clean_road_incidents(doc)[0] == False and clean_road_incidents.clean_road_incidents(doc)[1] == "no current incidents"):
        print("no current incidents")
    else:
        text = open(email_file,'r')
        email_string = text.read()
        email_json = universal_imports.json.loads(email_string) 
        text.close()
        sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'error- road incidents', message = 'error- road incidents',login = email_json["email"], password = email_json["password"])    
  
if __name__ == '__main__':
    call_all()
