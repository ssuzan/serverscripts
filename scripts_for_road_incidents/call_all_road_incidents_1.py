import sys
sys.path.append('/home/esgellman/waycare_bitbucket/serverscripts/globals')
import universal_imports
import requests

#downloaded_xml_path = "repository_xml"
#cleaned_json_path = "repository_json"
#tmp
download_url = 'ftp://bugatti.nvfast.org/RealtimeXML/'

def call_all():
    Host = 'bugatti.nvfast.org'
    url = "http://bugatti.nvfast.org/GetFASTIncidentWebService.asmx?WSDL"
    headers = {'Content-Type': 'application/soap+xml; charset=utf-8'}
    body = """<?xml version="1.0" encoding="utf-8"?>
    <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
    <soap12:Body>
    <GetCurrentFASTIncidents xmlns="http://bugatti.nvfast.org/" />
    </soap12:Body>
    </soap12:Envelope>"""
    response = requests.post(url,data=body,headers=headers)
    print (response.content)    

#arguement should be -all    
if __name__ == '__main__':
    call_all()
