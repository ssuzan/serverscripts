import sys
#path depends on computer
sys.path.append('/home/esgellman/waycare_bitbucket/serverscripts/globals')
import universal_imports

def clean_road_incidents_midsection(midsection):
    print("crim1")
    try:
        x = midsection[0]
    except:
        temp_storage_of_incident = midsection
        midsection = []
        midsection = [temp_storage_of_incident]
        return (clean_road_incidents_midsection(midsection))
    for i in range(0, len(midsection)):
        print(midsection[i])
        if (str(midsection[i].keys()) == "odict_keys(['IncidentNo', 'StartTime', 'Duration', 'BlockedLanes', 'BlockageDescription', 'Description', 'IncidentType', 'Memo', 'TowTruckComeTimeStamp', 'LaneClearedTimeStamp', 'Longitude', 'Latitude', 'RoadwayID', 'SegmentID', 'Route', 'Location'])"):
            try:
                int(midsection[i]['IncidentNo'])
                print([i,'IncidentNo'])
            except:
                print([False, i,'IncidentNo'])
                return [False, "error"]
            try:
                int(midsection[i]['Duration'])
                print([i, 'Duration'])
            except:
                print([False, i, 'Duration'])
                return [False, "error"]     
            try:
                int(midsection[i]['BlockedLanes'])
                print([i, 'BlockedLanes'])
            except:
                print([False, i, 'BlockedLanes'])
                return [False, "error"]      
            if (isinstance(midsection[i]['BlockageDescription'], str)):
                print([i, 'BlockageDescription'])
            else:
                return [False, "error"]  
            if (isinstance(midsection[i]['Description'], str)):
                print([i,'Description'])
            else:
                return [False, "error"]   
            if (isinstance(midsection[i]['IncidentType'], str)):
                print([i,'IncidentType'])
            else:
                return [False, "error"]  
            print(midsection[i]['Memo'])
            if (isinstance(midsection[i]['Memo'], str)):
                print([i,'Memo'])
            elif (midsection[i]['Memo'] == None):
                print ([None,i,'Memo'])
            else:
                return [False, "error"]   
            if (isinstance(midsection[i]['TowTruckComeTimeStamp'], str)):
                print(True)
            else:
                return [False, "error"]
            if (isinstance(midsection[i]['LaneClearedTimeStamp'], str)):
                print([i, 'LaneClearedTimeStamp'])
            else:
                return [False, "error"]                                            
            try:
                float(midsection[i]['Longitude'])
                print([i,'Longitude'])
            except:
                print([False, i,'Longitude'])
                return [False, "error"]
            try:
                float(midsection[i]['Latitude'])
                print([i,'Latitude'])
            except:
                print([False, i,'Latitude'])
                return [False, "error"]   
            try:
                int(midsection[i]['RoadwayID'])
                print([i,'RoadwayID'])
            except:
                print([False, i,'RoadwayID'])
                return [False, "error"]   
            try:
                int(midsection[i]['SegmentID'])
                print([i,'SegmentID'])
            except:
                print([False, i,'SegmentID'])
                return [False, "error"]  
            if (isinstance(midsection[i]['Route'], str)):
                print([i, 'Route'])
            else:
                return [False, "error"]   
            if (isinstance(midsection[i]['Location'], str)):
                print([i,'Location'])
            else:
                return [False, "error"] 
        else:
            return [False, "error"]    
    return [True, "no error"]
def clean_road_incidents(incidents_xml):
    if (str(incidents_xml.keys()) == "odict_keys(['soap:Envelope'])"):
        print(True)
        if (str(incidents_xml['soap:Envelope'].keys()) == "odict_keys(['@xmlns:soap', '@xmlns:xsi', '@xmlns:xsd', 'soap:Body'])"):
            print('soap:Envelope')
            if (incidents_xml['soap:Envelope']['@xmlns:soap'].startswith("http://www.w3.org/")):
                print('@xmlns:soap')
            else:
                return [False, "error"]
            if (incidents_xml['soap:Envelope']['@xmlns:xsi'].startswith("http://www.w3.org/")):
                print('@xmlns:xsi')
            else:
                return [False, "error"]   
            if (incidents_xml['soap:Envelope']['@xmlns:xsd'].startswith("http://www.w3.org/")):
                print('@xmlns:xsd')
            else:
                return [False, "error"]   
            if (str(incidents_xml['soap:Envelope']['soap:Body'].keys()) == "odict_keys(['GetCurrentFASTIncidentsResponse'])"):
                print('soap:Body')
                if(str(incidents_xml['soap:Envelope']['soap:Body']['GetCurrentFASTIncidentsResponse'].keys()) == "odict_keys(['@xmlns', 'GetCurrentFASTIncidentsResult'])"):
                    print('GetCurrentFASTIncidentsResponse')
                    if(incidents_xml['soap:Envelope']['soap:Body']['GetCurrentFASTIncidentsResponse']['@xmlns'] == "http://bugatti.nvfast.org/"):
                        print('@xmlns')
                        if(incidents_xml['soap:Envelope']['soap:Body']['GetCurrentFASTIncidentsResponse']['GetCurrentFASTIncidentsResult'] == None):
                            return [False, "no current incidents"]
                        elif(str(incidents_xml['soap:Envelope']['soap:Body']['GetCurrentFASTIncidentsResponse']['GetCurrentFASTIncidentsResult'].keys()) == "odict_keys(['Incident'])"):
                            print('GetCurrentFASTIncidentsResult')
                            return clean_road_incidents_midsection(incidents_xml['soap:Envelope']['soap:Body']['GetCurrentFASTIncidentsResponse']['GetCurrentFASTIncidentsResult']['Incident'])

                        else:
                            return [False, "error"]
                    else:
                        return [False, "error"]
                else:
                    print(incidents_xml['soap:Envelope']['soap:Body']['GetCurrentFASTIncidentsResponse'].keys())
                    return [False, "error"]
                                
            else:
                print(incidents_xml['soap:Envelope']['soap:Body'].keys())
                return [False, "error"]
        else:
            return [False, "error"]
    else:
        return [False, "error"]
    
    #print(incidents_xml['soap:Envelope']['soap:Body']['GetIncidentsOfLastDayResponse']['GetCurrentFASTIncidentsResult']['Incident'][0])
    #print(incidents_xml['soap:Envelope']['soap:Body']['GetIncidentsOfLastDayResponse']['GetCurrentFASTIncidentsResult']['Incident'][0].keys())
    
    

