#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
#sudo easy_install3 wget
import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts/globals')
import universal_imports
import sendemail
import push_to_fifo
from email.mime.text import MIMEText
email_file = os.environ['WORKSPACE']+'/serverscripts/globals/emails'
    
def store(original_sensor_data,cleaned_sensor_data,is_detector_report,save_path,dump_output_to,type_name):
    #print(original_sensor_data)
    now = str(universal_imports.datetime.utcnow())
    now = now.split(" ")
    now[0] = now[0].split("-")
    now[1] = now[1].split(":")
    curr_t = universal_imports.datetime.utcnow().strftime("%d-%m-%Y %H:%M:%S")
    filename = type_name+"_"+now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1]#+"_"+now[1][2].replace(".","_") 
    #saves file
    fullFiletxt = open(save_path+"/database_original/"+filename+'.json', 'w')
    fullFiletxt.write(universal_imports.json.dumps(original_sensor_data, sort_keys=True, indent=4, separators=(',', ': ')))  
    if (is_detector_report == True):    
        #saves individual files
        if (dump_output_to == "FILE"):
            for i in range(0,len(cleaned_sensor_data['DetectorData']['collection-period']['collection-period-item']['detector-reports']['detector-report'])):
                new_store_json = {'type':type_name,'date_time':str(curr_t).replace("-","_")+' UTC','suspicious':False,'data':cleaned_sensor_data['DetectorData']['collection-period']['collection-period-item']['detector-reports']['detector-report'][i]}                        
                filename = type_name+"_"+str(now[0][2])+"_"+str(now[0][1])+"_"+str(now[0][0])+"_"+str(now[1][0])+"_"+str(now[1][1])+"_detector_id_"+str(cleaned_sensor_data['DetectorData']['collection-period']['collection-period-item']['detector-reports']['detector-report'][i]['detector-id'])                
                incidentFiletxt = open(save_path+"/database/"+filename+'.json', 'w')
                incidentFiletxt.write(universal_imports.json.dumps(new_store_json, sort_keys=True, indent=4, separators=(',', ': ')))  
        #sends file to FIFO
        elif (dump_output_to == "FIFO"):
            for i in range(0,len(cleaned_sensor_data['DetectorData']['collection-period']['collection-period-item']['detector-reports']['detector-report'])):      
                new_store_json = {'type':type_name,'date_time':str(curr_t).replace("-","_")+' UTC','suspicious':False,'data':cleaned_sensor_data['DetectorData']['collection-period']['collection-period-item']['detector-reports']['detector-report'][i]}                        
                try:
                    push_to_fifo.push_to_fifo(new_store_json, "sensors_queue")         
                except:
                    text = open(email_file,'r')
                    email_string = text.read()
                    email_json = universal_imports.json.loads(email_string)                  
                    sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'error', message = 'fifo error in store_sensors', login = email_json['email'], password = email_json['password'])                  