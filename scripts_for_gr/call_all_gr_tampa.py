# Yaniv Costica
# Yaniv Costica 972524848989
# main file Tampa
# required installations:
import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts_production/globals')
import universal_imports
import sendemail
import push_to_fifo
from email.mime.text import MIMEText
from datetime import datetime
import fetch_gr
import clean_gr
import store_gr

dire = os.path.dirname(os.path.abspath(__file__))
save_path = os.environ['WORKSPACE']+'/DB_backups/data_from_gr'
original_backup_path = os.environ['WORKSPACE']+'/DB_backups/data_from_gr/database_original/'
email_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/emails'
incoming_files_path = os.environ['WORKSPACE']+'/DB_backups/data_from_gr/transactions/'

boundaries_file = os.path.join(dire, 'boundaries/tampa_boundaries.json')
dump_output_to = "ALL"
prod_fifo_ip = '10.0.3.58'
type_name = 'gr_alerts_live_tampa'

def call_all_gr():
    now = str(datetime.utcnow())
    now = now.split(" ")
    now[0] = now[0].split("-")
    now[1] = now[1].split(":")
    now_sec = now[1][2].split(".")
    curr_t = now[0][2]+"_"+now[0][1]+"_"+now[0][0]+" "+now[1][0]+":"+now[1][1]+":"+now_sec[0]
    print('gr_call_all_tampa.py started! time is: '+curr_t+' UTC')
    #filename = "data_from_gr_"+now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1]     

    text = open(email_file,'r')
    email_string = text.read()
    email_json = universal_imports.json.loads(email_string)

    #backing up original
    incoming_files = store_gr.store_original(incoming_files_path,original_backup_path , dump_output_to, False)
    #print(incoming_files)

    #fetching data from local file
    fetched = fetch_gr.fetch(original_backup_path, type_name, curr_t, incoming_files)
    if (fetched[0] == True):
        result = fetched[1]
        #Cleaning result:
        clean_res = clean_gr.clean(fetched[1], boundaries_file)
        if clean_res:
            #Storing result
            store_gr.store(curr_t,clean_res, save_path, dump_output_to, prod_fifo_ip, False)
    else:
        print("error in call_all_gr_tampa.py fetch")
        sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'gr_call_all_tampa fetch error', message = fetched[1], login = email_json["email"], password = email_json["password"])

    print('call_all_gr_tampa.py completed succesfuly! time is: ' + curr_t + ' UTC')


#Calling gr_call_all
call_all_gr()

'''
copy an event that created at bckp from the store_original

change:
 Name
 coordination tp tampa
 ID inside the file
'''