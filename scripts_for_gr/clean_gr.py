#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: json containing data from waze database
#output: properly formatted json containing data from gr database
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports
import sendemail
from email.mime.text import MIMEText
import json
# TODO sudo pip3 install shapely at the instance
from shapely import geometry
from shapely.geometry import Point, box, shape, LineString

email_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/emails'
text = open(email_file,'r')
email_string = text.read()
email_json = universal_imports.json.loads(email_string)


def filter_by_city_boundaries(location, boundaries_file):
    city_geo_filters = universal_imports.json.load(open(boundaries_file))
    point = Point(location['Longitude'], location['Latitude'])
    city_boundaries = city_geo_filters["location_filters"].get("city_boundaries")
    if city_boundaries:
        # (west, south, east, north)
        city_shape = box(
            city_boundaries['west'],
            city_boundaries['south'],
            city_boundaries['east'],
            city_boundaries['north']
        )
        if city_shape.contains(point):
            return True
    else:
        area_boundaries = city_geo_filters["location_filters"].get("area_boundaries")
        if area_boundaries:
            for feature in area_boundaries['features']:
                polygon = shape(feature['geometry'])
                if polygon.contains(point):
                    return True
    return False


# Get incident and boundaries file, returns [success\not (bool), incident\err_msg, in_boundaries\not (bool)]
def clean_incident(incident, boundaries_file):
    #working with the 'data' field
    incident_clean = incident['data'][0] #TODO replace
    #incident_clean = incident #TODO replace

    if('EventTime' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['EventTime'],str)):
            place_holder_variable = True
        else:
            incident_clean['EventTime'] = ''
            return(False, 'EventTime wrong type', False)
    else:
        incident_clean['EventTime'] = ''
        #return(False,'EventTime not present')
    if('EventType' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['EventType'],int)):
            place_holder_variable = True
        else:
            incident_clean['EventType'] = ''
            return(False,'EventType wrong type', False)
    else:
        incident_clean['EventType'] = ''
        #return(False,'EventType not present')
    if('EndSpeedKmh' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['EndSpeedKmh'],float)):
            place_holder_variable = True
        else:
            incident_clean['EndSpeedKmh'] = ''
            return(False,'EndSpeedKmh wrong type', False)
    else:
        incident_clean['EndSpeedKmh'] = ''
        #return(False,'EndSpeedKmh not present')
    if('EndSpeedMph' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['EndSpeedMph'],float)):
            place_holder_variable = True
        else:
            incident_clean['EndSpeedMph'] = ''
            return(False,'EndSpeedMph wrong type', False)
    else:
        incident_clean['EndSpeedMph'] = ''
        #return(False,'EndSpeedMph not present')
    if('EndTime' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['EndTime'],str)):
            place_holder_variable = True
        else:
            incident_clean['EndTime'] = ''
            return(False,'EndTime wrong type', False)
    else:
        incident_clean['EndTime'] = ''
        #return(False,'EndTime not present')
    if('MaxSpeedKmh' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['MaxSpeedKmh'],float)):
            place_holder_variable = True
        else:
            incident_clean['MaxSpeedKmh'] = ''
            return(False,'MaxSpeedKmh wrong type', False)
    else:
        incident_clean['MaxSpeedKmh'] = ''
        #return(False,'MaxSpeedKmh not present')
    if('MaxSpeedMph' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['MaxSpeedMph'],float)):
            place_holder_variable = True
        else:
            incident_clean['MaxSpeedMph'] = ''
            return(False,'MaxSpeedMph wrong type', False)
    else:
        incident_clean['MaxSpeedMph'] = ''
        #return(False,'MaxSpeedMph not present')
    if('Severity' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['Severity'],int)):
            place_holder_variable = True
        else:
            incident_clean['Severity'] = ''
            return(False,'Severity wrong type', False)
    else:
        incident_clean['Severity'] = ''
        #return(False,'Severity not present')
    if('StartSpeedKmh' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['StartSpeedKmh'],float)):
            place_holder_variable = True
        else:
            incident_clean['StartSpeedKmh'] = ''
            return(False,'StartSpeedKmh wrong type', False)
    else:
        incident_clean['StartSpeedKmh'] = ''
        #return(False,'StartSpeedKmh not present')
    if('StartSpeedMph' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['StartSpeedMph'],float)):
            place_holder_variable = True
        else:
            incident_clean['StartSpeedMph'] = ''
            return(False,'StartSpeedMph wrong type', False)
    else:
        incident_clean['StartSpeedMph'] = ''
        #return(False,'StartSpeedMph not present')
    if('StartTime' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['StartTime'],str)):
            place_holder_variable = True
        else:
            incident_clean['StartTime'] = ''
            return(False,'StartTime wrong type', False)
    else:
        incident_clean['StartTime'] = ''
        #return(False,'StartTime not present')
    if('TripId' in incident_clean.keys()):
        place_holder_variable = True
        if(isinstance(incident_clean['TripId'],int)):
            place_holder_variable = True
        else:
            incident_clean['TripId'] = ''
            return(False,'TripId wrong type', False)
    else:
        incident_clean['TripId'] = ''
        #return(False,'TripId not present')
 
      #"EndLocation": 
    if (not incident_clean['EndLocation']):
       place_holder_variable = True
       #print('EndLocation is null')
    else:
        test_inc = incident_clean['EndLocation']
        if('Heading' in test_inc.keys()):
            place_holder_variable = True
            if(isinstance(test_inc['Heading'],int)):
                place_holder_variable = True
            else:
                test_inc['Heading'] = ''
                return(False,'Heading wrong type', False)
        else:
            test_inc['Heading'] = ''
            #return(False,'Heading not present')
        if('Latitude' in test_inc.keys()):
            place_holder_variable = True
            if(isinstance(test_inc['Latitude'],float)):
                place_holder_variable = True
            else:
                test_inc['Latitude'] = ''
                return(False,'Latitude wrong type', False)
        else:
            test_inc['Latitude'] = ''
            #return(False,'Latitude not present')
        if('Longitude' in test_inc.keys()):
            place_holder_variable = True
            if(isinstance(test_inc['Longitude'],float)):
                place_holder_variable = True
            else:
                test_inc['Longitude'] = ''
                return(False,'Longitude wrong type', False)
        else:
            test_inc['Longitude'] = ''
            #return(False,'Longitude not present')
        if('SpeedKMH' in test_inc.keys()):
            place_holder_variable = True
            if(isinstance(test_inc['SpeedKMH'],float)):
                place_holder_variable = True
            else:
                test_inc['SpeedKMH'] = ''
                return(False,'SpeedKMH wrong type', False)
        else:
            test_inc['SpeedKMH'] = ''
            #return(False,'SpeedKMH not present')
        if('SpeedMPH' in test_inc.keys()):
            place_holder_variable = True
            if(isinstance(test_inc['SpeedMPH'],float)):
                place_holder_variable = True
            else:
                test_inc['SpeedMPH'] = ''
                return(False,'SpeedMPH wrong type', False)
        else:
            test_inc['SpeedMPH'] = ''
            #return(False,'SpeedMPH not present')

        incident_clean['EndLocation'] = test_inc
      #EndLocation end

      #"MaxLocation": null,
    if (not incident_clean['MaxLocation']):
       place_holder_variable = True
       #print('EndLocation is null')
    else:
        test_inc = incident_clean['MaxLocation']
        if('Heading' in test_inc.keys()):
            place_holder_variable = True
            if(isinstance(test_inc['Heading'],int)):
                place_holder_variable = True
            else:
                test_inc['Heading'] = ''
                return(False,'Heading wrong type', False)
        else:
            test_inc['Heading'] = ''
            #return(False,'Heading not present')
        if('Latitude' in test_inc.keys()):
            place_holder_variable = True
            if(isinstance(test_inc['Latitude'],float)):
                place_holder_variable = True
            else:
                test_inc['Latitude'] = ''
                return(False,'Latitude wrong type', False)
        else:
            test_inc['Latitude'] = ''
            #return(False,'Latitude not present')
        if('Longitude' in test_inc.keys()):
            place_holder_variable = True
            if(isinstance(test_inc['Longitude'],float)):
                place_holder_variable = True
            else:
                test_inc['Longitude'] = ''
                return(False,'Longitude wrong type', False)
        else:
            test_inc['Longitude'] = ''
            #return(False,'Longitude not present')
        if('SpeedKMH' in test_inc.keys()):
            place_holder_variable = True
            if(isinstance(test_inc['SpeedKMH'],float)):
                place_holder_variable = True
            else:
                test_inc['SpeedKMH'] = ''
                return(False,'SpeedKMH wrong type', False)
        else:
            test_inc['SpeedKMH'] = ''
            #return(False,'SpeedKMH not present')
        if('SpeedMPH' in test_inc.keys()):
            place_holder_variable = True
            if(isinstance(test_inc['SpeedMPH'],float)):
                place_holder_variable = True
            else:
                test_inc['SpeedMPH'] = ''
                return(False,'SpeedMPH wrong type', False)
        else:
            test_inc['SpeedMPH'] = ''
            #return(False,'SpeedMPH not present')

        incident_clean['MaxLocation'] = test_inc
      #MaxLocation end


    #"StartLocation": null,
    if (not incident_clean['StartLocation']):
       place_holder_variable = True
       #print('EndLocation is null')
    else:
        test_inc = incident_clean['StartLocation']
        if('Heading' in test_inc.keys()):
            place_holder_variable = True
            if(isinstance(test_inc['Heading'],int)):
                place_holder_variable = True
            else:
                test_inc['Heading'] = ''
                return(False,'Heading wrong type', False)
        else:
            test_inc['Heading'] = ''
            #return(False,'Heading not present')
        if('Latitude' in test_inc.keys()):
            place_holder_variable = True
            if(isinstance(test_inc['Latitude'],float)):
                place_holder_variable = True
            else:
                test_inc['Latitude'] = ''
                return(False,'Latitude wrong type', False)
        else:
            test_inc['Latitude'] = ''
            #return(False,'Latitude not present')
        if('Longitude' in test_inc.keys()):
            place_holder_variable = True
            if(isinstance(test_inc['Longitude'],float)):
                place_holder_variable = True
            else:
                test_inc['Longitude'] = ''
                return(False,'Longitude wrong type', False)
        else:
            test_inc['Longitude'] = ''
            #return(False,'Longitude not present')
        if('SpeedKMH' in test_inc.keys()):
            place_holder_variable = True
            if(isinstance(test_inc['SpeedKMH'],float)):
                place_holder_variable = True
            else:
                test_inc['SpeedKMH'] = ''
                return(False,'SpeedKMH wrong type', False)
        else:
            test_inc['SpeedKMH'] = ''
            #return(False,'SpeedKMH not present')
        if('SpeedMPH' in test_inc.keys()):
            place_holder_variable = True
            if(isinstance(test_inc['SpeedMPH'],float)):
                place_holder_variable = True
            else:
                test_inc['SpeedMPH'] = ''
                return(False,'SpeedMPH wrong type', False)
        else:
            test_inc['SpeedMPH'] = ''
            #return(False,'SpeedMPH not present')

        incident_clean['StartLocation'] = test_inc
      #StartLocation end

    #"Location": null,
    if (not incident_clean['Location']):
       place_holder_variable = True
       #print('EndLocation is null')
    else:
        test_inc = incident_clean['Location']
        if('Heading' in test_inc.keys()):
            place_holder_variable = True
            if(isinstance(test_inc['Heading'],int)):
                place_holder_variable = True
            else:
                test_inc['Heading'] = ''
                return(False,'Heading wrong type', False)
        else:
            test_inc['Heading'] = ''
            #return(False,'Heading not present')
        if('Latitude' in test_inc.keys()):
            place_holder_variable = True
            if(isinstance(test_inc['Latitude'],float)):
                place_holder_variable = True
            else:
                test_inc['Latitude'] = ''
                return(False,'Latitude wrong type', False)
        else:
            test_inc['Latitude'] = ''
            #return(False,'Latitude not present')
        if('Longitude' in test_inc.keys()):
            place_holder_variable = True
            if(isinstance(test_inc['Longitude'],float)):
                place_holder_variable = True
            else:
                test_inc['Longitude'] = ''
                return(False,'Longitude wrong type', False)
        else:
            test_inc['Longitude'] = ''
            #return(False,'Longitude not present')
        if('SpeedKMH' in test_inc.keys()):
            place_holder_variable = True
            if(isinstance(test_inc['SpeedKMH'],float)):
                place_holder_variable = True
            else:
                test_inc['SpeedKMH'] = ''
                return(False,'SpeedKMH wrong type', False)
        else:
            test_inc['SpeedKMH'] = ''
            #return(False,'SpeedKMH not present')
        if('SpeedMPH' in test_inc.keys()):
            place_holder_variable = True
            if(isinstance(test_inc['SpeedMPH'],float)):
                place_holder_variable = True
            else:
                test_inc['SpeedMPH'] = ''
                return(False,'SpeedMPH wrong type', False)
        else:
            test_inc['SpeedMPH'] = ''
            #return(False,'SpeedMPH not present')

        incident_clean['Location'] = test_inc
    #Location end


     #"EventData": null,
    known_elements= { 'EventTime':'EventTime', 'EventType':'EventType','EndSpeedKmh':'EndSpeedKmh','EndSpeedMph':'EndSpeedMph','EndTime':'EndTime','MaxSpeedKmh':'MaxSpeedKmh','MaxSpeedMph':'MaxSpeedMph','Severity':'Severity','StartSpeedKmh':'StartSpeedKmh','StartSpeedMph':'StartSpeedMph','StartTime':'StartTime','TripId':'TripId','EndLocation':'EndLocation','StartLocation':'StartLocation','MaxLocation':'MaxLocation', 'Location':'Location', 'EventData':'EventData'}

    for incident_key in incident_clean.keys():
        if incident_key not in known_elements.keys():
            #print(incident_key)
            #text = open(email_file,'r')
            #email_string = text.read()
            #email_json = universal_imports.json.loads(email_string)
            sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'gr alerts warning', message = "gr "+"clean warning unknown incident_key "+incident_key, login = email_json["email"], password = email_json["password"])         

    # True if coordinate is within the boundaries of boundaries_file, False if not
    is_in_boundaries = filter_by_city_boundaries(incident_clean['Location'], boundaries_file)
    #complete cleaning, returning result:
    incident[0] = incident_clean #TODO replace
    # incident['data'][0] = incident_clean #TODO replace
    return ([True, incident, is_in_boundaries])

def clean(json_clean, boundaries_file):
    for i in range(0,len(json_clean)):
        clean_r = clean_incident(json_clean[i], boundaries_file)
        # clean_r value: [success\not (bool), incident\err_msg, in_boundaries\not (bool)]
        if clean_r[2]: # if in boundaries
            if (clean_r[0] == False): # if there are no errors
                sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = "clean_gr error", message = str("clean_gr error: missing field "+clean_r[1]), login = email_json["email"], password = email_json["password"])
                return ('error')
            else:
                json_clean[i]=clean_r[1]
        else: # not in boundaries
            return False
    return json_clean

#boundaries_file = '/home/yaniv/Workspace/serverscripts_production/scripts_for_gr/boundaries/lv_boundaries.json'
#incident = universal_imports.json.load(open('input.json'))
#clean_incident()
