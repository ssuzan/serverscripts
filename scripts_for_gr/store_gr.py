#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: json input and file directory path
#output: json stored to directory
from datetime import datetime
import time
import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts_production/globals')
import universal_imports
import json, requests
import push_to_fifo
import push_to_fifo_dev
import sendemail
from email.mime.text import MIMEText

email_file = os.environ['WORKSPACE']+'/serverscripts_production/globals/emails'

def store_original(original_path, save_path, dump_to_output,filename):
    incoming_files = os.listdir(original_path)
    for filename in incoming_files:
        #print (filename)
        command = 'mv ' + original_path + filename + " " + save_path
        os.system(command)
    return(incoming_files)

def store(curr_t, cleaned_store_json, save_path, dump_output_to, prod_fifo_ip, filename):
    text = open(email_file,'r')
    email_string = text.read()
    email_json = universal_imports.json.loads(email_string)     

    for i in range(0,len(cleaned_store_json)):       
        new_store_json =cleaned_store_json[i]
        curr_id = cleaned_store_json[i]['ID']
        del cleaned_store_json[i]['ID']
        #print(curr_id)

        if ((dump_output_to == "FILE") or (dump_output_to == "ALL")):
            lastFiletxt = open(save_path+"/database/"+curr_id+'.json', 'w')
            lastFiletxt.write(universal_imports.json.dumps(new_store_json, sort_keys=True, indent=4, separators=(',', ': ')))
        #sends file to FIFO
        if ((dump_output_to == "FIFO") or (dump_output_to == "ALL")):
            try:
                push_to_fifo.push_to_fifo(universal_imports.json.dumps(new_store_json), prod_fifo_ip)
            except:
                text = open(email_file,'r')
                email_string = text.read()
                email_json = universal_imports.json.loads(email_string)
                sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'error in store_gr', message = 'fifo error in store_gr', login = email_json['email'], password = email_json['password'])

            try:
                push_to_fifo_dev.push_to_fifo_dev(universal_imports.json.dumps(new_store_json))
            except:
                text = open(email_file,'r')
                email_string = text.read()
                email_json = universal_imports.json.loads(email_string)
                sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'warning in store_gr', message = 'dev fifo error in store_gr', login = email_json['email'], password = email_json['password'])
 
        if(dump_output_to == "NONE"):
            print("call_all_gr() writing output to NONE")

