import sys,os
sys.path.append("os.environ['WORKSPACE']+'/serverscripts/globals'")
import universal_imports
from automation_building_scripts import automate_build_clean

def incident_clean(incident):
    errors = []
    if('PC12' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PC12'],int)):
            place_holder_variable = True
        else:
            incident['PC12'] = ''
            errors.append('PC12 not int')
    else:
        errors.append('PC12 not present')
        incident['PC12'] = ''
    if('DAYPLANNO' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['DAYPLANNO'],str)):
            place_holder_variable = True
        else:
            incident['DAYPLANNO'] = ''
            errors.append('DAYPLANNO not str')
    else:
        errors.append('DAYPLANNO not present')
        incident['DAYPLANNO'] = ''
    if('SAVG11' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SAVG11'],int)):
            place_holder_variable = True
        else:
            incident['SAVG11'] = ''
            errors.append('SAVG11 not int')
    else:
        errors.append('SAVG11 not present')
        incident['SAVG11'] = ''
    if('D28' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D28'],int)):
            place_holder_variable = True
        else:
            incident['D28'] = ''
            errors.append('D28 not int')
    else:
        errors.append('D28 not present')
        incident['D28'] = ''
    if('P1' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['P1'],int)):
            place_holder_variable = True
        else:
            incident['P1'] = ''
            errors.append('P1 not int')
    else:
        errors.append('P1 not present')
        incident['P1'] = ''
    if('PP7' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PP7'],int)):
            place_holder_variable = True
        else:
            incident['PP7'] = ''
            errors.append('PP7 not int')
    else:
        errors.append('PP7 not present')
        incident['PP7'] = ''
    if('CommStatus' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CommStatus'],int)):
            place_holder_variable = True
        else:
            incident['CommStatus'] = ''
            errors.append('CommStatus not int')
    else:
        errors.append('CommStatus not present')
        incident['CommStatus'] = ''
    if('SP2' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SP2'],int)):
            place_holder_variable = True
        else:
            incident['SP2'] = ''
            errors.append('SP2 not int')
    else:
        errors.append('SP2 not present')
        incident['SP2'] = ''
    if('D1' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D1'],int)):
            place_holder_variable = True
        else:
            incident['D1'] = ''
            errors.append('D1 not int')
    else:
        errors.append('D1 not present')
        incident['D1'] = ''
    if('SAVG3' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SAVG3'],int)):
            place_holder_variable = True
        else:
            incident['SAVG3'] = ''
            errors.append('SAVG3 not int')
    else:
        errors.append('SAVG3 not present')
        incident['SAVG3'] = ''
    if('D49' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D49'],int)):
            place_holder_variable = True
        else:
            incident['D49'] = ''
            errors.append('D49 not int')
    else:
        errors.append('D49 not present')
        incident['D49'] = ''
    if('Cid' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['Cid'],int)):
            place_holder_variable = True
        else:
            return (False,'Cid not int')
    else:
        return (False,'Cid not present')
    if('SP5' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SP5'],int)):
            place_holder_variable = True
        else:
            incident['SP5'] = ''
            errors.append('SP5 not int')
    else:
        errors.append('SP5 not present')
        incident['SP5'] = ''
    if('D47' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D47'],int)):
            place_holder_variable = True
        else:
            incident['D47'] = ''
            errors.append('D47 not int')
    else:
        errors.append('D47 not present')
        incident['D47'] = ''
    if('PC10' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PC10'],int)):
            place_holder_variable = True
        else:
            incident['PC10'] = ''
            errors.append('PC10 not int')
    else:
        errors.append('PC10 not present')
        incident['PC10'] = ''
    if('PREEMPTSTR' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PREEMPTSTR'],str)):
            place_holder_variable = True
        else:
            incident['PREEMPTSTR'] = ''
            errors.append('PREEMPTSTR not str')
    else:
        errors.append('PREEMPTSTR not present')
        incident['PREEMPTSTR'] = ''
    if('SAVG2' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SAVG2'],int)):
            place_holder_variable = True
        else:
            incident['SAVG2'] = ''
            errors.append('SAVG2 not int')
    else:
        errors.append('SAVG2 not present')
        incident['SAVG2'] = ''
    if('D46' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D46'],int)):
            place_holder_variable = True
        else:
            incident['D46'] = ''
            errors.append('D46 not int')
    else:
        errors.append('D46 not present')
        incident['D46'] = ''
    if('D35' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D35'],int)):
            place_holder_variable = True
        else:
            incident['D35'] = ''
            errors.append('D35 not int')
    else:
        errors.append('D35 not present')
        incident['D35'] = ''
    if('D8' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D8'],int)):
            place_holder_variable = True
        else:
            incident['D8'] = ''
            errors.append('D8 not int')
    else:
        errors.append('D8 not present')
        incident['D8'] = ''
    if('SA5' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SA5'],int)):
            place_holder_variable = True
        else:
            incident['SA5'] = ''
            errors.append('SA5 not int')
    else:
        errors.append('SA5 not present')
        incident['SA5'] = ''
    if('P4' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['P4'],int)):
            place_holder_variable = True
        else:
            incident['P4'] = ''
            errors.append('P4 not int')
    else:
        errors.append('P4 not present')
        incident['P4'] = ''
    if('CH7' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CH7'],int)):
            place_holder_variable = True
        else:
            incident['CH7'] = ''
            errors.append('CH7 not int')
    else:
        errors.append('CH7 not present')
        incident['CH7'] = ''
    if('SP15' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SP15'],int)):
            place_holder_variable = True
        else:
            incident['SP15'] = ''
            errors.append('SP15 not int')
    else:
        errors.append('SP15 not present')
        incident['SP15'] = ''
    if('CH2' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CH2'],int)):
            place_holder_variable = True
        else:
            incident['CH2'] = ''
            errors.append('CH2 not int')
    else:
        errors.append('CH2 not present')
        incident['CH2'] = ''
    if('PP6' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PP6'],int)):
            place_holder_variable = True
        else:
            incident['PP6'] = ''
            errors.append('PP6 not int')
    else:
        errors.append('PP6 not present')
        incident['PP6'] = ''
    if('CH22' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CH22'],int)):
            place_holder_variable = True
        else:
            incident['CH22'] = ''
            errors.append('CH22 not int')
    else:
        errors.append('CH22 not present')
        incident['CH22'] = ''
    if('P9' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['P9'],int)):
            place_holder_variable = True
        else:
            incident['P9'] = ''
            errors.append('P9 not int')
    else:
        errors.append('P9 not present')
        incident['P9'] = ''
    if('D41' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D41'],int)):
            place_holder_variable = True
        else:
            incident['D41'] = ''
            errors.append('D41 not int')
    else:
        errors.append('D41 not present')
        incident['D41'] = ''
    if('FAIL' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['FAIL'],int)):
            place_holder_variable = True
        else:
            incident['FAIL'] = ''
            errors.append('FAIL not int')
    else:
        errors.append('FAIL not present')
        incident['FAIL'] = ''
    if('CH17' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CH17'],int)):
            place_holder_variable = True
        else:
            incident['CH17'] = ''
            errors.append('CH17 not int')
    else:
        errors.append('CH17 not present')
        incident['CH17'] = ''
    if('P5' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['P5'],int)):
            place_holder_variable = True
        else:
            incident['P5'] = ''
            errors.append('P5 not int')
    else:
        errors.append('P5 not present')
        incident['P5'] = ''
    if('D14' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D14'],int)):
            place_holder_variable = True
        else:
            incident['D14'] = ''
            errors.append('D14 not int')
    else:
        errors.append('D14 not present')
        incident['D14'] = ''
    if('P11' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['P11'],int)):
            place_holder_variable = True
        else:
            incident['P11'] = ''
            errors.append('P11 not int')
    else:
        errors.append('P11 not present')
        incident['P11'] = ''
    if('O13' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['O13'],int)):
            place_holder_variable = True
        else:
            incident['O13'] = ''
            errors.append('O13 not int')
    else:
        errors.append('O13 not present')
        incident['O13'] = ''
    if('D23' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D23'],int)):
            place_holder_variable = True
        else:
            incident['D23'] = ''
            errors.append('D23 not int')
    else:
        errors.append('D23 not present')
        incident['D23'] = ''
    if('D51' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D51'],int)):
            place_holder_variable = True
        else:
            incident['D51'] = ''
            errors.append('D51 not int')
    else:
        errors.append('D51 not present')
        incident['D51'] = ''
    if('D5' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D5'],int)):
            place_holder_variable = True
        else:
            incident['D5'] = ''
            errors.append('D5 not int')
    else:
        errors.append('D5 not present')
        incident['D5'] = ''
    if('CH24' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CH24'],int)):
            place_holder_variable = True
        else:
            incident['CH24'] = ''
            errors.append('CH24 not int')
    else:
        errors.append('CH24 not present')
        incident['CH24'] = ''
    if('P6' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['P6'],int)):
            place_holder_variable = True
        else:
            incident['P6'] = ''
            errors.append('P6 not int')
    else:
        errors.append('P6 not present')
        incident['P6'] = ''
    if('SAVG4' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SAVG4'],int)):
            place_holder_variable = True
        else:
            incident['SAVG4'] = ''
            errors.append('SAVG4 not int')
    else:
        errors.append('SAVG4 not present')
        incident['SAVG4'] = ''
    if('SAVG15' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SAVG15'],int)):
            place_holder_variable = True
        else:
            incident['SAVG15'] = ''
            errors.append('SAVG15 not int')
    else:
        errors.append('SAVG15 not present')
        incident['SAVG15'] = ''
    if('P2' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['P2'],int)):
            place_holder_variable = True
        else:
            incident['P2'] = ''
            errors.append('P2 not int')
    else:
        errors.append('P2 not present')
        incident['P2'] = ''
    if('SP1' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SP1'],int)):
            place_holder_variable = True
        else:
            incident['SP1'] = ''
            errors.append('SP1 not int')
    else:
        errors.append('SP1 not present')
        incident['SP1'] = ''
    if('SA15' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SA15'],int)):
            place_holder_variable = True
        else:
            incident['SA15'] = ''
            errors.append('SA15 not int')
    else:
        errors.append('SA15 not present')
        incident['SA15'] = ''
    if('ALTTIME' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['ALTTIME'],int)):
            place_holder_variable = True
        else:
            incident['ALTTIME'] = ''
            errors.append('ALTTIME not int')
    else:
        errors.append('ALTTIME not present')
        incident['ALTTIME'] = ''
    if('D16' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D16'],int)):
            place_holder_variable = True
        else:
            incident['D16'] = ''
            errors.append('D16 not int')
    else:
        errors.append('D16 not present')
        incident['D16'] = ''
    if('PP8' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PP8'],int)):
            place_holder_variable = True
        else:
            incident['PP8'] = ''
            errors.append('PP8 not int')
    else:
        errors.append('PP8 not present')
        incident['PP8'] = ''
    if('C12' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['C12'],int)):
            place_holder_variable = True
        else:
            incident['C12'] = ''
            errors.append('C12 not int')
    else:
        errors.append('C12 not present')
        incident['C12'] = ''
    if('PC7' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PC7'],int)):
            place_holder_variable = True
        else:
            incident['PC7'] = ''
            errors.append('PC7 not int')
    else:
        errors.append('PC7 not present')
        incident['PC7'] = ''
    if('D24' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D24'],int)):
            place_holder_variable = True
        else:
            incident['D24'] = ''
            errors.append('D24 not int')
    else:
        errors.append('D24 not present')
        incident['D24'] = ''
    if('RMIN1' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['RMIN1'],str)):
            place_holder_variable = True
        else:
            incident['RMIN1'] = ''
            errors.append('RMIN1 not str')
    else:
        errors.append('RMIN1 not present')
        incident['RMIN1'] = ''
    if('P12' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['P12'],int)):
            place_holder_variable = True
        else:
            incident['P12'] = ''
            errors.append('P12 not int')
    else:
        errors.append('P12 not present')
        incident['P12'] = ''
    if('D13' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D13'],int)):
            place_holder_variable = True
        else:
            incident['D13'] = ''
            errors.append('D13 not int')
    else:
        errors.append('D13 not present')
        incident['D13'] = ''
    if('D63' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D63'],int)):
            place_holder_variable = True
        else:
            incident['D63'] = ''
            errors.append('D63 not int')
    else:
        errors.append('D63 not present')
        incident['D63'] = ''
    if('CH18' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CH18'],int)):
            place_holder_variable = True
        else:
            incident['CH18'] = ''
            errors.append('CH18 not int')
    else:
        errors.append('CH18 not present')
        incident['CH18'] = ''
    if('PC8' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PC8'],int)):
            place_holder_variable = True
        else:
            incident['PC8'] = ''
            errors.append('PC8 not int')
    else:
        errors.append('PC8 not present')
        incident['PC8'] = ''
    if('LOCALCOUNTER' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['LOCALCOUNTER'],int)):
            place_holder_variable = True
        else:
            incident['LOCALCOUNTER'] = ''
            errors.append('LOCALCOUNTER not int')
    else:
        errors.append('LOCALCOUNTER not present')
        incident['LOCALCOUNTER'] = ''
    if('SCHEDULENO' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SCHEDULENO'],str)):
            place_holder_variable = True
        else:
            incident['SCHEDULENO'] = ''
            errors.append('SCHEDULENO not str')
    else:
        errors.append('SCHEDULENO not present')
        incident['SCHEDULENO'] = ''
    if('D12' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D12'],int)):
            place_holder_variable = True
        else:
            incident['D12'] = ''
            errors.append('D12 not int')
    else:
        errors.append('D12 not present')
        incident['D12'] = ''
    if('SA3' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SA3'],int)):
            place_holder_variable = True
        else:
            incident['SA3'] = ''
            errors.append('SA3 not int')
    else:
        errors.append('SA3 not present')
        incident['SA3'] = ''
    if('PC15' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PC15'],int)):
            place_holder_variable = True
        else:
            incident['PC15'] = ''
            errors.append('PC15 not int')
    else:
        errors.append('PC15 not present')
        incident['PC15'] = ''
    if('SA1' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SA1'],int)):
            place_holder_variable = True
        else:
            incident['SA1'] = ''
            errors.append('SA1 not int')
    else:
        errors.append('SA1 not present')
        incident['SA1'] = ''
    if('PC2' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PC2'],int)):
            place_holder_variable = True
        else:
            incident['PC2'] = ''
            errors.append('PC2 not int')
    else:
        errors.append('PC2 not present')
        incident['PC2'] = ''
    if('SP3' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SP3'],int)):
            place_holder_variable = True
        else:
            incident['SP3'] = ''
            errors.append('SP3 not int')
    else:
        errors.append('SP3 not present')
        incident['SP3'] = ''
    if('SEQSTR' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SEQSTR'],str)):
            place_holder_variable = True
        else:
            incident['SEQSTR'] = ''
            errors.append('SEQSTR not str')
    else:
        errors.append('SEQSTR not present')
        incident['SEQSTR'] = ''
    if('RMAX2' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['RMAX2'],str)):
            place_holder_variable = True
        else:
            incident['RMAX2'] = ''
            errors.append('RMAX2 not str')
    else:
        errors.append('RMAX2 not present')
        incident['RMAX2'] = ''
    if('CH6' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CH6'],int)):
            place_holder_variable = True
        else:
            incident['CH6'] = ''
            errors.append('CH6 not int')
    else:
        errors.append('CH6 not present')
        incident['CH6'] = ''
    if('RMAX1' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['RMAX1'],str)):
            place_holder_variable = True
        else:
            incident['RMAX1'] = ''
            errors.append('RMAX1 not str')
    else:
        errors.append('RMAX1 not present')
        incident['RMAX1'] = ''
    if('PC3' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PC3'],int)):
            place_holder_variable = True
        else:
            incident['PC3'] = ''
            errors.append('PC3 not int')
    else:
        errors.append('PC3 not present')
        incident['PC3'] = ''
    if('D26' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D26'],int)):
            place_holder_variable = True
        else:
            incident['D26'] = ''
            errors.append('D26 not int')
    else:
        errors.append('D26 not present')
        incident['D26'] = ''
    if('O8' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['O8'],int)):
            place_holder_variable = True
        else:
            incident['O8'] = ''
            errors.append('O8 not int')
    else:
        errors.append('O8 not present')
        incident['O8'] = ''
    if('C16' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['C16'],int)):
            place_holder_variable = True
        else:
            incident['C16'] = ''
            errors.append('C16 not int')
    else:
        errors.append('C16 not present')
        incident['C16'] = ''
    if('D25' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D25'],int)):
            place_holder_variable = True
        else:
            incident['D25'] = ''
            errors.append('D25 not int')
    else:
        errors.append('D25 not present')
        incident['D25'] = ''
    if('CH16' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CH16'],int)):
            place_holder_variable = True
        else:
            incident['CH16'] = ''
            errors.append('CH16 not int')
    else:
        errors.append('CH16 not present')
        incident['CH16'] = ''
    if('SAVG14' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SAVG14'],int)):
            place_holder_variable = True
        else:
            incident['SAVG14'] = ''
            errors.append('SAVG14 not int')
    else:
        errors.append('SAVG14 not present')
        incident['SAVG14'] = ''
    if('SAVG13' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SAVG13'],int)):
            place_holder_variable = True
        else:
            incident['SAVG13'] = ''
            errors.append('SAVG13 not int')
    else:
        errors.append('SAVG13 not present')
        incident['SAVG13'] = ''
    if('SA9' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SA9'],int)):
            place_holder_variable = True
        else:
            incident['SA9'] = ''
            errors.append('SA9 not int')
    else:
        errors.append('SA9 not present')
        incident['SA9'] = ''
    if('D58' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D58'],int)):
            place_holder_variable = True
        else:
            incident['D58'] = ''
            errors.append('D58 not int')
    else:
        errors.append('D58 not present')
        incident['D58'] = ''
    if('C15' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['C15'],int)):
            place_holder_variable = True
        else:
            incident['C15'] = ''
            errors.append('C15 not int')
    else:
        errors.append('C15 not present')
        incident['C15'] = ''
    if('D50' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D50'],int)):
            place_holder_variable = True
        else:
            incident['D50'] = ''
            errors.append('D50 not int')
    else:
        errors.append('D50 not present')
        incident['D50'] = ''
    if('SP12' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SP12'],int)):
            place_holder_variable = True
        else:
            incident['SP12'] = ''
            errors.append('SP12 not int')
    else:
        errors.append('SP12 not present')
        incident['SP12'] = ''
    if('D21' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D21'],int)):
            place_holder_variable = True
        else:
            incident['D21'] = ''
            errors.append('D21 not int')
    else:
        errors.append('D21 not present')
        incident['D21'] = ''
    if('PP16' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PP16'],int)):
            place_holder_variable = True
        else:
            incident['PP16'] = ''
            errors.append('PP16 not int')
    else:
        errors.append('PP16 not present')
        incident['PP16'] = ''
    if('EVENTNO' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['EVENTNO'],str)):
            place_holder_variable = True
        else:
            incident['EVENTNO'] = ''
            errors.append('EVENTNO not str')
    else:
        errors.append('EVENTNO not present')
        incident['EVENTNO'] = ''
    if('D29' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D29'],int)):
            place_holder_variable = True
        else:
            incident['D29'] = ''
            errors.append('D29 not int')
    else:
        errors.append('D29 not present')
        incident['D29'] = ''
    if('ALTOPT' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['ALTOPT'],int)):
            place_holder_variable = True
        else:
            incident['ALTOPT'] = ''
            errors.append('ALTOPT not int')
    else:
        errors.append('ALTOPT not present')
        incident['ALTOPT'] = ''
    if('D34' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D34'],int)):
            place_holder_variable = True
        else:
            incident['D34'] = ''
            errors.append('D34 not int')
    else:
        errors.append('D34 not present')
        incident['D34'] = ''
    if('C5' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['C5'],int)):
            place_holder_variable = True
        else:
            incident['C5'] = ''
            errors.append('C5 not int')
    else:
        errors.append('C5 not present')
        incident['C5'] = ''
    if('D2' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D2'],int)):
            place_holder_variable = True
        else:
            incident['D2'] = ''
            errors.append('D2 not int')
    else:
        errors.append('D2 not present')
        incident['D2'] = ''
    if('PP13' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PP13'],int)):
            place_holder_variable = True
        else:
            incident['PP13'] = ''
            errors.append('PP13 not int')
    else:
        errors.append('PP13 not present')
        incident['PP13'] = ''
    if('SA14' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SA14'],int)):
            place_holder_variable = True
        else:
            incident['SA14'] = ''
            errors.append('SA14 not int')
    else:
        errors.append('SA14 not present')
        incident['SA14'] = ''
    if('RMAX3' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['RMAX3'],str)):
            place_holder_variable = True
        else:
            incident['RMAX3'] = ''
            errors.append('RMAX3 not str')
    else:
        errors.append('RMAX3 not present')
        incident['RMAX3'] = ''
    if('RPED1' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['RPED1'],str)):
            place_holder_variable = True
        else:
            incident['RPED1'] = ''
            errors.append('RPED1 not str')
    else:
        errors.append('RPED1 not present')
        incident['RPED1'] = ''
    if('SA2' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SA2'],int)):
            place_holder_variable = True
        else:
            incident['SA2'] = ''
            errors.append('SA2 not int')
    else:
        errors.append('SA2 not present')
        incident['SA2'] = ''
    if('PP10' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PP10'],int)):
            place_holder_variable = True
        else:
            incident['PP10'] = ''
            errors.append('PP10 not int')
    else:
        errors.append('PP10 not present')
        incident['PP10'] = ''
    if('C1' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['C1'],int)):
            place_holder_variable = True
        else:
            incident['C1'] = ''
            errors.append('C1 not int')
    else:
        errors.append('C1 not present')
        incident['C1'] = ''
    if('SAVG8' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SAVG8'],int)):
            place_holder_variable = True
        else:
            incident['SAVG8'] = ''
            errors.append('SAVG8 not int')
    else:
        errors.append('SAVG8 not present')
        incident['SAVG8'] = ''
    if('ALTCIR' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['ALTCIR'],int)):
            place_holder_variable = True
        else:
            incident['ALTCIR'] = ''
            errors.append('ALTCIR not int')
    else:
        errors.append('ALTCIR not present')
        incident['ALTCIR'] = ''
    if('D60' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D60'],int)):
            place_holder_variable = True
        else:
            incident['D60'] = ''
            errors.append('D60 not int')
    else:
        errors.append('D60 not present')
        incident['D60'] = ''
    if('C11' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['C11'],int)):
            place_holder_variable = True
        else:
            incident['C11'] = ''
            errors.append('C11 not int')
    else:
        errors.append('C11 not present')
        incident['C11'] = ''
    if('CH12' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CH12'],int)):
            place_holder_variable = True
        else:
            incident['CH12'] = ''
            errors.append('CH12 not int')
    else:
        errors.append('CH12 not present')
        incident['CH12'] = ''
    if('SAVG10' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SAVG10'],int)):
            place_holder_variable = True
        else:
            incident['SAVG10'] = ''
            errors.append('SAVG10 not int')
    else:
        errors.append('SAVG10 not present')
        incident['SAVG10'] = ''
    if('D52' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D52'],int)):
            place_holder_variable = True
        else:
            incident['D52'] = ''
            errors.append('D52 not int')
    else:
        errors.append('D52 not present')
        incident['D52'] = ''
    if('SA13' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SA13'],int)):
            place_holder_variable = True
        else:
            incident['SA13'] = ''
            errors.append('SA13 not int')
    else:
        errors.append('SA13 not present')
        incident['SA13'] = ''
    if('OK' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['OK'],int)):
            place_holder_variable = True
        else:
            incident['OK'] = ''
            errors.append('OK not int')
    else:
        errors.append('OK not present')
        incident['OK'] = ''
    if('CH8' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CH8'],int)):
            place_holder_variable = True
        else:
            incident['CH8'] = ''
            errors.append('CH8 not int')
    else:
        errors.append('CH8 not present')
        incident['CH8'] = ''
    if('D54' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D54'],int)):
            place_holder_variable = True
        else:
            incident['D54'] = ''
            errors.append('D54 not int')
    else:
        errors.append('D54 not present')
        incident['D54'] = ''
    if('D59' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D59'],int)):
            place_holder_variable = True
        else:
            incident['D59'] = ''
            errors.append('D59 not int')
    else:
        errors.append('D59 not present')
        incident['D59'] = ''
    if('PC6' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PC6'],int)):
            place_holder_variable = True
        else:
            incident['PC6'] = ''
            errors.append('PC6 not int')
    else:
        errors.append('PC6 not present')
        incident['PC6'] = ''
    if('D10' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D10'],int)):
            place_holder_variable = True
        else:
            incident['D10'] = ''
            errors.append('D10 not int')
    else:
        errors.append('D10 not present')
        incident['D10'] = ''
    if('SA12' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SA12'],int)):
            place_holder_variable = True
        else:
            incident['SA12'] = ''
            errors.append('SA12 not int')
    else:
        errors.append('SA12 not present')
        incident['SA12'] = ''
    if('C6' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['C6'],int)):
            place_holder_variable = True
        else:
            incident['C6'] = ''
            errors.append('C6 not int')
    else:
        errors.append('C6 not present')
        incident['C6'] = ''
    if('CH1' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CH1'],int)):
            place_holder_variable = True
        else:
            incident['CH1'] = ''
            errors.append('CH1 not int')
    else:
        errors.append('CH1 not present')
        incident['CH1'] = ''
    if('O5' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['O5'],int)):
            place_holder_variable = True
        else:
            incident['O5'] = ''
            errors.append('O5 not int')
    else:
        errors.append('O5 not present')
        incident['O5'] = ''
    if('SAVG6' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SAVG6'],int)):
            place_holder_variable = True
        else:
            incident['SAVG6'] = ''
            errors.append('SAVG6 not int')
    else:
        errors.append('SAVG6 not present')
        incident['SAVG6'] = ''
    if('D6' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D6'],int)):
            place_holder_variable = True
        else:
            incident['D6'] = ''
            errors.append('D6 not int')
    else:
        errors.append('D6 not present')
        incident['D6'] = ''
    if('C3' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['C3'],int)):
            place_holder_variable = True
        else:
            incident['C3'] = ''
            errors.append('C3 not int')
    else:
        errors.append('C3 not present')
        incident['C3'] = ''
    if('C10' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['C10'],int)):
            place_holder_variable = True
        else:
            incident['C10'] = ''
            errors.append('C10 not int')
    else:
        errors.append('C10 not present')
        incident['C10'] = ''
    if('PP9' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PP9'],int)):
            place_holder_variable = True
        else:
            incident['PP9'] = ''
            errors.append('PP9 not int')
    else:
        errors.append('PP9 not present')
        incident['PP9'] = ''
    if('CH20' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CH20'],int)):
            place_holder_variable = True
        else:
            incident['CH20'] = ''
            errors.append('CH20 not int')
    else:
        errors.append('CH20 not present')
        incident['CH20'] = ''
    if('D4' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D4'],int)):
            place_holder_variable = True
        else:
            incident['D4'] = ''
            errors.append('D4 not int')
    else:
        errors.append('D4 not present')
        incident['D4'] = ''
    if('ACTIONNO' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['ACTIONNO'],str)):
            place_holder_variable = True
        else:
            incident['ACTIONNO'] = ''
            errors.append('ACTIONNO not str')
    else:
        errors.append('ACTIONNO not present')
        incident['ACTIONNO'] = ''
    if('SP13' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SP13'],int)):
            place_holder_variable = True
        else:
            incident['SP13'] = ''
            errors.append('SP13 not int')
    else:
        errors.append('SP13 not present')
        incident['SP13'] = ''
    if('SA10' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SA10'],int)):
            place_holder_variable = True
        else:
            incident['SA10'] = ''
            errors.append('SA10 not int')
    else:
        errors.append('SA10 not present')
        incident['SA10'] = ''
    if('PP12' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PP12'],int)):
            place_holder_variable = True
        else:
            incident['PP12'] = ''
            errors.append('PP12 not int')
    else:
        errors.append('PP12 not present')
        incident['PP12'] = ''
    if('C8' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['C8'],int)):
            place_holder_variable = True
        else:
            incident['C8'] = ''
            errors.append('C8 not int')
    else:
        errors.append('C8 not present')
        incident['C8'] = ''
    if('C4' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['C4'],int)):
            place_holder_variable = True
        else:
            incident['C4'] = ''
            errors.append('C4 not int')
    else:
        errors.append('C4 not present')
        incident['C4'] = ''
    if('D9' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D9'],int)):
            place_holder_variable = True
        else:
            incident['D9'] = ''
            errors.append('D9 not int')
    else:
        errors.append('D9 not present')
        incident['D9'] = ''
    if('PC9' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PC9'],int)):
            place_holder_variable = True
        else:
            incident['PC9'] = ''
            errors.append('PC9 not int')
    else:
        errors.append('PC9 not present')
        incident['PC9'] = ''
    if('O6' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['O6'],int)):
            place_holder_variable = True
        else:
            incident['O6'] = ''
            errors.append('O6 not int')
    else:
        errors.append('O6 not present')
        incident['O6'] = ''
    if('O11' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['O11'],int)):
            place_holder_variable = True
        else:
            incident['O11'] = ''
            errors.append('O11 not int')
    else:
        errors.append('O11 not present')
        incident['O11'] = ''
    if('D57' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D57'],int)):
            place_holder_variable = True
        else:
            incident['D57'] = ''
            errors.append('D57 not int')
    else:
        errors.append('D57 not present')
        incident['D57'] = ''
    if('D19' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D19'],int)):
            place_holder_variable = True
        else:
            incident['D19'] = ''
            errors.append('D19 not int')
    else:
        errors.append('D19 not present')
        incident['D19'] = ''
    if('D22' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D22'],int)):
            place_holder_variable = True
        else:
            incident['D22'] = ''
            errors.append('D22 not int')
    else:
        errors.append('D22 not present')
        incident['D22'] = ''
    if('D40' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D40'],int)):
            place_holder_variable = True
        else:
            incident['D40'] = ''
            errors.append('D40 not int')
    else:
        errors.append('D40 not present')
        incident['D40'] = ''
    if('PATTERN_NO' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PATTERN_NO'],int)):
            place_holder_variable = True
        else:
            incident['PATTERN_NO'] = ''
            errors.append('PATTERN_NO not int')
    else:
        errors.append('PATTERN_NO not present')
        incident['PATTERN_NO'] = ''
    if('RMIN2' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['RMIN2'],str)):
            place_holder_variable = True
        else:
            incident['RMIN2'] = ''
            errors.append('RMIN2 not str')
    else:
        errors.append('RMIN2 not present')
        incident['RMIN2'] = ''
    if('PP3' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PP3'],int)):
            place_holder_variable = True
        else:
            incident['PP3'] = ''
            errors.append('PP3 not int')
    else:
        errors.append('PP3 not present')
        incident['PP3'] = ''
    if('D45' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D45'],int)):
            place_holder_variable = True
        else:
            incident['D45'] = ''
            errors.append('D45 not int')
    else:
        errors.append('D45 not present')
        incident['D45'] = ''
    if('O2' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['O2'],int)):
            place_holder_variable = True
        else:
            incident['O2'] = ''
            errors.append('O2 not int')
    else:
        errors.append('O2 not present')
        incident['O2'] = ''
    if('SA4' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SA4'],int)):
            place_holder_variable = True
        else:
            incident['SA4'] = ''
            errors.append('SA4 not int')
    else:
        errors.append('SA4 not present')
        incident['SA4'] = ''
    if('D38' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D38'],int)):
            place_holder_variable = True
        else:
            incident['D38'] = ''
            errors.append('D38 not int')
    else:
        errors.append('D38 not present')
        incident['D38'] = ''
    if('TIMEDIFF' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['TIMEDIFF'],int)):
            place_holder_variable = True
        else:
            incident['TIMEDIFF'] = ''
            errors.append('TIMEDIFF not int')
    else:
        errors.append('TIMEDIFF not present')
        incident['TIMEDIFF'] = ''
    if('D62' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D62'],int)):
            place_holder_variable = True
        else:
            incident['D62'] = ''
            errors.append('D62 not int')
    else:
        errors.append('D62 not present')
        incident['D62'] = ''
    if('O14' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['O14'],int)):
            place_holder_variable = True
        else:
            incident['O14'] = ''
            errors.append('O14 not int')
    else:
        errors.append('O14 not present')
        incident['O14'] = ''
    if('SPLITINDEXSTR' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SPLITINDEXSTR'],str)):
            place_holder_variable = True
        else:
            incident['SPLITINDEXSTR'] = ''
            errors.append('SPLITINDEXSTR not str')
    else:
        errors.append('SPLITINDEXSTR not present')
        incident['SPLITINDEXSTR'] = ''
    if('SA8' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SA8'],int)):
            place_holder_variable = True
        else:
            incident['SA8'] = ''
            errors.append('SA8 not int')
    else:
        errors.append('SA8 not present')
        incident['SA8'] = ''
    if('PC16' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PC16'],int)):
            place_holder_variable = True
        else:
            incident['PC16'] = ''
            errors.append('PC16 not int')
    else:
        errors.append('PC16 not present')
        incident['PC16'] = ''
    if('O12' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['O12'],int)):
            place_holder_variable = True
        else:
            incident['O12'] = ''
            errors.append('O12 not int')
    else:
        errors.append('O12 not present')
        incident['O12'] = ''
    if('CH14' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CH14'],int)):
            place_holder_variable = True
        else:
            incident['CH14'] = ''
            errors.append('CH14 not int')
    else:
        errors.append('CH14 not present')
        incident['CH14'] = ''
    if('REALTIMECLK' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['REALTIMECLK'],str)):
            place_holder_variable = True
        else:
            incident['REALTIMECLK'] = ''
            errors.append('REALTIMECLK not str')
    else:
        errors.append('REALTIMECLK not present')
        incident['REALTIMECLK'] = ''
    if('SAVG7' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SAVG7'],int)):
            place_holder_variable = True
        else:
            incident['SAVG7'] = ''
            errors.append('SAVG7 not int')
    else:
        errors.append('SAVG7 not present')
        incident['SAVG7'] = ''
    if('SP6' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SP6'],int)):
            place_holder_variable = True
        else:
            incident['SP6'] = ''
            errors.append('SP6 not int')
    else:
        errors.append('SP6 not present')
        incident['SP6'] = ''
    if('RPED3' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['RPED3'],str)):
            place_holder_variable = True
        else:
            incident['RPED3'] = ''
            errors.append('RPED3 not str')
    else:
        errors.append('RPED3 not present')
        incident['RPED3'] = ''
    if('PP5' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PP5'],int)):
            place_holder_variable = True
        else:
            incident['PP5'] = ''
            errors.append('PP5 not int')
    else:
        errors.append('PP5 not present')
        incident['PP5'] = ''
    if('CH9' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CH9'],int)):
            place_holder_variable = True
        else:
            incident['CH9'] = ''
            errors.append('CH9 not int')
    else:
        errors.append('CH9 not present')
        incident['CH9'] = ''
    if('PP1' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PP1'],int)):
            place_holder_variable = True
        else:
            incident['PP1'] = ''
            errors.append('PP1 not int')
    else:
        errors.append('PP1 not present')
        incident['PP1'] = ''
    if('D11' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D11'],int)):
            place_holder_variable = True
        else:
            incident['D11'] = ''
            errors.append('D11 not int')
    else:
        errors.append('D11 not present')
        incident['D11'] = ''
    if('SP16' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SP16'],int)):
            place_holder_variable = True
        else:
            incident['SP16'] = ''
            errors.append('SP16 not int')
    else:
        errors.append('SP16 not present')
        incident['SP16'] = ''
    if('CH13' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CH13'],int)):
            place_holder_variable = True
        else:
            incident['CH13'] = ''
            errors.append('CH13 not int')
    else:
        errors.append('CH13 not present')
        incident['CH13'] = ''
    if('SP_CNTDN_R1' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SP_CNTDN_R1'],str)):
            place_holder_variable = True
        else:
            incident['SP_CNTDN_R1'] = ''
            errors.append('SP_CNTDN_R1 not str')
    else:
        errors.append('SP_CNTDN_R1 not present')
        incident['SP_CNTDN_R1'] = ''
    if('D15' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D15'],int)):
            place_holder_variable = True
        else:
            incident['D15'] = ''
            errors.append('D15 not int')
    else:
        errors.append('D15 not present')
        incident['D15'] = ''
    if('PC5' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PC5'],int)):
            place_holder_variable = True
        else:
            incident['PC5'] = ''
            errors.append('PC5 not int')
    else:
        errors.append('PC5 not present')
        incident['PC5'] = ''
    if('DTSTAMP' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['DTSTAMP'],str)):
            place_holder_variable = True
        else:
            incident['DTSTAMP'] = ''
            errors.append('DTSTAMP not str')
    else:
        errors.append('DTSTAMP not present')
        incident['DTSTAMP'] = ''
    if('Name' in incident.keys()):
        place_holder_variable = True
        if 'Name' == 'null':
            place_holder_variable = True
        else:
            errors.append('Name not none')
    else:
        errors.append('Name not present')
        incident['Name'] = ''
    if('D55' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D55'],int)):
            place_holder_variable = True
        else:
            incident['D55'] = ''
            errors.append('D55 not int')
    else:
        errors.append('D55 not present')
        incident['D55'] = ''
    if('D39' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D39'],int)):
            place_holder_variable = True
        else:
            incident['D39'] = ''
            errors.append('D39 not int')
    else:
        errors.append('D39 not present')
        incident['D39'] = ''
    if('D32' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D32'],int)):
            place_holder_variable = True
        else:
            incident['D32'] = ''
            errors.append('D32 not int')
    else:
        errors.append('D32 not present')
        incident['D32'] = ''
    if('D31' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D31'],int)):
            place_holder_variable = True
        else:
            incident['D31'] = ''
            errors.append('D31 not int')
    else:
        errors.append('D31 not present')
        incident['D31'] = ''
    if('SA11' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SA11'],int)):
            place_holder_variable = True
        else:
            incident['SA11'] = ''
            errors.append('SA11 not int')
    else:
        errors.append('SA11 not present')
        incident['SA11'] = ''
    if('ALTDET' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['ALTDET'],int)):
            place_holder_variable = True
        else:
            incident['ALTDET'] = ''
            errors.append('ALTDET not int')
    else:
        errors.append('ALTDET not present')
        incident['ALTDET'] = ''
    if('C13' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['C13'],int)):
            place_holder_variable = True
        else:
            incident['C13'] = ''
            errors.append('C13 not int')
    else:
        errors.append('C13 not present')
        incident['C13'] = ''
    if('CYCLE_NO' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CYCLE_NO'],int)):
            place_holder_variable = True
        else:
            incident['CYCLE_NO'] = ''
            errors.append('CYCLE_NO not int')
    else:
        errors.append('CYCLE_NO not present')
        incident['CYCLE_NO'] = ''
    if('SP_CNTDN_R3' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SP_CNTDN_R3'],str)):
            place_holder_variable = True
        else:
            incident['SP_CNTDN_R3'] = ''
            errors.append('SP_CNTDN_R3 not str')
    else:
        errors.append('SP_CNTDN_R3 not present')
        incident['SP_CNTDN_R3'] = ''
    if('D30' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D30'],int)):
            place_holder_variable = True
        else:
            incident['D30'] = ''
            errors.append('D30 not int')
    else:
        errors.append('D30 not present')
        incident['D30'] = ''
    if('TBC' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['TBC'],int)):
            place_holder_variable = True
        else:
            incident['TBC'] = ''
            errors.append('TBC not int')
    else:
        errors.append('TBC not present')
        incident['TBC'] = ''
    if('D17' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D17'],int)):
            place_holder_variable = True
        else:
            incident['D17'] = ''
            errors.append('D17 not int')
    else:
        errors.append('D17 not present')
        incident['D17'] = ''
    if('P7' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['P7'],int)):
            place_holder_variable = True
        else:
            incident['P7'] = ''
            errors.append('P7 not int')
    else:
        errors.append('P7 not present')
        incident['P7'] = ''
    if('CH5' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CH5'],int)):
            place_holder_variable = True
        else:
            incident['CH5'] = ''
            errors.append('CH5 not int')
    else:
        errors.append('CH5 not present')
        incident['CH5'] = ''
    if('D48' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D48'],int)):
            place_holder_variable = True
        else:
            incident['D48'] = ''
            errors.append('D48 not int')
    else:
        errors.append('D48 not present')
        incident['D48'] = ''
    if('TOTAL' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['TOTAL'],int)):
            place_holder_variable = True
        else:
            incident['TOTAL'] = ''
            errors.append('TOTAL not int')
    else:
        errors.append('TOTAL not present')
        incident['TOTAL'] = ''
    if('PP11' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PP11'],int)):
            place_holder_variable = True
        else:
            incident['PP11'] = ''
            errors.append('PP11 not int')
    else:
        errors.append('PP11 not present')
        incident['PP11'] = ''
    if('PC14' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PC14'],int)):
            place_holder_variable = True
        else:
            incident['PC14'] = ''
            errors.append('PC14 not int')
    else:
        errors.append('PC14 not present')
        incident['PC14'] = ''
    if('D53' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D53'],int)):
            place_holder_variable = True
        else:
            incident['D53'] = ''
            errors.append('D53 not int')
    else:
        errors.append('D53 not present')
        incident['D53'] = ''
    if('SAVG9' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SAVG9'],int)):
            place_holder_variable = True
        else:
            incident['SAVG9'] = ''
            errors.append('SAVG9 not int')
    else:
        errors.append('SAVG9 not present')
        incident['SAVG9'] = ''
    if('SP11' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SP11'],int)):
            place_holder_variable = True
        else:
            incident['SP11'] = ''
            errors.append('SP11 not int')
    else:
        errors.append('SP11 not present')
        incident['SP11'] = ''
    if('O16' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['O16'],int)):
            place_holder_variable = True
        else:
            incident['O16'] = ''
            errors.append('O16 not int')
    else:
        errors.append('O16 not present')
        incident['O16'] = ''
    if('PC4' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PC4'],int)):
            place_holder_variable = True
        else:
            incident['PC4'] = ''
            errors.append('PC4 not int')
    else:
        errors.append('PC4 not present')
        incident['PC4'] = ''
    if('D61' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D61'],int)):
            place_holder_variable = True
        else:
            incident['D61'] = ''
            errors.append('D61 not int')
    else:
        errors.append('D61 not present')
        incident['D61'] = ''
    if('D7' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D7'],int)):
            place_holder_variable = True
        else:
            incident['D7'] = ''
            errors.append('D7 not int')
    else:
        errors.append('D7 not present')
        incident['D7'] = ''
    if('O7' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['O7'],int)):
            place_holder_variable = True
        else:
            incident['O7'] = ''
            errors.append('O7 not int')
    else:
        errors.append('O7 not present')
        incident['O7'] = ''
    if('SAVG1' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SAVG1'],int)):
            place_holder_variable = True
        else:
            incident['SAVG1'] = ''
            errors.append('SAVG1 not int')
    else:
        errors.append('SAVG1 not present')
        incident['SAVG1'] = ''
    if('CRDSRC' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CRDSRC'],str)):
            place_holder_variable = True
        else:
            incident['CRDSRC'] = ''
            errors.append('CRDSRC not str')
    else:
        errors.append('CRDSRC not present')
        incident['CRDSRC'] = ''
    if('CH23' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CH23'],int)):
            place_holder_variable = True
        else:
            incident['CH23'] = ''
            errors.append('CH23 not int')
    else:
        errors.append('CH23 not present')
        incident['CH23'] = ''
    if('D42' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D42'],int)):
            place_holder_variable = True
        else:
            incident['D42'] = ''
            errors.append('D42 not int')
    else:
        errors.append('D42 not present')
        incident['D42'] = ''
    if('D20' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D20'],int)):
            place_holder_variable = True
        else:
            incident['D20'] = ''
            errors.append('D20 not int')
    else:
        errors.append('D20 not present')
        incident['D20'] = ''
    if('D56' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D56'],int)):
            place_holder_variable = True
        else:
            incident['D56'] = ''
            errors.append('D56 not int')
    else:
        errors.append('D56 not present')
        incident['D56'] = ''
    if('RMAX4' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['RMAX4'],str)):
            place_holder_variable = True
        else:
            incident['RMAX4'] = ''
            errors.append('RMAX4 not str')
    else:
        errors.append('RMAX4 not present')
        incident['RMAX4'] = ''
    if('SAVG16' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SAVG16'],int)):
            place_holder_variable = True
        else:
            incident['SAVG16'] = ''
            errors.append('SAVG16 not int')
    else:
        errors.append('SAVG16 not present')
        incident['SAVG16'] = ''
    if('SP_CNTDN_R4' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SP_CNTDN_R4'],str)):
            place_holder_variable = True
        else:
            incident['SP_CNTDN_R4'] = ''
            errors.append('SP_CNTDN_R4 not str')
    else:
        errors.append('SP_CNTDN_R4 not present')
        incident['SP_CNTDN_R4'] = ''
    if('O10' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['O10'],int)):
            place_holder_variable = True
        else:
            incident['O10'] = ''
            errors.append('O10 not int')
    else:
        errors.append('O10 not present')
        incident['O10'] = ''
    if('P13' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['P13'],int)):
            place_holder_variable = True
        else:
            incident['P13'] = ''
            errors.append('P13 not int')
    else:
        errors.append('P13 not present')
        incident['P13'] = ''
    if('SP4' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SP4'],int)):
            place_holder_variable = True
        else:
            incident['SP4'] = ''
            errors.append('SP4 not int')
    else:
        errors.append('SP4 not present')
        incident['SP4'] = ''
    if('SA16' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SA16'],int)):
            place_holder_variable = True
        else:
            incident['SA16'] = ''
            errors.append('SA16 not int')
    else:
        errors.append('SA16 not present')
        incident['SA16'] = ''
    if('O1' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['O1'],int)):
            place_holder_variable = True
        else:
            incident['O1'] = ''
            errors.append('O1 not int')
    else:
        errors.append('O1 not present')
        incident['O1'] = ''
    if('SP_CNTDN_R2' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SP_CNTDN_R2'],str)):
            place_holder_variable = True
        else:
            incident['SP_CNTDN_R2'] = ''
            errors.append('SP_CNTDN_R2 not str')
    else:
        errors.append('SP_CNTDN_R2 not present')
        incident['SP_CNTDN_R2'] = ''
    if('C2' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['C2'],int)):
            place_holder_variable = True
        else:
            incident['C2'] = ''
            errors.append('C2 not int')
    else:
        errors.append('C2 not present')
        incident['C2'] = ''
    if('PC13' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PC13'],int)):
            place_holder_variable = True
        else:
            incident['PC13'] = ''
            errors.append('PC13 not int')
    else:
        errors.append('PC13 not present')
        incident['PC13'] = ''
    if('O9' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['O9'],int)):
            place_holder_variable = True
        else:
            incident['O9'] = ''
            errors.append('O9 not int')
    else:
        errors.append('O9 not present')
        incident['O9'] = ''
    if('P10' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['P10'],int)):
            place_holder_variable = True
        else:
            incident['P10'] = ''
            errors.append('P10 not int')
    else:
        errors.append('P10 not present')
        incident['P10'] = ''
    if('O15' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['O15'],int)):
            place_holder_variable = True
        else:
            incident['O15'] = ''
            errors.append('O15 not int')
    else:
        errors.append('O15 not present')
        incident['O15'] = ''
    if('SP10' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SP10'],int)):
            place_holder_variable = True
        else:
            incident['SP10'] = ''
            errors.append('SP10 not int')
    else:
        errors.append('SP10 not present')
        incident['SP10'] = ''
    if('CH15' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CH15'],int)):
            place_holder_variable = True
        else:
            incident['CH15'] = ''
            errors.append('CH15 not int')
    else:
        errors.append('CH15 not present')
        incident['CH15'] = ''
    if('PP15' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PP15'],int)):
            place_holder_variable = True
        else:
            incident['PP15'] = ''
            errors.append('PP15 not int')
    else:
        errors.append('PP15 not present')
        incident['PP15'] = ''
    if('RPED4' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['RPED4'],str)):
            place_holder_variable = True
        else:
            incident['RPED4'] = ''
            errors.append('RPED4 not str')
    else:
        errors.append('RPED4 not present')
        incident['RPED4'] = ''
    if('P16' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['P16'],int)):
            place_holder_variable = True
        else:
            incident['P16'] = ''
            errors.append('P16 not int')
    else:
        errors.append('P16 not present')
        incident['P16'] = ''
    if('PP14' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PP14'],int)):
            place_holder_variable = True
        else:
            incident['PP14'] = ''
            errors.append('PP14 not int')
    else:
        errors.append('PP14 not present')
        incident['PP14'] = ''
    if('SA6' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SA6'],int)):
            place_holder_variable = True
        else:
            incident['SA6'] = ''
            errors.append('SA6 not int')
    else:
        errors.append('SA6 not present')
        incident['SA6'] = ''
    if('D18' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D18'],int)):
            place_holder_variable = True
        else:
            incident['D18'] = ''
            errors.append('D18 not int')
    else:
        errors.append('D18 not present')
        incident['D18'] = ''
    if('C7' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['C7'],int)):
            place_holder_variable = True
        else:
            incident['C7'] = ''
            errors.append('C7 not int')
    else:
        errors.append('C7 not present')
        incident['C7'] = ''
    if('PC1' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PC1'],int)):
            place_holder_variable = True
        else:
            incident['PC1'] = ''
            errors.append('PC1 not int')
    else:
        errors.append('PC1 not present')
        incident['PC1'] = ''
    if('SP7' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SP7'],int)):
            place_holder_variable = True
        else:
            incident['SP7'] = ''
            errors.append('SP7 not int')
    else:
        errors.append('SP7 not present')
        incident['SP7'] = ''
    if('CH3' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CH3'],int)):
            place_holder_variable = True
        else:
            incident['CH3'] = ''
            errors.append('CH3 not int')
    else:
        errors.append('CH3 not present')
        incident['CH3'] = ''
    if('P8' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['P8'],int)):
            place_holder_variable = True
        else:
            incident['P8'] = ''
            errors.append('P8 not int')
    else:
        errors.append('P8 not present')
        incident['P8'] = ''
    if('D37' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D37'],int)):
            place_holder_variable = True
        else:
            incident['D37'] = ''
            errors.append('D37 not int')
    else:
        errors.append('D37 not present')
        incident['D37'] = ''
    if('COORD' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['COORD'],str)):
            place_holder_variable = True
        else:
            incident['COORD'] = ''
            errors.append('COORD not str')
    else:
        errors.append('COORD not present')
        incident['COORD'] = ''
    if('P15' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['P15'],int)):
            place_holder_variable = True
        else:
            incident['P15'] = ''
            errors.append('P15 not int')
    else:
        errors.append('P15 not present')
        incident['P15'] = ''
    if('SP9' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SP9'],int)):
            place_holder_variable = True
        else:
            incident['SP9'] = ''
            errors.append('SP9 not int')
    else:
        errors.append('SP9 not present')
        incident['SP9'] = ''
    if('PC11' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PC11'],int)):
            place_holder_variable = True
        else:
            incident['PC11'] = ''
            errors.append('PC11 not int')
    else:
        errors.append('PC11 not present')
        incident['PC11'] = ''
    if('SAVG5' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SAVG5'],int)):
            place_holder_variable = True
        else:
            incident['SAVG5'] = ''
            errors.append('SAVG5 not int')
    else:
        errors.append('SAVG5 not present')
        incident['SAVG5'] = ''
    if('D36' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D36'],int)):
            place_holder_variable = True
        else:
            incident['D36'] = ''
            errors.append('D36 not int')
    else:
        errors.append('D36 not present')
        incident['D36'] = ''
    if('RMIN4' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['RMIN4'],str)):
            place_holder_variable = True
        else:
            incident['RMIN4'] = ''
            errors.append('RMIN4 not str')
    else:
        errors.append('RMIN4 not present')
        incident['RMIN4'] = ''
    if('D43' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D43'],int)):
            place_holder_variable = True
        else:
            incident['D43'] = ''
            errors.append('D43 not int')
    else:
        errors.append('D43 not present')
        incident['D43'] = ''
    if('PP2' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PP2'],int)):
            place_holder_variable = True
        else:
            incident['PP2'] = ''
            errors.append('PP2 not int')
    else:
        errors.append('PP2 not present')
        incident['PP2'] = ''
    if('D64' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D64'],int)):
            place_holder_variable = True
        else:
            incident['D64'] = ''
            errors.append('D64 not int')
    else:
        errors.append('D64 not present')
        incident['D64'] = ''
    if('FREE' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['FREE'],str)):
            place_holder_variable = True
        else:
            incident['FREE'] = ''
            errors.append('FREE not str')
    else:
        errors.append('FREE not present')
        incident['FREE'] = ''
    if('O4' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['O4'],int)):
            place_holder_variable = True
        else:
            incident['O4'] = ''
            errors.append('O4 not int')
    else:
        errors.append('O4 not present')
        incident['O4'] = ''
    if('SP8' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SP8'],int)):
            place_holder_variable = True
        else:
            incident['SP8'] = ''
            errors.append('SP8 not int')
    else:
        errors.append('SP8 not present')
        incident['SP8'] = ''
    if('PP4' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PP4'],int)):
            place_holder_variable = True
        else:
            incident['PP4'] = ''
            errors.append('PP4 not int')
    else:
        errors.append('PP4 not present')
        incident['PP4'] = ''
    if('SAVG12' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SAVG12'],int)):
            place_holder_variable = True
        else:
            incident['SAVG12'] = ''
            errors.append('SAVG12 not int')
    else:
        errors.append('SAVG12 not present')
        incident['SAVG12'] = ''
    if('D33' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D33'],int)):
            place_holder_variable = True
        else:
            incident['D33'] = ''
            errors.append('D33 not int')
    else:
        errors.append('D33 not present')
        incident['D33'] = ''
    if('O3' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['O3'],int)):
            place_holder_variable = True
        else:
            incident['O3'] = ''
            errors.append('O3 not int')
    else:
        errors.append('O3 not present')
        incident['O3'] = ''
    if('CH10' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CH10'],int)):
            place_holder_variable = True
        else:
            incident['CH10'] = ''
            errors.append('CH10 not int')
    else:
        errors.append('CH10 not present')
        incident['CH10'] = ''
    if('CH21' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CH21'],int)):
            place_holder_variable = True
        else:
            incident['CH21'] = ''
            errors.append('CH21 not int')
    else:
        errors.append('CH21 not present')
        incident['CH21'] = ''
    if('D27' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D27'],int)):
            place_holder_variable = True
        else:
            incident['D27'] = ''
            errors.append('D27 not int')
    else:
        errors.append('D27 not present')
        incident['D27'] = ''
    if('RMIN3' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['RMIN3'],str)):
            place_holder_variable = True
        else:
            incident['RMIN3'] = ''
            errors.append('RMIN3 not str')
    else:
        errors.append('RMIN3 not present')
        incident['RMIN3'] = ''
    if('P3' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['P3'],int)):
            place_holder_variable = True
        else:
            incident['P3'] = ''
            errors.append('P3 not int')
    else:
        errors.append('P3 not present')
        incident['P3'] = ''
    if('CH19' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CH19'],int)):
            place_holder_variable = True
        else:
            incident['CH19'] = ''
            errors.append('CH19 not int')
    else:
        errors.append('CH19 not present')
        incident['CH19'] = ''
    if('P14' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['P14'],int)):
            place_holder_variable = True
        else:
            incident['P14'] = ''
            errors.append('P14 not int')
    else:
        errors.append('P14 not present')
        incident['P14'] = ''
    if('OFFSETSTR' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['OFFSETSTR'],str)):
            place_holder_variable = True
        else:
            incident['OFFSETSTR'] = ''
            errors.append('OFFSETSTR not str')
    else:
        errors.append('OFFSETSTR not present')
        incident['OFFSETSTR'] = ''
    if('D44' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D44'],int)):
            place_holder_variable = True
        else:
            incident['D44'] = ''
            errors.append('D44 not int')
    else:
        errors.append('D44 not present')
        incident['D44'] = ''
    if('CH4' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CH4'],int)):
            place_holder_variable = True
        else:
            incident['CH4'] = ''
            errors.append('CH4 not int')
    else:
        errors.append('CH4 not present')
        incident['CH4'] = ''
    if('SP14' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SP14'],int)):
            place_holder_variable = True
        else:
            incident['SP14'] = ''
            errors.append('SP14 not int')
    else:
        errors.append('SP14 not present')
        incident['SP14'] = ''
    if('RPED2' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['RPED2'],str)):
            place_holder_variable = True
        else:
            incident['RPED2'] = ''
            errors.append('RPED2 not str')
    else:
        errors.append('RPED2 not present')
        incident['RPED2'] = ''
    if('Offline' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['Offline'],int)):
            place_holder_variable = True
        else:
            incident['Offline'] = ''
            errors.append('Offline not int')
    else:
        errors.append('Offline not present')
        incident['Offline'] = ''
    if('SA7' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['SA7'],int)):
            place_holder_variable = True
        else:
            incident['SA7'] = ''
            errors.append('SA7 not int')
    else:
        errors.append('SA7 not present')
        incident['SA7'] = ''
    if('D3' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['D3'],int)):
            place_holder_variable = True
        else:
            incident['D3'] = ''
            errors.append('D3 not int')
    else:
        errors.append('D3 not present')
        incident['D3'] = ''
    if('C14' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['C14'],int)):
            place_holder_variable = True
        else:
            incident['C14'] = ''
            errors.append('C14 not int')
    else:
        errors.append('C14 not present')
        incident['C14'] = ''
    if('C9' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['C9'],int)):
            place_holder_variable = True
        else:
            incident['C9'] = ''
            errors.append('C9 not int')
    else:
        errors.append('C9 not present')
        incident['C9'] = ''
    if('CH11' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CH11'],int)):
            place_holder_variable = True
        else:
            incident['CH11'] = ''
            errors.append('CH11 not int')
    else:
        errors.append('CH11 not present')
        incident['CH11'] = ''    
    return (errors,incident)
def fast_lights_clean(fast_light_clean_data):
    errors = []
    for i in range(0,len(fast_light_clean_data)):
        print(incident_clean(fast_light_clean_data[i])[0])
    #automate_build_clean.automate_clean(fast_light_clean_data[0],"incident")
    return(True, 'no error',errors)
