#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
#sudo pip3 install mygeotab
import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts/globals')
import universal_imports
import mygeotab
import sendemail
from email.mime.text import MIMEText
import fast_lights_clean
#import fast_lights_store
import fast_lights_fetch

save_path = os.environ['WORKSPACE']+'/DB_backups/data_from_geotab/database/'
original_backup_path = os.environ['WORKSPACE']+'/DB_backups/data_from_geotab/database_original/'
email_file = os.environ['WORKSPACE']+'/serverscripts/globals/emails'

dump_output_to = "FIFO"

def fast_lights_call_all():
    text = open(email_file,'r')
    email_string = text.read()
    email_json = universal_imports.json.loads(email_string)  
    fetched = fast_lights_fetch.fast_lights_fetch()
    curr_t = universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")  
    x = fast_lights_fetch.fast_lights_fetch()
    print(fast_lights_clean.fast_lights_clean(x))
       
    
#Calling geotab_call_all
fast_lights_call_all()
print('geotab_call_all.py completed successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
