#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts/globals')
import universal_imports
import xmltodict
import nhp_store

x = ['sp_output2.xml','sp_output3.xml','sp_output3.xml','sp_output2.xml']
def nhp_fetch(filename):
    #connecting to geotab and authentication 
    
    
    try:
        text = open(x[1],'r')
        sp_string = text.read()        
        nhp_store.nhp_store_original(sp_string,filename)
        try:
            doc = xmltodict.parse(sp_string)
            return [True, doc]
        except:
            return [False,'nhp failed to convert to json '+filename]
    except:
        return [False,'nhp failed to fetch '+filename]
