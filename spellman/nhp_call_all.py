#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
#sudo pip3 install mygeotab
import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts/globals')
import universal_imports
from apscheduler.schedulers.blocking import BlockingScheduler
import APScheduler
import sendemail
from email.mime.text import MIMEText
import xmltodict
import nhp_clean
import nhp_fetch
import nhp_store

save_path = os.environ['WORKSPACE']+'/DB_backups/data_from_geotab/database/'
original_backup_path = os.environ['WORKSPACE']+'/DB_backups/data_from_geotab/database_original/'
email_file = os.environ['WORKSPACE']+'/serverscripts/globals/emails'
type_name = 'road_incidents_nhp_live_lv'

dump_output_to = "FILE"
#number of iterations before element is removed from ongoing duplicate check list
default_age = 3
#minutes between iterations (.5 for half minute)
minutes_between_script_calls = 1
#ongoing duplicate check list
hash_log = {}

#n is for iterating through test files
n = [0]
def nhp_call_all():
    now = str(universal_imports.datetime.utcnow())
    now = now.split(" ")
    now[0] = now[0].split("-")
    now[1] = now[1].split(":")
    filename = type_name+"_"+now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1]+"_"+now[1][2].replace(".","_")           
    text = open(email_file,'r')
    email_string = text.read()
    email_json = universal_imports.json.loads(email_string)  
    curr_t = universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")  
    dead_hashes = []
    #calls fetch
    doc = nhp_fetch.nhp_fetch(filename)
    #n[0]+=1 
    if(doc[0] == True):
        doc_json = dict(doc[1])
        element_names = {}
        clean_results = nhp_clean.nhp_clean(doc_json,filename)
        if clean_results[0] == True:
            try:
                #tests for existing CADActiveCallTable
                incidents = clean_results[1]['PublicSafetyEnvelope']['PublicSafety']['Response']['CADActiveCallTable']                
                try:
                    results = nhp_store.nhp_store(curr_t, incidents,hash_log,default_age,dump_output_to,filename)                                  
                    hashes = results[0]
                    repeat_hashes = results[1]
                    for key in hashes:
                        hash_log[key] = hashes[key]
                    for key in hash_log.keys():
                        if key not in repeat_hashes and key not in hashes.keys():
                            hash_log[key][1]-=1
                            if hash_log[key][1] == 0:
                                dead_hashes.append(key)
                        else:
                            hash_log[key][1] = default_age
                    for i in range(0,len(dead_hashes)):
                        del hash_log[dead_hashes[i]]                      
                except:
                    sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'nhp error', message = "nhp store error "+filename, login = email_json["email"], password = email_json["password"]) 
                    for key in hash_log.keys():
                        hash_log[key][1]-=1
                        if hash_log[key][1] == 0:
                            dead_hashes.append(key)
                    for i in range(0,len(dead_hashes)):
                        del hash_log[dead_hashes[i]]     
            except:
                for key in hash_log.keys():
                    hash_log[key][1]-=1
                    if hash_log[key][1] == 0:
                        dead_hashes.append(key)
                for i in range(0,len(dead_hashes)):
                    del hash_log[dead_hashes[i]]                  
             
                            
        else:
            sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'nhp error', message = "nhp clean error " +filename+ " "+clean_results[1], login = email_json["email"], password = email_json["password"]) 
            for key in hash_log.keys():
                hash_log[key][1]-=1
                #print(key+" "+str(hash_log[key][1]))
                if hash_log[key][1] == 0:
                    dead_hashes.append(key)
            for i in range(0,len(dead_hashes)):
                del hash_log[dead_hashes[i]]                  
    else:
        sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'nhp error', message = doc[1], login = email_json["email"], password = email_json["password"]) 
        for key in hash_log.keys():
            hash_log[key][1]-=1
            #print(key+" "+str(hash_log[key][1]))
            if hash_log[key][1] == 0:
                dead_hashes.append(key)
        for i in range(0,len(dead_hashes)):
            del hash_log[dead_hashes[i]]  
    #print(hash_log)
    print('nhp_call_all.py completed successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
    
    
def scheduled_call():
    print('nhp_call_all.scheduled_call() started successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
    try:
        initialize_new_schedule()
    except:
        place_holder_variable = "holding a place"
    #initialize new scheduler
    sched = BlockingScheduler(timezone='UTC')
    sched.add_job(nhp_call_all, 'interval', id='job_id1', seconds=5)
    sched.start()
    
#spellman_call_all()
scheduled_call()
#spellman_call_all(0)
#spellman_call_all(2)

