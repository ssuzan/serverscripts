#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import sys,os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts/globals')
import universal_imports
import sendemail
from email.mime.text import MIMEText
email_file = os.environ['WORKSPACE']+'/serverscripts/globals/emails'


def nhp_incident_clean(incident,filename):
    if('LongTermCallID' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['LongTermCallID'],str)):
            place_holder_variable = True
        else:
            print(incident['LongTermCallID'])
            return(False,'LongTermCallID not correct type')
    else:
        incident['LongTermCallID'] = ''    
    if('AgencyCode' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['AgencyCode'],str)):
            place_holder_variable = True
        else:
            print(incident['AgencyCode'])
            return(False,'AgencyCode not correct type')
    else:
        incident['AgencyCode'] = ''
    if('CallPriority' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CallPriority'],str)):
            place_holder_variable = True
        else:
            print(incident['CallPriority'])
            return(False,'CallPriority not correct type')
    else:
        incident['CallPriority'] = ''
    if('PlateNumberOfStoppedVeh' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PlateNumberOfStoppedVeh'],str)):
            place_holder_variable = True
        else:
            print(incident['PlateNumberOfStoppedVeh'])
            return(False,'PlateNumberOfStoppedVeh not correct type')
    else:
        incident['PlateNumberOfStoppedVeh'] = ''
    if('GeobaseAddressID' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['GeobaseAddressID'],str)):
            place_holder_variable = True
        else:
            print(incident['GeobaseAddressID'])
            return(False,'GeobaseAddressID not correct type')
    else:
        incident['GeobaseAddressID'] = ''
    if('DispatchLevelStatus' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['DispatchLevelStatus'],str)):
            place_holder_variable = True
        else:
            print(incident['DispatchLevelStatus'])
            return(False,'DispatchLevelStatus not correct type')
    else:
        incident['DispatchLevelStatus'] = ''
    if('IncidentNature' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['IncidentNature'],str)):
            place_holder_variable = True
        else:
            print(incident['IncidentNature'])
            return(False,'IncidentNature not correct type')
    else:
        incident['IncidentNature'] = ''
    if('XCoordinateGeobase' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['XCoordinateGeobase'],str)):
            place_holder_variable = True
        else:
            print(incident['XCoordinateGeobase'])
            return(False,'XCoordinateGeobase not correct type')
    else:
        incident['XCoordinateGeobase'] = ''
    if('HighBitsOfXCoordinate' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['HighBitsOfXCoordinate'],str)):
            place_holder_variable = True
        else:
            print(incident['HighBitsOfXCoordinate'])
            return(False,'HighBitsOfXCoordinate not correct type')
    else:
        incident['HighBitsOfXCoordinate'] = ''
    if('HowManyTimesTextChanged' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['HowManyTimesTextChanged'],str)):
            place_holder_variable = True
        else:
            print(incident['HowManyTimesTextChanged'])
            return(False,'HowManyTimesTextChanged not correct type')
    else:
        incident['HowManyTimesTextChanged'] = ''
    if('CallTypeLawFireEMS' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CallTypeLawFireEMS'],str)):
            place_holder_variable = True
        else:
            print(incident['CallTypeLawFireEMS'])
            return(False,'CallTypeLawFireEMS not correct type')
    else:
        incident['CallTypeLawFireEMS'] = ''
    if('WhenCallWasOpened' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['WhenCallWasOpened'],str)):
            place_holder_variable = True
        else:
            print(incident['WhenCallWasOpened'])
            return(False,'WhenCallWasOpened not correct type')
    else:
        incident['WhenCallWasOpened'] = ''
    if('TimeDateReported' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['TimeDateReported'],str)):
            place_holder_variable = True
        else:
            print(incident['TimeDateReported'])
            return(False,'TimeDateReported not correct type')
    else:
        incident['TimeDateReported'] = ''
    if('ResponsibleUnitNumber' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['ResponsibleUnitNumber'],str)):
            place_holder_variable = True
        else:
            print(incident['ResponsibleUnitNumber'])
            return(False,'ResponsibleUnitNumber not correct type')
    else:
        incident['ResponsibleUnitNumber'] = ''
    if('WhenTimeoutWillExpire' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['WhenTimeoutWillExpire'],str)):
            place_holder_variable = True
        else:
            print(incident['WhenTimeoutWillExpire'])
            return(False,'WhenTimeoutWillExpire not correct type')
    else:
        incident['WhenTimeoutWillExpire'] = ''
    if('PlateStateAbbreviation' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['PlateStateAbbreviation'],str)):
            place_holder_variable = True
        else:
            print(incident['PlateStateAbbreviation'])
            return(False,'PlateStateAbbreviation not correct type')
    else:
        incident['PlateStateAbbreviation'] = ''
    if('CityCode' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CityCode'],str)):
            place_holder_variable = True
        else:
            print(incident['CityCode'])
            return(False,'CityCode not correct type')
    else:
        incident['CityCode'] = ''
    #here
    if('CADMasterCallTable' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CADMasterCallTable'],dict)):
            if('CallPriority' in incident['CADMasterCallTable'].keys()):
                place_holder_variable = True
                if(isinstance(incident['CADMasterCallTable']['CallPriority'],str)):
                    place_holder_variable = True
                else:
                    print(incident['CADMasterCallTable']['CallPriority'])
                    return(False,'CallPriority not correct type')
            else:
                incident['CADMasterCallTable']['CallPriority'] = ''
            if('TimeDateOccurredEarliest' in incident['CADMasterCallTable'].keys()):
                place_holder_variable = True
                if(isinstance(incident['CADMasterCallTable']['TimeDateOccurredEarliest'],str)):
                    place_holder_variable = True
                else:
                    print(incident['CADMasterCallTable']['TimeDateOccurredEarliest'])
                    return(False,'TimeDateOccurredEarliest not correct type')
            else:
                incident['CADMasterCallTable']['TimeDateOccurredEarliest'] = ''
            if('TimeDateOccurredLatest' in incident['CADMasterCallTable'].keys()):
                place_holder_variable = True
                if(isinstance(incident['CADMasterCallTable']['TimeDateOccurredLatest'],str)):
                    place_holder_variable = True
                else:
                    print(incident['CADMasterCallTable']['TimeDateOccurredLatest'])
                    return(False,'TimeDateOccurredLatest not correct type')
            else:
                incident['CADMasterCallTable']['TimeDateOccurredLatest'] = ''
            if('CityCode' in incident['CADMasterCallTable'].keys()):
                place_holder_variable = True
                if(isinstance(incident['CADMasterCallTable']['CityCode'],str)):
                    place_holder_variable = True
                else:
                    print(incident['CADMasterCallTable']['CityCode'])
                    return(False,'CityCode not correct type')
            else:
                incident['CADMasterCallTable']['CityCode'] = ''
            if('ComplainantNameNumber' in incident['CADMasterCallTable'].keys()):
                place_holder_variable = True
                if(isinstance(incident['CADMasterCallTable']['ComplainantNameNumber'],str)):
                    place_holder_variable = True
                else:
                    print(incident['CADMasterCallTable']['ComplainantNameNumber'])
                    return(False,'ComplainantNameNumber not correct type')
            else:
                incident['CADMasterCallTable']['ComplainantNameNumber'] = ''
            if('CallReceived' in incident['CADMasterCallTable'].keys()):
                place_holder_variable = True
                if(isinstance(incident['CADMasterCallTable']['CallReceived'],str)):
                    place_holder_variable = True
                else:
                    print(incident['CADMasterCallTable']['CallReceived'])
                    return(False,'CallReceived not correct type')
            else:
                incident['CADMasterCallTable']['CallReceived'] = ''
            if('TimeDateReported' in incident['CADMasterCallTable'].keys()):
                place_holder_variable = True
                if(isinstance(incident['CADMasterCallTable']['TimeDateReported'],str)):
                    place_holder_variable = True
                else:
                    print(incident['CADMasterCallTable']['TimeDateReported'])
                    return(False,'TimeDateReported not correct type')
            else:
                incident['CADMasterCallTable']['TimeDateReported'] = ''
            if('GeobaseAddressID' in incident['CADMasterCallTable'].keys()):
                place_holder_variable = True
                if(isinstance(incident['CADMasterCallTable']['GeobaseAddressID'],str)):
                    place_holder_variable = True
                else:
                    print(incident['CADMasterCallTable']['GeobaseAddressID'])
                    return(False,'GeobaseAddressID not correct type')
            else:
                incident['CADMasterCallTable']['GeobaseAddressID'] = ''
            if('HowReceived' in incident['CADMasterCallTable'].keys()):
                place_holder_variable = True
                if(isinstance(incident['CADMasterCallTable']['HowReceived'],str)):
                    place_holder_variable = True
                else:
                    print(incident['CADMasterCallTable']['HowReceived'])
                    return(False,'HowReceived not correct type')
            else:
                incident['CADMasterCallTable']['HowReceived'] = ''
            if('PersonToContact' in incident['CADMasterCallTable'].keys()):
                place_holder_variable = True
                if(isinstance(incident['CADMasterCallTable']['PersonToContact'],str)):
                    place_holder_variable = True
                else:
                    print(incident['CADMasterCallTable']['PersonToContact'])
                    return(False,'PersonToContact not correct type')
            else:
                incident['CADMasterCallTable']['PersonToContact'] = ''
            if('RecordNumber' in incident['CADMasterCallTable'].keys()):
                place_holder_variable = True
                if(isinstance(incident['CADMasterCallTable']['RecordNumber'],str)):
                    place_holder_variable = True
                else:
                    print(incident['CADMasterCallTable']['RecordNumber'])
                    return(False,'RecordNumber not correct type')
            else:
                incident['CADMasterCallTable']['RecordNumber'] = ''
            if('CallTaker' in incident['CADMasterCallTable'].keys()):
                place_holder_variable = True
                if(isinstance(incident['CADMasterCallTable']['CallTaker'],str)):
                    place_holder_variable = True
                else:
                    print(incident['CADMasterCallTable']['CallTaker'])
                    return(False,'CallTaker not correct type')
            else:
                incident['CADMasterCallTable']['CallTaker'] = ''
            if('CallNature' in incident['CADMasterCallTable'].keys()):
                place_holder_variable = True
                if(isinstance(incident['CADMasterCallTable']['CallNature'],str)):
                    place_holder_variable = True
                else:
                    print(incident['CADMasterCallTable']['CallNature'])
                    return(False,'CallNature not correct type')
            else:
                incident['CADMasterCallTable']['CallNature'] = ''
            if('CallTypeLawFireEMS' in incident['CADMasterCallTable'].keys()):
                place_holder_variable = True
                if(isinstance(incident['CADMasterCallTable']['CallTypeLawFireEMS'],str)):
                    place_holder_variable = True
                else:
                    print(incident['CADMasterCallTable']['CallTypeLawFireEMS'])
                    return(False,'CallTypeLawFireEMS not correct type')
            else:
                incident['CADMasterCallTable']['CallTypeLawFireEMS'] = ''
            if('PersonToContactPhone' in incident['CADMasterCallTable'].keys()):
                place_holder_variable = True
                if(isinstance(incident['CADMasterCallTable']['PersonToContactPhone'],str)):
                    place_holder_variable = True
                else:
                    print(incident['CADMasterCallTable']['PersonToContactPhone'])
                    return(False,'PersonToContactPhone not correct type')
            else:
                incident['CADMasterCallTable']['PersonToContactPhone'] = ''
            if('RespondToAddress' in incident['CADMasterCallTable'].keys()):
                place_holder_variable = True
                if(isinstance(incident['CADMasterCallTable']['RespondToAddress'],str)):
                    place_holder_variable = True
                else:
                    print(incident['CADMasterCallTable']['RespondToAddress'])
                    return(False,'RespondToAddress not correct type')
            else:
                incident['CADMasterCallTable']['RespondToAddress'] = ''
            CadMaster_keys = {'CallTypeLawFireEMS': 'CallTypeLawFireEMS', 'PlateNumberOfStoppedVeh': 'PlateNumberOfStoppedVeh', 'PersonToContact': 'PersonToContact', 'TimeDateOccurredEarliest': 'TimeDateOccurredEarliest', 'TimeDateOccurredLatest': 'TimeDateOccurredLatest', 'CallTaker': 'CallTaker', 'CallPriority': 'CallPriority', 'PersonToContactPhone': 'PersonToContactPhone', 'HowReceived': 'HowReceived', 'PlateStateAbbreviation': 'PlateStateAbbreviation', 'CallNature': 'CallNature', 'RespondToAddress': 'RespondToAddress', 'GeobaseAddressID': 'GeobaseAddressID', 'CityCode': 'CityCode', 'ComplainantNameNumber': 'ComplainantNameNumber', 'CallReceived': 'CallReceived', 'RecordNumber': 'RecordNumber', 'TimeDateReported': 'TimeDateReported'}
            for CadMaster_incident_key in incident['CADMasterCallTable'].keys():
                    if CadMaster_incident_key not in CadMaster_keys.keys():
                        print(CadMaster_incident_key)
                        text = open(email_file,'r')
                        email_string = text.read()
                        email_json = universal_imports.json.loads(email_string)
                        sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'nhp warning', message = "nhp clean warning unknown incident_key CadMasterCallTable "+CadMaster_incident_key +" incident " + incident['LongTermCallID'] + " in file "+ filename, login = email_json["email"], password = email_json["password"])             
   
        else:
            print(incident['CADMasterCallTable'])
            return(False,'CADMasterCallTable not correct type')
    else:
        incident['CADMasterCallTable'] = ''
    if('StatusCodeOfCall' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['StatusCodeOfCall'],str)):
            place_holder_variable = True
        else:
            print(incident['StatusCodeOfCall'])
            return(False,'StatusCodeOfCall not correct type')
    else:
        incident['StatusCodeOfCall'] = ''
    if('CallZoneCode' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CallZoneCode'],str)):
            place_holder_variable = True
        else:
            print(incident['CallZoneCode'])
            return(False,'CallZoneCode not correct type')
    else:
        incident['CallZoneCode'] = ''
    
    if('YCoordinateGeobase' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['YCoordinateGeobase'],str)):
            place_holder_variable = True
        else:
            print(incident['YCoordinateGeobase'])
            return(False,'YCoordinateGeobase not correct type')
    else:
        incident['YCoordinateGeobase'] = ''
    if('CallNumber' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['CallNumber'],str)):
            place_holder_variable = True
        else:
            print(incident['CallNumber'])
            return(False,'CallNumber not correct type')
    else:
        incident['CallNumber'] = ''
    if('Disposition' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['Disposition'],str)):
            place_holder_variable = True
        else:
            print(incident['Disposition'])
            return(False,'Disposition not correct type')
    else:
        incident['Disposition'] = ''
    if('MainRadioLogTable' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['MainRadioLogTable'],dict)):
            incident['MainRadioLogTable'] = [incident['MainRadioLogTable']]
        if(isinstance(incident['MainRadioLogTable'],list)):
            for i in range(0,len(incident['MainRadioLogTable'])):
                if('AgencyCode' in incident['MainRadioLogTable'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(incident['MainRadioLogTable'][i]['AgencyCode'],str)):
                        place_holder_variable = True
                    else:
                        print(incident['MainRadioLogTable'][i]['AgencyCode'])
                        return(False,'AgencyCode not correct type')
                else:
                    incident['MainRadioLogTable'][i]['AgencyCode'] = ''
                if('UserWhoLoggedCall' in incident['MainRadioLogTable'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(incident['MainRadioLogTable'][i]['UserWhoLoggedCall'],str)):
                        place_holder_variable = True
                    else:
                        print(incident['MainRadioLogTable'][i]['UserWhoLoggedCall'])
                        return(False,'UserWhoLoggedCall not correct type')
                else:
                    incident['MainRadioLogTable'][i]['UserWhoLoggedCall'] = ''
                if('Shift' in incident['MainRadioLogTable'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(incident['MainRadioLogTable'][i]['Shift'],str)):
                        place_holder_variable = True
                    else:
                        print(incident['MainRadioLogTable'][i]['Shift'])
                        return(False,'Shift not correct type')
                else:
                    incident['MainRadioLogTable'][i]['Shift'] = ''
                if('SequenceNumber' in incident['MainRadioLogTable'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(incident['MainRadioLogTable'][i]['SequenceNumber'],str)):
                        place_holder_variable = True
                    else:
                        print(incident['MainRadioLogTable'][i]['SequenceNumber'])
                        return(False,'SequenceNumber not correct type')
                else:
                    incident['MainRadioLogTable'][i]['SequenceNumber'] = ''
                if('GeoYCoordinateOfUnit' in incident['MainRadioLogTable'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(incident['MainRadioLogTable'][i]['GeoYCoordinateOfUnit'],str)):
                        place_holder_variable = True
                    else:
                        print(incident['MainRadioLogTable'][i]['GeoYCoordinateOfUnit'])
                        return(False,'GeoYCoordinateOfUnit not correct type')
                else:
                    incident['MainRadioLogTable'][i]['GeoYCoordinateOfUnit'] = ''
                if('TimeDateOfEntry' in incident['MainRadioLogTable'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(incident['MainRadioLogTable'][i]['TimeDateOfEntry'],str)):
                        place_holder_variable = True
                    else:
                        print(incident['MainRadioLogTable'][i]['TimeDateOfEntry'])
                        return(False,'TimeDateOfEntry not correct type')
                else:
                    incident['MainRadioLogTable'][i]['TimeDateOfEntry'] = ''
                if('UnitZoneCode' in incident['MainRadioLogTable'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(incident['MainRadioLogTable'][i]['UnitZoneCode'],str)):
                        place_holder_variable = True
                    else:
                        print(incident['MainRadioLogTable'][i]['UnitZoneCode'])
                        return(False,'UnitZoneCode not correct type')
                else:
                    incident['MainRadioLogTable'][i]['UnitZoneCode'] = ''
                if('TenCode' in incident['MainRadioLogTable'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(incident['MainRadioLogTable'][i]['TenCode'],str)):
                        place_holder_variable = True
                    else:
                        print(incident['MainRadioLogTable'][i]['TenCode'])
                        return(False,'TenCode not correct type')
                else:
                    incident['MainRadioLogTable'][i]['TenCode'] = ''
                if('LongTermCallID' in incident['MainRadioLogTable'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(incident['MainRadioLogTable'][i]['LongTermCallID'],str)):
                        place_holder_variable = True
                    else:
                        print(incident['MainRadioLogTable'][i]['LongTermCallID'])
                        return(False,'LongTermCallID not correct type')
                else:
                    incident['MainRadioLogTable'][i]['LongTermCallID'] = ''
                if('CallType' in incident['MainRadioLogTable'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(incident['MainRadioLogTable'][i]['CallType'],str)):
                        place_holder_variable = True
                    else:
                        print(incident['MainRadioLogTable'][i]['CallType'])
                        return(False,'CallType not correct type')
                else:
                    incident['MainRadioLogTable'][i]['CallType'] = ''
                if('UnitNumber' in incident['MainRadioLogTable'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(incident['MainRadioLogTable'][i]['UnitNumber'],str)):
                        place_holder_variable = True
                    else:
                        print(incident['MainRadioLogTable'][i]['UnitNumber'])
                        return(False,'UnitNumber not correct type')
                else:
                    incident['MainRadioLogTable'][i]['UnitNumber'] = ''
                if('Description' in incident['MainRadioLogTable'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(incident['MainRadioLogTable'][i]['Description'],str)):
                        place_holder_variable = True
                    else:
                        print(incident['MainRadioLogTable'][i]['Description'])
                        return(False,'Description not correct type')
                else:
                    incident['MainRadioLogTable'][i]['Description'] = ''
                if('GeoXCoordinateOfUnit' in incident['MainRadioLogTable'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(incident['MainRadioLogTable'][i]['GeoXCoordinateOfUnit'],str)):
                        place_holder_variable = True
                    else:
                        print(incident['MainRadioLogTable'][i]['GeoXCoordinateOfUnit'])
                        return(False,'GeoXCoordinateOfUnit not correct type')
                else:
                    incident['MainRadioLogTable'][i]['GeoXCoordinateOfUnit'] = ''
                               
                MainRadioLogTableKeys = {'GeoYCoordinateOfUnit': 'GeoYCoordinateOfUnit', 'AgencyCode': 'AgencyCode', 'SequenceNumber': 'SequenceNumber', 'Shift': 'Shift', 'Description': 'Description', 'TenCode': 'TenCode', 'UnitNumber': 'UnitNumber', 'CallType': 'CallType', 'UserWhoLoggedCall': 'UserWhoLoggedCall', 'UnitZoneCode': 'UnitZoneCode', 'TimeDateOfEntry': 'TimeDateOfEntry', 'GeoXCoordinateOfUnit': 'GeoXCoordinateOfUnit', 'LongTermCallID': 'LongTermCallID'}
                for MainRadioLogTable_incident_key in incident['MainRadioLogTable'][i].keys():
                    if MainRadioLogTable_incident_key not in MainRadioLogTableKeys.keys():
                        print(MainRadioLogTable_incident_key)
                        text = open(email_file,'r')
                        email_string = text.read()
                        email_json = universal_imports.json.loads(email_string)
                        sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'nhp warning', message = "nhp clean warning unknown MainRadioLogTable incident_key "+MainRadioLogTable_incident_key +" incident " +incident['LongTermCallID']+ " in file "+ filename, login = email_json["email"], password = email_json["password"])             

        else:
            print(incident['MainRadioLogTable'])
            return(False,'MainRadioLogTable not correct type')
    else:
        incident['MainRadioLogTable'] = ''
    if('TimeoutHasExpired' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['TimeoutHasExpired'],str)):
            place_holder_variable = True
        else:
            print(incident['TimeoutHasExpired'])
            return(False,'TimeoutHasExpired not correct type')
    else:
        incident['TimeoutHasExpired'] = ''
    if('RespondToAddress' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['RespondToAddress'],str)):
            place_holder_variable = True
        else:
            print(incident['RespondToAddress'])
            return(False,'RespondToAddress not correct type')
    else:
        incident['RespondToAddress'] = ''
    if('WhenStatusDeclared' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['WhenStatusDeclared'],str)):
            place_holder_variable = True
        else:
            print(incident['WhenStatusDeclared'])
            return(False,'WhenStatusDeclared not correct type')
    else:
        incident['WhenStatusDeclared'] = ''
    if('ClearanceCodeLawOnly'  in incident.keys()):
        if(isinstance(incident['ClearanceCodeLawOnly'],str)):
            place_holder_variable = True
        else:
            print(incident['ClearanceCodeLawOnly'])
            return(False,'ClearanceCodeLawOnly not correct type')
    else:
        incident['ClearanceCodeLawOnly'] = ''   
    if('HoldUntilTimeDate'  in incident.keys()):
        if(isinstance(incident['HoldUntilTimeDate'],str)):
            place_holder_variable = True
        else:
            print(incident['HoldUntilTimeDate'])
            return(False,'HoldUntilTimeDate not correct type')
    else:
        incident['HoldUntilTimeDate'] = ''    
    if('RelatedRecordNumber' in incident.keys()):
        place_holder_variable = True
        if(isinstance(incident['RelatedRecordNumber'],str)):
            place_holder_variable = True
        else:
            print(incident['RelatedRecordNumber'])
            return(False,'RelatedRecordNumber not correct type')
    else:
        incident['RelatedRecordNumber'] = ''
    known_keys = {'PlateNumberOfStoppedVeh': 'PlateNumberOfStoppedVeh', 'HowManyTimesTextChanged': 'HowManyTimesTextChanged', 'AgencyCode': 'AgencyCode', 'StatusCodeOfCall': 'StatusCodeOfCall', 'GeobaseAddressID': 'GeobaseAddressID', 'WhenCallWasOpened': 'WhenCallWasOpened', 'TimeDateReported': 'TimeDateReported', 'Disposition': 'Disposition', 'CallZoneCode': 'CallZoneCode', 'CallNumber': 'CallNumber', 'DispatchLevelStatus': 'DispatchLevelStatus', 'RelatedRecordNumber': 'RelatedRecordNumber', 'HighBitsOfXCoordinate': 'HighBitsOfXCoordinate', 'WhenStatusDeclared': 'WhenStatusDeclared', 'LongTermCallID': 'LongTermCallID', 'PlateStateAbbreviation': 'PlateStateAbbreviation', 'CityCode': 'CityCode', 'WhenTimeoutWillExpire': 'WhenTimeoutWillExpire', 'CallTypeLawFireEMS': 'CallTypeLawFireEMS', 'XCoordinateGeobase': 'XCoordinateGeobase', 'MainRadioLogTable': 'MainRadioLogTable', 'IncidentNature': 'IncidentNature', 'RespondToAddress': 'RespondToAddress', 'YCoordinateGeobase': 'YCoordinateGeobase', 'ResponsibleUnitNumber': 'ResponsibleUnitNumber', 'CADMasterCallTable': 'CADMasterCallTable', 'CallPriority': 'CallPriority', 'TimeoutHasExpired': 'TimeoutHasExpired','ClearanceCodeLawOnly':'ClearanceCodeLawOnly','HoldUntilTimeDate':'HoldUntilTimeDate'}
    for incident_key in incident.keys():
        if incident_key not in known_keys.keys():
            print("unknown key found "+incident_key)
            text = open(email_file,'r')
            email_string = text.read()
            email_json = universal_imports.json.loads(email_string)
            sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'nhp warning', message = "nhp "+"clean warning unknown incident_key "+incident_key +" incident "+incident['LongTermCallID']+" in file "+ filename, login = email_json["email"], password = email_json["password"])             
    return(True,'no error',incident)


def nhp_clean(hiarchy,filename):
    text = open(email_file,'r')
    email_string = text.read()
    email_json = universal_imports.json.loads(email_string)      
    if('PublicSafetyEnvelope' in hiarchy.keys()):
        place_holder_variable = True
        if(isinstance(hiarchy['PublicSafetyEnvelope'],dict)):
            if('PublicSafety' in hiarchy['PublicSafetyEnvelope'].keys()):
                place_holder_variable = True
                if(isinstance(hiarchy['PublicSafetyEnvelope']['PublicSafety'],dict)):
                    if('Response' in hiarchy['PublicSafetyEnvelope']['PublicSafety'].keys()):
                        if(isinstance(hiarchy['PublicSafetyEnvelope']['PublicSafety']['Response'],dict)):
                            if('CADActiveCallTable' in hiarchy['PublicSafetyEnvelope']['PublicSafety']['Response'].keys()):
                                if(isinstance(hiarchy['PublicSafetyEnvelope']['PublicSafety']['Response']['CADActiveCallTable'],dict)):
                                    hiarchy['PublicSafetyEnvelope']['PublicSafety']['Response']['CADActiveCallTable'] = [hiarchy['PublicSafetyEnvelope']['PublicSafety']['Response']['CADActiveCallTable']]
                                if(isinstance(hiarchy['PublicSafetyEnvelope']['PublicSafety']['Response']['CADActiveCallTable'],list)):
                                    for i in range(0,len(hiarchy['PublicSafetyEnvelope']['PublicSafety']['Response']['CADActiveCallTable'])):
                                        print(hiarchy['PublicSafetyEnvelope']['PublicSafety']['Response']['CADActiveCallTable'][i])
                                        if (isinstance(hiarchy['PublicSafetyEnvelope']['PublicSafety']['Response']['CADActiveCallTable'][i],dict)):
                                            result = nhp_incident_clean(hiarchy['PublicSafetyEnvelope']['PublicSafety']['Response']['CADActiveCallTable'][i],filename)
                                            if result[0] == True:
                                                hiarchy['PublicSafetyEnvelope']['PublicSafety']['Response']['CADActiveCallTable'][i] = result[2]
                                            else:
                                                if 'LongTermCallID' in hiarchy['PublicSafetyEnvelope']['PublicSafety']['Response']['CADActiveCallTable'][i].keys():
                                                    sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'nhp clean error', message = "nhp clean error filename is " + filename + " LongTermCallID is " + hiarchy['PublicSafetyEnvelope']['PublicSafety']['Response']['CADActiveCallTable'][i]['LongTermCallID'] + " compromised", login = email_json["email"], password = email_json["password"]) 
                                                else:
                                                    sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'nhp clean error', message = "nhp clean error LongTermCallID unknown compromised filename is "+filename, login = email_json["email"], password = email_json["password"]) 
                                                hiarchy['PublicSafetyEnvelope']['PublicSafety']['Response']['CADActiveCallTable'][i] = {'compromised':'compromised'}
                                        else:
                                            hiarchy['PublicSafetyEnvelope']['PublicSafety']['Response']['CADActiveCallTable'][i] = {'compromised':'compromised'}                                            
                                            sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'nhp clean error', message = "nhp clean error LongTermCallID unknown compromised filename is "+filename, login = email_json["email"], password = email_json["password"]) 
                                            
                                else:
                                    print(hiarchy['PublicSafetyEnvelope']['PublicSafety']['Response']['CADActiveCallTable'])
                                    return(False,'CADActiveCallTable not correct type')
                            else:
                                hiarchy['PublicSafetyEnvelope']['PublicSafety']['Response']['CADActiveCallTable'] = []
                        else:
                            print(hiarchy['PublicSafetyEnvelope']['PublicSafety']['Response'])
                            return(False,'Response not correct type')
                    else:
                        hiarchy['PublicSafetyEnvelope']['PublicSafety']['Response'] = ''
                else:
                    print(hiarchy['PublicSafetyEnvelope']['PublicSafety'])
                    return(False,'PublicSafety not correct type')
            else:
                hiarchy['PublicSafetyEnvelope']['PublicSafety'] = ''
        else:
            print(hiarchy['PublicSafetyEnvelope'])
            return(False,'PublicSafetyEnvelope not correct type')
    else:
        hiarchy['PublicSafetyEnvelope'] = ''
    return(True,hiarchy)
