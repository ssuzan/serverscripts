try:
    #tests for existing CADActiveCallTable
    incidents = clean_results[1]['PublicSafetyEnvelope']['PublicSafety']['Response']['CADActiveCallTable']
    doc_json = clean_results[1]
    results = nhp_store.nhp_store(curr_t,doc_xml,True, incidents,hash_log,default_age,dump_output_to)                                  
    hashes = results[0]
    repeat_hashes = results[1]
    for key in hashes:
        hash_log[key] = hashes[key]
    for key in hash_log.keys():
        if key not in repeat_hashes and key not in hashes.keys():
            hash_log[key][1]-=1
            if hash_log[key][1] == 0:
                dead_hashes.append(key)
        else:
            hash_log[key][1] = default_age
    for i in range(0,len(dead_hashes)):
        del hash_log[dead_hashes[i]]                      
except:
    sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'nhp error', message = "nhp "+"store error "+filename, login = email_json["email"], password = email_json["password"]) 
    print("store error")
    nhp_store.nhp_store(curr_t,doc_xml,False,doc_xml,hash_log,default_age,dump_output_to)
    for key in hash_log.keys():
        hash_log[key][1]-=1
        if hash_log[key][1] == 0:
            dead_hashes.append(key)
    for i in range(0,len(dead_hashes)):
        del hash_log[dead_hashes[i]]     