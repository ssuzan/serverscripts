#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
#sudo easy_install3 wget
import os
import sys
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports
import push_to_fifo
import push_to_fifo_dev
import sendemail
from email.mime.text import MIMEText
email_file = os.environ['WORKSPACE']+'/serverscripts/globals/emails'
    
def store_original(original_store_xml, save_path,dump_to_output,filename):
    DeviceFile = open(save_path+"/database_original/"+filename+'.xml', 'wb')
    DeviceFile.write(original_store_xml)  
    DeviceFile.close()    

    
def store(original_sensor_data,cleaned_sensor_data,is_detector_report,save_path,dump_output_to,type_name,filename, is_prod, is_dev):
    #print(original_sensor_data)
    curr_t = universal_imports.datetime.utcnow().strftime("%d-%m-%Y %H:%M:%S")
    none_flag = dump_output_to not in ["FILE", "FIFO", "ALL"]
    #saves file
    #fullFiletxt = open(save_path+"/database_original/"+filename+'.json', 'w')
    #fullFiletxt.write(universal_imports.json.dumps(original_sensor_data, sort_keys=True, indent=4, separators=(',', ': ')))  
    if (none_flag == True):
        print("store_fast_sensors- dump_output_to is set to NONE")
    else :
        if (is_detector_report == True):    
             #saves individual files
            new_store_json = {'type':type_name,'date_time':str(curr_t).replace("-","_")+' UTC','suspicious':    False,'error':False,'data':cleaned_sensor_data['DetectorData']['collection-period']['collection-period-item']['detector-reports']}

            if ((dump_output_to == "FILE") or (dump_output_to == "ALL")):
                filename_curr = filename+"_sensors"
                incidentFiletxt = open(save_path+"/database/"+filename_curr+'.json', 'w')
                incidentFiletxt.write(universal_imports.json.dumps(new_store_json, sort_keys=True, indent=4, separators=(',', ': ')))

            #sends file to FIFO
            if ((dump_output_to == "FIFO") or (dump_output_to == "ALL")):
                data_fifo = universal_imports.json.dumps(new_store_json)
                try:
                    if (is_prod):
                        #print("pushing to prod FIFO")
                        push_to_fifo.push_to_fifo(data_fifo )         
                except:
                    text = open(email_file,'r')
                    email_string = text.read()
                    email_json = universal_imports.json.loads(email_string)                  
                    sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'error', message = 'fifo error in store_sensors', login = email_json['email'], password = email_json['password'])                  
                #pushing to dev FIFO:
                try:
                    if (is_dev):
                        #print("pushing to dev FIFO")
                        push_to_fifo_dev.push_to_fifo_dev(data_fifo)         
                except:
                    text = open(email_file,'r')
                    email_string = text.read()
                    email_json = universal_imports.json.loads(email_string)                  
                    sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'warning', message = 'dev fifo error in store_sensors', login = email_json['email'], password = email_json['password'])                  
     
    
