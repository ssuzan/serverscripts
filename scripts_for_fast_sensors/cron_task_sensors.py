#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports
import apscheduler
from apscheduler.schedulers.blocking import BlockingScheduler
import call_all_sensors

def scheduled_call():
    print('sensors_scheduled_call started successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
    #initialize new scheduler
    sched = BlockingScheduler(timezone='UTC',coalesce=True)
    sched.add_job(call_all_sensors.call_all_sensors, 'interval', id='job_id1', seconds=60,coalesce=True)
    sched.start()

scheduled_call()
