#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports
import store_sensors
import xmltodict
import requests

def fetch(url_sensors,save_path, dump_output_to,filename, store_original):
    r = requests.get(url_sensors)
    r = xmltodict.parse(r.content)
    r = universal_imports.json.dumps(r, sort_keys=True, indent=4, separators=(',', ': '))
    r = universal_imports.json.loads(r)
 
    try:
        #gets json from url
        r = requests.get(url_sensors)
        if (r.status_code== 200):
            if (store_original):
                store_sensors.store_original(r.content,save_path,dump_output_to,filename)
            try:
                r = xmltodict.parse(r.content)
                r = universal_imports.json.dumps(r, sort_keys=True, indent=4, separators=(',', ': '))
                r = universal_imports.json.loads(r)
            except:
                r = universal_imports.json.loads('{"error" : "fetch_sensors failure to convert to json %s"}'%filename)
        else:
            r = universal_imports.json.loads('{"error" : "fetch_sensors incorrect status_code %s}'%filename)
    except requests.exceptions.ConnectionError:
        r = universal_imports.json.loads('{"error" : "fetch_sensors requests.exceptions.ConnectionError %s"}'%filename)  
    except Exception as e:
        r = universal_imports.json.loads('{"error" : "fetch_sensors exception %s %s"}'%(e,filename))  
    #print(r)
    return r
