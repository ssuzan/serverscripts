# Dor Cohen - 18/5/18
# dor.cohen@waycaretech.com
"""
Module for detecting anomalies on FAST sesnors
- main func is detect_anomalies()
- gets as input output of detect_faults_sensors.detect() on call_all
(a parsed json file)
- reports on statistical-based anomalies
- currently implements:
(1) rolling statistics for speed/count on all lanes (aggregated) per detector
(2) alert on speed > 2 std
"""
import sys
import os
import time
import smtplib
import pickle
import requests
import numpy as np
from datetime import datetime, timedelta
# local modules
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports
import sendemail
import debug

# globals
# get email settings from disk
email_file = workspace_environ + '/serverscripts/globals/emails_dor'
text = open(email_file,'r')
email_string = text.read()
email_json = universal_imports.json.loads(email_string)
# send mail with the following cameras mapping from buggati:
cameras_mapping = {'349_3_152': 117, '26_1_97': 601, '404_2_212': 208, '32_1_142': 115}
# full script path
full_script_path = os.path.dirname(os.path.realpath(sys.argv[0]))

# helpers
def init_dictionary_stats(det_id):
	stats = (0,0,0)
	return {det_id: stats}

def load_values(filename='detectors_stats.pickle'):
	"""Load all detectors lanes data using pickle"""
	# parse filename and add full path
	filename = os.path.join(full_script_path, filename)
	# if file doesn't exist return empty dict
	if not os.path.isfile(filename):
		return {}
	# load file
	with open(filename, 'rb') as handle:
		data = pickle.load(handle)

	return data

def save_values(values, filename='detectors_stats.pickle'):
	"""Save all detectors lanes data using pickle"""
	# parse filename and add full path
	filename = os.path.join(full_script_path, filename)
	# write (delete old file)
	with open(filename, 'wb') as handle:
		pickle.dump(values, handle, protocol=pickle.HIGHEST_PROTOCOL)

	return True

def parse_lane_data(det_id, lanes_data):
	"""Parse lane data: count,speed for each lane - for single detector
	IN: 
	det_id - detector_id to parse
	lanes_data - actual lanes data, as appears on json
	
	OUT: 
	dictionary with format {'detector_id': [(count1,speed1),..., (countN,speedN)]}
	"""
	lanes_num = len(lanes_data)
	lanes_status = {}
	lane_data = []
	# collect count,speed
	for i in range(lanes_num):
		count = lanes_data[i]['lane-vehicle-count']
		speed = lanes_data[i]['lane-vehicle-speed']
		lane_data.append((count,speed))

	lanes_status[det_id] = lane_data
	return lanes_status

# functions for detecting anomalies

def iterate_once(det_list):
	"""Iterate over detector list as appears on json structure,
	returns mapping (det_id -> index) and values (see parse_lane_data)"""
	mapper = {}
	values = {}
	faults = {}
	for index, elem in enumerate(det_list):
		# get index
		det_id = elem['detector-id']
		mapper[det_id] = index
		# get errors
		czsp = None
		error_count, error_speed = None, None
		try:
			czsp = elem['integrity-issue-czsp']
			error_count = elem['error-count']
			error_speed = elem['error-speed']
		except:
			pass
		errors_dict = {det_id: {'czsp': czsp, 'count': error_count, 'speed': error_speed}}
		# get values
		det = det_list[index]
		lanes_data = det['lane-data']['lane-data-item']
		lanes_status = parse_lane_data(det_id, lanes_data)
		# update dict
		values.update(lanes_status)
		faults.update(errors_dict)

	return mapper, values, faults

def update(existingAggregate, newValue):
	"""for a new value newValue, compute the new count, new mean, the new M2.
	mean accumulates the mean of the entire dataset
	M2 aggregates the squared distance from the mean
	count aggregates the number of samples seen so far
	"""
	(count, mean, M2) = existingAggregate
	count = count + 1 
	delta = newValue - mean
	mean = mean + delta / count
	delta2 = newValue - mean
	M2 = M2 + delta * delta2

	return (count, mean, M2)

def finalize(existingAggregate):
	"""retrieve the mean, variance and sample variance from an aggregate"""
	(count, mean, M2) = existingAggregate
	if count < 2:
		return None
	else:
		(mean, variance, sampleVariance) = (mean, M2/count, M2/(count - 1)) 
		return (mean, variance, sampleVariance)

def detect_anomalies(cleaned_sensors_data, current_time):
	det_list = cleaned_sensors_data['DetectorData']['collection-period']['collection-period-item']['detector-reports']['detector-report']
	# iterate once to read values
	mapper, values, errors = iterate_once(det_list)
	# values are {'det_id': [(c1,s1)...], 'det_id2': [(c1,s1)...]...}
	# mapping: (detector_id -> list_indices on json file)
	# errors (example): {det_id: {'czsp': 1010, 'count': '0000', 'speed': 1010}}

	detector_stats = load_values()
	
	# iterate once again to collect values to dict
	for det_id, index in mapper.items():
		# json structure for this det
		det = det_list[index] 
		# get lanes data
		lanes_status_single_det = values[det_id] # [(c1,s1)...]
		values_array = np.array(lanes_status_single_det).astype(int)

		counts = values_array.T[0]
		speeds = values_array.T[1]

		print("Detector: %s" % str(det_id))
		print("Values:", counts, speeds)

		# TODO: should handle stuck speeds

		# filter faulty lanes
		czsp = errors[det_id]['czsp']
		if czsp is not None:
			# convert back to array
			faulty_indices = np.array(list(map(int, czsp)))
			healthy_indices_mask = 1 - faulty_indices
			speeds = np.take(speeds, np.where(healthy_indices_mask))

		# if filtered all speeds don't update statistics
		if speeds.size == 0:
			print("Filtered all values, skipping..")
			print()
			continue

		speeds_median = np.median(speeds)
		counts_sum = np.median(counts)

		print("Speeds filtered:", speeds)
		print("Current stats: count_sum: %d, speeds_median %.3f" % (counts_sum, speeds_median))
		
		# retrieve stats from struct
		if det_id not in detector_stats:
			# detector doesn't exist on disk
			# need to init a dictionary for it
			stats_dict = init_dictionary_stats(det_id) # {'det_id': (0,0,0)}
		else:
			# detector exists
			stats_dict = {det_id: detector_stats[det_id]}

		# update values
		current_values = stats_dict[det_id]
		new_value = speeds_median
		stats_dict[det_id] = update(current_values, new_value)
		detector_stats.update(stats_dict)

		# get mean/std
		res = finalize(stats_dict[det_id])
		if res:
			print("Rolling stats: Mean:%.3f, Std:%.3f" % (res[0], res[2]**0.5))
		else:
			print("Couldn't compute mean/variance")
		
		print()

	save_values(detector_stats)

	return cleaned_sensors_data