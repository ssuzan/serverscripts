# test detect_faults_sensors.py
import os
workspace_environ = str(os.environ['WORKSPACE'])
from detect_faults_sensors import *
import datetime

det_id = '349_3_152'
curr_t = datetime.datetime.utcnow()
email_file = workspace_environ + '/serverscripts/globals/emails_dor'
text = open(email_file,'r')
email_string = text.read()
email_json = universal_imports.json.loads(email_string)
send_report(det_id, curr_t, email_json)