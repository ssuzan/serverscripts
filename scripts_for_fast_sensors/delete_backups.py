import os
workspace_environ = str(os.environ['WORKSPACE'])

# max files to keep
max_files = 120

def sorted_ls(path):
	mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
	return list(sorted(os.listdir(path), key=mtime))

def delete_old_backups(files_num=1):
	"""Delete most old files"""
	db_backups_path = os.path.join(workspace_environ, 'DB_backups', 'data_from_fast_sensors')
	original_path = os.path.join(db_backups_path, 'database_original')
	stored_path = os.path.join(db_backups_path, 'database')
	original = sorted_ls(original_path)
	stored = sorted_ls(stored_path)

	for elem, path in [(original, original_path), (stored, stored_path)]:
		del_list = elem[0:(len(elem)-max_files)]
		for dfile in del_list:
			path_to_delete = os.path.join(path, dfile)
			os.remove(path_to_delete)
		print("delete_backups: Deleted %d items" % len(del_list))
