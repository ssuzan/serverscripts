#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports
import apscheduler
from apscheduler.schedulers.blocking import BlockingScheduler
import call_all_sensors_dev
import delete_backups

def scheduled_call():
    print('sensors_dev_scheduled_call started successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
    #initialize new scheduler
    sched = BlockingScheduler(timezone='UTC',coalesce=True)
    sched.add_job(call_all_sensors_dev.call_all_sensors_dev, 'interval', id='job_id1', seconds=60, coalesce=True)
    #sched.add_job(delete_backups.delete_old_backups, 'interval', id='job_id2', seconds=3600, coalesce=True)
    sched.start()

scheduled_call()
