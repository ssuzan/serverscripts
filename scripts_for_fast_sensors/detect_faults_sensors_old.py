"""
Dor Cohen - 14/5/18
dor.cohen@waycaretech.com

Module for detecting faults on FAST sensors
- main function is detect()
- called between clean_sensors and store_sensors
- gets as input parsed json file
- reports on faulty sensors, currently implements:
-- stuck values rule: detector reports the same values of (count,speed)
"""
import sys
import os
import time
import smtplib
import pickle
import requests
from datetime import datetime, timedelta
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
# local modules
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports

# globals
email_file = workspace_environ + '/serverscripts/globals/emails_dor'
# mapping from json ids to detector_id
mapping_file = 'detectors_mapping_json.pickle' # unused
chosen_detectors = ['349_3_152', '26_1_97', '404_2_212', '32_1_142'] # limited for development
# cameras mapping from buggati
cameras_mapping = {'349_3_152': 117, '26_1_97': 601, '404_2_212': 208, '32_1_142': 115}

def get_script_path():
	"""Helper for getting the script path"""
	return os.path.dirname(os.path.realpath(sys.argv[0]))

def cam_feed(det_id):
	"""Returns current cameras feed in bytes code
	   IN: det_id as detector_id
	"""
	feed_id = cameras_mapping[det_id]
	url = "http://bugatti.nvfast.org/Snapshots/%s.jpeg" % str(feed_id)
	req = requests.get(url)
	image_bytes = req.content
	return image_bytes

def send_report(det_id,
				current_time,
				email_json,
				smtp_server='smtp.gmail.com:587'):
	"""Send an email report if detected fault, message includes
	an image from the nearest detector.
	   IN: det_id - detector_id that is faulty
		   current_time - time fault detected on datetime type
		   email_json - a dictionary with all relevant information
		   for sending mail
		   smtp_server - send from
	"""
	# unpack settings
	from_addr = email_json['email']
	to_addr_list = email_json['send_emails']
	login = email_json['email']
	password = email_json['password']
	
	# message content
	subject = "LV Fast sensors: Detector %s stuck" % det_id
	datetime_str = current_time.strftime("%d-%m-%Y %H:%M")
	message = "Sensor %s is stuck, current time (UTC): %s" % (det_id, datetime_str)

	# generate message
	msg = MIMEMultipart()
	msg['Subject'] = subject
	msg['From'] = from_addr
	msg['To'] = to_addr_list
	mime_text = MIMEText(message)
	# attach picture
	image_bytes = cam_feed(det_id)
	mime_img = MIMEImage(image_bytes)
	msg.attach(mime_text)
	msg.attach(mime_img)

	# connect and send
	s = smtplib.SMTP(smtp_server)
	s.starttls()
	s.login(login, password)
	s.send_message(msg)
	s.quit()

def parse_det_id(lanes_status):
	"""Parse det_id from lanes_status dictionary
	   IN: lanes_status - as returned from parse_lane_data
	   OUT: None if failed to parse, det_id o.w.
	"""
	dict_keys = [*lanes_status] # unpack iterable, python >= 3.5
	if not dict_keys:
		print("lanes_status doesn't contain detector_id")
		return None
	det_id = dict_keys[0]
	return det_id

def generate_filename(det_id,
					  date_time):
	"""Generate data filename
	   IN: det_id - detector's id as marked on buggati
		   date_time - date in datetime type
	   OUT: string indicating the filename
	"""
	# parse date to string
	filename = str(det_id) + '.pickle'
	path = get_script_path()
	# add full path
	filename = os.path.join(path, filename)
	return filename

def compare_lanes_data(prev_lanes_status,
					   curr_lanes_status):
	"""Compares the lanes status from both times
	   IN: prev_lanes_status - previous lanes_status as returned from parse_lane_data
		   curr_lanes_status - current lanes_status
	   OUT: True if exactly same values
			False o.w.
	"""
	# compare two dictionaries
	res = prev_lanes_status == curr_lanes_status
	return res

def save_lanes_data(lanes_status,
					current_time):
	"""Saves lanes data using pickle
	   IN: lanes_status - dictionary with the lanes count/speeds
		   as returned from parse lane data. key is the detector id 
		   current_time - current time for saving the data
	   OUT: True if succeeded, False o.w.
	""" 
	# parse detector_id from dictionary
	det_id = parse_det_id(lanes_status)
	if det_id == None:
		return False
	# generate filename and save
	filename = generate_filename(det_id, current_time)
	with open(filename, 'wb') as handle:
		pickle.dump(lanes_status, handle, protocol=pickle.HIGHEST_PROTOCOL)

	return True

def load_lanes_data(det_id, 
					date_time):
	"""load lanes data using pickle
	   IN: det_id - the id as noted on buggati.
		   date_time - the datetime for parsing the filename
	   OUT: lanes_status - dictionary with the lanes count/speeds
			as returned from parse lane data
			False - if file doesn't exist
	"""
	# validate det_id
	if det_id == None:
		return False
	# parse filename
	filename = generate_filename(det_id, date_time)
	if not os.path.isfile(filename):
		return False
	# load file
	with open(filename, 'rb') as handle:
		data = pickle.load(handle)

	return data

def is_stuck(lanes_status,
			 current_time):
	"""1st order rule: Returns True/False if a sensor is stuck -
	   stuck means values (speed/count) are exactly the same
	   IN: lanes_status - dictionary with key as detector_id and 
		   a list as value with (speed,count) tuples, each tuple 
		   indicates on a different lane, order is  fixed.
		   current_time - UTC time as datetime type
	   OUT: True if stuck, False o.w.
	"""
	# parse detector_id
	det_id = parse_det_id(lanes_status)
	if det_id == None:
		return False
	# get current time (in datetime type)
	current = current_time
	# get previous time
	delta = timedelta(minutes=1)
	previous = current - delta
	previous_lanes_status = load_lanes_data(det_id, previous)
	# if file does exist
	if previous_lanes_status is not False:
		res = compare_lanes_data(previous_lanes_status, lanes_status)
		if res == True:
			return_value = True
		else:
			return_value = False
	else:
		return_value = False

	# save file
	ret = save_lanes_data(lanes_status, current)
	if ret:
		print("Saving data for detector %s succeeded." % str(det_id))
	else:
		print("Saving data for detector %s failed." % str(det_id))

	return return_value

def parse_lane_data(det_id, 
					lanes_data):
	"""Parse lane data: count,speed for each lane
	   IN: det_id - detector_id to parse
		   lanes_data - actual lanes data, as appears on json
	   OUT: dictionary with format {'detector_id': [(count1,speed1),..., (countN,speedN)]}
	"""
	lanes_num = len(lanes_data)
	lanes_status = {}
	lane_data = []
	# collect count,speed
	for i in range(lanes_num):
		count = lanes_data[i]['lane-vehicle-count']
		speed = lanes_data[i]['lane-vehicle-speed']
		lane_data.append((count,speed))

	lanes_status[det_id] = lane_data
	return lanes_status	

def locate_detector(det_list, 
					det_id):
	"""Locate detector index in list, each detector on json file is given
	   an index, this index is different than the detector_id
	   IN: det_list - as parsed from json file
		   det_id - detector's id to select
	   OUT: index on list
			None if not found 
	"""
	t = time.time()
	ret_value = None
	for index, elem in enumerate(det_list):
		if elem['detector-id'] == det_id:
			ret_value = index
			break

	time_elapsed = time.time() - t
	print("Locating detector on json took %lf seconds" % time_elapsed)
	return ret_value

def alert_stuck_sensor(det_id, 
					   current_time):
	"""Alerts on stuck sensor, sends an email with details
	   IN: det_id - detector_id as in buggati
		   current_time - in datetime format
	"""
	# get email settings from disk
	text = open(email_file,'r')
	email_string = text.read()
	email_json = universal_imports.json.loads(email_string)
	# send mail
	send_report(det_id, current_time, email_json)

def detect(cleaned_sensors_data,
		   current_time):
	"""Detect faults on sensors"""
	for det_id in chosen_detectors:
		# retrieve data from cleaned json
		#index = indexer[det_id] # static mapping doesn't work, because the detector list changes every minute
		det_list = cleaned_sensors_data['DetectorData']['collection-period']['collection-period-item']['detector-reports']['detector-report']
		# locate detector on json struct
		index = locate_detector(det_list, det_id)
		if index == None:
			print("Couldn't find detector %s on parsed list." % det_id)
			continue
		det = det_list[index]
		det_status = det['detector-status'] # unused
		
		# get lanes data
		lanes_data = det['lane-data']['lane-data-item']
		lanes_status = parse_lane_data(det_id, lanes_data)
		# apply rule
		if is_stuck(lanes_status, current_time):
			print('DETECTOR IS STUCK!')
			alert_stuck_sensor(det_id, current_time)
		else:
			print('DETECTOR IS WORKING!')
