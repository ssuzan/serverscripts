#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import os
import sys
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports
import sendemail
email_file = workspace_environ+'/serverscripts/globals/emails_dor'
import fetch_sensors
import store_sensors
import clean_sensors
import detect_faults_sensors
import detect_anomalies_sensors
from datetime import datetime
#import getopt
import smtplib
from email.mime.text import MIMEText
from automation_building_scripts import automate_build_clean
is_development_file =  os.environ['WORKSPACE']+'/.is_development'
is_production_file =   os.environ['WORKSPACE']+'/.is_production'
is_prod_flag= False
is_dev_flag= True
#is_prod_flag= os.path.isfile(is_production_file) 
#is_dev_flag=  os.path.isfile(is_development_file)

store_original = True
download_url = 'http://bugatti.nvfast.org/realtimexml/FMSRealtimeData.xml'
save_path = workspace_environ+'/DB_backups/data_from_fast_sensors'
dump_output_to = "FILE"

import cProfile
    
def call_all_sensors_dev():
    curr_t = universal_imports.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
    print("call_all_sensors_dev() running at time: "+curr_t)  

    now = str(datetime.utcnow())
    now = now.split(" ")
    now[0] = now[0].split("-")
    now[1] = now[1].split(":")
    filename = "sensor_data_"+now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1]#+"_"+now[1][2].replace(".","_") 
    text = open(email_file,'r')
    email_string = text.read()
    email_json = universal_imports.json.loads(email_string)                  

    # fetching sensors from FAST url:
    #try:
    returned = fetch_sensors.fetch(download_url,save_path, dump_output_to, filename,store_original)
    if ('error' in returned.keys()):
        sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'sensors error', message = "fetch sensors "+returned['error'], login = email_json['email'], password = email_json['password'])                  
    else:
    #Cleaning sensors data
        clean_return = clean_sensors.clean(returned)
        if (clean_return[0] == True):
            try:
                # detecting faults
                #x, y = clean_return[2], datetime.utcnow()
                #cProfile.runctx('detect_faults_sensors.detect(x, y)', {'x': x, 'y': y}, {'detect_faults_sensors': detect_faults_sensors}, filename='detect_profile.out')
                clean_return[2] = detect_faults_sensors.detect(clean_return[2], datetime.utcnow())
                clean_return[2] = detect_anomalies_sensors.detect_anomalies(clean_return[2], datetime.utcnow())
            except Exception as e:
                # alert if this call raised an error
                print(str(e))
                sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'error', message = "detect faults exception " + str(e), login = email_json['email'], password = email_json['password']) 

            try:
                #Storing sensors data       
                store_sensors.store(returned,clean_return[2],clean_return[1],save_path,dump_output_to,'fast_sensors_live_lv',filename,is_prod_flag,is_dev_flag)
            except:
                sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'error', message = "clean_sensors "+clean_return[1], login = email_json['email'], password = email_json['password']) 
        else:
           sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'error', message = "clean_sensors "+clean_return[1], login = email_json['email'], password = email_json['password'])  
    print('call_all_sensors_dev.py completed successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
    #except:
        #sendemail.sendemail(from_addr = email_json['email'], to_addr_list = email_json['send_emails'], cc_addr_list = [''], subject = 'sensors error', message = "fetch sensors "+str(filename), login = email_json['email'], password = email_json['password']) 

 
        
if __name__ == "__main__":
    call_all_sensors_dev()
