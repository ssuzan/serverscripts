# Dor Cohen - 14/5/18
# dor.cohen@waycaretech.com
"""
Module for detecting faults on FAST sensors
- main function is detect()
- called between clean_sensors and store_sensors
- gets as input parsed json file
- reports on faulty sensors, currently implements:
- (1) data integrity rule: count zero speed positive
- (2) stuck values rule: detector reports the same values of (count,speed)
"""

import sys
import os
import time
import smtplib
import pickle
import requests
import numpy as np
from datetime import datetime, timedelta
# local modules
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports
import sendemail

# globals
# email alerts indicator
alerts = False
# get email settings from disk
email_file = workspace_environ + '/serverscripts/globals/emails_dor'
text = open(email_file,'r')
email_string = text.read()
email_json = universal_imports.json.loads(email_string)
# send mail with the following cameras mapping from buggati:
cameras_mapping = {'349_3_152': 117, '26_1_97': 601, '404_2_212': 208, '32_1_142': 115}
# script path
full_script_path = os.path.dirname(os.path.realpath(sys.argv[0]))

# helpers
def cam_feed(det_id):
	"""Returns current cameras feed in bytes code
	IN: 
	det_id as detector_id

	OUT: 
	images as bytes string
	"""
	# return empty bytes string if mapping isn't available
	if det_id not in cameras_mapping:
		return None
	feed_id = cameras_mapping[det_id]
	url = "http://bugatti.nvfast.org/Snapshots/%s.jpeg" % str(feed_id)
	req = requests.get(url)
	image_bytes = req.content
	return image_bytes

def send_report(det_id, lanes_status, p_lanes_status, current_time, email_file, stuck_val):
	"""Send an email report if detected fault, message includes
	an image from the nearest camera, uses sendemail_picture(..) from globals.

	IN:
	det_id - detector_id that is faulty
	lanes_status - [(c1,s1)...]
	p_lanes_status - previous
	current_time - time fault detected on datetime type
	email_file - a dictionary with all relevant info for sending mail
	"""

	# generate message content
	subject = "LV Fast sensors: Detector %s stuck" % det_id
	datetime_str = current_time.strftime("%d-%m-%Y %H:%M")
	message = "Sensor %s is stuck with value: %d, current time (UTC): %s\n" % (det_id, stuck_val, datetime_str)
	message += "Current values: %s\n" % str(lanes_status)
	message += "Previous values: %s\n" % str(p_lanes_status)

	# attach picture
	image_bytes = cam_feed(det_id)

	sendemail.sendemail_picture(email_file, subject, message, image_bytes)

def alert_stuck_sensor(det_id, lanes_status, p_lanes_status, current_time, stuck_val):
	"""Alerts on stuck sensor, sends an email with details
	IN: 
	det_id - detector_id as in buggati
	current_time - in datetime format
	"""
	# send only for chosen detectors (otherwise it takes too much time)
	if det_id not in cameras_mapping:
		return
	# send mail
	send_report(det_id, lanes_status, p_lanes_status, current_time, email_json, stuck_val)

def load_values(filename='detectors_lanes_data.pickle'):
	"""Load all detectors lanes data using pickle"""
	# parse filename and add full path
	filename = os.path.join(full_script_path, filename)
	# if file doesn't exist return empty dict
	if not os.path.isfile(filename):
		return {}
	# load file
	with open(filename, 'rb') as handle:
		data = pickle.load(handle)

	return data

def save_values(values, filename='detectors_lanes_data.pickle'):
	"""Save all detectors lanes data using pickle"""
	# parse filename and add full path
	filename = os.path.join(full_script_path, filename)
	# write (delete old file)
	with open(filename, 'wb') as handle:
		pickle.dump(values, handle, protocol=pickle.HIGHEST_PROTOCOL)

	return True	

def numpy_array_str(arr):
	"""Convert numpy int array to string representation"""
	s = ''
	for val in arr:
		s += str(val)

	return s

# functions for detecting failures

# **Function for research purpose, not production**
def check_data_integrity(lanes_status):
	"""Perform check of data integrity issues
	IN:
	lanes_status - [(count_lane1, speed_lane1),...]

	OUT:
	0 - no issues
	1 - count zero speed positive on all lanes
	2 - speed zero count positive on all lanes
	3 - czsp some lane
	4 - cpsz some lane
	"""
	# init objects
	curr = np.array(lanes_status).astype(int)
	lanes = len(curr)
	czsp = np.array([[True,False]])
	czsp_mask = czsp.repeat(lanes, axis=0)
	cpsz = np.array([[False,True]])
	cpsz_mask = cpsz.repeat(lanes, axis=0)

	# compute
	res = curr == 0
	czsp_res = res[czsp_mask]
	cpsz_res = res[cpsz_mask]
	
	ret_value = 0
	# all values are zeros
	if res.all():
		pass
	elif czsp_res.all():
		ret_value = 1
	elif cpsz_res.all():
		ret_value = 2
	else:
		if czsp_res.any():
			ret_value = 3
		elif cpsz_res.any():
			ret_value = 4

	return ret_value

# **Function for research purpose, not production**
def compare_lanes_data(prev_lanes_status, curr_lanes_status):
	"""Compares the lanes status from both times 
	IN: 
	prev_lanes_status - previous lanes_status [(count_lane1, speed_lane1),...]
	curr_lanes_status - current lanes_status
	
	OUT: 
	0 - no error
	1 - all values stuck - count and speed
	2 - speed stuck
	3 - count stuck
	4 - speed is stuck in specific lane
	5 - count is stuck in specific lane
	"""
	# compare two dictionaries - deprecated
	#res = prev_lanes_status == curr_lanes_status

	# perform compare on numpy
	prev = np.array(prev_lanes_status).astype(int)
	curr = np.array(curr_lanes_status).astype(int)

	# one of the lanes was deleted in 'clean' step
	if prev.shape != curr.shape:
		return None

	res = prev == curr

	count_cmp_res = res.T[0]
	speed_cmp_res = res.T[1]

	all_count = count_cmp_res.all()
	all_speed = speed_cmp_res.all()
	
	ret_value = 0
	# all values are the same
	if all_count and all_speed:
		ret_value = 1
	# only speed values are same
	elif all_speed:
		ret_value = 2
	# only count values are same
	elif all_count:
		ret_value = 3
	# speed/count values aren't the same
	else:
		any_count = count_cmp_res.any()
		any_speed = speed_cmp_res.any()
		if any_speed:
			ret_value = 4
		elif any_count:
			ret_value = 5

	return ret_value

def check_integrity_czsp(lanes_status):
	"""Perform check of data integrity CZSP issue
	IN:
	lanes_status - [(c1,s1),..]

	OUT:
	a binary vector with size=lanes, indicates faulty lanes
	e.g., detector has 4 lanes:
	1001 - lanes 1 and 4 are faulty
	0000 - no lanes are faulty
	"""
	curr = np.array(lanes_status).astype(int)
	lanes = len(curr)
	czsp = np.array([[True,False]])
	czsp_mask = czsp.repeat(lanes, axis=0)

	# compute
	res = curr == 0
	if res.all():
		ret_value = '0' * lanes
	else:
		czsp_res = res[czsp_mask]
		ret_value = numpy_array_str(czsp_res.astype(int))

	return ret_value

def compare_lanes_stuck(prev_lanes_status, curr_lanes_status):
	"""Compares the lanes status from both times, returns errors strings
	IN: 
	prev_lanes_status - previous lanes_status [(count_lane1, speed_lane1),...]
	curr_lanes_status - current lanes_status
	
	OUT: speed,count error strings -
	each value gets a binary vector with size=lanes
	e.g., detector has 4 lanes:
	1111,0001 - speed is stuck on all lanes, count is stuck on last lane
	"""
	# perform compare on numpy
	prev = np.array(prev_lanes_status).astype(int)
	curr = np.array(curr_lanes_status).astype(int)

	# one of the lanes was deleted in 'clean' step
	if prev.shape != curr.shape:
		return None

	# compare
	res = prev == curr

	count_cmp_res = res.T[0]
	speed_cmp_res = res.T[1]

	# if any error return faults string
	ret_speed, ret_count = numpy_array_str(speed_cmp_res.astype(int)), numpy_array_str(count_cmp_res.astype(int))

	# otherwise none
	return ret_speed, ret_count

def apply_rules(lanes_status, p_lanes_status, cleaned_sensors_data, det_id, index, current_time):
	"""Apply errors and integrity issues rules. Currently implements:
	(1) 0th order rule: data integrity: e.g., count is zero speed positive
	(2) 1st order rule: values (count/speed) are stuck on all lanes, or some
	IN:
	lanes_status - as returned from parse_lane_data
	p_lanes_status - previous
	cleaned_sensors_data - actual json structure to pass
	det_id - detector_id
	index - its index on current json structure
	curren_time - in datetime format

	OUT:
	cleaned_sensors_data - modified with rule value
	"""
	integrity_val = None
	speed_val, count_val = None, None
	# 0th order
	try:
		integrity_val = check_integrity_czsp(lanes_status)
	except Exception as e:
		print("Exception occured in data integrity check:", str(e))
		print("Current time: %s, detector_id: %s, index on json: %s" % (str(current_time), str(det_id), str(index)) )
	
	# 1st order - load from disk previous period
	# check if detector exists in previous period (or filtered by clean)
	if p_lanes_status is not None:
		# perform comparing
		try:
			speed_val, count_val = compare_lanes_stuck(lanes_status, p_lanes_status)
		except Exception as e:
			print("Exception occured in compare lanes:", str(e))
			print("Current time: %s, detector_id: %s, index on json: %s" % (str(current_time), str(det_id), str(index)) )
			
		# perform alert on mail - NOTICE: IT CAUSES MAJOR OVERHEAD
		"""
		if alerts:
			if stuck_val:
				alert_stuck_sensor(det_id, lanes_status, p_lanes_status, current_time, stuck_val)
		"""
	
	# notice it'll assign None if failed (or if detector filtered in previous period, or had != #lanes)
	cleaned_sensors_data['DetectorData']['collection-period']['collection-period-item']['detector-reports']['detector-report'][index]['integrity-issue-czsp'] = integrity_val
	cleaned_sensors_data['DetectorData']['collection-period']['collection-period-item']['detector-reports']['detector-report'][index]['error-speed'] = speed_val
	cleaned_sensors_data['DetectorData']['collection-period']['collection-period-item']['detector-reports']['detector-report'][index]['error-count'] = count_val
	return cleaned_sensors_data

def parse_lane_data(det_id, lanes_data):
	"""Parse lane data: count,speed for each lane - for single detector
	IN: 
	det_id - detector_id to parse
	lanes_data - actual lanes data, as appears on json
	
	OUT: 
	dictionary with format {'detector_id': [(count1,speed1),..., (countN,speedN)]}
	"""
	lanes_num = len(lanes_data)
	lanes_status = {}
	lane_data = []
	# collect count,speed
	for i in range(lanes_num):
		count = lanes_data[i]['lane-vehicle-count']
		speed = lanes_data[i]['lane-vehicle-speed']
		lane_data.append((count,speed))

	lanes_status[det_id] = lane_data
	return lanes_status	

def iterate_once(det_list):
	"""Iterate over detector list as appears on json structure,
	returns mapping (det_id -> index) and values (see next func)"""
	mapper = {}
	values = {}
	for index, elem in enumerate(det_list):
		# get index
		det_id = elem['detector-id']
		mapper[det_id] = index
		# get values
		det = det_list[index]
		lanes_data = det['lane-data']['lane-data-item']
		lanes_status = parse_lane_data(det_id, lanes_data)
		# update dict
		values.update(lanes_status)
	
	return mapper, values

def detect(cleaned_sensors_data, current_time):
	"""Detect faults on sensors
	IN:
	cleaned_sensors_data - as appears on json structure
	current_time - taken from call_all

	OUT:
	cleaned_sensors_data - modified with rules
	"""
	# get current json content
	det_list = cleaned_sensors_data['DetectorData']['collection-period']['collection-period-item']['detector-reports']['detector-report']
	# get current mapping (detector_id -> list_indices)
	# and values. store in memory
	mapper, values = iterate_once(det_list)
	# values are {'det_id': [(c1,s1)...], 'det_id2': [(c1,s1)...]...}
	p_values = load_values()
	# iterate once again to apply rules
	for det_id, index in mapper.items():
		# json structure for this det
		det = det_list[index] 
		# get lanes data of specific detector
		lanes_status = values[det_id] # [(c1,s1)...]
		
		# check previous values (1st order rule)
		
		# check if key exists on previous period
		p_lanes_status = None
		if det_id in p_values:
			p_lanes_status = p_values[det_id]
		# apply rules: errors and integrity issues
		cleaned_sensors_data = apply_rules(lanes_status, p_lanes_status, cleaned_sensors_data, det_id, index, current_time)

	ret = save_values(values)
	if not ret:
		curr_time_str = current_time.strftime("%d-%m-%Y %H:%M:%S")
		print("Couldn't save values for time %s" % curr_time_str)

	return cleaned_sensors_data