#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports
import apscheduler
from apscheduler.schedulers.blocking import BlockingScheduler
import sendemail
email_file = workspace_environ+'/serverscripts/globals/emails'
import requests
import smtplib
import clean_inrix_dsd
import fetch_inrix_dsd
import store_inrix_dsd
import hashlib
from automation_building_scripts import automate_build_clean
from automation_building_scripts import automate_build_new_elements
from email.mime.text import MIMEText
from os import listdir
import time


type_name = 'inrix_dsd_live_lv'
inrix_url = "https://uas-api.inrix.com/v1/appToken?appId=600cbab9-26ab-4d7a-858c-ff8361dca85a&hashToken=9545974727b44bcc6e3e03e4fd77bd2dead27bb7"
save_path = workspace_environ+'/DB_backups/data_from_inrix'
dump_output_to = "FILE"

#number of iterations before element is removed from ongoing duplicate check list
default_age = 3
#minutes between iterations (.5 for half minute)
minutes_between_script_calls = 1
#ongoing duplicate check list
hash_log = {}
class url:
    second_url = ''
    #x is part of testing
    x = 0
url_class = url()



def call_all_wz():
    curr_t = universal_imports.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
    print("call_all_inrix_dsd() running at time: "+curr_t)
    #1st fetch 2nd clean 3rd store
    now = str(universal_imports.datetime.utcnow())
    now = now.split(" ")
    now[0] = now[0].split("-")
    now[1] = now[1].split(":")    
    filename = type_name+"_"+now[0][2]+"_"+now[0][1]+"_"+now[0][0]+"_"+now[1][0]+"_"+now[1][1]+"_"+now[1][2].replace(".","_")        
    y = fetch_inrix_dsd.downloadInrix(url_class.second_url,save_path,filename,url_class.x)
    url_class.x+=1
    text = open(email_file,'r')
    string = text.read()
    email_json = universal_imports.json.loads(string) 
    dead_hashes = []
    #automate_build_new_elements.automate_new_elements(y['jams'][0]['line'][0],"json_clean['jams'][i]['line'][n]")    
    #automate_build_new_elements.automate_new_elements(y['jams'][0],"json_clean['jams'][i]")
    #automate_build_clean.automate_clean(y['result']['dangerousSlowdowns'][0]['location']['geometry']['coordinates'][0],"incident['location']['geometry']['coordinates'][0]")
    if 'error' in y.keys():
        sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'fetch_inrix() error', message = "fetch_inrix() error "+filename+" "+str(y['error']), login = email_json["email"], password = email_json["password"])                        
    else:
        clean_results = clean_inrix_dsd.clean(y)
        if(clean_results[0] == True):
            #hashing starts here
            y = clean_results[2]
            if ('result not present' not in clean_results[3] and 'result not dict' not in clean_results[3] and 'dangerousSlowdowns not present' not in clean_results[3] and 'dangerousSlowdowns not dict or list' not in clean_results[3]):         
                if len(clean_results[3])>0:
                    sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'clean_inrix() warning', message = "clean_inrix() warning "+filename+" "+str(clean_results[3]), login = email_json["email"], password = email_json["password"])                               
                results = store_inrix_dsd.store(y, save_path,hash_log,dump_output_to,default_age,filename,clean_results[3]) 
                hashes = results[0]
                repeat_hashes = results[1]
                for key in hashes:
                    hash_log[key] = hashes[key]
                for key in hash_log.keys():
                    if key not in repeat_hashes and key not in hashes.keys():
                        hash_log[key][1]-=1
                        #print(key+" "+str(hash_log[key][1]))
                        if hash_log[key][1] == 0:
                            dead_hashes.append(key)
                    else:
                        hash_log[key][1] = default_age
                for i in range(0,len(dead_hashes)):
                    del hash_log[dead_hashes[i]]    
            else:
                for key in hash_log.keys():
                    hash_log[key][1]-=1
                    #print(key+" "+str(hash_log[key][1]))
                    if hash_log[key][1] == 0:
                        dead_hashes.append(key)
    
                for i in range(0,len(dead_hashes)):
                    del hash_log[dead_hashes[i]]                  
        else:
            sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'clean_inrix() error', message = "error in clean_inrix() "+filename, login = email_json["email"], password = email_json["password"])    
    print('call_all_inrix_dsd() completed successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')


#do: call self with delay if failure, run x number of times
def get_inrix_dsd_key():
    curr_t = universal_imports.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
    print("get_inrix_dsd_key() running at time: "+curr_t)
    text = open(email_file,'r')
    string = text.read()
    email_json = universal_imports.json.loads(string) 
    error_allowances = 5
    while error_allowances >=0:
        try:
            #gets json from url
            r1 = requests.get(inrix_url)
            try:
                r1 = r1.json()
                try:
                    new_url = "http://dsd-api.inrix.com/v1/DangerousSlowdowns?box=36.5|-116,35.937371|%20-114.64&units=0&accesstoken="+r1['result']['token']+"&compress=true"
                    break
                except:
                    time.sleep(5)
                    error_allowances -=1
                    new_url = ""
                    sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'get_inrix_key() error', message = "get_inrix_key() error "+"unable to create new_url string error_allowances "+str(error_allowances), login = email_json["email"], password = email_json["password"])                
            except:
                time.sleep(5)
                error_allowances-=1
                new_url = ""                
                sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'get_inrix_key() error', message = "get_inrix_key() error "+"unable to convert response to json error_allowances "+str(error_allowances), login = email_json["email"], password = email_json["password"])                            
        except requests.exceptions.ConnectionError:
            time.sleep(5)
            error_allowances-=1
            new_url = "" 
            sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'get_inrix_key() error', message = "get_inrix_key() error "+"requests.exceptions.ConnectionError error_allowances "+str(error_allowances), login = email_json["email"], password = email_json["password"])                                  
    url_class.second_url = new_url  
    
    print('get_inrix_dsd_key() completed successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
    
def scheduled_call():
    print('call_all_inrix_dsd.scheduled_call() started successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
    get_inrix_dsd_key()
    sched = BlockingScheduler(timezone='UTC', coalesce=True)
    sched.add_job(call_all_wz, 'interval', id='job_id1', seconds=10, coalesce=True)
    #get_inrix_key should run daily
    sched.add_job(get_inrix_dsd_key, 'interval', id='job_id2', hours=24, coalesce=True)
    sched.start()
#call_all_wz()
scheduled_call()
#print(hash_log)