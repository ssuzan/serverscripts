#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#input: waze database https request (url) 
#output: json of data given by https request or json containing error message
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports
import jsonlib_python3
import requests
import xmltodict

access_file = "access.xml" 
def fetch_example(filename):
    headers = {
        'Content-Type': 'text/xml;charset=UTF-8',
    }
    text = open(access_file,'r')
    access_string = text.read()
    access_json = xmltodict.parse(access_string)
    user = access_json['soapenv:Envelope']['soapenv:Body']['mes:centerActiveVerificationRequestMsg']['authentication']["user-id"]
    password=access_json['soapenv:Envelope']['soapenv:Body']['mes:centerActiveVerificationRequestMsg']['authentication']["password"]
    data = open(access_file,'r')
    #connecting to Spillman and authentication 
    #r= requests.post('http://10.10.55.5:4081/DataExchangeWayCare/REST', headers=headers, data=data, auth=(user, password))
    r= requests.post(filename, headers=headers, data=data, auth=(user, password))    
    #if (r.status_code== 200):
    print(r.content)

fetch_example("https://colondexsrv.its.nv.gov/tmddws/TmddWS.svc/OC")