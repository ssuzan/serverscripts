#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import os
import sys
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports
import weather_underground_store
import requests

def weather_underground_fetch(weather_api_string,pws_curr,call_url,filename,save_path):
    try:
        #gets json from url
        #weather_underground_url = call_url%(weather_api_string.strip("\n"),pws_curr)
        r = requests.get(call_url)
        try:
            weather_underground_store.weather_underground_store_original(filename,r.content,save_path)
        except:
            return(False, "storing orignal error")
        try:
            r = r.json()
            return(True,r)
        except:
            return(False,"conversion to json error")
    except:
        return(False,"downloading error")
