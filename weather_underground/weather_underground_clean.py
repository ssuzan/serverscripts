#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports
from automation_building_scripts import automate_build_new_elements


    
def weather_underground_clean(weather_data,filename):
    errors = []
    #automate_build_new_elements.automate_new_elements(weather_data,'weather_data')     
    
    if('response' in weather_data.keys()):
        place_holder_variable = True
        if(isinstance(weather_data['response'],dict)):
            place_holder_variable = True
            if('features' in weather_data['response'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['response']['features'],dict)):
                    place_holder_variable = True
                    if('conditions' in weather_data['response']['features'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['response']['features']['conditions'],int)):
                            place_holder_variable = True
                        else:
                            weather_data['response']['features']['conditions'] = ''
                            errors.append('conditions not int')
                    else:
                        errors.append('conditions not present')
                        weather_data['response']['features']['conditions'] = ''
                else:
                    weather_data['response']['features'] = ''
                    errors.append('features not dict')
            else:
                errors.append('features not present')
                weather_data['response']['features'] = ''
            if('termsofService' in weather_data['response'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['response']['termsofService'],str)):
                    place_holder_variable = True
                else:
                    weather_data['response']['termsofService'] = ''
                    errors.append(False,'termsofService not str')
            else:
                errors.append('termsofService not present')
                weather_data['response']['termsofService'] = ''
            if('version' in weather_data['response'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['response']['version'],str)):
                    place_holder_variable = True
                else:
                    weather_data['response']['version'] = ''
                    errors.append(False,'version not str')
            else:
                errors.append('version not present')
                weather_data['response']['version'] = ''
        else:
            weather_data['response'] = ''
            errors.append('response not dict')
    else:
        errors.append('response not present')
        weather_data['response'] = ''
    if('current_observation' in weather_data.keys()):
        place_holder_variable = True
        if(isinstance(weather_data['current_observation'],dict)):
            place_holder_variable = True
            if('weather' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['weather'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['weather'] = ''
                    errors.append(False,'weather not str')
            else:
                errors.append('weather not present')
                weather_data['current_observation']['weather'] = ''
            if('windchill_f' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['windchill_f'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['windchill_f'] = ''
                    errors.append(False,'windchill_f not str')
            else:
                errors.append('windchill_f not present')
                weather_data['current_observation']['windchill_f'] = ''
            if('ob_url' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['ob_url'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['ob_url'] = ''
                    errors.append(False,'ob_url not str')
            else:
                errors.append('ob_url not present')
                weather_data['current_observation']['ob_url'] = ''
            if('wind_dir' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['wind_dir'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['wind_dir'] = ''
                    errors.append(False,'wind_dir not str')
            else:
                errors.append('wind_dir not present')
                weather_data['current_observation']['wind_dir'] = ''
            if('pressure_mb' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['pressure_mb'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['pressure_mb'] = ''
                    errors.append(False,'pressure_mb not str')
            else:
                errors.append('pressure_mb not present')
                weather_data['current_observation']['pressure_mb'] = ''
            if('temp_c' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['temp_c'],int)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['temp_c'] = ''
                    errors.append('temp_c not int')
            else:
                errors.append('temp_c not present')
                weather_data['current_observation']['temp_c'] = ''
            if('precip_1hr_in' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['precip_1hr_in'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['precip_1hr_in'] = ''
                    errors.append(False,'precip_1hr_in not str')
            else:
                errors.append('precip_1hr_in not present')
                weather_data['current_observation']['precip_1hr_in'] = ''
            if('precip_today_in' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['precip_today_in'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['precip_today_in'] = ''
                    errors.append(False,'precip_today_in not str')
            else:
                errors.append('precip_today_in not present')
                weather_data['current_observation']['precip_today_in'] = ''
            if('pressure_in' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['pressure_in'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['pressure_in'] = ''
                    errors.append(False,'pressure_in not str')
            else:
                errors.append('pressure_in not present')
                weather_data['current_observation']['pressure_in'] = ''
            if('precip_today_metric' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['precip_today_metric'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['precip_today_metric'] = ''
                    errors.append(False,'precip_today_metric not str')
            else:
                errors.append('precip_today_metric not present')
                weather_data['current_observation']['precip_today_metric'] = ''
            if('history_url' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['history_url'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['history_url'] = ''
                    errors.append(False,'history_url not str')
            else:
                errors.append('history_url not present')
                weather_data['current_observation']['history_url'] = ''
            if('solarradiation' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['solarradiation'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['solarradiation'] = ''
                    errors.append(False,'solarradiation not str')
            else:
                errors.append('solarradiation not present')
                weather_data['current_observation']['solarradiation'] = ''
            if('image' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['image'],dict)):
                    place_holder_variable = True
                    if('title' in weather_data['current_observation']['image'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['current_observation']['image']['title'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['current_observation']['image']['title'] = ''
                            errors.append(False,'title not str')
                    else:
                        errors.append('title not present')
                        weather_data['current_observation']['image']['title'] = ''
                    if('url' in weather_data['current_observation']['image'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['current_observation']['image']['url'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['current_observation']['image']['url'] = ''
                            errors.append(False,'url not str')
                    else:
                        errors.append('url not present')
                        weather_data['current_observation']['image']['url'] = ''
                    if('link' in weather_data['current_observation']['image'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['current_observation']['image']['link'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['current_observation']['image']['link'] = ''
                            errors.append(False,'link not str')
                    else:
                        errors.append('link not present')
                        weather_data['current_observation']['image']['link'] = ''
                else:
                    weather_data['current_observation']['image'] = ''
                    errors.append('image not dict')
            else:
                errors.append('image not present')
                weather_data['current_observation']['image'] = ''
            if('wind_gust_kph' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['wind_gust_kph'],str)):
                    place_holder_variable = True
                elif(isinstance(weather_data['current_observation']['wind_gust_kph'],int)):
                    weather_data['current_observation']['wind_gust_kph'] = str(weather_data['current_observation']['wind_gust_kph'])              
                else:
                    weather_data['current_observation']['wind_gust_kph'] = ''
                    errors.append('wind_gust_kph not int')
            else:
                errors.append('wind_gust_kph not present')
                weather_data['current_observation']['wind_gust_kph'] = ''
            if('feelslike_f' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['feelslike_f'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['feelslike_f'] = ''
                    errors.append(False,'feelslike_f not str')
            else:
                errors.append('feelslike_f not present')
                weather_data['current_observation']['feelslike_f'] = ''
            if('estimated' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['estimated'],dict)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['estimated'] = ''
                    errors.append('estimated not dict')
            else:
                errors.append('estimated not present')
                weather_data['current_observation']['estimated'] = ''
            if('dewpoint_string' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['dewpoint_string'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['dewpoint_string'] = ''
                    errors.append(False,'dewpoint_string not str')
            else:
                errors.append('dewpoint_string not present')
                weather_data['current_observation']['dewpoint_string'] = ''
            if('observation_epoch' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['observation_epoch'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['observation_epoch'] = ''
                    errors.append(False,'observation_epoch not str')
            else:
                errors.append('observation_epoch not present')
                weather_data['current_observation']['observation_epoch'] = ''
            if('heat_index_f' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['heat_index_f'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['heat_index_f'] = ''
                    errors.append(False,'heat_index_f not str')
            else:
                errors.append('heat_index_f not present')
                weather_data['current_observation']['heat_index_f'] = ''
            if('precip_today_string' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['precip_today_string'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['precip_today_string'] = ''
                    errors.append(False,'precip_today_string not str')
            else:
                errors.append('precip_today_string not present')
                weather_data['current_observation']['precip_today_string'] = ''
            if('dewpoint_f' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['dewpoint_f'],int)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['dewpoint_f'] = ''
                    errors.append('dewpoint_f not int')
            else:
                errors.append('dewpoint_f not present')
                weather_data['current_observation']['dewpoint_f'] = ''
            if('feelslike_string' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['feelslike_string'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['feelslike_string'] = ''
                    errors.append(False,'feelslike_string not str')
            else:
                errors.append('feelslike_string not present')
                weather_data['current_observation']['feelslike_string'] = ''
            if('nowcast' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['nowcast'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['nowcast'] = ''
                    errors.append(False,'nowcast not str')
            else:
                errors.append('nowcast not present')
                weather_data['current_observation']['nowcast'] = ''
            if('UV' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['UV'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['UV'] = ''
                    errors.append(False,'UV not str')
            else:
                errors.append('UV not present')
                weather_data['current_observation']['UV'] = ''
            if('wind_degrees' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['wind_degrees'],int)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['wind_degrees'] = ''
                    errors.append('wind_degrees not int')
            else:
                errors.append('wind_degrees not present')
                weather_data['current_observation']['wind_degrees'] = ''
            if('dewpoint_c' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['dewpoint_c'],int)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['dewpoint_c'] = ''
                    errors.append('dewpoint_c not int')
            else:
                errors.append('dewpoint_c not present')
                weather_data['current_observation']['dewpoint_c'] = ''
            if('visibility_km' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['visibility_km'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['visibility_km'] = ''
                    errors.append(False,'visibility_km not str')
            else:
                errors.append('visibility_km not present')
                weather_data['current_observation']['visibility_km'] = ''
            if('wind_string' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['wind_string'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['wind_string'] = ''
                    errors.append(False,'wind_string not str')
            else:
                errors.append('wind_string not present')
                weather_data['current_observation']['wind_string'] = ''
            if('heat_index_c' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['heat_index_c'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['heat_index_c'] = ''
                    errors.append(False,'heat_index_c not str')
            else:
                errors.append('heat_index_c not present')
                weather_data['current_observation']['heat_index_c'] = ''
            if('heat_index_string' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['heat_index_string'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['heat_index_string'] = ''
                    errors.append(False,'heat_index_string not str')
            else:
                errors.append('heat_index_string not present')
                weather_data['current_observation']['heat_index_string'] = ''
            if('local_tz_short' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['local_tz_short'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['local_tz_short'] = ''
                    errors.append(False,'local_tz_short not str')
            else:
                errors.append('local_tz_short not present')
                weather_data['current_observation']['local_tz_short'] = ''
            if('wind_kph' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['wind_kph'],int)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['wind_kph'] = ''
                    errors.append('wind_kph not int')
            else:
                errors.append('wind_kph not present')
                weather_data['current_observation']['wind_kph'] = ''
            if('relative_humidity' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['relative_humidity'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['relative_humidity'] = ''
                    errors.append(False,'relative_humidity not str')
            else:
                errors.append('relative_humidity not present')
                weather_data['current_observation']['relative_humidity'] = ''
            if('local_time_rfc822' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['local_time_rfc822'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['local_time_rfc822'] = ''
                    errors.append(False,'local_time_rfc822 not str')
            else:
                errors.append('local_time_rfc822 not present')
                weather_data['current_observation']['local_time_rfc822'] = ''
            if('pressure_trend' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['pressure_trend'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['pressure_trend'] = ''
                    errors.append(False,'pressure_trend not str')
            else:
                errors.append('pressure_trend not present')
                weather_data['current_observation']['pressure_trend'] = ''
            if('precip_1hr_string' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['precip_1hr_string'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['precip_1hr_string'] = ''
                    errors.append(False,'precip_1hr_string not str')
            else:
                errors.append('precip_1hr_string not present')
                weather_data['current_observation']['precip_1hr_string'] = ''
            if('temperature_string' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['temperature_string'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['temperature_string'] = ''
                    errors.append(False,'temperature_string not str')
            else:
                errors.append('temperature_string not present')
                weather_data['current_observation']['temperature_string'] = ''
            if('local_tz_long' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['local_tz_long'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['local_tz_long'] = ''
                    errors.append(False,'local_tz_long not str')
            else:
                errors.append('local_tz_long not present')
                weather_data['current_observation']['local_tz_long'] = ''
            if('icon' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['icon'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['icon'] = ''
                    errors.append(False,'icon not str')
            else:
                errors.append('icon not present')
                weather_data['current_observation']['icon'] = ''
            if('forecast_url' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['forecast_url'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['forecast_url'] = ''
                    errors.append(False,'forecast_url not str')
            else:
                errors.append('forecast_url not present')
                weather_data['current_observation']['forecast_url'] = ''
            if('local_tz_offset' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['local_tz_offset'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['local_tz_offset'] = ''
                    errors.append(False,'local_tz_offset not str')
            else:
                errors.append('local_tz_offset not present')
                weather_data['current_observation']['local_tz_offset'] = ''
            if('observation_time_rfc822' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['observation_time_rfc822'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['observation_time_rfc822'] = ''
                    errors.append(False,'observation_time_rfc822 not str')
            else:
                errors.append('observation_time_rfc822 not present')
                weather_data['current_observation']['observation_time_rfc822'] = ''
            if('station_id' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['station_id'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['station_id'] = ''
                    errors.append(False,'station_id not str')
            else:
                errors.append('station_id not present')
                weather_data['current_observation']['station_id'] = ''
            if('observation_location' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['observation_location'],dict)):
                    place_holder_variable = True
                    if('country' in weather_data['current_observation']['observation_location'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['current_observation']['observation_location']['country'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['current_observation']['observation_location']['country'] = ''
                            errors.append(False,'country not str')
                    else:
                        errors.append('country not present')
                        weather_data['current_observation']['observation_location']['country'] = ''
                    if('state' in weather_data['current_observation']['observation_location'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['current_observation']['observation_location']['state'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['current_observation']['observation_location']['state'] = ''
                            errors.append(False,'state not str')
                    else:
                        errors.append('state not present')
                        weather_data['current_observation']['observation_location']['state'] = ''
                    if('country_iso3166' in weather_data['current_observation']['observation_location'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['current_observation']['observation_location']['country_iso3166'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['current_observation']['observation_location']['country_iso3166'] = ''
                            errors.append(False,'country_iso3166 not str')
                    else:
                        errors.append('country_iso3166 not present')
                        weather_data['current_observation']['observation_location']['country_iso3166'] = ''
                    if('latitude' in weather_data['current_observation']['observation_location'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['current_observation']['observation_location']['latitude'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['current_observation']['observation_location']['latitude'] = ''
                            errors.append(False,'latitude not str')
                    else:
                        errors.append('latitude not present')
                        weather_data['current_observation']['observation_location']['latitude'] = ''
                    if('elevation' in weather_data['current_observation']['observation_location'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['current_observation']['observation_location']['elevation'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['current_observation']['observation_location']['elevation'] = ''
                            errors.append(False,'elevation not str')
                    else:
                        errors.append('elevation not present')
                        weather_data['current_observation']['observation_location']['elevation'] = ''
                    if('full' in weather_data['current_observation']['observation_location'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['current_observation']['observation_location']['full'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['current_observation']['observation_location']['full'] = ''
                            errors.append(False,'full not str')
                    else:
                        errors.append('full not present')
                        weather_data['current_observation']['observation_location']['full'] = ''
                    if('longitude' in weather_data['current_observation']['observation_location'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['current_observation']['observation_location']['longitude'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['current_observation']['observation_location']['longitude'] = ''
                            errors.append(False,'longitude not str')
                    else:
                        errors.append('longitude not present')
                        weather_data['current_observation']['observation_location']['longitude'] = ''
                    if('city' in weather_data['current_observation']['observation_location'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['current_observation']['observation_location']['city'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['current_observation']['observation_location']['city'] = ''
                            errors.append(False,'city not str')
                    else:
                        errors.append('city not present')
                        weather_data['current_observation']['observation_location']['city'] = ''
                else:
                    weather_data['current_observation']['observation_location'] = ''
                    errors.append('observation_location not dict')
            else:
                errors.append('observation_location not present')
                weather_data['current_observation']['observation_location'] = ''
            if('temp_f' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['temp_f'],int)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['temp_f'] = ''
                    errors.append('temp_f not int')
            else:
                errors.append('temp_f not present')
                weather_data['current_observation']['temp_f'] = ''
            if('wind_mph' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['wind_mph'],int)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['wind_mph'] = ''
                    errors.append('wind_mph not int')
            else:
                errors.append('wind_mph not present')
                weather_data['current_observation']['wind_mph'] = ''
            if('visibility_mi' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['visibility_mi'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['visibility_mi'] = ''
                    errors.append(False,'visibility_mi not str')
            else:
                errors.append('visibility_mi not present')
                weather_data['current_observation']['visibility_mi'] = ''
            if('wind_gust_mph' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['wind_gust_mph'],str)):
                    place_holder_variable = True
                elif(isinstance(weather_data['current_observation']['wind_gust_mph'],int)):
                    weather_data['current_observation']['wind_gust_mph'] = str(weather_data['current_observation']['wind_gust_mph'])
                else:
                    weather_data['current_observation']['wind_gust_mph'] = ''
                    errors.append('wind_gust_mph not int')
            else:
                errors.append('wind_gust_mph not present')
                weather_data['current_observation']['wind_gust_mph'] = ''
            if('precip_1hr_metric' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['precip_1hr_metric'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['precip_1hr_metric'] = ''
                    errors.append(False,'precip_1hr_metric not str')
            else:
                errors.append('precip_1hr_metric not present')
                weather_data['current_observation']['precip_1hr_metric'] = ''
            if('feelslike_c' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['feelslike_c'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['feelslike_c'] = ''
                    errors.append(False,'feelslike_c not str')
            else:
                errors.append('feelslike_c not present')
                weather_data['current_observation']['feelslike_c'] = ''
            if('display_location' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['display_location'],dict)):
                    place_holder_variable = True
                    if('wmo' in weather_data['current_observation']['display_location'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['current_observation']['display_location']['wmo'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['current_observation']['display_location']['wmo'] = ''
                            errors.append(False,'wmo not str')
                    else:
                        errors.append('wmo not present')
                        weather_data['current_observation']['display_location']['wmo'] = ''
                    if('elevation' in weather_data['current_observation']['display_location'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['current_observation']['display_location']['elevation'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['current_observation']['display_location']['elevation'] = ''
                            errors.append(False,'elevation not str')
                    else:
                        errors.append('elevation not present')
                        weather_data['current_observation']['display_location']['elevation'] = ''
                    if('country_iso3166' in weather_data['current_observation']['display_location'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['current_observation']['display_location']['country_iso3166'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['current_observation']['display_location']['country_iso3166'] = ''
                            errors.append(False,'country_iso3166 not str')
                    else:
                        errors.append('country_iso3166 not present')
                        weather_data['current_observation']['display_location']['country_iso3166'] = ''
                    if('full' in weather_data['current_observation']['display_location'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['current_observation']['display_location']['full'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['current_observation']['display_location']['full'] = ''
                            errors.append(False,'full not str')
                    else:
                        errors.append('full not present')
                        weather_data['current_observation']['display_location']['full'] = ''
                    if('longitude' in weather_data['current_observation']['display_location'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['current_observation']['display_location']['longitude'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['current_observation']['display_location']['longitude'] = ''
                            errors.append(False,'longitude not str')
                    else:
                        errors.append('longitude not present')
                        weather_data['current_observation']['display_location']['longitude'] = ''
                    if('city' in weather_data['current_observation']['display_location'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['current_observation']['display_location']['city'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['current_observation']['display_location']['city'] = ''
                            errors.append(False,'city not str')
                    else:
                        errors.append('city not present')
                        weather_data['current_observation']['display_location']['city'] = ''
                    if('country' in weather_data['current_observation']['display_location'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['current_observation']['display_location']['country'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['current_observation']['display_location']['country'] = ''
                            errors.append(False,'country not str')
                    else:
                        errors.append('country not present')
                        weather_data['current_observation']['display_location']['country'] = ''
                    if('magic' in weather_data['current_observation']['display_location'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['current_observation']['display_location']['magic'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['current_observation']['display_location']['magic'] = ''
                            errors.append(False,'magic not str')
                    else:
                        errors.append('magic not present')
                        weather_data['current_observation']['display_location']['magic'] = ''
                    if('state_name' in weather_data['current_observation']['display_location'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['current_observation']['display_location']['state_name'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['current_observation']['display_location']['state_name'] = ''
                            errors.append(False,'state_name not str')
                    else:
                        errors.append('state_name not present')
                        weather_data['current_observation']['display_location']['state_name'] = ''
                    if('state' in weather_data['current_observation']['display_location'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['current_observation']['display_location']['state'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['current_observation']['display_location']['state'] = ''
                            errors.append(False,'state not str')
                    else:
                        errors.append('state not present')
                        weather_data['current_observation']['display_location']['state'] = ''
                    if('latitude' in weather_data['current_observation']['display_location'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['current_observation']['display_location']['latitude'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['current_observation']['display_location']['latitude'] = ''
                            errors.append(False,'latitude not str')
                    else:
                        errors.append('latitude not present')
                        weather_data['current_observation']['display_location']['latitude'] = ''
                    if('zip' in weather_data['current_observation']['display_location'].keys()):
                        place_holder_variable = True
                        if(isinstance(weather_data['current_observation']['display_location']['zip'],str)):
                            place_holder_variable = True
                        else:
                            weather_data['current_observation']['display_location']['zip'] = ''
                            errors.append(False,'zip not str')
                    else:
                        errors.append('zip not present')
                        weather_data['current_observation']['display_location']['zip'] = ''
                else:
                    weather_data['current_observation']['display_location'] = ''
                    errors.append('display_location not dict')
            else:
                errors.append('display_location not present')
                weather_data['current_observation']['display_location'] = ''
            if('windchill_string' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['windchill_string'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['windchill_string'] = ''
                    errors.append(False,'windchill_string not str')
            else:
                errors.append('windchill_string not present')
                weather_data['current_observation']['windchill_string'] = ''
            if('local_epoch' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['local_epoch'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['local_epoch'] = ''
                    errors.append(False,'local_epoch not str')
            else:
                errors.append('local_epoch not present')
                weather_data['current_observation']['local_epoch'] = ''
            if('icon_url' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['icon_url'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['icon_url'] = ''
                    errors.append(False,'icon_url not str')
            else:
                errors.append('icon_url not present')
                weather_data['current_observation']['icon_url'] = ''
            if('observation_time' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['observation_time'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['observation_time'] = ''
                    errors.append(False,'observation_time not str')
            else:
                errors.append('observation_time not present')
                weather_data['current_observation']['observation_time'] = ''
            if('windchill_c' in weather_data['current_observation'].keys()):
                place_holder_variable = True
                if(isinstance(weather_data['current_observation']['windchill_c'],str)):
                    place_holder_variable = True
                else:
                    weather_data['current_observation']['windchill_c'] = ''
                    errors.append(False,'windchill_c not str')
            else:
                errors.append('windchill_c not present')
                weather_data['current_observation']['windchill_c'] = ''
        else:
            weather_data['current_observation'] = ''
            errors.append('current_observation not dict')
    else:
        errors.append('current_observation not present')
        weather_data['current_observation'] = ''    


    
    
    for key in weather_data:
        if key not in ['response', 'current_observation']:
            errors.append('new key '+key+' in weather_data')
    for key_0 in weather_data['response']:
        if key_0 not in ['version', 'termsofService', 'features']:
            errors.append('new key '+key_0+" in weather_data['response']")
        for key_1 in weather_data['response']['features']:
            if key_1 not in ['conditions']:
                errors.append('new key '+key_1+" in weather_data['response']['features']")
    for key_0 in weather_data['current_observation']:
        if key_0 not in ['heat_index_string', 'precip_today_string', 'nowcast', 'wind_kph', 'windchill_string', 'icon_url', 'wind_gust_mph', 'UV', 'dewpoint_f', 'windchill_c', 'forecast_url', 'wind_dir', 'precip_1hr_string', 'windchill_f', 'icon', 'station_id', 'wind_mph', 'feelslike_string', 'feelslike_f', 'wind_string', 'visibility_mi', 'local_tz_offset', 'dewpoint_c', 'wind_gust_kph', 'temp_c', 'display_location', 'pressure_trend', 'wind_degrees', 'observation_epoch', 'local_tz_long', 'feelslike_c', 'observation_time_rfc822', 'local_epoch', 'relative_humidity', 'visibility_km', 'ob_url', 'heat_index_f', 'pressure_in', 'image', 'precip_1hr_in', 'observation_time', 'local_time_rfc822', 'precip_today_metric', 'pressure_mb', 'dewpoint_string', 'temperature_string', 'observation_location', 'local_tz_short', 'weather', 'precip_today_in', 'history_url', 'temp_f', 'solarradiation', 'estimated', 'precip_1hr_metric', 'heat_index_c']:
            errors.append('new key '+key_0+" in weather_data['current_observation']")
        for key_1 in weather_data['current_observation']['display_location']:
            if key_1 not in ['country', 'country_iso3166', 'elevation', 'state', 'state_name', 'city', 'zip', 'latitude', 'magic', 'wmo', 'longitude', 'full']:
                errors.append('new key '+key_1+" in weather_data['current_observation']['display_location']")
        for key_1 in weather_data['current_observation']['image']:
            if key_1 not in ['title', 'url', 'link']:
                errors.append('new key '+key_1+" in weather_data['current_observation'][image]")
        for key_1 in weather_data['current_observation']['observation_location']:
            if key_1 not in ['country', 'country_iso3166', 'city', 'full', 'elevation', 'longitude', 'state', 'latitude']:
                errors.append('new key '+key_1+" in weather_data['current_observation']['observation_location']")
        for key_1 in weather_data['current_observation']['estimated']:
            if key_1 not in []:
                errors.append('new key '+key_1+" in weather_data['current_observation']['estimated']")    
    return(True,weather_data,errors)
