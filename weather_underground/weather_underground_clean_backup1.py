#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports

#checks all except for image,estimated
def weather_underground_clean_current_observations_check(current_observations):
    errors = []
    #print(current_observations.keys())
    if('display_location' in current_observations.keys()):
        if(isinstance(current_observations['display_location'],dict)):
            #print(current_observations['display_location']) 
            if('full' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['full'],str)):
                    place_holder_variable = True
                else:
                    errors.append("current_observations['display_location']['full'] not correct type")
                    current_observations['display_location']['full'] = '' 
            else:
                errors.append("current_observations['display_location']['full'] not present")                
                current_observations['display_location']['full'] = ''    
            if('state_name' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['state_name'],str)):
                    place_holder_variable = True
                else:
                    errors.append("current_observations['display_location']['state_name'] not corrent type")
                    current_observations['display_location']['state_name'] = ''  
            else:
                errors.append("current_observations['display_location']['state_name'] not present")                
                current_observations['display_location']['state_name'] = ''   
            if('magic' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['magic'],str)):
                    place_holder_variable = True
                else:
                    errors.append("current_observations['display_location']['magic'] not correct type")
                    current_observations['display_location']['magic'] = ''   
            else:
                errors.append("current_observations['display_location']['magic'] not present")                
                current_observations['display_location']['magic'] = ''    
            if('country' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['country'],str)):
                    place_holder_variable = True
                else:
                    errors.append("current_observations['display_location']['country'] not correct type")
                    current_observations['display_location']['country'] = '' 
            else:
                errors.append("current_observations['display_location']['country'] not present")                
                current_observations['display_location']['country'] = '' 
            if('elevation' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['elevation'],str)):
                    place_holder_variable = True
                else:
                    errors.append("current_observations['display_location']['elevation'] not correct type")
                    current_observations['display_location']['elevation'] = ''
            else:
                errors.append("current_observations['display_location']['elevation'] not present")                
                current_observations['display_location']['elevation'] = ''
            if('country_iso3166' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['country_iso3166'],str)):
                    place_holder_variable = True
                else:
                    errors.append("current_observations['display_location']['country_iso3166'] not correct type")
                    current_observations['display_location']['country_iso3166'] = ''     
            else:
                errors.append("current_observations['display_location']['country_iso3166'] not present")                
                current_observations['display_location']['country_iso3166'] = ''     
            if('longitude' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['longitude'],str)):
                    place_holder_variable = True
                else:
                    errors.append("current_observations['display_location']['longitude'] not correct type")
                    current_observations['display_location']['longitude'] = '' 
            else:
                errors.append("current_observations['display_location']['longitude'] not present")                
                current_observations['display_location']['longitude'] = '' 
            if('latitude' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['latitude'],str)):
                    place_holder_variable = True
                else:
                    errors.append("current_observations['display_location']['latitude'] not correct type")
                    current_observations['display_location']['longitude'] = '' 
            else:
                errors.append("current_observations['display_location']['latitude'] not present")                
                current_observations['display_location']['latitude'] = ''
            if('wmo' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['wmo'],str)):
                    place_holder_variable = True
                else:
                    errors.append("current_observations['display_location']['wmo'] not correct type")
                    current_observations['display_location']['wmo'] = ''
            else:
                errors.append("current_observations['display_location']['wmo'] not present")                
                current_observations['display_location']['wmo'] = ''
            if('state' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['state'],str)):
                    place_holder_variable = True
                else:
                    errors.append("current_observations['display_location']['state'] ")
                    current_observations['display_location']['state'] = ''   
            else:
                current_observations['display_location']['state'] = ''   
            if('city' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['city'],str)):
                    place_holder_variable = True
                else:
                    errors.append("current_observations['display_location']['city'] not correct type")
                    current_observations['display_location']['city'] = ''
            else:
                errors.append("current_observations['display_location']['city'] not present")                
                current_observations['display_location']['city'] = ''
            if('zip' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['zip'],str)):
                    place_holder_variable = True
                else:
                    errors.append("current_observations['display_location']['zip'] not correct type")
                    current_observations['display_location']['zip'] = ''             
            else:
                errors.append("current_observations['display_location']['zip'] not present")                
                current_observations['display_location']['zip'] = ''             
        else:
            errors.append("current_observations['display_location'] not correct type")
            current_observations['display_location'] = ''
    else:
        errors.append("current_observations['display_location'] not present")        
        current_observations['display_location'] = ''
    if('observation_location' in current_observations.keys()):
        if(isinstance(current_observations['observation_location'],dict)):
            #print(current_observations['observation_location']) 
            if('full' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['full'],str)):
                    place_holder_variable = True
                else:
                    errors.append("current_observations['display_location']['full'] not correct type")
                    current_observations['display_location']['full'] = ''      
            else:
                errors.append("current_observations['display_location']['full'] not present")
                current_observations['display_location']['full'] = ''      
            if('state_name' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['state_name'],str)):
                    place_holder_variable = True
                else:
                    errors.append("current_observations['display_location']['state_name'] not correct type")
                    current_observations['display_location']['state_name'] = '' 
            else:
                errors.append("current_observations['display_location']['state_name'] not present")                
                current_observations['display_location']['state_name'] = '' 
            if('magic' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['magic'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['magic'])
                    return(False,'magic not str')
            else:
                current_observations['display_location']['magic'] = ''    
            if('country' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['country'],str)):
                    place_holder_variable = True
                else:
                    errors.append("current_observations['display_location']['country'] not correct type")
                    current_observations['display_location']['country'] = ''  
            else:
                errors.append("current_observations['display_location']['country'] not present")                
                current_observations['display_location']['country'] = ''  
            if('elevation' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['elevation'],str)):
                    place_holder_variable = True
                else:
                    errors.append("current_observations['display_location']['elevation'] not correct type")
                    current_observations['display_location']['elevation'] = ''
            else:
                errors.append("current_observations['display_location']['elevation'] not present")                
                current_observations['display_location']['elevation'] = ''
            if('country_iso3166' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['country_iso3166'],str)):
                    place_holder_variable = True
                else:
                    errors.append("current_observations['display_location']['country_iso3166'] not correct type")
                    current_observations['display_location']['country_iso3166'] = ''    
            else:
                errors.append("current_observations['display_location']['country_iso3166'] not present")                
                current_observations['display_location']['country_iso3166'] = ''    
            if('longitude' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['longitude'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['longitude'])
                    return(False,'longitude not str')
            else:
                current_observations['display_location']['longitude'] = ''  
            if('latitude' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['latitude'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['latitude'])
                    return(False,'latitude not str')
            else:
                current_observations['display_location']['latitude'] = ''
            if('wmo' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['wmo'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['wmo'])
                    return(False,'wmo not str')
            else:
                current_observations['display_location']['wmo'] = ''
            if('state' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['state'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['state'])
                    return(False,'state not str')
            else:
                current_observations['display_location']['state'] = ''  
            if('city' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['city'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['city'])
                    return(False,'city not str')
            else:
                current_observations['display_location']['city'] = ''
            if('zip' in current_observations['display_location'].keys()):
                if(isinstance(current_observations['display_location']['zip'],str)):
                    place_holder_variable = True
                else:
                    print(current_observations['display_location']['zip'])
                    return(False,'zip not str')
            else:
                current_observations['display_location']['zip'] = ''               
        else:
            print(current_observations['observation_location'])
            return(False,'observation_location not dict')
    else:
        current_observations['observation_location'] = ''  
    if('station_id' in current_observations.keys()):
        if(isinstance(current_observations['station_id'],str)):
            place_holder_variable = True
        else:
            print(current_observations['station_id'])
            return(False,'station_id not dict')        
    else:
        current_observations['station_id'] = ''
    if('dewpoint_c' in current_observations.keys()):
        if(isinstance(current_observations['dewpoint_c'],int) or isinstance(current_observations['dewpoint_c'],float)):
            place_holder_variable = True
        else:
            print(current_observations['dewpoint_c'])
            return(False,'dewpoint_c not int')  
    else:
        current_observations['dewpoint_c'] = ''
    if('heat_index_c' in current_observations.keys()):
        if(isinstance(current_observations['heat_index_c'],str)or isinstance(current_observations['heat_index_c'],int) or isinstance(current_observations['heat_index_c'],float)):
            place_holder_variable = True
        else:
            print(current_observations['heat_index_c'])
            return(False,'heat_index_c not str')
    else:
        current_observations['heat_index_c'] = ''
    if('solarradiation' in current_observations.keys()):
        if(isinstance(current_observations['solarradiation'],str)):
            place_holder_variable = True
        else:
            print(current_observations['solarradiation'])
            return(False,'solarradiation not str')
    else:
        current_observations['solarradiation'] = ''  
   
    if('image' in current_observations.keys()):
        if(isinstance(current_observations['image'],dict)):
            place_holder_variable = True
        else:
            print(current_observations['image'])
            return(False,'image not dict')
    else:
        current_observations['image'] = ''      
    if('precip_today_string' in current_observations.keys()):
        if(isinstance(current_observations['precip_today_string'],str)):
            place_holder_variable = True
        else:
            print(current_observations['precip_today_string'])
            return(False,'precip_today_string not str')
    else:
        current_observations['precip_today_string'] = '' 
    if('precip_1hr_in' in current_observations.keys()):
        if(isinstance(current_observations['precip_1hr_in'],str)):
            place_holder_variable = True
        else:
            print(current_observations['precip_1hr_in'])
            return(False,'precip_1hr_in not str')
    else:
        current_observations['precip_1hr_in'] = '' 
    if('precip_today_in' in current_observations.keys()):
        if(isinstance(current_observations['precip_today_in'],str)):
            place_holder_variable = True
        else:
            print(current_observations['precip_today_in'])
            return(False,'precip_today_in not str')
    else:
        current_observations['precip_today_in'] = ''
    if('estimated' in current_observations.keys()):
        if(isinstance(current_observations['estimated'],dict)):
            place_holder_variable = True
        else:
            print(current_observations['estimated'])
            return(False,'estimated not dict')
    else:
        current_observations['estimated'] = ''   
    
    if('temperature_string' in current_observations.keys()):
        if(isinstance(current_observations['temperature_string'],str)):
            place_holder_variable = True
        else:
            print(current_observations['temperature_string'])
            return(False,'temperature_string not str')
    else:
        current_observations['temperature_string'] = '' 
    if('history_url' in current_observations.keys()):
        if(isinstance(current_observations['history_url'],str)):
            place_holder_variable = True
        else:
            print(current_observations['history_url'])
            return(False,'history_url not str')
    else:
        current_observations['history_url'] = ''    
    if('local_tz_short' in current_observations.keys()):
        if(isinstance(current_observations['local_tz_short'],str)):
            place_holder_variable = True
        else:
            print(current_observations['local_tz_short'])
            return(False,'local_tz_short not str')
    else:
        current_observations['local_tz_short'] = ''         
    if('visibility_km' in current_observations.keys()):
        if(isinstance(current_observations['visibility_km'],str)):
            place_holder_variable = True
        else:
            print(current_observations['visibility_km'])
            return(False,'visibility_km not str')
    else:
        current_observations['visibility_km'] = ''  
    if('wind_mph' in current_observations.keys()):
        if(isinstance(current_observations['wind_mph'],float) or isinstance(current_observations['wind_mph'],int)):
            place_holder_variable = True
        else:
            print(current_observations['wind_mph'])
            return(False,'wind_mph not float')
    else:
        current_observations['wind_mph'] = ''    
    if('precip_today_metric' in current_observations.keys()):
        if(isinstance(current_observations['precip_today_metric'],str)):
            place_holder_variable = True
        else:
            print(current_observations['precip_today_metric'])
            return(False,'precip_today_metric not str')
    else:
        current_observations['precip_today_metric'] = ''   
    if('precip_1hr_string' in current_observations.keys()):
        if(isinstance(current_observations['precip_1hr_string'],str)):
            place_holder_variable = True
        else:
            print(current_observations['precip_1hr_string'])
            return(False,'precip_1hr_string not str')
    else:
        current_observations['precip_1hr_string'] = ''    
    if('nowcast' in current_observations.keys()):
        if(isinstance(current_observations['nowcast'],str)):
            place_holder_variable = True
        else:
            print(current_observations['nowcast'])
            return(False,'nowcast not str')
    else:
        current_observations['nowcast'] = ''   
    if('wind_kph' in current_observations.keys()):
        if(isinstance(current_observations['wind_kph'],int)):
            place_holder_variable = True
        else:
            print(current_observations['wind_kph'])
            return(False,'wind_kph not int')
    else:
        current_observations['wind_kph'] = ''    
    if('windchill_f' in current_observations.keys()):
        if(isinstance(current_observations['windchill_f'],str)):
            place_holder_variable = True
        else:
            print(current_observations['windchill_f'])
            return(False,'windchill_f not str')
    else:
        current_observations['windchill_f'] = ''       
    if('windchill_string' in current_observations.keys()):
        if(isinstance(current_observations['windchill_string'],str)):
            place_holder_variable = True
        else:
            print(current_observations['windchill_string'])
            return(False,'windchill_string not str')
    else:
        current_observations['windchill_string'] = ''   
    if('wind_dir' in current_observations.keys()):
        if(isinstance(current_observations['wind_dir'],str)):
            place_holder_variable = True
        else:
            print(current_observations['wind_dir'])
            return(False,'wind_dir not str')
    else:
        current_observations['wind_dir'] = ''   
    if('temp_f' in current_observations.keys()):
        if(isinstance(current_observations['temp_f'],float) or isinstance(current_observations['temp_f'],int)):
            place_holder_variable = True
        else:
            print(current_observations['temp_f'])
            return(False,'temp_f not float')
    else:
        current_observations['temp_f'] = ''    
    if('relative_humidity' in current_observations.keys()):
        if(isinstance(current_observations['relative_humidity'],str)):
            place_holder_variable = True
        else:
            print(current_observations['relative_humidity'])
            return(False,'relative_humidity not str')
    else:
        current_observations['relative_humidity'] = ''
    if('pressure_trend' in current_observations.keys()):
        if(isinstance(current_observations['pressure_trend'],str)):
            place_holder_variable = True
        else:
            print(current_observations['pressure_trend'])
            return(False,'pressure_trend not str')
    else:
        current_observations['pressure_trend'] = ''  
    if('heat_index_string' in current_observations.keys()):
        if(isinstance(current_observations['heat_index_string'],str)):
            place_holder_variable = True
        else:
            print(current_observations['heat_index_string'])
            return(False,'heat_index_string not str')
    else:
        current_observations['heat_index_string'] = ''    
    if('UV' in current_observations.keys()):
        if(isinstance(current_observations['UV'],str)):
            place_holder_variable = True
        else:
            print(current_observations['UV'])
            return(False,'UV not str')
    else:
        current_observations['UV'] = ''    
    if('local_epoch' in current_observations.keys()):
        if(isinstance(current_observations['local_epoch'],str)):
            place_holder_variable = True
        else:
            print(current_observations['local_epoch'])
            return(False,'local_epoch not str')
    else:
        current_observations['local_epoch'] = ''    
    if('heat_index_f' in current_observations.keys()):
        if(isinstance(current_observations['heat_index_f'],str) or isinstance(current_observations['heat_index_f'],int) or isinstance(current_observations['heat_index_f'],float)):
            place_holder_variable = True
        else:
            print(current_observations['heat_index_f'])
            return(False,'heat_index_f not str')
    else:
        current_observations['heat_index_f'] = ''     
    if('feelslike_string' in current_observations.keys()):
        if(isinstance(current_observations['feelslike_string'],str)):
            place_holder_variable = True
        else:
            print(current_observations['feelslike_string'])
            return(False,'feelslike_string not str')
    else:
        current_observations['feelslike_string'] = ''   
    if('wind_degrees' in current_observations.keys()):
        if(isinstance(current_observations['wind_degrees'],int)):
            place_holder_variable = True
        else:
            print(current_observations['wind_degrees'])
            return(False,'wind_degrees not int')
    else:
        current_observations['wind_degrees'] = ''   
    if('feelslike_f' in current_observations.keys()):
        if(isinstance(current_observations['feelslike_f'],str)):
            place_holder_variable = True
        else:
            print(current_observations['feelslike_f'])
            return(False,'feelslike_f not str')
    else:
        current_observations['feelslike_f'] = ''
    if('local_tz_offset' in current_observations.keys()):
        if(isinstance(current_observations['local_tz_offset'],str)):
            place_holder_variable = True
        else:
            print(current_observations['local_tz_offset'])
            return(False,'local_tz_offset not str')
    else:
        current_observations['local_tz_offset'] = '' 
    if('forecast_url' in current_observations.keys()):
        if(isinstance(current_observations['forecast_url'],str)):
            place_holder_variable = True
        else:
            print(current_observations['forecast_url'])
            return(False,'forecast_url not str')
    else:
        current_observations['forecast_url'] = '' 
    if('precip_1hr_metric' in current_observations.keys()):
        if(isinstance(current_observations['precip_1hr_metric'],str)):
            place_holder_variable = True
        else:
            print(current_observations['precip_1hr_metric'])
            return(False,'precip_1hr_metric not str')
    else:
        current_observations['precip_1hr_metric'] = ''     
    if('weather' in current_observations.keys()):
        if(isinstance(current_observations['weather'],str)):
            place_holder_variable = True
        else:
            print(current_observations['weather'])
            return(False,'weather not str')
    else:
        current_observations['weather'] = ''  
    if('observation_time' in current_observations.keys()):
        if(isinstance(current_observations['observation_time'],str)):
            place_holder_variable = True
        else:
            print(current_observations['observation_time'])
            return(False,'observation_time not str')
    else:
        current_observations['observation_time'] = ''    
    if('wind_string' in current_observations.keys()):
        if(isinstance(current_observations['wind_string'],str)):
            place_holder_variable = True
        else:
            print(current_observations['wind_string'])
            return(False,'wind_string not str')
    else:
        current_observations['wind_string'] = ''  
    if('feelslike_c' in current_observations.keys()):
        if(isinstance(current_observations['feelslike_c'],str)):
            place_holder_variable = True
        else:
            print(current_observations['feelslike_c'])
            return(False,'feelslike_c not str')
    else:
        current_observations['feelslike_c'] = ''
    if('local_tz_long' in current_observations.keys()):
        if(isinstance(current_observations['local_tz_long'],str)):
            place_holder_variable = True
        else:
            print(current_observations['local_tz_long'])
            return(False,'local_tz_long not str')
    else:
        current_observations['local_tz_long'] = ''  
    if('icon' in current_observations.keys()):
        if(isinstance(current_observations['icon'],str)):
            place_holder_variable = True
        else:
            print(current_observations['icon'])
            return(False,'icon not str')
    else:
        current_observations['icon'] = ''
    if('wind_gust_kph' in current_observations.keys()):
        if(isinstance(current_observations['wind_gust_kph'],int)):
            place_holder_variable = True
        else:
            print(current_observations['wind_gust_kph'])
            return(False,'wind_gust_kph not int')
    else:
        current_observations['wind_gust_kph'] = ''  
    if('wind_gust_mph' in current_observations.keys()):
        if(isinstance(current_observations['wind_gust_mph'],int)):
            place_holder_variable = True
        else:
            print(current_observations['wind_gust_mph'])
            return(False,'wind_gust_mph not int')
    else:
        current_observations['wind_gust_mph'] = ''       
    if('temp_c' in current_observations.keys()):
        if(isinstance(current_observations['temp_c'],float) or isinstance(current_observations['temp_c'],int)):
            place_holder_variable = True
        else:
            print(current_observations['temp_c'])
            return(False,'temp_c not float')
    else:
        current_observations['temp_c'] = ''  
    if('dewpoint_string' in current_observations.keys()):
        if(isinstance(current_observations['dewpoint_string'],str)):
            place_holder_variable = True
        else:
            print(current_observations['dewpoint_string'])
            return(False,'dewpoint_string not str')
    else:
        current_observations['dewpoint_string'] = ''
    if('local_time_rfc822' in current_observations.keys()):
        if(isinstance(current_observations['local_time_rfc822'],str)):
            place_holder_variable = True
        else:
            print(current_observations['local_time_rfc822'])
            return(False,'local_time_rfc822 not str')
    else:
        current_observations['local_time_rfc822'] = ''    
    if('pressure_in' in current_observations.keys()):
        if(isinstance(current_observations['pressure_in'],str)):
            place_holder_variable = True
        else:
            print(current_observations['pressure_in'])
            return(False,'pressure_in not str')
    else:
        current_observations['pressure_in'] = ''   
    if('pressure_mb' in current_observations.keys()):
        if(isinstance(current_observations['pressure_mb'],str)):
            place_holder_variable = True
        else:
            print(current_observations['pressure_mb'])
            return(False,'pressure_mb not str')
    else:
        current_observations['pressure_mb'] = ''   
    if('icon_url' in current_observations.keys()):
        if(isinstance(current_observations['icon_url'],str)):
            place_holder_variable = True
        else:
            print(current_observations['icon_url'])
            return(False,'icon_url not str')
    else:
        current_observations['icon_url'] = ''     
    if('observation_time_rfc822' in current_observations.keys()):
        if(isinstance(current_observations['observation_time_rfc822'],str)):
            place_holder_variable = True
        else:
            print(current_observations['observation_time_rfc822'])
            return(False,'observation_time_rfc822 not str')
    else:
        current_observations['observation_time_rfc822'] = ''    
    if('windchill_c' in current_observations.keys()):
        if(isinstance(current_observations['windchill_c'],str)):
            place_holder_variable = True
        else:
            print(current_observations['windchill_c'])
            return(False,'windchill_c not str')
    else:
        current_observations['windchill_c'] = ''   
    if('dewpoint_f' in current_observations.keys()):
        if(isinstance(current_observations['dewpoint_f'],int)):
            place_holder_variable = True
        else:
            print(current_observations['dewpoint_f'])
            return(False,'dewpoint_f not int')
    else:
        current_observations['dewpoint_f'] = ''   
    if('visibility_mi' in current_observations.keys()):
        if(isinstance(current_observations['visibility_mi'],str)):
            place_holder_variable = True
        else:
            print(current_observations['visibility_mi'])
            return(False,'visibility_mi not str')
    else:
        current_observations['visibility_mi'] = ''  
    if('observation_epoch' in current_observations.keys()):
        if(isinstance(current_observations['observation_epoch'],str)):
            place_holder_variable = True
        else:
            print(current_observations['observation_epoch'])
            return(False,'observation_epoch not str')
    else:
        current_observations['observation_epoch']      
    if('ob_url' in current_observations.keys()):
        if(isinstance(current_observations['ob_url'],str)):
            place_holder_variable = True
        else:
            print(current_observations['ob_url'])
            return(False,'ob_url not str')
    else:
        current_observations['ob_url'] = ''         
    return(True,'no errors',current_observations)
    
    
def weather_underground_clean(weather_file):
    #print(weather_file.keys())
    if ('response' in weather_file.keys()):
        #print(weather_file['response'].keys())
        if('features' in weather_file['response'].keys()):
            if(isinstance(weather_file['response']['features'],dict)):
                if ('conditions' in weather_file['response']['features'].keys()):
                    if(isinstance(weather_file['response']['features']['conditions'],int)):
                        place_holder_variable = True
                    else:
                        weather_file['response']['features']['conditions'] = ''
                else:
                    weather_file['response']['features']['conditions'] = ''
            else:
                weather_file['response']['features'] = ''
        else:
            weather_file['response']['features'] = ''
        if('version' in weather_file['response'].keys()):
            if(isinstance(weather_file['response']['version'],str)):
                place_holder_variable = True
            else:
                weather_file['response']['version'] = ''
        else:
            weather_file['response']['version'] = ''
        if('termsofService' in weather_file['response'].keys()):
            if(isinstance(weather_file['response']['termsofService'],str)):
                place_holder_variable = True
            else:
                weather_file['response']['termsofService'] = ''
        else:
            weather_file['response']['termsofService'] = ''
    else:
        weather_file['response not present'] = ''
    if('current_observation' in weather_file.keys()):
        curr_check_results = weather_underground_clean_current_observations_check(weather_file['current_observation'])
        if(curr_check_results[0] == True):
            weather_file['current_observation'] = weather_underground_clean_current_observations_check(weather_file['current_observation'])[2]
        else:
            return(curr_check_results)
    else:
        weather_file['current_observation'] = ''
        return([True,'current_observation not present', weather_file])
    return([True,'current_observation present',weather_file])
