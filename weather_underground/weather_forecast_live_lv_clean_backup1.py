#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import sys
import os
sys.path.append(os.environ['WORKSPACE']+'/serverscripts/globals')
import universal_imports

def clean(hourly_weather):
    if('response' in hourly_weather.keys()):
        place_holder_variable = True
        if(isinstance(hourly_weather['response'],dict)):
            place_holder_variable = True
            if('version' in hourly_weather['response'].keys()):
                place_holder_variable = True
                if(isinstance(hourly_weather['response']['version'],str)):
                    place_holder_variable = True
                else:
                    print(hourly_weather['response']['version'])
                    return(False,'version not str')
            else:
                hourly_weather['response']['version'] = ''
            if('termsofService' in hourly_weather['response'].keys()):
                place_holder_variable = True
                if(isinstance(hourly_weather['response']['termsofService'],str)):
                    place_holder_variable = True
                else:
                    print(hourly_weather['response']['termsofService'])
                    return(False,'termsofService not str')
            else:
                hourly_weather['response']['termsofService'] = ''
            if('features' in hourly_weather['response'].keys()):
                place_holder_variable = True
                if(isinstance(hourly_weather['response']['features'],dict)):
                    place_holder_variable = True
                    if('hourly' in hourly_weather['response']['features'].keys()):
                        place_holder_variable = True
                        if(isinstance(hourly_weather['response']['features']['hourly'],int)):
                            place_holder_variable = True
                        else:
                            print(hourly_weather['response']['features']['hourly'])
                            return(False,'hourly not int')
                    else:
                        hourly_weather['response']['features']['hourly'] = ''                    
                else:
                    print(hourly_weather['response']['features'])
                    return(False,'features not dict')
            else:
                hourly_weather['response']['features'] = ''            
        else:
            print(hourly_weather['response'])
            return(False,'response not dict') 
    else:
        hourly_weather['response'] = ''    
    if('hourly_forecast' in hourly_weather.keys()):
        place_holder_variable = True
        if(isinstance(hourly_weather['hourly_forecast'],list)):
            for i in range(0,len(hourly_weather['hourly_forecast'])):
                #print(str(hourly_weather['hourly_forecast'][i])+"\n\n")
                if('dewpoint' in hourly_weather['hourly_forecast'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(hourly_weather['hourly_forecast'][i]['dewpoint'],dict)):
                        place_holder_variable = True
                        if('english' in hourly_weather['hourly_forecast'][i]['dewpoint'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['dewpoint']['english'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['dewpoint']['english'])
                                return(False,'english not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['dewpoint']['english'] = '' 
                        if('metric' in hourly_weather['hourly_forecast'][i]['dewpoint'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['dewpoint']['metric'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['dewpoint']['metric'])
                                return(False,'metric not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['dewpoint']['metric'] = ''
                                          
                    else:
                        print(hourly_weather['hourly_forecast'][i]['dewpoint'])
                        return(False,'dewpoint not dict')
                else:
                    hourly_weather['hourly_forecast'][i]['dewpoint'] = ''
                if('wx' in hourly_weather['hourly_forecast'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(hourly_weather['hourly_forecast'][i]['wx'],str)):
                        place_holder_variable = True
                    else:
                        print(hourly_weather['hourly_forecast'][i]['wx'])
                        return(False,'wx not str')
                else:
                    hourly_weather['hourly_forecast'][i]['wx'] = ''
                if('humidity' in hourly_weather['hourly_forecast'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(hourly_weather['hourly_forecast'][i]['humidity'],str)):
                        place_holder_variable = True
                    else:
                        print(hourly_weather['hourly_forecast'][i]['humidity'])
                        return(False,'humidity not str')
                else:
                    hourly_weather['hourly_forecast'][i]['humidity'] = ''
                if('pop' in hourly_weather['hourly_forecast'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(hourly_weather['hourly_forecast'][i]['pop'],str)):
                        place_holder_variable = True
                    else:
                        print(hourly_weather['hourly_forecast'][i]['pop'])
                        return(False,'pop not str')
                else:
                    hourly_weather['hourly_forecast'][i]['pop'] = ''
                if('sky' in hourly_weather['hourly_forecast'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(hourly_weather['hourly_forecast'][i]['sky'],str)):
                        place_holder_variable = True
                    else:
                        print(hourly_weather['hourly_forecast'][i]['sky'])
                        return(False,'sky not str')
                else:
                    hourly_weather['hourly_forecast'][i]['sky'] = ''
                if('mslp' in hourly_weather['hourly_forecast'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(hourly_weather['hourly_forecast'][i]['mslp'],dict)):
                        place_holder_variable = True
                        if('english' in hourly_weather['hourly_forecast'][i]['mslp'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['mslp']['english'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['mslp']['english'])
                                return(False,'english not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['mslp']['english'] = ''
                        if('metric' in hourly_weather['hourly_forecast'][i]['mslp'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['mslp']['metric'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['mslp']['metric'])
                                return(False,'metric not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['mslp']['metric'] = ''                        
                    else:
                        print(hourly_weather['hourly_forecast'][i]['mslp'])
                        return(False,'mslp not dict')
                else:
                    hourly_weather['hourly_forecast'][i]['mslp'] = ''
                if('FCTTIME' in hourly_weather['hourly_forecast'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME'],dict)):
                        place_holder_variable = True
                        if('hour' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['hour'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['hour'])
                                return(False,'hour not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['hour'] = ''
                        if('year' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['year'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['year'])
                                return(False,'year not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['year'] = ''
                        if('mon_abbrev' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['mon_abbrev'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['mon_abbrev'])
                                return(False,'mon_abbrev not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['mon_abbrev'] = ''  
    
                        if('weekday_name_night_unlang' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['weekday_name_night_unlang'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['weekday_name_night_unlang'])
                                return(False,'weekday_name_night_unlang not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['weekday_name_night_unlang'] = ''
                        if('weekday_name_night' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['weekday_name_night'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['weekday_name_night'])
                                return(False,'weekday_name_night not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['weekday_name_night'] = ''
                        if('hour_padded' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['hour_padded'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['hour_padded'])
                                return(False,'hour_padded not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['hour_padded'] = ''
                        if('mday' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['mday'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['mday'])
                                return(False,'mday not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['mday'] = ''
                        if('tz' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['tz'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['tz'])
                                return(False,'tz not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['tz'] = ''
                        if('mon' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['mon'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['mon'])
                                return(False,'mon not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['mon'] = ''
                        if('ampm' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['ampm'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['ampm'])
                                return(False,'ampm not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['ampm'] = ''
                        if('mday_padded' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['mday_padded'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['mday_padded'])
                                return(False,'mday_padded not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['mday_padded'] = ''
                        if('weekday_name' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['weekday_name'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['weekday_name'])
                                return(False,'weekday_name not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['weekday_name'] = ''
                        if('month_name_abbrev' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['month_name_abbrev'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['month_name_abbrev'])
                                return(False,'month_name_abbrev not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['month_name_abbrev'] = ''
                        if('civil' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['civil'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['civil'])
                                return(False,'civil not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['civil'] = ''
                        if('sec' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['sec'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['sec'])
                                return(False,'sec not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['sec'] = ''
                        if('min' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['min'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['min'])
                                return(False,'min not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['min'] = ''
                        if('pretty' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['pretty'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['pretty'])
                                return(False,'pretty not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['pretty'] = ''
                        if('weekday_name_unlang' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['weekday_name_unlang'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['weekday_name_unlang'])
                                return(False,'weekday_name_unlang not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['weekday_name_unlang'] = ''
                        if('UTCDATE' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['UTCDATE'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['UTCDATE'])
                                return(False,'UTCDATE not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['UTCDATE'] = ''
                        if('month_name' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['month_name'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['month_name'])
                                return(False,'month_name not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['month_name'] = ''
                        if('isdst' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['isdst'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['isdst'])
                                return(False,'isdst not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['isdst'] = ''
                        if('yday' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['yday'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['yday'])
                                return(False,'yday not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['yday'] = ''
                        if('age' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['age'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['age'])
                                return(False,'age not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['age'] = ''
                        if('epoch' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['epoch'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['epoch'])
                                return(False,'epoch not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['epoch'] = ''
                        if('min_unpadded' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['min_unpadded'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['min_unpadded'])
                                return(False,'min_unpadded not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['min_unpadded'] = ''
                        if('weekday_name_abbrev' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['weekday_name_abbrev'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['weekday_name_abbrev'])
                                return(False,'weekday_name_abbrev not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['weekday_name_abbrev'] = ''
                        if('mon_padded' in hourly_weather['hourly_forecast'][i]['FCTTIME'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['FCTTIME']['mon_padded'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['FCTTIME']['mon_padded'])
                                return(False,'mon_padded not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['FCTTIME']['mon_padded'] = ''                    
                    else:
                        print(hourly_weather['hourly_forecast'][i]['FCTTIME'])
                        return(False,'FCTTIME not dict')
                else:
                    hourly_weather['hourly_forecast'][i]['FCTTIME'] = ''
                if('fctcode' in hourly_weather['hourly_forecast'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(hourly_weather['hourly_forecast'][i]['fctcode'],str)):
                        place_holder_variable = True
                    else:
                        print(hourly_weather['hourly_forecast'][i]['fctcode'])
                        return(False,'fctcode not str')
                else:
                    hourly_weather['hourly_forecast'][i]['fctcode'] = ''
                if('windchill' in hourly_weather['hourly_forecast'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(hourly_weather['hourly_forecast'][i]['windchill'],dict)):
                        place_holder_variable = True
                        if('english' in hourly_weather['hourly_forecast'][i]['windchill'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['windchill']['english'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['windchill']['english'])
                                return(False,'english not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['windchill']['english'] = ''
                        if('metric' in hourly_weather['hourly_forecast'][i]['windchill'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['windchill']['metric'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['windchill']['metric'])
                                return(False,'metric not str')
                        else:
                            return(False,'metric not present')                        
                    else:
                        print(hourly_weather['hourly_forecast'][i]['windchill'])
                        return(False,'windchill not dict')
                else:
                    hourly_weather['hourly_forecast'][i]['windchill'] = ''
                if('feelslike' in hourly_weather['hourly_forecast'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(hourly_weather['hourly_forecast'][i]['feelslike'],dict)):
                        place_holder_variable = True
                        if('english' in hourly_weather['hourly_forecast'][i]['feelslike'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['feelslike']['english'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['feelslike']['english'])
                                return(False,'english not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['feelslike']['english'] = ''
                        if('metric' in hourly_weather['hourly_forecast'][i]['feelslike'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['feelslike']['metric'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['feelslike']['metric'])
                                return(False,'metric not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['feelslike']['metric'] = ''                        
                    else:
                        print(hourly_weather['hourly_forecast'][i]['feelslike'])
                        return(False,'feelslike not dict')
                else:
                    hourly_weather['hourly_forecast'][i]['feelslike'] = ''
                if('qpf' in hourly_weather['hourly_forecast'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(hourly_weather['hourly_forecast'][i]['qpf'],dict)):
                        place_holder_variable = True
                        if('english' in hourly_weather['hourly_forecast'][i]['qpf'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['qpf']['english'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['qpf']['english'])
                                return(False,'english not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['qpf']['english'] = ''
                        if('metric' in hourly_weather['hourly_forecast'][i]['qpf'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['qpf']['metric'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['qpf']['metric'])
                                return(False,'metric not str')
                        else:
                            return(False,'metric not present')                        
                    else:
                        print(hourly_weather['hourly_forecast'][i]['qpf'])
                        return(False,'qpf not dict')
                else:
                    hourly_weather['hourly_forecast'][i]['qpf'] = ''
                if('wspd' in hourly_weather['hourly_forecast'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(hourly_weather['hourly_forecast'][i]['wspd'],dict)):
                        place_holder_variable = True
                        if('english' in hourly_weather['hourly_forecast'][i]['wspd'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['wspd']['english'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['wspd']['english'])
                                return(False,'english not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['wspd']['english'] = ''
                        if('metric' in hourly_weather['hourly_forecast'][i]['wspd'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['wspd']['metric'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['wspd']['metric'])
                                return(False,'metric not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['wspd']['metric'] = ''                        
                    else:
                        print(hourly_weather['hourly_forecast'][i]['wspd'])
                        return(False,'wspd not dict')
                else:
                    hourly_weather['hourly_forecast'][i]['wspd'] = ''
                if('heatindex' in hourly_weather['hourly_forecast'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(hourly_weather['hourly_forecast'][i]['heatindex'],dict)):
                        place_holder_variable = True
                        if('english' in hourly_weather['hourly_forecast'][i]['heatindex'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['heatindex']['english'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['heatindex']['english'])
                                return(False,'english not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['heatindex']['english'] = ''
                        if('metric' in hourly_weather['hourly_forecast'][i]['heatindex'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['heatindex']['metric'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['heatindex']['metric'])
                                return(False,'metric not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['heatindex']['metric'] = ''                        
                    else:
                        print(hourly_weather['hourly_forecast'][i]['heatindex'])
                        return(False,'heatindex not dict')
                else:
                    hourly_weather['hourly_forecast'][i]['heatindex'] = ''
                if('wdir' in hourly_weather['hourly_forecast'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(hourly_weather['hourly_forecast'][i]['wdir'],dict)):
                        place_holder_variable = True
                        if('degrees' in hourly_weather['hourly_forecast'][i]['wdir'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['wdir']['degrees'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['wdir']['degrees'])
                                return(False,'degrees not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['wdir']['degrees'] = ''
                        if('dir' in hourly_weather['hourly_forecast'][i]['wdir'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['wdir']['dir'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['wdir']['dir'])
                                return(False,'dir not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['wdir']['dir'] = ''                       
                    else:
                        print(hourly_weather['hourly_forecast'][i]['wdir'])
                        return(False,'wdir not dict')
                else:
                    hourly_weather['hourly_forecast'][i]['wdir'] = ''
                if('snow' in hourly_weather['hourly_forecast'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(hourly_weather['hourly_forecast'][i]['snow'],dict)):
                        place_holder_variable = True
                        if('english' in hourly_weather['hourly_forecast'][i]['snow'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['snow']['english'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['snow']['english'])
                                return(False,'english not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['snow']['english'] = ''
                        if('metric' in hourly_weather['hourly_forecast'][i]['snow'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['snow']['metric'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['snow']['metric'])
                                return(False,'metric not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['snow']['metric'] = ''                       
                    else:
                        print(hourly_weather['hourly_forecast'][i]['snow'])
                        return(False,'snow not dict')
                else:
                    hourly_weather['hourly_forecast'][i]['snow'] = ''
                if('condition' in hourly_weather['hourly_forecast'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(hourly_weather['hourly_forecast'][i]['condition'],str)):
                        place_holder_variable = True
                    else:
                        print(hourly_weather['hourly_forecast'][i]['condition'])
                        return(False,'condition not str')
                else:
                    hourly_weather['hourly_forecast'][i]['condition'] = ''
                if('uvi' in hourly_weather['hourly_forecast'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(hourly_weather['hourly_forecast'][i]['uvi'],str)):
                        place_holder_variable = True
                    else:
                        print(hourly_weather['hourly_forecast'][i]['uvi'])
                        return(False,'uvi not str')
                else:
                    hourly_weather['hourly_forecast'][i]['uvi'] = ''
                if('icon' in hourly_weather['hourly_forecast'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(hourly_weather['hourly_forecast'][i]['icon'],str)):
                        place_holder_variable = True
                    else:
                        print(hourly_weather['hourly_forecast'][i]['icon'])
                        return(False,'icon not str')
                else:
                    hourly_weather['hourly_forecast'][i]['icon'] = ''
                if('temp' in hourly_weather['hourly_forecast'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(hourly_weather['hourly_forecast'][i]['temp'],dict)):
                        place_holder_variable = True
                        if('english' in hourly_weather['hourly_forecast'][i]['temp'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['temp']['english'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['temp']['english'])
                                return(False,'english not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['temp']['english'] = ''
                        if('metric' in hourly_weather['hourly_forecast'][i]['temp'].keys()):
                            place_holder_variable = True
                            if(isinstance(hourly_weather['hourly_forecast'][i]['temp']['metric'],str)):
                                place_holder_variable = True
                            else:
                                print(hourly_weather['hourly_forecast'][i]['temp']['metric'])
                                return(False,'metric not str')
                        else:
                            hourly_weather['hourly_forecast'][i]['temp']['metric'] = ''                       
                    else:
                        print(hourly_weather['hourly_forecast'][i]['temp'])
                        return(False,'temp not dict')
                    
                else:
                    hourly_weather['hourly_forecast'][i]['temp'] = ''
                if('icon_url' in hourly_weather['hourly_forecast'][i].keys()):
                    place_holder_variable = True
                    if(isinstance(hourly_weather['hourly_forecast'][i]['icon_url'],str)):
                        place_holder_variable = True
                    else:
                        print(hourly_weather['hourly_forecast'][i]['icon_url'])
                        return(False,'icon_url not str')
                else:
                    hourly_weather['hourly_forecast'][i]['icon_url'] = ''
        else:
            print(hourly_weather['hourly_forecast'])
            return(False,'hourly_forecast not list')
    else:
        hourly_weather['hourly_forecast'] = ''
        return([True,'hourly_forecast not present',hourly_weather])    
    return([True, 'hourly_forecast present',hourly_weather])
