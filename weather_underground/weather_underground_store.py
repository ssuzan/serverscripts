#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import os
import sys
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts/globals")
import universal_imports
import sendemail
import push_to_fifo
from email.mime.text import MIMEText

email_file = os.environ['WORKSPACE']+'/serverscripts/globals/emails'

def weather_underground_store_original(filename,r,save_path):
    #storing source and result
    original_file = open(save_path+"/database_original/"+filename+'.json', 'wb')
    original_file.write(r)  
    original_file.close()      

def weather_underground_store(filename,r_cleaned,save_path, dump_output_to,type_name,errors):
    
    if (dump_output_to == "FILE"):
        #i is used because weather underground does not have individual unit ids and I could not find a suitible id in weather forcast live
        lastFiletxt = open(save_path+"/database/"+filename+'.json', 'w')
        new_store_json = {'type':type_name,'date_time':str(universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")).replace("-","_") +" UTC",'suspicious':False,'error':False,'data':r_cleaned}
        if(len(errors)>0):
            new_store_json['error'] = True
        lastFiletxt.write(universal_imports.json.dumps(new_store_json, sort_keys=True, indent=4, separators=(',', ': ')))  
        lastFiletxt.close()
    elif (dump_output_to == "FIFO"):
        try:
            push_to_fifo.push_to_fifo(r_cleaned, "input queue weather_underground")          
        except:
            text = open(email_file,'r')
            email_string = text.read()
            email_json = universal_imports.json.loads(email_string)            
            sendemail.sendemail(from_addr = email_json["email"], to_addr_list = email_json["send_emails"], cc_addr_list = [''], subject = 'error', message = "weather_underground_store fifo error", login = email_json["email"], password = email_json["password"])                                
            
                
